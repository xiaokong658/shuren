var lunhui = {

	//成功弹出层
	success: function(message,url){
		layer.msg(message, {icon: 6,time:2000}, function(index){
            layer.close(index);
            window.location.href=url;
        });
	},

	// 错误弹出层
	error: function(message) {
        layer.msg(message, {icon: 5,time:2000}, function(index){
            layer.close(index);
        });       
    },

	// 确认弹出层
    confirm : function(id,url) {
        layer.confirm('确认删除此条记录吗?', {icon: 3, title:'提示'}, function(index){
	        $.getJSON(url, {'id' : id}, function(res){
	            if(res.code == 1){
	                layer.msg(res.msg,{icon:1,time:1500,shade: 0.1});
	                Ajaxpage()
	            }else{
	                layer.msg(res.msg,{icon:0,time:1500,shade: 0.1});
	            }
	        });
	        layer.close(index);
	    })
    },

    //状态
    status : function(id,url,open,close,name){
		if(open==''||open == null){
			open = '开启';
		}
		if(close==''||close == null){
			close = '禁用';
		}
		if(name==''||name == null){
			name = 'zt';
		}
	    $.post(url,{id:id},function(data){
	        if(data.code==1){
	            var a='<span class="label label-danger">'+close+'</span>'
	            $('#'+name+id).html(a);
	            layer.msg(data.msg,{icon:2,time:1500,shade: 0.1,});
	            return false;
	        }else{
	            var b='<span class="label label-info">'+open+'</span>'
	            $('#'+name+id).html(b);
	            layer.msg(data.msg,{icon:1,time:1500,shade: 0.1,});
	            return false;
	        }         	        
	    });
	    return false;
	},

	// 确认弹出层
	confirm_essay : function(id,status,url) {

		if(status==1){
			var msg = '确定采用此文章吗？'
		}else if(status==2){
			var msg = '确定拟采用此文章吗？'
		}else if(status==3){
			var msg = '确定对此文章退稿吗？'
		}
		layer.confirm('<p>'+msg+'</p><div><p>请输入备注</p><textarea name="essaynote" rows="5" style="width: 285px;" id="essaynote"></textarea></div>', {icon: 3, title:'提示'}, function(index){



			var essaynote=$('#essaynote').val();
			$.getJSON(url, {'id' : id, 'status':status,'essaynote':essaynote}, function(res){
				if(res.code == 1){
					layer.msg(res.msg,{icon:1,time:1500,shade: 0.1});
					Ajaxpage()
				}else{
					layer.msg(res.msg,{icon:0,time:1500,shade: 0.1});
				}
			});
			layer.close(index);
		})
	}



}