<?php

namespace app\mobile\model;
use think\Model;
use think\Db;

class ArticleModel extends Model
{
    protected $name = 'article';

    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = true;


    /**
     * 根据搜索条件获取用户列表信息
     * @author [田建龙] [864491238@qq.com]
     */
    public function getArticleByWhere($map, $Nowpage, $limits)
    {
        return $this->field('shuren_article.*,name')->join('shuren_article_cate', 'shuren_article.cate_id = shuren_article_cate.id')->where($map)->page($Nowpage, $limits)->order('id desc')->select();
    }


    public function getArticleByWhere1($map, $Nowpage, $limits)
    {
        return $this->field('shuren_article.*,name')->join('shuren_article_cate', 'shuren_article.cate_id = shuren_article_cate.id')->where($map)->page($Nowpage, $limits)->order('shuren_article.status asc,shuren_article.id desc')->select();
    }
    /**
     * [insertArticle 添加文章]
     * @author [田建龙] [864491238@qq.com]
     */
    public function insertArticle($param)
    {
        try{
            $result = $this->allowField(true)->save($param);
            if(false === $result){
                return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '文章添加成功'];
            }
        }catch( PDOException $e){
            return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
        }
    }



    /**
     * [updateArticle 编辑文章]
     * @author [田建龙] [864491238@qq.com]
     */
    public function updateArticle($param)
    {
        try{
            $result = $this->allowField(true)->save($param, ['id' => $param['id']]);
            if(false === $result){
                return ['code' => 0, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '文章编辑成功'];
            }
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }

    public function updateUserinfo($param)
    {
        try{
            $result = Db::name('member')->where('id',$param['id'])->update($param);
            if(false === $result){
                return ['code' => 0, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '用户信息编辑成功'];
            }
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }

    /**
     * [updateArticle 编辑文章]
     * @author [田建龙] [864491238@qq.com]
     */
    public function updateArticle1($param)
    {
        try{
            $result = $this->allowField(true)->save($param, ['id' => $param['id']]);
            if(false === $result){
                return ['code' => 0, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '文章编辑成功'];
            }
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }

    /**
     * [getOneArticle 根据文章id获取一条信息]
     * @author [田建龙] [864491238@qq.com]
     */
    public function getOneArticle($id)
    {
        return $this->where('id', $id)->find();
    }



    /**
     * [delArticle 删除文章]
     * @author [田建龙] [864491238@qq.com]
     */
    public function delArticle($id)
    {
        try{
            $this->where('id', $id)->delete();
            return ['code' => 1, 'data' => '', 'msg' => '文章删除成功'];
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }


    public function insertEssayArticle($param)
    {
        $param['update_time']=time();
        try{
            $result = Db::name('article_edit')->insert($param);
            if(false === $result){
                return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '文档修改已保存成功'];
            }
        }catch( PDOException $e){
            return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
        }
    }

    public function updateEssayArticle($param,$edit_id)
    {
        $param['update_time']=time();
        try{
            $result = Db::name('article_edit')->where('id',$edit_id)->update($param);
            if(false === $result){
                return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '文档修改已保存成功'];
            }
        }catch( PDOException $e){
            return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
        }
    }


    public function getArticleComment($map, $Nowpage, $limits)
    {
        return Db::name('article_comment')->field('shuren_article_comment.*,shuren_article.title,shuren_member.account,shuren_member.nickname')->join('shuren_article', 'shuren_article_comment.article_id = shuren_article.id')->join('shuren_member', 'shuren_article_comment.user_id = shuren_member.id')->where($map)->page($Nowpage, $limits)->order('shuren_article_comment.status asc,shuren_article_comment.id desc')->select();
    }

    public function getDebateComment($map, $Nowpage, $limits)
    {
        return Db::name('debate_list')->field('shuren_debate_list.*,shuren_debate.title,shuren_member.account,shuren_member.nickname')->join('shuren_debate', 'shuren_debate_list.debate_id = shuren_debate.id')->join('shuren_member', 'shuren_debate_list.user_id = shuren_member.id')->where($map)->page($Nowpage, $limits)->order('shuren_debate_list.status asc,shuren_debate_list.id desc')->select();
    }

    public function getDebateCommentList($map, $Nowpage, $limits)
    {
        return Db::name('debate_comment')->field('shuren_debate_comment.*,shuren_debate_list.debate_id,shuren_debate_list.content,shuren_debate.title,shuren_member.account,shuren_member.nickname')->
        join('shuren_debate_list','shuren_debate_comment.debate_list_id = shuren_debate_list.id ')->join('shuren_debate', 'shuren_debate_list.debate_id = shuren_debate.id')->join('shuren_member', 'shuren_debate_list.user_id = shuren_member.id')->where($map)->page($Nowpage, $limits)->order('shuren_debate_comment.status asc,shuren_debate_comment.id desc')->select();
    }








}