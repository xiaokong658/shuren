<?php
namespace app\web\controller;
use app\admin\model\ArticleModel;
use app\admin\model\DebateModel;
use app\admin\model\EssayModel;
use app\admin\model\GongwenModel;
use app\admin\model\TempModel;
use app\admin\model\ArticleCateModel;
use think\Controller;
use think\Log;
use think\View;
use think\Db;
class Article extends  Base
{


    public function article()
    {

        $type = input('param.type');
        $keyword = input('param.keyword');
        $cate_list = Db::name('article_cate')->field('id,name')->where('status',1)->select();

        $page = input('param.page');
        if(empty($page)){
            $page = 1;
        }
        $a = ($page-1)*8;
        $map['status'] = 1;
        if($type){
            $map['cate_id'] = $type;
        }

        if($keyword){
            $map['title'] = ['like','%'.$keyword.'%'];
            $map1['content'] = ['like','%'.$keyword.'%'];
        }

        if(input('param.page'))
        {
            if($keyword){
                $list  =  Db::name('article')->field('id,title,create_time,photo,remark,views')->where($map)->whereor($map1)->order('id desc')->limit($a,8)->select();
            }
                $list  =  Db::name('article')->field('id,title,create_time,photo,remark,views')->where($map)->order('id desc')->limit($a,8)->select();

            foreach($list as $key=>$item){
                $list[$key]['create_time'] = date('Y-m-d',$item['create_time']);
                $list[$key]['zan']  = Db::name('article_zan')->where('article_id',$item['id'])->count();
                $list[$key]['collect']  = Db::name('article_collect')->where('article_id',$item['id'])->count();
                $list[$key]['comment']  = Db::name('article_comment')->where('article_id',$item['id'])->count();
            }
            return json(['code' => 1, 'list'=>$list,  'msg' => 'ok']);
        }




        $this->assign('cate',$cate_list);
        $this->assign('keyword',$keyword);
        $this->assign('type',$type);
        return $this->fetch();

    }

    public function essay()
    {

        $type = input('param.type');
        $keyword = input('param.keyword');

        $cate_list = Db::name('article_cate')->field('id,name')->where('status',1)->select();


        $page = input('param.page');
        if(empty($page)){
            $page = 1;
        }
        $a = ($page-1)*8;
        $map =array();
//        $map['status'] = 1;
        if($type){
            $map['cate_id'] = $type;
        }

        if($keyword){
            $map['title'] = ['like','%'.$keyword.'%'];
        }




        if(input('param.page'))
        {
            $list  =  Db::name('essay')->field('id,title,create_time,photo,remark,views')->where($map)->order('id desc')->limit($a,8)->select();
            foreach($list as $key=>$item){
                $list[$key]['create_time'] = date('Y-m-d',$item['create_time']);
                $list[$key]['zan']  = Db::name('essay_zan')->where('essay_id',$item['id'])->count();
                $list[$key]['collect']  = Db::name('essay_collect')->where('essay_id',$item['id'])->count();
//                $list[$key]['comment']  = Db::name('essay_comment')->where('essay_id',$item['id'])->count();
            }
            return json(['code' => 1, 'list'=>$list,  'msg' => 'ok']);
        }



        $this->assign('cate',$cate_list);
        $this->assign('keyword',$keyword);
        $this->assign('type',$type);
        return $this->fetch();

    }


    public function debate()
    {
        $page = input('param.page');
        if(empty($page)){
            $page = 1;
        }

        $keyword = input('param.keyword');

        $a = ($page-1)*8;
        $map = array();
        if($keyword){
            $map['title'] = ['like','%'.$keyword.'%'];
        }
        if(input('param.page'))
        {
            $list  =  Db::name('debate')->field('id,title,create_time,photo')->where($map)->order('id desc')->limit($a,8)->select();
            foreach($list as $key=>$item){
                $list[$key]['create_time'] = date('Y-m-d H:i:s',$item['create_time']);
                $list[$key]['zan']  = Db::name('debates_zan')->where('debate_id',$item['id'])->count();
                $list[$key]['collect']  = Db::name('debate_collect')->where('debate_id',$item['id'])->count();
                $list[$key]['comment']  = Db::name('debate_list')->where('debate_id',$item['id'])->count();


            }
            return json(['code' => 1, 'list'=>$list,  'msg' => 'ok']);
        }

        $this->assign('keyword',$keyword);
        return $this->fetch();

    }


    public function index()
    {

        $id = input('param.id');
        $this->read_article($id);
        $article = Db::name('article')->where('id',$id)->find();

        $article['create_time1'] = date('Y-m-d H:i:s',$article['create_time']);


        if(empty($article['user_id'])){
            $map6 = array(
                'instit_id'=>$article['instit_id'],
                'group_id'=>8
            );
            $user_info = Db::name('member')->field('id,nickname,head_img,desc,group_id')->where($map6)->find();
            $author = $user_info['nickname'];
            $article['user_id'] = $user_info['id'];
        }else{

            // $auth = Db::name('member')->field('nickname')->where('id',$article['user_id'])->find();
            $user_info = Db::name('member')->field('id,nickname,head_img,desc,group_id')->where('id',$article['user_id'])->find();
            $author = $user_info['nickname'];
        }

        $author_info = get_user_article_count($user_info['id']);



//        if($user_info['group_id']==8){
//            $article_count =  Db::name('article')->where("status=1 and (user_id='".$user_info['id']."' or (user_id = 0 and instit_id=".$article['instit_id']."))")->count();
//            $debate_count =  Db::name('debate')->where("user_id='".$user_info['id']."' or (user_id = 0 and instit_id=".$article['instit_id'].")")->count();
//            $fans_count = Db::name('fans')->where('fans_id',$user_info['id'])->count();
//        }    else{
//            $article_count =  Db::name('article')->where("status=1 and user_id  = '".$user_info['id']."'")->count();
//            $debate_count =  Db::name('debate')->where("user_id",$user_info['id'])->count();
//            $fans_count = Db::name('fans')->where('fans_id',$user_info['id'])->count();
//        }


        $user_id = session('user_id');

        $zans = Db::name('article_zan')->field('count(*) as num')->where('article_id',$id)->find();
        $map = array(
            'user_id'=>$user_id,
            'article_id'=>$id,
        );
        $myzan = Db::name('article_zan')->field('is_zan')->where($map)->find();
        if($myzan){
            $is_zan = $myzan['is_zan'];
        }else{
            $is_zan = 0;
        }
        $collects = Db::name('article_collect')->where('article_id',$id)->count();
        $map = array(
            'user_id'=>$user_id,
            'article_id'=>$id,
        );
        $mycollect = Db::name('article_collect')->field('is_collect')->where($map)->find();
        if($mycollect){
            $is_collect = $mycollect['is_collect'];
        }else{
            $is_collect = 0;
        }
        $map10 = array(
            'article_id'=>$id,
            'status'=>1,
        );
        $commentall = Db::name('article_comment')->where($map10)->count();
        $commentpage =$commentall/5;
        $commentpage = (int)$commentpage;
        $commentyu = $commentall%5;

        $comment_list =  Db::name('article_comment')->alias('a')->field('a.id,a.content,a.addtime,b.nickname,b.head_img')->join('shuren_member b','a.user_id = b.id')->where('article_id',$id)->select();

        if($article['vote_id']>0){
            $vote = Db::name('vote')->where('id',$article['vote_id'])->find();
            $choose = Db::name('vote_choose')->where('vote_id',$article['vote_id'])->select();
            foreach($choose as $key => $val){
                $map = [];
                $map['vote_id'] = $article['vote_id'];
//            $map['choose'] = ['like','%,'.$v['id'].',%'];
                $v_id = $val['id'];
                $map['choose'] = ['like',"%," . $v_id . ",%"];
                //  print_R($map);
                $count = Db::name('vote_log')->field('count(id) as num')->where($map)->find();
                $choose[$key]['num'] = $count['num'];
            }
            if(empty($choose[0]['img'])){
                $imgs = 0;
            }else{
                $imgs = 1;
            }
        }else{
            $vote = array('max_num'=>0);
            $choose=array();
            $imgs=0;
        }
        if($article['baoming_id']>0){
            $baoming = Db::name('baoming')->where('id',$article['baoming_id'])->find();
            $xiangmu = explode('|',$baoming['desc']);
            $arr = array();
            foreach($xiangmu as $k=>$v){
                $arr[$k]['name'] = $v;
                $arr[$k]['title'] = 'tt'.$k;
                $i = $k;
            }
        }else{
            $baoming =array();
            $arr =array();
            $i = '';
        }

        $this->assign('baoming',$baoming);
        $this->assign('xiangmu',$arr);
        $this->assign('i',$i);
        $this->assign('vote',$vote);
        $this->assign('choose',$choose);
        $this->assign('imgs',$imgs);


        //$view = new View();
        //print_R($article);
        $this->assign('article',$article);    //文章内容
        $this->assign('comment_list',$comment_list);    //文章内容
        $this->assign('author',$author);   //总的点赞数
        if($commentyu>0){
            $this->assign('all',$commentpage+1);   //总的点赞数
        }else{
            $this->assign('all',$commentpage);   //总的点赞数
        }
        $this->assign('zan',$zans['num']);   //总的点赞数
        $this->assign('is_zan',$is_zan);     //我是否赞
        $this->assign('collect',$collects);

        if(empty($user_id)){
            $user_id = 0;
        }

        $tp_article = Db::name('pt_article_edit')->alias('a')->field('a.title,a.remark,a.content,a.update_time,b.name,b.id')->join('instit b','a.by_instit_id=b.id')->where('a.article_id',$id)->select();




        $map_info['status'] = 1;
        $article_list =  Db::name('article')->field('id,user_id,instit_id,title,create_time,photo')->where($map_info)->order('views desc')->limit(5)->select();
        if($article_list){
            foreach($article_list as $key=>$val){
                $author =   get_article_author($val['user_id'],$val['instit_id']);
                $article_list[$key]['nickname'] =   $author['nickname'];
                $article_list[$key]['head_img'] =   $author['head_img'];
                $article_list[$key]['user_id'] =   $author['id'];
                $today = date('Y-m-d');
                $day = date('Y-m-d',$val['create_time']);
                if($today==$day){
                    $article_list[$key]['addtime'] = '今天 '.date('H:i',$val['create_time']);
                }else{
                    $article_list[$key]['addtime'] = date('Y-m-d H:i',$val['create_time']);
                }
                $article_list[$key]['comment_count'] = Db::name('article_comment')->where(['status'=>1,'article_id'=>$val['id']])->count();
            }
        }


        $cate_list = Db::name('article_cate')->field('id,name')->where('status',1)->select();


        $this->assign('tp_article',$tp_article);
        $this->assign('cate_list',$cate_list);
        $this->assign('user_id',$user_id);
        $this->assign('commentall',$commentall);
        $this->assign('user_info',$user_info);
        $this->assign('id',$id);

        $this->assign('author_info',$author_info);
        $this->assign('tuijian_list',$article_list);
        $this->assign('is_collect',$is_collect);

        $url = $_SERVER['SERVER_NAME'];

        $this->assign('url','http://'.$url.'/index/index/article_detail/id/'.$id.'.html');
        return $this->fetch();

    }

    public function essay_detail()
    {
        $id=input('param.id');
        $map  = array();
        $map['id'] = $id;
        $user_id = session('user_id');

       $info = Db::name('essay')->where($map)->find();
//
//        if($info['user_id']){
//            $auther_info = Db::name('member')->field('instit_id,group_id')->where('id',$info['user_id'])->find();
//        }





        $info['create_time1'] = date('Y-m-d H:i:s',$info['create_time']);

        if($info['end_time']>time()){
            $can_tou =1;
        }else{
            $can_tou =0;
        }
        $info['end_time'] = date('Y-m-d H:i:s',$info['end_time']);
        $map1 = array(
            'user_id'=>$user_id,
            'status'=>1
        );
        $myarticle = Db::name('article')->field('id,title')->where($map1)->select();
        $zans = Db::name('essay_zan')->field('count(*) as num')->where('essay_id',$id)->find();
        $map = array(
            'user_id'=>$user_id,
            'essay_id'=>$id,
        );
        $myzan = Db::name('essay_zan')->field('is_zan')->where($map)->find();
        if($myzan){
            $is_zan = $myzan['is_zan'];
        }else{
            $is_zan = 0;
        }
        $collects = Db::name('essay_collect')->field('count(*) as num')->where('essay_id',$id)->find();
        $map = array(
            'user_id'=>$user_id,
            'essay_id'=>$id,
        );
        $mycollect = Db::name('essay_collect')->field('is_collect')->where($map)->find();
        if($mycollect){
            $is_collect = $mycollect['is_collect'];
        }else{
            $is_collect = 0;
        }
        if(empty($user_id)){
            $user_id = 0;
        }

        $map10 = array(
            'a.essay_id'=>$id,
            'a.status'=>1
        );

        $list = Db::name('essay_article')->alias('a')->field('a.id,a.article_id,a.add_time,b.title,b.photo')->
        join('shuren_article b','a.article_id = b.id')->where($map10)->order('id desc')->select();
//        if($list){
//            foreach($list as $key=>$item){
//                $map11['article_id'] = $item['article_id'];
//                $count = Db::name('article_edit')->where($map11)->count();
//                $list[$key]['count'] = $count;
//                if($count>0){
//                    $article_eidt_info = Db::name('article_edit')->where($map11)->find();
//                    $list[$key]['article_eidt_id'] = $article_eidt_info['id'];
//                    $list[$key]['article_eidt_title'] = $article_eidt_info['title'];
//                    $list[$key]['update_time'] = date('m-d H:i',$article_eidt_info['update_time']);
//                }
//                $list[$key]['add_time'] = date('m-d H:i',$item['add_time']);
//            }
//        }


        $map6 = array(
            'instit_id'=>$info['instit_id'],
            'group_id'=>8
        );
        $user_info = Db::name('member')->field('id,nickname,head_img,desc')->where($map6)->find();



       $article_count =  Db::name('article')->where("status=1 and (user_id='".$user_info['id']."' or (user_id = 0 and instit_id=".$info['instit_id']."))")->count();
        $debate_count =  Db::name('debate')->where("user_id='".$user_info['id']."' or (user_id = 0 and instit_id=".$info['instit_id'].")")->count();
        $fans_count = Db::name('fans')->where('fans_id',$user_info['id'])->count();


        $this->assign('zan',$zans['num']);   //总的点赞数
        $this->assign('is_zan',$is_zan);     //我是否赞
        $this->assign('collect',$collects['num']);
        $this->assign('is_collect',$is_collect);
        $this->assign('user_id',$user_id);
        $this->assign('myarticle',$myarticle);
        $this->assign('user_info',$user_info);


        $this->assign('fans_count',$fans_count);
        $this->assign('article_count',$article_count+$debate_count);


        $this->assign('info',$info);
        $this->assign('id',$id);
        $this->assign('list',$list);





        $this->assign('can_tou',$can_tou);
        return $this->fetch();
    }


    public function debate_detail()
    {
        $id=input('param.id');
        $map  = array();
        $map['id'] = $id;

        $info = Db::name('debate')->where($map)->find();
        if($info['end_time']<time()){
            $info['can'] = 0;
        }else{
            $info['can'] = 1;
        }
        $info['end_time1'] = date('Y-m-d H:i:s',$info['end_time']);
        $comment_num =  Db::name('debate_list')->where('debate_id',$id)->count();
        $user_id = session('user_id');
        if(empty($user_id)){
            $user_id = 0;
        }

        $zans = Db::name('debates_zan')->field('count(*) as num')->where('debate_id',$id)->find();
        $map = array(
            'user_id'=>$user_id,
            'debate_id'=>$id,
        );
        $myzan = Db::name('debates_zan')->field('is_zan')->where($map)->find();
        if($myzan){
            $is_zan = $myzan['is_zan'];
        }else{
            $is_zan = 0;
        }
        $collects = Db::name('debate_collect')->field('count(*) as num')->where('debate_id',$id)->find();

        $map = array(
            'user_id'=>$user_id,
            'debate_id'=>$id,
        );
        $mycollect = Db::name('debate_collect')->field('is_collect')->where($map)->find();
        if($mycollect){
            $is_collect = $mycollect['is_collect'];
        }else{
            $is_collect = 0;
        }



            // $auth = Db::name('member')->field('nickname')->where('id',$article['user_id'])->find();
        $user_info = Db::name('member')->field('nickname,head_img,desc')->where('id',$info['user_id'])->find();


        $author_info = get_user_article_count($info['id']);


        $info['create_time1'] = date('Y-m-d H:i:s',$info['create_time']);



        $this->assign('user_id',$user_id);
        $this->assign('info',$info);
        $this->assign('comment_num',$comment_num);
        $this->assign('user_info',$user_info);
        $this->assign('id',$id);
        $this->assign('zan',$zans['num']);   //总的点赞数
        $this->assign('is_zan',$is_zan);     //我是否赞
        $this->assign('collect',$collects['num']);
        $this->assign('author_info',$author_info);
        $this->assign('is_collect',$is_collect);


        return $this->fetch();
    }

    public  function  user_sign(){
        $user_id = session('user_id');

        $integral = $this->get_config_v('every_sign_integral');
        $cycle = $this->get_config_v('sign_cycle');



        $result =  $this->user_signs($user_id,$integral,$cycle);
        if($result['code']==1){
            return json(['code' => 1, 'data' => $result['integral'], 'msg' => '签到成功！']);
        }
        if($result['code']==-2){
            return json(['code' => 5, 'data' => '2', 'msg' => '请勿重复签到']);
        }

    }


    public function write()
    {
        $this->user_base();
        $user_id = session('user_id');
        if(empty($user_id)){
                $this->redirect('user/login');
        }

        $member = Db::name('member')->field('id,group_id,nickname,head_img,account,integral')->where('id',$user_id)->find();

        $day = strtotime(date('Y-m-d'));
        $is_sign = Db::name('sign_log')->where(['user_id'=>$user_id,'sign_time'=>$day])->count();


        if(request()->isAjax()){
            $param = input('post.');
            $article = new ArticleModel();
            $param['instit_id'] = session('instit_id');
            $param['user_id'] = session('user_id');

            $thismember = Db::name('member')->field('group_id')->where('id',$user_id)->find();

            if(empty($param['id'])){
                unset($param['id']);
                if($thismember['group_id']==8){
                    $param['status'] = 1;
                }
                $flag = $article->insertArticle($param);
            }else{
                if($thismember['group_id']==8){
                    $param['status'] = 1;
                }else{
                    $param['status'] = 0;
                }
                $flag = $article->updateArticle($param);
            }
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }


        $id = input('param.id');
        if($id){

            $article = new ArticleModel();
            $info = $article->getOneArticle($id);


        }else{
            $info = array(
                'title'=>'',
                'remark'=>'',
                'id'=>'',
                'content'=>'',
                'photo'=>'',
                'cate_id'=>'',
                'column_id'=>'',
            );
        }

        $lanmu = Db::name('user_column')->where('user_id',$user_id)->select();

        $cate = new ArticleCateModel();
        $this->assign('cate',$cate->getAllCate(0));
        $this->assign('info',$info);
        $this->assign('member',$member);
        $this->assign('m','2');
        $this->assign('is_sign',$is_sign);
        $this->assign('lanmu',$lanmu);
        return $this->fetch();

    }

    public function cg_add_article()
    {

        if(request()->isAjax()){

            $param = input('post.');
            $article = new ArticleModel();
            $param['instit_id'] = session('instit_id');
            $param['user_id'] = session('user_id');
            $param['create_time'] = time();
            $param['status'] = -2;

            if(empty($param['id'])){
                unset($param['id']);
                $flag = $article->insertArticle($param);
            }else{
                $flag = $article->updateArticle($param);
            }



            if($flag['code']==1){
                $flag['msg'] = '草稿保存成功';
            }

            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);

            if($flag){
                $this->redirect('article/draft');
            }
        }
    }



    public function write_debate()
    {
        $this->user_base();
        $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }

        $member = Db::name('member')->field('id,group_id,nickname,head_img,account,integral')->where('id',$user_id)->find();


        if(request()->isAjax()){

//            $user = new UserModel();
//            $user_info =  $user->getUserGroupinfo();

            $param = input('post.');



            $param['user_id'] = session('user_id');
            $param['instit_id'] =  session('instit_id');
            $param['end_time'] = strtotime($param['end_time'])+3600*24-1;
            $article = new DebateModel();
            $flag = $article->insertArticle($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }
        $day = strtotime(date('Y-m-d'));
        $is_sign = Db::name('sign_log')->where(['user_id'=>$user_id,'sign_time'=>$day])->count();
        $this->assign('is_sign',$is_sign);
        $this->assign('member',$member);
        $this->assign('m','6');
        return $this->fetch();

    }


    public function collect()
    {


        $this->user_base();
        $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }

        $member = Db::name('member')->field('id,group_id,nickname,head_img,account,integral')->where('id',$user_id)->find();

        $Nowpage = input('param.page') ? input('param.page'):1;
        $type = input('param.type') ? input('param.type'):1;
        $limits = 10;// 获取总条数

            $map['a.user_id'] = $user_id;
            if($type==1){
                $count = Db::name('article_collect')->where('user_id',$user_id)->count();//计算总页面


                $list = Db::name('article_collect')->alias('a')->field('a.id as collect_id,b.id,b.title,b.photo,b.create_time,b.user_id,b.writer')->join('article b', 'a.article_id = b.id')->where($map)->order('a.id desc')->select();

                foreach ($list as $key => $val) {
                    if (!empty($val['user_id'])) {
                        $author = Db::name('member')->field('nickname')->where('id', $val['user_id'])->find();
                        $list[$key]['author'] = $author['nickname'];
                    }
                    $list[$key]['create_time'] = date('Y-m-d', $val['create_time']);
                }
                if(input('param.page')) {
                    return json(['code' => 1, 'list'=>$list,  'msg' => 'ok']);
                }
            }elseif($type==2){
                $count = Db::name('essay_collect')->where('user_id',$user_id)->count();//计算总页面
                $essay = Db::name('essay_collect')->alias('a')->field('a.id as collect_id,b.id,b.title,b.photo,b.create_time,b.user_id,b.writer,b.instit_id')->join('essay b', 'a.essay_id = b.id')->where($map)->order('a.id desc')->select();
                foreach ($essay as $key => $val) {
                    $author = Db::name('instit')->field('name')->where('id', $val['instit_id'])->find();
                    $essay[$key]['author'] = $author['name'];
                    $essay[$key]['create_time'] = date('Y-m-d', $val['create_time']);
                }
                if(input('param.page')) {
                    return json(['code' => 1, 'list'=>$essay,  'msg' => 'ok']);
                }
            }elseif($type==3) {
                $count = Db::name('debate_collect')->where('user_id',$user_id)->count();//计算总页面
                $debate = Db::name('debate_collect')->alias('a')->field('a.id as collect_id,b.id,b.title,b.photo,b.create_time,b.user_id,b.writer')->join('debate b', 'a.debate_id = b.id')->where($map)->order('a.id desc')->select();
                foreach ($debate as $key => $val) {
                    $author = Db::name('member')->field('nickname')->where('id', $val['user_id'])->find();
                    $debate[$key]['author'] = $author['nickname'];
                    $debate[$key]['create_time'] = date('Y-m-d', $val['create_time']);
                }
                if(input('param.page')) {
                    return json(['code' => 1, 'list'=>$debate,  'msg' => 'ok']);
                }
            }



        $allpage = intval(ceil($count / $limits));
        $day = strtotime(date('Y-m-d'));
        $is_sign = Db::name('sign_log')->where(['user_id'=>$user_id,'sign_time'=>$day])->count();
        $this->assign('is_sign',$is_sign);
        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('member',$member);
        $this->assign('type', $type); //总页数
        $this->assign('m','10');
        return $this->fetch();







    }

    public function myessay()
    {

        $this->user_base();
        $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }

        $member = Db::name('member')->field('id,group_id,nickname,head_img,account,integral')->where('id',$user_id)->find();
        $map['a.user_id'] = $user_id;
        $map1['user_id'] = $user_id;
        //  $map['status'] = 1;

        $Nowpage = input('param.page') ? input('param.page'):1;
        $limits = 10;// 获取总条数
        $count = Db::name('essay_article')->where($map1)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));

        if(input('param.page'))
        {
            //$list  =  Db::name('article')->field('id,title,create_time,photo')->where($map)->order('id desc')->limit($a,5)->select();

            if($member['group_id']==8){
                $map3 = array(
                    'instit_id'=>session('instit_id')
                );
                $list = Db::name('essay')->where($map3)->page($Nowpage,$limits)->order('my_ding desc,id desc')->select();
                foreach($list as $key=>$item){
                    $list[$key]['add_time'] = date('Y-m-d H:i:s',$item['create_time']);
                    $list[$key]['end_time'] = date('Y-m-d H:i:s',$item['end_time']);
                }
            }else{
                $list = Db::name('essay_article')->alias('a')->field('a.article_id,a.status,a.essay_id,c.id,b.photo,a.add_time,b.title as essay_title,c.title as article_title')->join('essay b','a.essay_id=b.id')->join('article c','a.article_id=c.id')->where($map)->page($Nowpage,$limits)->order('a.id desc')->select();
                foreach($list as $key=>$item){
                    $list[$key]['add_time'] = date('Y-m-d H:i:s',$item['add_time']);
                    if($item['status']==1){
                        $list[$key]['note'] = '已采用';
                    }if($item['status']==2){
                        $list[$key]['note'] = '拟采用';
                    }if($item['status']==3){
                        $list[$key]['note'] = '已退稿';
                    }else{
                        $list[$key]['note'] = '';
                    }
                }
            }


            return json(['code' => 1, 'list'=>$list,  'msg' => 'ok']);
        }

        $day = strtotime(date('Y-m-d'));
        $is_sign = Db::name('sign_log')->where(['user_id'=>$user_id,'sign_time'=>$day])->count();
        $this->assign('is_sign',$is_sign);

        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('member',$member);
        $this->assign('m','5');

        return $this->fetch();

    }

    public function myarticle()
    {

        $this->user_base();
        $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }

        $member = Db::name('member')->field('id,group_id,nickname,head_img,account,integral')->where('id',$user_id)->find();
        $map['a.user_id'] = $user_id;
        $map1['user_id'] = $user_id;
//        $map1['status'] = ['neq',-1];
        //  $map['status'] = 1;

        $Nowpage = input('param.page') ? input('param.page'):1;
        $limits = 10;// 获取总条数
        $count = Db::name('article')->where($map1)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));

        if(input('param.page'))
        {
            //$list  =  Db::name('article')->field('id,title,create_time,photo')->where($map)->order('id desc')->limit($a,5)->select();

//            if($member['group_id']==8){
//                $map3 = array(
//                    'instit_id'=>session('instit_id')
//                );
//                $list = Db::name('essay')->where($map3)->page($Nowpage,$limits)->order('id desc')->select();
//                foreach($list as $key=>$item){
//                    $list[$key]['add_time'] = date('Y-m-d H:i:s',$item['create_time']);
//                    $list[$key]['end_time'] = date('Y-m-d H:i:s',$item['end_time']);
//                }
//
//
//                $list = Db::name('artcile')->where($map3)->page($Nowpage,$limits)->order('id desc')->select();
//
//
//
//            }else{
//                $list = Db::name('essay_article')->alias('a')->field('a.article_id,a.status,a.essay_id,c.id,b.photo,a.add_time,b.title as essay_title,c.title as article_title')->join('essay b','a.essay_id=b.id')->join('article c','a.article_id=c.id')->where($map)->page($Nowpage,$limits)->order('a.id desc')->select();
//                foreach($list as $key=>$item){
//                    $list[$key]['add_time'] = date('Y-m-d H:i:s',$item['add_time']);
//                    if($item['status']==1){
//                        $list[$key]['note'] = '已采用';
//                    }if($item['status']==2){
//                        $list[$key]['note'] = '拟采用';
//                    }if($item['status']==3){
//                        $list[$key]['note'] = '已退稿';
//                    }else{
//                        $list[$key]['note'] = '';
//                    }
//                }
//            }


            $list = Db::name('article')->where($map1)->page($Nowpage,$limits)->order('my_ding desc,id desc')->select();


            foreach($list as $key=>$item){
                    $list[$key]['create_time1'] = date('Y-m-d H:i:s',$item['create_time']);
                    switch($item['status']){
                        case '0' : $status = '未审核';break;
                        case '1' : $status = '已发布';break;
                        case '-4' : $status = '已驳回';break;
                        default: $status = '';
                    }
                    $list[$key]['status1'] = $status;
                }

            return json(['code' => 1, 'list'=>$list,  'msg' => 'ok']);
        }
        $day = strtotime(date('Y-m-d'));
        $is_sign = Db::name('sign_log')->where(['user_id'=>$user_id,'sign_time'=>$day])->count();
        $this->assign('is_sign',$is_sign);
        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('m','3');
        $this->assign('member',$member);

        return $this->fetch();

    }


    public function mydebate()
    {

        $this->user_base();
        $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }

        $member = Db::name('member')->field('id,group_id,nickname,head_img,account,integral')->where('id',$user_id)->find();
        $map1['user_id'] = $user_id;
        //  $map['status'] = 1;

        $Nowpage = input('param.page') ? input('param.page'):1;
        $limits = 10;// 获取总条数
        $count = Db::name('debate')->where($map1)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));

        if(input('param.page'))
        {

            $list = Db::name('debate')->where($map1)->page($Nowpage,$limits)->order('my_ding desc,id desc')->select();
            foreach($list as $key=>$item){
                $list[$key]['create_time1'] = date('Y-m-d H:i:s',$item['create_time']);
//                switch($item['status']){
//                    case '0' : $status = '未审核';break;
//                    case '1' : $status = '已发布';break;
//                    case '-4' : $status = '已驳回';break;
//                    default: $status = '';
//                }
//                $list[$key]['status1'] = $status;
            }

            return json(['code' => 1, 'list'=>$list,  'msg' => 'ok']);
        }
        $day = strtotime(date('Y-m-d'));
        $is_sign = Db::name('sign_log')->where(['user_id'=>$user_id,'sign_time'=>$day])->count();
        $this->assign('is_sign',$is_sign);
        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('m','7');
        $this->assign('member',$member);
        return $this->fetch();
    }


    public function draft()
    {
        $this->user_base();
        $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }

        $member = Db::name('member')->field('id,group_id,nickname,head_img,account,integral')->where('id',$user_id)->find();


        $map['user_id'] = $user_id;
        $map['status'] = -2;

        $Nowpage = input('param.page') ? input('param.page'):1;
        $limits = 10;// 获取总条数
        $count = Db::name('article')->where($map)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));

        if(input('param.page'))
        {
            $list  =  Db::name('article')->field('id,title,create_time,photo')->where($map)->order('id desc')->page($Nowpage,10)->select();
            foreach($list as $key=>$item){
                $list[$key]['create_time'] = date('Y-m-d H:i:s',$item['create_time']);
            }
            return json(['code' => 1, 'list'=>$list,  'msg' => 'ok']);
        }
        $day = strtotime(date('Y-m-d'));
        $is_sign = Db::name('sign_log')->where(['user_id'=>$user_id,'sign_time'=>$day])->count();
        $this->assign('is_sign',$is_sign);
        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('member',$member);
        $this->assign('m','9');
        return $this->fetch();


    }

    public function temp_message()
    {
        $this->user_base();
        $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }

        $member = Db::name('member')->field('id,group_id,nickname,head_img,account,integral')->where('id',$user_id)->find();


        $user =  DB::name('member')->field('account,group_id')->where('id',$user_id)->find();
        if($user['group_id']==9){
            $map2 = array(
                'mobile'=>$user['account'],
                'instit_id'=>session('instit_id'),
                'is_leader'=>1
            );
            $teach =  DB::name('teach')->field('id')->where($map2)->find();

            if($teach){
                $map3 = array(
                    'leader'=>$teach['id'],
                    'instit_id'=>session('instit_id'),
                    'status'=>1
                );
                $banji_info = DB::name('theclass')->where($map3)->find();
            }

            if($teach&&$banji_info){
                $data = array(
                    'is_send' => 1,
                    'class_id'=>$banji_info['id']
                );
                $auth = 'teach';
            }else{
                $data = array(
                    'is_send' => 0,
                    'class_id'=>''
                );
                $auth = 'teach2';
            }
        }


        if($user['group_id']==8){
            $auth = 'admin';
        }

        if($user['group_id']==1){
            $auth = 'student';
        }

        if($user['group_id']==2){
            $auth = 'youke';
        }

        $key = input('key');
        $map = [];




        $Nowpage = input('get.page') ? input('get.page'):1;
        $limits = 10;// 获取总条数


        $nowyear = date('Y');
        $nowtime = strtotime($nowyear.'-08-25');
        if($nowtime>time()){
            $year = $nowyear-1;
        }else{
            $year = $nowyear;
        }



        if($auth=='admin'){
            $map['shuren_temp_message.instit_id'] = session('instit_id');
            $map1['instit_id'] = session('instit_id');
            $count = Db::name('temp_message')->where($map1)->count();//计算总页面
            $allpage = intval(ceil($count / $limits));
            $user = new TempModel();
            $lists = $user->getTempByWhere($map, $Nowpage, $limits);

        }elseif($auth=='teach'){
            $map = array();
            $map2= array();
            $map['shuren_temp_message.instit_id'] = session('instit_id');
            $map['shuren_temp_message.year'] = $year;

            $map2['shuren_temp_message.type'] =  ['in','2,3'];
            $map12['type'] = ['in','2,3'];

            $map2['shuren_temp_message.class_id'] =  $data['class_id'];
            $map12['class_id'] = $data['class_id'];

            $map1['instit_id'] = session('instit_id');
            $map1['year'] = $year;

            $count = Db::name('temp_message')->where($map1)->whereor($map12)->count();//计算总页面
            $allpage = intval(ceil($count / $limits));
            $user = new TempModel();
            // $lists = $user->getTempByWhereTeach($map,$map2, $Nowpage, $limits);
            $lists =  Db::name('temp_message')->field('shuren_temp_message.*,shuren_instit.name')->join('shuren_instit','shuren_temp_message.instit_id = shuren_instit.id')->where("shuren_temp_message.instit_id = ". session('instit_id')." and year = ".$year)->where("shuren_temp_message.class_id = ".$data['class_id'] ." or shuren_temp_message.type in (2,3)")->page($Nowpage, $limits)->order('id desc')->select();
        }elseif($auth=='student'){

            $cla_arr = Db::name('class_student')->where('account',$user['account'])->select();

            if($cla_arr){
                $all_class_str= '';
                foreach($cla_arr as $key=>$v){
                    $all_class_str =$all_class_str . $v['class_id'].',';
                }

                $all_class_str = substr($all_class_str,0,-1);


                $map = array();
                //  $map2= array();
                // $map1['shuren_temp_message.instit_id'] = session('instit_id');
                // $map['shuren_temp_message.year'] = $year;

                // $map2['shuren_temp_message.type'] =  ['in','2'];
                $map12['type'] = 2;

                // $map2['shuren_temp_message.class_id'] = ['in','2'];
                $map12['class_id'] = ['in',$all_class_str];

                $map1['instit_id'] = session('instit_id');
                //$map1['year'] = $year;

                $count = Db::name('temp_message')->where($map1)->whereor($map12)->count();//计算总页面
                $allpage = intval(ceil($count / $limits));
                $user = new TempModel();

                // $lists = $user->getTempByWhereTeach($map,$map2, $Nowpage, $limits);
                $lists =  Db::name('temp_message')->field('shuren_temp_message.*,shuren_instit.name')->join('shuren_instit','shuren_temp_message.instit_id = shuren_instit.id')->
                where("shuren_temp_message.instit_id = ". session('instit_id')." and year = ".$year)->where("shuren_temp_message.class_id in (".$all_class_str .") or shuren_temp_message.type =2 ")
                    ->page($Nowpage, $limits)->order('id desc')->select();

//
//               $sql =   Db::name('temp_message')->getLastSql();
//                echo $sql;
            }


        }elseif($auth=='youke'){




                $map = array();
                $map12['type'] = 2;
                $map1['instit_id'] = session('instit_id');
                $count = Db::name('temp_message')->where($map1)->whereor($map12)->count();//计算总页面
                $allpage = intval(ceil($count / $limits));
                $user = new TempModel();
                echo "d";
                $lists =  Db::name('temp_message')->field('shuren_temp_message.*,shuren_instit.name')->join('shuren_instit','shuren_temp_message.instit_id = shuren_instit.id')->
                where("shuren_temp_message.instit_id = ". session('instit_id'))->where(" shuren_temp_message.type =2 ")
                    ->page($Nowpage, $limits)->order('id desc')->select();
//               $sql =   Db::name('temp_message')->getLastSql();
//                echo $sql;

        }elseif($auth=='teach2'){
            $map['shuren_temp_message.instit_id'] = session('instit_id');
            $map['shuren_temp_message.year'] = $year;
            $map['shuren_temp_message.type'] =  ['in','2','3'];
            $map1['instit_id'] = session('instit_id');
            $map1['year'] = $year;
            $map1['type'] = ['in','2','3'];
            $count = Db::name('temp_message')->where($map1)->count();//计算总页面
            $allpage = intval(ceil($count / $limits));
            $user = new TempModel();
            $lists = $user->getTempByWhere($map, $Nowpage, $limits);
        }



        if($lists){
            foreach($lists as $k=>$v){
                $contents = json_decode($v['content'],true);

                if($v['type']=='1'){
                    $class_info =  Db::name('theclass')->alias('a')->field('n.nianji,a.banji')->join('nianji n','a.nianji = n.id')->where('a.id',$v['class_id'])->find();
                    $lists[$k]['duixiang'] = $class_info['nianji'].$class_info['banji'];
                    $lists[$k]['message'] = $contents['keyword4'];
                }elseif($v['type']=='2'){
                    $lists[$k]['duixiang'] = '全体学生';
                    $lists[$k]['message'] = $contents['keyword4'];
                }elseif($v['type']=='3'){
                    $lists[$k]['duixiang'] = '全体教师';
                    $lists[$k]['message'] = $contents['keyword3'];
                }

                $lists[$k]['addtime1'] = date('Y-m-d H:i:s',$v['addtime']);
            }
        }

        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('val', $key);

        if(input('get.page'))
        {
            return json(['list'=>$lists]);
        }


        $day = strtotime(date('Y-m-d'));
        $is_sign = Db::name('sign_log')->where(['user_id'=>$user_id,'sign_time'=>$day])->count();
        $this->assign('is_sign',$is_sign);
        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('auth', $auth); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('m',11);
        $this->assign('member',$member);

        return $this->fetch();

    }



    public function temp_detail()
    {
        $this->user_base();
        $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }

        $member = Db::name('member')->field('id,group_id,nickname,head_img,account,integral')->where('id',$user_id)->find();

        $id = input('param.id');

        $info =  Db::name('temp_message')->field('shuren_temp_message.*,shuren_instit.name')->join('shuren_instit','shuren_temp_message.instit_id = shuren_instit.id')->where("shuren_temp_message.id",$id)->find();
        $contents = json_decode($info['content'],true);
        if($info['type']=='1'){
            $class_info =  Db::name('theclass')->alias('a')->field('n.nianji,a.banji')->join('nianji n','a.nianji = n.id')->where('a.id',$info['class_id'])->find();
            $info['duixiang'] = $class_info['nianji'].$class_info['banji'];
            $info['message'] = $contents['keyword4'];
        }elseif($info['type']=='2'){
            $info['duixiang'] = '全体学生';
            $info['message'] = $contents['keyword4'];
        }elseif($info['type']=='3'){
            $info['duixiang'] = '全体教师';
            $info['message'] = $contents['keyword3'];
        }


        $info['addtime1'] = date('Y-m-d H:i:s',$info['addtime']);

  //  print_R($info);
        $day = strtotime(date('Y-m-d'));
        $is_sign = Db::name('sign_log')->where(['user_id'=>$user_id,'sign_time'=>$day])->count();
        $this->assign('is_sign',$is_sign);
        $this->assign('info',$info);
        $this->assign('member',$member);
        $this->assign('content',$contents);
        $this->assign('m','11');
        return $this->fetch();

    }


    public function index_notice()
    {
        $this->user_base();
        $id = input('param.id');
        $info = Db::name('notice')->where('id',$id)->find();
        $user_id = session('user_id');
        $member = Db::name('member')->field('id,group_id,nickname,head_img,account,integral')->where('id',$user_id)->find();
        $day = strtotime(date('Y-m-d'));
        $is_sign = Db::name('sign_log')->where(['user_id'=>$user_id,'sign_time'=>$day])->count();
        $this->assign('is_sign',$is_sign);
        $this->assign('info',$info);
        $this->assign('m','1');
        $this->assign('member',$member);
        return $this->fetch();

    }

    public function comment_list(){

        $article_id=input('param.id');
        $page=input('param.page');
        $a = ($page-1)*5;

        $map = array(
            'a.article_id'=>  $article_id,
            'a.status'=>  1,
        );

        $list = Db::name('article_comment')->alias('a')->field('a.user_id,.a.id,a.content,a.addtime,b.nickname,b.head_img')->join('shuren_member b','a.user_id = b.id')->where($map)->order('id desc')->limit($a,3)->select();
//      $flag = Db::name('article_comment')->insert($data);
        if($list){
            foreach($list as $key=>$item){
                $list[$key]['addtime'] = date('Y-m-d H:i:s',$item['addtime']);
            }

        }
        return json($list);
    }


    public function zan_status()
    {
        // return json(['code' => -1,'count'=>1, 'msg' => '点赞已取消']);
        $id=input('param.id');
        $user_id = session('user_id');

        if(empty($user_id)){
            return json(['code' => 11, 'msg' => '您尚未登录！']);
        }
        //$user_id = 14;
        $map = array(
            'user_id'=>$user_id,
            'article_id'=>$id,
        );
        $is_zan = Db::name('article_zan')->where($map)->find();
        if(!$is_zan){
            $is_zan['is_zan'] = 0;
        }
        if($is_zan['is_zan']==1)
        {
            Db::name('article_zan')->where('id',$is_zan['id'])->delete();
            $count = Db::name('article_zan')->where('article_id',$id)->count();
            return json(['code' => -1,'count'=>$count, 'msg' => '点赞已取消']);
        }else{
            $data = array(
                'user_id'=>$user_id,
                'article_id'=>$id,
                'is_zan'=>1,
            );
            $flag = Db::name('article_zan')->insert($data);
            $count = Db::name('article_zan')->where('article_id',$id)->count();
            return json(['code' => 1,'count'=>$count,  'msg' => '点赞成功']);
        }

    }

    public function collect_status()
    {

        $id=input('param.id');
        $user_id = session('user_id');
        if(empty($user_id)){
            return json(['code' => 11, 'msg' => '您尚未登录！']);
        }
        $map = array(
            'user_id'=>$user_id,
            'article_id'=>$id,
        );

        $is_zan = Db::name('article_collect')->where($map)->find();
        if(!$is_zan){
            $is_zan['is_collect'] = 0;
        }
        if($is_zan['is_collect']==1)
        {
            Db::name('article_collect')->where('id',$is_zan['id'])->delete();
            $count = Db::name('article_collect')->where('article_id',$id)->count();
            return json(['code' => -1,'count'=>$count, 'msg' => '收藏已取消']);
        }else{
            $data = array(
                'user_id'=>$user_id,
                'article_id'=>$id,
                'is_collect'=>1,
                'create_time'=>time()
            );
            Db::name('article_collect')->insert($data);
            $count = Db::name('article_collect')->where('article_id',$id)->count();
            return json(['code' => 1,'count'=>$count,  'msg' => '收藏成功']);
        }
    }

    public function article_comment(){

        $article_id=input('param.id');
        $content=input('param.content');
        $user_id = session('user_id');

        $data = array(
            'article_id'=>  $article_id,
            'user_id'=>  $user_id,
            'content'=>  $content,
            'addtime'=>time(),
            'status'=>1
        );

        $flag = Db::name('article_comment')->insert($data);
        if($flag){


            $member = Db::name('member')->where('id',$data['user_id'])->find();
            $data['addtime'] = date('Y-m-d H:i:s',$data['addtime']);
            if(!empty($member['head_img'])){
                $data['img'] = $member['head_img'];
            }else{
                $data['img'] = $member['head_img'];
            }
            $data['nickname'] = $member['nickname'];
            return json(['code' => 1, 'list'=>$data,   'msg' => '评论成功']);
        }else{
            return json(['code' => -1,  'msg' => '评论失败']);
        }
    }

    public function essay_zan_status()
    {
        // return json(['code' => -1,'count'=>1, 'msg' => '点赞已取消']);
        $id=input('param.id');
        $user_id = session('user_id');

        if(empty($user_id)){
            return json(['code' => 11, 'msg' => '您尚未登录！']);
        }
        //$user_id = 14;
        $map = array(
            'user_id'=>$user_id,
            'essay_id'=>$id,
        );
        $is_zan = Db::name('essay_zan')->where($map)->find();
        if(!$is_zan){
            $is_zan['is_zan'] = 0;
        }
        if($is_zan['is_zan']==1)
        {
            Db::name('essay_zan')->where('id',$is_zan['id'])->delete();
            $count = Db::name('essay_zan')->where('essay_id',$id)->count();
            return json(['code' => -1,'count'=>$count, 'msg' => '点赞已取消']);
        }else{
            $data = array(
                'user_id'=>$user_id,
                'essay_id'=>$id,
                'is_zan'=>1,
            );
            $flag = Db::name('essay_zan')->insert($data);
            $count = Db::name('essay_zan')->where('essay_id',$id)->count();
            return json(['code' => 1,'count'=>$count,  'msg' => '点赞成功']);
        }

    }

    public function debate_zan_status()
    {
        // return json(['code' => -1,'count'=>1, 'msg' => '点赞已取消']);
        $id=input('param.id');
        $user_id = session('user_id');

        if(empty($user_id)){
            return json(['code' => 11, 'msg' => '您尚未登录！']);
        }
        //$user_id = 14;
        $map = array(
            'user_id'=>$user_id,
            'debate_id'=>$id,
        );
        $is_zan = Db::name('debates_zan')->where($map)->find();
        if(!$is_zan){
            $is_zan['is_zan'] = 0;
        }
        if($is_zan['is_zan']==1)
        {
            Db::name('debates_zan')->where('id',$is_zan['id'])->delete();
            $count = Db::name('debates_zan')->where('debate_id',$id)->count();
            return json(['code' => -1,'count'=>$count, 'msg' => '点赞已取消']);
        }else{
            $data = array(
                'user_id'=>$user_id,
                'debate_id'=>$id,
                'is_zan'=>1,
            );
            $flag = Db::name('debates_zan')->insert($data);
            $count = Db::name('debates_zan')->where('debate_id',$id)->count();
            return json(['code' => 1,'count'=>$count,  'msg' => '点赞成功']);
        }

    }

    public function debate_collect_status()
    {

        $id=input('param.id');
        $user_id = session('user_id');
        if(empty($user_id)){
            return json(['code' => 11, 'msg' => '您尚未登录！']);
        }
        $map = array(
            'user_id'=>$user_id,
            'debate_id'=>$id,
        );
        $is_zan = Db::name('debate_collect')->where($map)->find();
        if(!$is_zan){
            $is_zan['is_collect'] = 0;
        }
        if($is_zan['is_collect']==1)
        {
            Db::name('debate_collect')->where('id',$is_zan['id'])->delete();
            $count = Db::name('debate_collect')->where('debate_id',$id)->count();
            return json(['code' => -1,'count'=>$count, 'msg' => '收藏已取消']);
        }else{
            $data = array(
                'user_id'=>$user_id,
                'debate_id'=>$id,
                'is_collect'=>1,
            );
            Db::name('debate_collect')->insert($data);
            $count = Db::name('debate_collect')->where('debate_id',$id)->count();
            return json(['code' => 1,'count'=>$count,  'msg' => '收藏成功']);
        }
    }


    public function essay_collect_status()
    {

        $id=input('param.id');
        $user_id = session('user_id');
        if(empty($user_id)){
            return json(['code' => 11, 'msg' => '您尚未登录！']);
        }
        $map = array(
            'user_id'=>$user_id,
            'essay_id'=>$id,
        );
        $is_zan = Db::name('essay_collect')->where($map)->find();
        if(!$is_zan){
            $is_zan['is_collect'] = 0;
        }
        if($is_zan['is_collect']==1)
        {
            Db::name('essay_collect')->where('id',$is_zan['id'])->delete();
            $count = Db::name('essay_collect')->where('essay_id',$id)->count();
            return json(['code' => -1,'count'=>$count, 'msg' => '收藏已取消']);
        }else{
            $data = array(
                'user_id'=>$user_id,
                'essay_id'=>$id,
                'is_collect'=>1,
            );
            Db::name('essay_collect')->insert($data);
            $count = Db::name('essay_collect')->where('essay_id',$id)->count();
            return json(['code' => 1,'count'=>$count,  'msg' => '收藏成功']);
        }
    }

    public function essay_join(){

        $article_id=input('param.article_id');
        $essay_id=input('param.essay_id');
        $user_id =input('param.user_id');

        $data = array(
            'article_id'=>  $article_id,
            'user_id'=>  $user_id,
            'essay_id'=>  $essay_id,

        );

        $is_part = Db::name('essay_article')->where($data)->find();
        if($is_part){
            return json(['code' => 3, 'list'=>$data,   'msg' => '此文章已参与本次征文！']);
        }
        //'add_time'=>time()
        $data['add_time'] = time();
        $flag = Db::name('essay_article')->insert($data);
        if($flag){
            $article = Db::name('article')->where('id',$data['article_id'])->find();
            $data['add_time'] = date('Y-m-d H:i:s',$data['add_time']);
            $data['id'] = $article['id'];
            $data['title'] = $article['title'];
            $data['photo'] = $article['photo'];
            $data['remark'] = $article['remark'];
            return json(['code' => 1, 'list'=>$data,   'msg' => '参与征文成功']);
        }else{
            return json(['code' => -1,  'msg' => '参与征文失败']);
        }
    }

    public function essay_article_list(){
        $essay_id=input('param.id');


        $page=input('param.page');

        if($page==-1){


            $map10 = array(
                'a.essay_id'=>$essay_id,
                'a.status'=>['<>',1],
            );

            $list = Db::name('essay_article')->alias('a')->field('a.id,b.remark,a.article_id,a.add_time,b.title,b.photo')->
            join('shuren_article b','a.article_id = b.id')->where($map10)->order('id desc')->select();
        }else{
            $a = ($page-1)*5;

            $map10 = array(
                'a.essay_id'=>$essay_id,
                'a.status'=>['<>',1],
            );

            $list = Db::name('essay_article')->alias('a')->field('a.id,b.remark,a.article_id,a.add_time,b.title,b.photo')->
            join('shuren_article b','a.article_id = b.id')->where($map10)->order('id desc')->limit($a,5)->select();
        }





        // $list = Db::name('essay_article')->alias('a')->field('a.id,a.essay_id,a.status,a.article_id,a.add_time,b.title,b.photo')->
        // join('shuren_article b','a.article_id = b.id')->where($map)->order('id desc')->limit($a,5)->select();
//      $flag = Db::name('article_comment')->insert($data);
        if($list){
            foreach($list as $key=>$item){

                $map['article_id'] = $item['article_id'];

                $count = Db::name('article_edit')->where($map)->count();
                $list[$key]['count'] = $count;
                if($count>0){
                    $article_eidt_info = Db::name('article_edit')->where($map)->find();
                    $list[$key]['article_eidt_id'] = $article_eidt_info['id'];
                    $list[$key]['article_eidt_title'] = $article_eidt_info['title'];
                    $list[$key]['update_time'] = date('m-d H:i',$article_eidt_info['update_time']);
                }



                $list[$key]['add_time'] = date('m-d H:i',$item['add_time']);
            }
            return json(['code' => 1, 'list'=>$list,  'msg' => 'ok']);
        }
    }

    public function debate_list_zan_status()
    {
        // return json(['code' => -1,'count'=>1, 'msg' => '点赞已取消']);
        $id=input('param.id');
        $user_id = session('user_id');


        if(empty($user_id)){
            return json(['code' => 11, 'msg' => '您尚未登录！']);
        }
        //$user_id = 14;
        $map = array(
            'user_id'=>$user_id,
            'debate_list_id'=>$id,
        );
        $is_zan = Db::name('debate_zan')->where($map)->find();
        if(!$is_zan){
            $is_zan['is_zan'] = 0;
        }
        if($is_zan['is_zan']==1)
        {
            Db::name('debate_zan')->where('id',$is_zan['id'])->delete();
            $count = Db::name('debate_zan')->where('debate_list_id',$id)->count();
            return json(['code' => -1,'count'=>$count,  'msg' => '取消点赞']);
        }else{
            $data = array(
                'user_id'=>$user_id,
                'debate_list_id'=>$id,
                'is_zan'=>1,
            );
            $flag = Db::name('debate_zan')->insert($data);
            $count = Db::name('debate_zan')->where('debate_list_id',$id)->count();
            return json(['code' => 1,'count'=>$count,  'msg' => '点赞成功']);

        }

    }


    public function debate_list(){
        $article_id=input('param.id');
        $page=input('param.page');
        $a = ($page-1)*5;

        $user_id = session('user_id');
        $map10 = array(
            'a.debate_id'=>$article_id,
            'a.status'=>1,
        );
        $list =  Db::name('debate_list')->alias('a')->field('a.*,b.nickname,b.head_img')->join('shuren_member b','a.user_id = b.id')->where($map10)->order('id desc')->limit($a,5)->select();


        if($list){
            foreach($list as $key=>$item){
                $list[$key]['add_time'] = date('m-d H:i',$item['add_time']);
                if($item['type']==1){
                    $list[$key]['note'] = "正方";
                }elseif($item['type']==2){
                    $list[$key]['note'] = "反方";
                }else{
                    $list[$key]['note'] = "";
                }
                $map['debate_list_id'] = $item['id'];
                $map['is_zan'] = 1;
                $zan_count = Db::name('debate_zan')->where($map)->count();
                $map1['debate_list_id'] = $item['id'];
                $map1['status'] = 1;
                $comment_count = Db::name('debate_comment')->where($map1)->count();
                $list[$key]['zan_count'] = $zan_count;
                $list[$key]['comment_count'] = $comment_count;

                $map2['user_id'] = $user_id;
                $map2['debate_list_id'] = $item['id'];
                $my_zan = Db::name('debate_zan')->where($map2)->count();
                $list[$key]['is_zan'] = $my_zan;

            }
            return json(['code' => 1, 'list'=>$list,  'msg' => 'ok']);
        }
    }


    public function debate_comment(){

        $article_id=input('param.id');
        $content=input('param.content');
        $type=input('param.type');
        $user_id = session('user_id');
        if(empty($user_id)){
            return json(['code' => 11, 'msg' => '您尚未登录！']);
        }
        $data = array(
            'debate_id'=>  $article_id,
            'user_id'=>  $user_id,
            'content'=>  $content,
            'type'=>  $type,
            'status'=> 1,
            'add_time'=>time()
        );
        $flag = Db::name('debate_list')->insert($data);
        if($flag){
            $member = Db::name('member')->where('id',$data['user_id'])->find();
            $data['add_time'] = date('Y-m-d H:i:s',$data['add_time']);
            if(!empty($member['head_img'])){
                $data['img'] = "/uploads/face/".$member['head_img'];
            }else{
                $data['img'] = $member['head_img'];
            }
            $data['nickname'] = $member['nickname'];
            $data['id'] = $flag;
            return json(['code' => 1, 'list'=>$data,   'msg' => '评论成功']);
        }else{
            return json(['code' => -1,  'msg' => '评论失败']);
        }
    }



    public function user_info()
    {

       $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }

//        $param = Array (
//            'nickname' => '顾燕红',
//            'desc' => '我怀念的是无话不说,我怀念的是绝对炙热，我怀念的！！！！！！！sdfsdfsdf！！ssss1111',
//            'head_img' => '20170511/97428272b79a36a7c2ac8b963184c78c.png',
//            'id' => 212082 );
//

        if(request()->isAjax()){
            $param = input('post.');
//            logResult(json_encode($param));
            $member1 = Db::name('member')->field('group_id')->where('id',$param['id'])->find();
            if($member1['group_id']==8){
                unset($param['nickname']);
            }
            if(!empty($param['password'])){
                $param['password'] = md5(md5($param['password']) . config('auth_key'));
            }else{
                unset( $param['password']);
            }
            unset($param['file']);
            $result = Db::name('member')->where('id',$param['id'])->update($param);
            if($result){
            return json(['code' => 1,  'msg' =>'用户信息编辑成功'] );

            }else{
                return json(['code' => 1,  'msg' =>'用户信息编辑成功'] );
            }
        }



        $member = Db::name('member')->field('id,realname,peopleid,group_id,nickname,head_img,account,desc,id,integral')->where('id',$user_id)->find();
        $day = strtotime(date('Y-m-d'));
        $is_sign = Db::name('sign_log')->where(['user_id'=>$user_id,'sign_time'=>$day])->count();
        $this->assign('is_sign',$is_sign);
        $this->assign('is_login',1);
        $this->assign('m','8');
        $this->assign('user_id',$user_id);
        $this->assign('member',$member);

        return $this->fetch();

    }


    public function write_essay()
    {
        $this->user_base();
        $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }

        $member = Db::name('member')->field('id,group_id,nickname,head_img,account,integral')->where('id',$user_id)->find();

        if(request()->isAjax()){


            $param = input('post.');
          //  $article = new ArticleModel();
            $param['instit_id'] = session('instit_id');
            $param['user_id'] = session('user_id');
            $param['end_time'] = strtotime($param['end_time'])+3600*24-1;
          //  $flag = $article->insertArticle($param);
            $article = new EssayModel();
            $flag = $article->insertArticle($param);

            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }


        $id = input('param.id');
        if($id){

            $article = new ArticleModel();
            $info = $article->getOneArticle($id);
        }else{
            $info = array(
                'title'=>'',
                'remark'=>'',
                'id'=>'',
                'content'=>'',
                'photo'=>'',
                'cate_id'=>'',
            );
        }

        $cate = new ArticleCateModel();
        $this->assign('cate',$cate->getAllCate(0));
        $day = strtotime(date('Y-m-d'));
        $is_sign = Db::name('sign_log')->where(['user_id'=>$user_id,'sign_time'=>$day])->count();
        $this->assign('is_sign',$is_sign);
        $this->assign('info',$info);
        $this->assign('member',$member);
        $this->assign('m','4');
        return $this->fetch();

    }



    public function temp_add1(){


        $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }
        $member = Db::name('member')->field('id,group_id,nickname,head_img,account,integral')->where('id',$user_id)->find();

        if(request()->isAjax()){
            $param = input('post.');
            $res['temp'] = $param['temp'];
            $rv['first'] = $param['first'];
            $rv['keyword1'] = $param['keyword1'];
            $rv['keyword2'] = $param['keyword2'];
            $rv['keyword3'] = $param['keyword3'];
            $rv['remark'] = $param['remark'];
            $res['is_url'] = $param['is_url'];
            $res['from_username'] = $member['account'];
            $res['instit_id'] = session('instit_id');
            $res['addtime'] = time();
//            if($param['temp']=='XXXXX'){
//                $res['type'] =2 ;
//            }else
                if($param['temp']=='员工通知'){
                $res['type'] =3;
            }
            $res['content'] = json_encode($rv);
            $nowyear = date('Y');
            $nowtime = strtotime($nowyear.'-08-25');
            if($nowtime>time()){
                $year = $nowyear-1;
            }else{
                $year = $nowyear;
            }
            $res['year'] = $year;
            $flag =   Db::name('temp_message')->insert($res);
            $maps = array(
                'a.instit_id'=>session('instit_id'),
                'm.open_id'=>['neq',''],
//                'a.year'=>$year,
            );

//            if($param['temp']=='XXXXX'){
//                $data['template_id']= 'g2_gHgAVftHaPCTyCOz9LdU86eiXAFu1Cw19XgZOA3k';
//                $list = Db::name('class_student')->alias('a')->field('m.open_id')->join('member m','a.account=m.account')->where($maps)->select();
//            }else
                if($param['temp']=='员工通知'){
                $list = Db::name('teach')->alias('a')->field('m.id,m.nickname,m.open_id')->join('member m','a.mobile=m.account')->where($maps)->select();
                $data['template_id']= 'OJp_jc7_2XCJhlwYf20cXKXFbSdnrz4cXG9yNwBVJVY';
            }





            if($flag){
                foreach($list as $k => $v){
                    $data['touser']		= $v['open_id'];
                    // $data['url']		= "http://".$_SERVER["HTTP_HOST"]."/index.php?m=default&c=user&a=order_tracking&order_id=".$order_id;
                    $data['topcolor']	= '#7B68EE';
                    $data['data']['first']['value']	= $param['first'];
                    $data['data']['first']['color']	= '#743A3A';
                    $data['data']['keyword1']['value']	=$param['keyword1'];
                    $data['data']['keyword1']['color']	= '#666666';
                    $data['data']['keyword2']['value']	= $param['keyword2'];
                    $data['data']['keyword2']['color']	=  '#666666';
                    $data['data']['keyword3']['value']	= $param['keyword3'];
                   $data['data']['keyword3']['color']	=  '#666666';
//                    $data['data']['keyword4']['value']	= $param['keyword4'];
//                    $data['data']['keyword4']['color']	=  '#666666';
                    $data['data']['remark']['value']	=  $param['remark'];
                    $data['data']['remark']['color']	= '#3300CC';

                    $result = send_temp_message(session('instit_id'),$data);
                }
                return json(['code' => 1,  'msg' => '模板消息发送成功']);
            }else{
                return json(['code' => -1,  'msg' => '模板消息发送失败']);
            }
        }

        $this->assign('member',$member);
        $this->assign('m','11');
        $day = strtotime(date('Y-m-d'));
        $is_sign = Db::name('sign_log')->where(['user_id'=>$user_id,'sign_time'=>$day])->count();
        $this->assign('is_sign',$is_sign);
        return $this->fetch();
    }


    public function temp_add(){


        $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }
        $member = Db::name('member')->field('id,group_id,nickname,head_img,account,integral')->where('id',$user_id)->find();

        $map2 = array(
            'mobile'=>$member['account'],
            'instit_id'=>session('instit_id'),
            'is_leader'=>1
        );
        $teach =  DB::name('teach')->field('id')->where($map2)->find();
        if($teach){
            $map3 = array(
                'leader'=>$teach['id'],
                'instit_id'=>session('instit_id'),
                'status'=>1
            );
            $banji_info = DB::name('theclass')->where($map3)->find();
        }

        if(request()->isAjax()){

            $param = input('post.');
            $res['temp'] = $param['temp'];
            $res['class_id'] = $param['class_id'];
            $rv['first'] = $param['first'];
            $rv['keyword1'] = $param['keyword1'];
            $rv['keyword2'] = $param['keyword2'];
            $rv['keyword3'] = $param['keyword3'];
            $rv['keyword4'] = $param['keyword4'];
            $rv['remark'] = $param['remark'];
            $res['is_url'] = $param['is_url'];
            $res['from_username'] = $member['account'];
            $res['instit_id'] = session('instit_id');
            $res['addtime'] = time();
            if($param['temp']=='作业提醒'){
                $res['type'] =1 ;
            }else if($param['temp']=='班级通知'){
                $res['type'] =1;
            }
            $res['content'] = json_encode($rv);
            $nowyear = date('Y');
            $nowtime = strtotime($nowyear.'-08-25');
            if($nowtime>time()){
                $year = $nowyear-1;
            }else{
                $year = $nowyear;
            }
            $res['year'] = $year;
            $flag =   Db::name('temp_message')->insert($res);
            $maps = array(
                'a.class_id'=>$banji_info['id'],
                'a.instit_id'=>session('instit_id'),
                'm.open_id'=>['neq',''],
                'a.year'=>$year,
            );
            $list = Db::name('class_student')->alias('a')->field('m.open_id')->join('member m','a.account=m.account')->where($maps)->select();
            if($flag){
                foreach($list as $k => $v){
                    $data['touser']		= $v['open_id'];
                    if($param['temp']=='作业提醒'){
                        $data['template_id']= 'CrrHGLdX1Wg3KSBvWYkDu1dgDLw6rpY4ykkCnJk81BA';
                    }else if($param['temp']=='班级通知'){
                        $data['template_id']= 'g2_gHgAVftHaPCTyCOz9LdU86eiXAFu1Cw19XgZOA3k';
                    }
                    // $data['url']		= "http://".$_SERVER["HTTP_HOST"]."/index.php?m=default&c=user&a=order_tracking&order_id=".$order_id;
                    $data['topcolor']	= '#7B68EE';
                    $data['data']['first']['value']	= $param['first'];
                    $data['data']['first']['color']	= '#743A3A';
                    $data['data']['keyword1']['value']	=$param['keyword1'];
                    $data['data']['keyword1']['color']	= '#666666';
                    $data['data']['keyword2']['value']	= $param['keyword2'];
                    $data['data']['keyword2']['color']	=  '#666666';
                    $data['data']['keyword3']['value']	= $param['keyword3'];
                    $data['data']['keyword3']['color']	=  '#666666';
                    $data['data']['keyword4']['value']	= $param['keyword4'];
                    $data['data']['keyword4']['color']	=  '#666666';
                    $data['data']['remark']['value']	=  $param['remark'];
                    $data['data']['remark']['color']	= '#3300CC';
                    $result = send_temp_message(session('instit_id'),$data);
                }
                return json(['code' => 1,  'msg' => '模板消息发送成功']);
            }else{
                return json(['code' => -1,  'msg' => '模板消息发送失败']);
            }
        }
        $day = strtotime(date('Y-m-d'));
        $is_sign = Db::name('sign_log')->where(['user_id'=>$user_id,'sign_time'=>$day])->count();
        $this->assign('is_sign',$is_sign);
        $this->assign('member',$member);
        $this->assign('m','11');
        $this->assign('class_id',$banji_info['id']);
        return $this->fetch();
    }


    public function user_index(){
        $this->user_base();

        $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }
        $member = Db::name('member')->field('id,group_id,nickname,head_img,account,create_time,integral')->where('id',$user_id)->find();


        $info = get_user_article_count($user_id);

        $Nowpage = input('param.page') ? input('param.page'):1;
        $limits = 10;// 获取总条数


        $map['addtime']  = ['gt',$member['create_time']];

        $count = Db::name('notice')->where($map)->count();//计()算总页面

        $allpage = intval(ceil($count / $limits));

        if(input('param.page')) {
            $notice_list = Db::name('notice')->where($map)->page($Nowpage,$limits)->order('id desc')->select();
            foreach($notice_list as $key=>$val){
                $notice_list[$key]['addtime'] = date('Y-m-d',$val['addtime']);
            }
            return json(['code' => 1, 'list'=>$notice_list,  'msg' => 'ok']);
        }
        $day = strtotime(date('Y-m-d'));
        $is_sign = Db::name('sign_log')->where(['user_id'=>$user_id,'sign_time'=>$day])->count();



        $this->assign('is_sign',$is_sign);

        $this->assign('info',$info);
        $this->assign('m','1');
        $this->assign('member',$member);
        $this->assign('user_id',$user_id);
        $this->assign('allpage',$allpage);
        return $this->fetch();
    }


    public  function tongji(){
        $this->user_base();
        if(input('param.page')) {
            $day = input('param.day');
            if (!empty($day)) {
                if ($day == 7) {
                    $end = date('Y-m-d');
                    $start = date('Y-m-d', time() - 6 * 3600 * 24);
                }
                if ($day == 15) {
                    $end = date('Y-m-d');
                    $start = date('Y-m-d', time() - 14 * 3600 * 24);
                }

                if ($day == 30) {
                    $end = date('Y-m-d');
                    $start = date('Y-m-d', time() - 29 * 3600 * 24);
                }

            } else {
                $start = input('param.start');
                $end = input('param.end');
            }
            $type = input('param.type');
            $kaishi = strtotime($start);
            $jieshu = strtotime($end);
            $arr = array();
            $arr1 = array();
            $arr2 = array();

            $k = 0;
            $cqs = '';
            for ($i = $kaishi; $i <= $jieshu; $i = $i + 86400) {
                $starttime = $i;
                $endtime = $i + 86400;
                $user_id = session('user_id');
                if ($type == 1) {
                    //统计发稿数量
                    $map['user_id'] = $user_id;
                    $map['status'] = 1;
                    $map1['create_time'] = ['gt', $starttime];
                    $map['create_time'] = ['lt', $endtime];
                    $article_count = Db::name('article')->where($map)->where($map1)->count();
                    $arr[$k]['dates'] = date('Y-m-d', $i);
                    $arr[$k]['count'] = $article_count;
                    $arr1[$k] = $article_count;
                    $arr2[$k] = date('Y-m-d', $i);
                }

                if ($type == 2) {
                    //统计发的话题数
                    $map2['user_id'] = $user_id;
                    $map3['create_time'] = ['gt', $starttime];
                    $map2['create_time'] = ['lt', $endtime];
                    $debate_count = Db::name('debate')->where($map2)->where($map3)->count();
                    $arr[$k]['dates'] = date('Y-m-d', $i);
                    $arr[$k]['count'] = $debate_count;
                    $arr1[$k] = $debate_count;
                    $arr2[$k] = date('Y-m-d', $i);
                }

                if ($type == 3) {
                    //新增粉丝数
                    $map4['fans_id'] = $user_id;
                    $map5['create_time'] = ['gt', $starttime];
                    $map4['create_time'] = ['lt', $endtime];
                    $fans_count = Db::name('fans')->where($map4)->where($map5)->count();
                    $arr[$k]['dates'] = date('Y-m-d', $i);
                    $arr[$k]['count'] = $fans_count;
                    $arr1[$k] = $fans_count;
                    $arr2[$k] = date('Y-m-d', $i);
                }

                if ($type == 4) {
                    //收藏数
                    $map6['b.user_id'] = $user_id;
                    $map7['a.create_time'] = ['gt', $starttime];
                    $map6['a.create_time'] = ['lt', $endtime];
                    $collect_count = Db::name('article_collect')->alias('a')->join('article b', 'a.article_id=b.id')->where($map6)->where($map7)->count();
                    $arr[$k]['dates'] = date('Y-m-d', $i);
                    $arr[$k]['count'] = $collect_count;
                    $arr1[$k] = $collect_count;
                    $arr2[$k] = date('Y-m-d', $i);
                }
                $k++;
            }
            $cqs = substr($cqs,0,-1);

           // print_R($arr1);



            return json(['code' => 1, 'list' => $arr, 'start' => $start, 'end' => $end,'cqs'=>$arr1,'riqi'=>$arr2]);
        }



        $member = Db::name('member')->field('id,group_id,nickname,head_img,account,create_time,integral')->where('id',session('user_id'))->find();

        $day = strtotime(date('Y-m-d'));
        $is_sign = Db::name('sign_log')->where(['user_id'=>session('user_id'),'sign_time'=>$day])->count();
        $this->assign('is_sign',$is_sign);
        $info = get_user_article_count(session('user_id'));
        $this->assign('info',$info);
        $this->assign('m','15');
        $this->assign('member',$member);
        return $this->fetch();

    }


    public  function danpian(){
        $this->user_base();
        if(input('param.page')) {
            $day = input('param.day');
            if (!empty($day)) {
                if ($day == 7) {
                    $end = date('Y-m-d');
                    $start = date('Y-m-d', time() - 6 * 3600 * 24);
                }
                if ($day == 15) {
                    $end = date('Y-m-d');
                    $start = date('Y-m-d', time() - 14 * 3600 * 24);
                }

                if ($day == 30) {
                    $end = date('Y-m-d');
                    $start = date('Y-m-d', time() - 29 * 3600 * 24);
                }

            } else {
                $start = input('param.start');
                $end = input('param.end');
            }

            $kaishi = strtotime($start);
            $jieshu = strtotime($end);


            $map['user_id'] = session('user_id');
            $map['status'] = 1;
            $map1['create_time'] = ['gt', $kaishi];
            $map['create_time'] = ['lt', $jieshu];


            $article = Db::name('article')->field('id,title,create_time,views')->where($map)->where($map1)->select();


            foreach($article as $key=>$val){
                $ma['article_id'] = $val['id'];
                $pl_count = Db::name('article_comment')->where($ma)->count();
                $article[$key]['comment'] = $pl_count;
                $article[$key]['create_time1'] = date('Y-m-d H:i:s',$val['create_time']);

            }

            return json(['code' => 1, 'list' => $article, 'start' => $start, 'end' => $end]);

        }


            $member = Db::name('member')->field('id,group_id,nickname,head_img,account,create_time,integral')->where('id',session('user_id'))->find();
            $info = get_user_article_count(session('user_id'));
            $day = strtotime(date('Y-m-d'));
            $is_sign = Db::name('sign_log')->where(['user_id'=>session('user_id'),'sign_time'=>$day])->count();
            $this->assign('is_sign',$is_sign);
            $this->assign('info',$info);
            $this->assign('m','16');
            $this->assign('member',$member);
            return $this->fetch();

//            for ($i = $kaishi; $i <= $jieshu; $i = $i + 86400) {
//                $starttime = $i;
//                $endtime = $i + 86400;
//                $user_id = session('user_id');
//                if ($type == 1) {
//                    //统计发稿数量
//                    $map['user_id'] = $user_id;
//                    $map['status'] = 1;
//                    $map1['create_time'] = ['gt', $starttime];
//                    $map['create_time'] = ['lt', $endtime];
//                    $article_count = Db::name('article')->where($map)->where($map1)->count();
//                    $arr[$k]['dates'] = date('Y-m-d', $i);
//                    $arr[$k]['count'] = $article_count;
//                    $arr1[$k] = $article_count;
//                }
//
//                if ($type == 2) {
//                    //统计发的话题数
//                    $map2['user_id'] = $user_id;
//                    $map3['create_time'] = ['gt', $starttime];
//                    $map2['create_time'] = ['lt', $endtime];
//                    $debate_count = Db::name('debate')->where($map2)->where($map3)->count();
//                    $arr[$k]['dates'] = date('Y-m-d', $i);
//                    $arr[$k]['count'] = $debate_count;
//                    $arr1[$k] = $debate_count;
//                }
//
//                if ($type == 3) {
//                    //新增粉丝数
//                    $map4['fans_id'] = $user_id;
//                    $map5['create_time'] = ['gt', $starttime];
//                    $map4['create_time'] = ['lt', $endtime];
//                    $fans_count = Db::name('fans')->where($map4)->where($map5)->count();
//                    $arr[$k]['dates'] = date('Y-m-d', $i);
//                    $arr[$k]['count'] = $fans_count;
//                    $arr1[$k] = $fans_count;
//                }
//
//                if ($type == 4) {
//                    //收藏数
//                    $map6['b.user_id'] = $user_id;
//                    $map7['a.create_time'] = ['gt', $starttime];
//                    $map6['a.create_time'] = ['lt', $endtime];
//                    $collect_count = Db::name('article_collect')->alias('a')->join('article b', 'a.article_id=b.id')->where($map6)->where($map7)->count();
//                    $arr[$k]['dates'] = date('Y-m-d', $i);
//                    $arr[$k]['count'] = $collect_count;
//                    $arr1[$k] = $collect_count;
//                }
//                $k++;
//            }
//            $cqs = substr($cqs,0,-1);
//            return json(['code' => 1, 'list' => $arr, 'start' => $start, 'end' => $end,'cqs'=>$arr1]);
//        }


    }


    public function user_column()
    {

        $this->user_base();
        $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }

        $member = Db::name('member')->field('id,group_id,nickname,head_img,account,integral')->where('id',$user_id)->find();
        $map1['user_id'] = $user_id;

        $Nowpage = input('param.page') ? input('param.page'):1;
        $limits = 10;// 获取总条数
        $count = Db::name('user_column')->where($map1)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));

        $list = Db::name('user_column')->where($map1)->page($Nowpage,$limits)->order('id desc')->select();
        foreach($list as $key=>$val){
            $list[$key]['sort'] = $key+1;
        }
        $day = strtotime(date('Y-m-d'));
        $is_sign = Db::name('sign_log')->where(['user_id'=>$user_id,'sign_time'=>$day])->count();
        $this->assign('is_sign',$is_sign);
        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('m','18');
        $this->assign('member',$member);
        $this->assign('lists',$list);
        return $this->fetch();
    }


    public function add_column()
    {

        $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }

        $member = Db::name('member')->field('id,group_id,nickname,head_img,account,integral')->where('id',$user_id)->find();
        $day = strtotime(date('Y-m-d'));
        $is_sign = Db::name('sign_log')->where(['user_id'=>$user_id,'sign_time'=>$day])->count();
        if(request()->isAjax()){
            $param = input('post.');
            $count =  DB::name('user_column')->where('user_id',$user_id)->count();

            if($count>=5){
              //  return json(['code' => -1,  'msg' =>'每人最多添加5个菜单']);
            }

            $param['user_id'] = $user_id;
            $new_id = DB::name('user_column')->insertGetId($param);
            if($new_id){
                return json(['code' => 1,  'msg' =>'新增栏目成功','new_id'=>$new_id]);
            }else{
                return json(['code' => -1,  'msg' =>'新增栏目失败']);
            }
        }

        $this->assign('member',$member);
        $this->assign('m','18');
        $this->assign('is_sign',$is_sign);
        return $this->fetch();
    }


    public function edit_column()
    {

        $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }

        $member = Db::name('member')->field('id,group_id,nickname,head_img,account,integral')->where('id',$user_id)->find();
        $day = strtotime(date('Y-m-d'));
        $is_sign = Db::name('sign_log')->where(['user_id'=>$user_id,'sign_time'=>$day])->count();
        if(request()->isAjax()){
            $param = input('post.');

            $new_id = DB::name('user_column')->where('id',$param['id'])->update($param);
            if($new_id){
                return json(['code' => 1,  'msg' =>'修改栏目成功']);
            }else{
                return json(['code' => -1,  'msg' =>'修改栏目失败']);
            }
        }

        $id= input('param.id');
        $map = array(
            'user_id'=>$user_id,
            'id'=>$id
        );
        $column = Db::name('user_column')->where($map)->find();


        $this->assign('member',$member);
        $this->assign('m','18');
        $this->assign('is_sign',$is_sign);
        $this->assign('column',$column);
        return $this->fetch();
    }

    public function del_column()
    {

        $id = input('param.id');
        $count = Db::name('article')->where('column_id',$id)->count();
        if($count>0){
            return json(['code' =>-1,  'msg' =>'该文章栏目有文章存在，无法删除']);
        }

        $id= Db::name('user_column')->where('id',$id)->delete();

        return json(['code' =>1,'msg'=>'删除成功']);
    }

    public function picture()
    {
        $this->user_base();
        $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }
        $member = Db::name('member')->field('id,group_id,nickname,head_img,account,integral')->where('id',$user_id)->find();
        $map1['user_id'] = $user_id;
        $Nowpage = input('param.page') ? input('param.page'):1;
        $limits = 10;// 获取总条数
        $count = Db::name('user_picture')->where($map1)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));

        if(input('param.page'))
        {
            $list = Db::name('user_picture')->where($map1)->page($Nowpage,$limits)->order('id desc')->select();

            return json(['code' => 1, 'list'=>$list,  'msg' => 'ok']);
        }
        $day = strtotime(date('Y-m-d'));
        $is_sign = Db::name('sign_log')->where(['user_id'=>$user_id,'sign_time'=>$day])->count();
        $this->assign('is_sign',$is_sign);
        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('m','19');
        $this->assign('member',$member);
        return $this->fetch();
    }


    public function add_picture()
    {

        $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }
        if(request()->isAjax()){
            $param = input('post.');
            $param['user_id'] = $user_id;
            unset($param['file']);
            $new_id = DB::name('user_picture')->insertGetId($param);

            if($new_id){
                return json(['code' => 1,  'msg' =>'新增图片成功']);
            }else{
                return json(['code' => -1,  'msg' =>'新增图片失败']);
            }
        }
        $member = Db::name('member')->field('id,group_id,nickname,head_img,account,integral')->where('id',$user_id)->find();
        $day = strtotime(date('Y-m-d'));
        $is_sign = Db::name('sign_log')->where(['user_id'=>$user_id,'sign_time'=>$day])->count();
        $this->assign('member',$member);
        $this->assign('m','19');
        $this->assign('is_sign',$is_sign);
        return $this->fetch();
    }


    public function edit_picture()
    {

        $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }

        $member = Db::name('member')->field('id,group_id,nickname,head_img,account,integral')->where('id',$user_id)->find();
        $day = strtotime(date('Y-m-d'));
        $is_sign = Db::name('sign_log')->where(['user_id'=>$user_id,'sign_time'=>$day])->count();
        if(request()->isAjax()){
            $param = input('post.');
            unset($param['file']);
            $new_id = DB::name('user_picture')->where('id',$param['id'])->update($param);
            if($new_id){
                return json(['code' => 1,  'msg' =>'修改图片成功']);
            }else{
                return json(['code' => -1,  'msg' =>'修改图片失败']);
            }
        }

        $id= input('param.id');
        $map = array(
            'user_id'=>$user_id,
            'id'=>$id
        );
        $picture = Db::name('user_picture')->where($map)->find();
        $this->assign('member',$member);
        $this->assign('m','19');
        $this->assign('is_sign',$is_sign);
        $this->assign('picture',$picture);
        return $this->fetch();
    }


    public function del_picture()
    {

        $id = input('param.id');
        $info = Db::name('user_picture')->where('id',$id)->find();
        $id= Db::name('user_picture')->where('id',$id)->delete();
        unlink(ROOT_PATH . 'public_html' . DS . 'uploads/userimage/'.$info['path']);
        return json(['code' =>1,'msg'=>'删除成功']);
    }



    public function nav_manager()
    {
        $this->user_base();
        $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }
        $member = Db::name('member')->field('id,group_id,nickname,head_img,account,integral')->where('id',$user_id)->find();
        $day = strtotime(date('Y-m-d'));
        $is_sign = Db::name('sign_log')->where(['user_id'=>$user_id,'sign_time'=>$day])->count();
        $this->assign('is_sign',$is_sign);
        $this->assign('m','20');
        $this->assign('member',$member);
        return $this->fetch();
    }


    public function edit_nav()
    {

        $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }
        $type = input('param.type');
        $member = Db::name('member')->field('id,group_id,nickname,head_img,account,integral')->where('id',$user_id)->find();
        $day = strtotime(date('Y-m-d'));
        $is_sign = Db::name('sign_log')->where(['user_id'=>$user_id,'sign_time'=>$day])->count();
        $info = Db::name('user_nav')->where('user_id',$user_id)->find();



        $user_column = Db::name('user_column')->where('user_id',$user_id)->select();


        if(request()->isAjax()){
            $param = input('post.');
            $data = array();
            if($param['type']==1){
                $data['left'] = $param['content'];
            }
             if($param['type']==2){
                $data['right'] = $param['content'];
             }
            if($param['type']==3){
                $data['top'] = $param['content'];
            }

            if($param['type']==4){
                $data['center'] = $param['content'];
            }

            if($param['type']==5){
                $data['jigou'] = $param['content'];
            }

            if($param['type']==6){
                $data['tuijian1_img'] = $param['img'];
                $data['tuijian1'] = $param['title'];
            }
            if($param['type']==7){
                $data['tuijian2_img'] = $param['img'];
                $data['tuijian2'] = $param['title'];
            }
            if($param['type']==8){
                $data['tuijian3_img'] = $param['img'];
                $data['tuijian3'] = $param['title'];
            }

            if($param['type']==9){
                $data['article_num'] = $param['content'];
            }

            if($param['type']==10){
                            $data['banben'] = $param['content'];
                        }


            if($info){
                $data['id'] = $info['id'];
                $new_id = DB::name('user_nav')->where('id',$data['id'])->update($data);
                if($new_id){
                    return json(['code' => 1,  'msg' =>'信息修改成功']);
                }else{
                    return json(['code' => -1,  'msg' =>'信息修改失败']);
                }
            }else{

                $data['user_id'] = $user_id;
                $new_id = DB::name('user_nav')->insertGetId($data);
                if($new_id){
                    return json(['code' => 1,  'msg' =>'信息修改成功']);
                }else{
                    return json(['code' => -1,  'msg' =>'信息修改失败']);
                }
            }
        }


        if($type==1){
            $info['content'] = $info['left'];
        }
        if($type==2){
            $info['content'] = $info['right'];
        }
        if($type==3){
            $info['content'] = $info['top'];
        }

        if($type==4){
            $info['content'] = $info['center'];
        }
        if($type==5){
            $info['content'] = $info['jigou'];
        }
        $id= input('param.id');
        $map = array(
            'user_id'=>$user_id,
            'id'=>$id
        );
        $picture = Db::name('user_picture')->where($map)->find();
        $this->assign('member',$member);
        $this->assign('user_column',$user_column);
        $this->assign('type',$type);
        $this->assign('info',$info);
        $this->assign('m','20');
        $this->assign('is_sign',$is_sign);
        $this->assign('info',$info);

        $this->assign('picture',$picture);
        return $this->fetch();
    }

    public function user_base(){
        $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }

        $user_info = Db::name('member')->where('id',$user_id)->find();
        if($user_info['group_id']!=8){
            if(empty($user_info['nickname'])||empty($user_info['desc'])||empty($user_info['realname'])||empty($user_info['head_img'])){
              //  $this->redirect('article/user_info');
            }
        }

    }


    public function baomingajax()
    {


        if (request()->isAjax()) {

            $user_id = session('user_id');
            if(empty($user_id)){
                return json(['code'=>-11,'msg'=>'请先登录']);
            }

            $param = input('post.');
            $vote = Db::name('baoming')->where('id',$param['baoming_id'])->find();
            $xiangmu = explode('|',$vote['desc']);
            $arr = array();
            foreach($xiangmu as $k=>$v){
                $c = $k;
            }
            for($i=0;$i<=$c;$i++){
                $cv = 'tt'.$i;
                $arr[$i]['content'] = $param[$cv];
            }


            $data1 = array(
                'user_id'=>$user_id,
                'baoming_id'=>$param['baoming_id'],
                'desc'=>json_encode($arr)
            );

            $cdsd = Db::name('baoming_log')->where($data1)->find();
            if($cdsd){
                return json(['code'=>-1,'msg'=>'您已成功报名，请勿重复报名！']);
            }

            $data = array(
                'user_id'=>$user_id,
                'addtime'=>time(),
                'baoming_id'=>$param['baoming_id'],
                'desc'=>json_encode($arr)
            );
            Db::name('baoming_log')->insert($data);
            return json(['code'=>1,'msg'=>'报名成功！']);

        }




    }

    public function real_vote()
    {

        $user_id = session('user_id');
        if(empty($user_id)){
            return json(['code'=>-11,'msg'=>'请先登录']);
        }

        $id = input('param.id');
        $choose = input('param.choose');

        $vote = Db::name('vote')->where('id',$id)->find();
        if(!empty($vote['starttime'])){
            if($vote['starttime']>time()){
                return json(['code'=>-1,'msg'=>'投票活动尚未开始!']);
            }
        }

        if(!empty($vote['endtime'])){
            if($vote['endtime']<time()){
                return json(['code'=>-1,'msg'=>'投票活动已经结束!']);
            }
        }
        $map = array(
            'user_id'=>session('user_id'),
            'vote_id'=>$id,
        );
        if($vote['cycle']==1){
            $starttime = strtotime(date('Y-m-d'));
            $endtime = $starttime+3600*24-1;
            $map['addtime'] = ['>=',$starttime];
            $map1['addtime'] = ['<=',$endtime];
            $count = Db::name('vote_log')->where($map)->where($map1)->count();
            $msg = '今天已投过票了!';
        }

        if($vote['cycle']==2){
            $count = Db::name('vote_log')->where($map)->count();
            $msg = '您已参与过本次投票!';
        }

        if($count>=1){
            return json(['code'=>-1,'msg'=>$msg]);
        }

        $data =  array(
            'user_id'=>session('user_id'),
            'vote_id'=>$id,
            'choose'=>$choose,
            'addtime'=>time()
        );
        $thi =   Db::name('vote_log')->insertGetId($data);

        if($thi){
            return json(['code'=>1,'msg'=>'投票成功！']);
        }

    }


    public function gongwen()
    {

        $this->user_base();
        $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }
        $map = [];
        $member = Db::name('member')->field('id,group_id,nickname,head_img,account,integral')->where('id',$user_id)->find();


        $Nowpage = input('param.page') ? input('param.page'):1;
        $limits = 10;// 获取总条数



            $instit_id= session('instit_id');
                if($instit_id==1){
                    $lists = DB::name('gongwen')->where($map)->page($Nowpage,$limits)->order('id desc')->select();
                    $count = Db::name('gongwen')->where($map)->count();//计算总页面
                }else{
                    $map1['fa_id'] = $instit_id;
                    $map1['shou_id'] = $instit_id;
                    $lists = DB::name('gongwen')->where($map)->whereor($map1)->page($Nowpage,$limits)->order('id desc')->select();
                    $count = Db::name('gongwen')->where($map)->whereor($map1)->count();//计算总页面
                }


                $allpage = intval(ceil($count / $limits));

                foreach($lists as $k=>$v){
                    $lists[$k]['addtime1'] = date('Y-m-d H:i:s',$v['addtime']);
                    $inst11 = Db::name('instit')->where('id',$v['fa_id'])->find();
                    $inst12 = Db::name('instit')->where('id',$v['shou_id'])->find();
                    $lists[$k]['fa_name'] = $inst11['name'];
                    $lists[$k]['shou_name'] = $inst12['name'];
                }
        if(input('param.page'))
        {
                return json(['code' => 1, 'list'=>$lists,  'msg' => 'ok']);
        }

        if($instit_id!=1){
            $childcount = Db::name('instit')->where('pid',$instit_id)->count();
            if($childcount>0){
                $can = 1;
            }else{
                $can = 0;
            }
        }else{
            $can = 1;
        }

        $day = strtotime(date('Y-m-d'));
        $is_sign = Db::name('sign_log')->where(['user_id'=>$user_id,'sign_time'=>$day])->count();
        $this->assign('is_sign',$is_sign);

        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('can',$can);
        $this->assign('member',$member);
        $this->assign('m','21');

        return $this->fetch();

    }



    public function gongwen_read()
    {

        $this->user_base();
        $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }

        $member = Db::name('member')->field('id,group_id,nickname,head_img,account,integral')->where('id',$user_id)->find();

        $id = input('param.id');
        $gongwen = new GongwenModel();
        $gongwens = $gongwen->getOneGongwen($id);
        $inst11 = Db::name('instit')->where('id',$gongwens['fa_id'])->find();
        $inst12 = Db::name('instit')->where('id',$gongwens['shou_id'])->find();
        $gongwens['fa_name'] = $inst11['name'];
        $gongwens['shou_name'] = $inst12['name'];
        $this->assign('gongwen',$gongwens);
        $day = strtotime(date('Y-m-d'));
        $is_sign = Db::name('sign_log')->where(['user_id'=>$user_id,'sign_time'=>$day])->count();
        $this->assign('is_sign',$is_sign);
        $this->assign('member',$member);
        $this->assign('m','21');
        return $this->fetch();



    }


    public function gongwen_add(){


        $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }
        $member = Db::name('member')->field('id,group_id,nickname,head_img,account,integral')->where('id',$user_id)->find();

        if(request()->isAjax()){

            $param = input('post.');
            $param['fa_id'] = session('instit_id');
            $param['addtime'] = time();

            if(empty($param['content'])){
                return json(['code' => -1, 'msg'=>'内容不能为空！']);
            }

            $gongwen = new GongwenModel();
            $flag = $gongwen->insertgongwen($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }

        $instit_id = session('instit_id');
        if($instit_id!=1){
            $instit = Db::name('instit')->where('pid',$instit_id)->select();
        }else{
            $instit = Db::name('instit')->where('id !=1 ')->select();
        }


        $this->assign('instits',$instit);


        $day = strtotime(date('Y-m-d'));
        $is_sign = Db::name('sign_log')->where(['user_id'=>$user_id,'sign_time'=>$day])->count();
        $this->assign('is_sign',$is_sign);
        $this->assign('member',$member);
        $this->assign('m','21');
        return $this->fetch();
    }



}
