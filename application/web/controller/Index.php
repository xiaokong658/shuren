<?php
namespace app\web\controller;
use think\Controller;
use think\View;
use think\Db;
use think\Config;
class Index extends  Base
{

    public function index()
    {

        $keys = input('param.key');
        if($keys&&$keys!=="")
        {
            $map_info['title'] = ['like',"%" . $keys . "%"];
        }

        $ad = $this->get_ad('28');
        $map12 = array(
            'group_id'=>8,
            'desc'=>['neq','']
        );
        $new_instit = Db::name('member')->field('id,head_img,nickname,desc')->where($map12)->order('create_time desc')->limit(5)->select();


        if(input('param.page')){
            $page = input('param.page');
            $map_info['status'] = 1;
            $article_list =  Db::name('article')->field('id,user_id,instit_id,title,create_time,photo')->where($map_info)->order('id desc')->page($page,15)->select();
            if($article_list){
                foreach($article_list as $key=>$val){
                    $author =   get_article_author($val['user_id'],$val['instit_id']);
                    $article_list[$key]['nickname'] =   $author['nickname'];
                    $article_list[$key]['head_img'] =   $author['head_img'];
                    $article_list[$key]['user_id'] =   $author['id'];
                    $today = date('Y-m-d');
                    $day = date('Y-m-d',$val['create_time']);
                    if($today==$day){
                        $article_list[$key]['addtime'] = '今天 '.date('H:i',$val['create_time']);
                    }else{
                        $article_list[$key]['addtime'] = date('Y-m-d H:i',$val['create_time']);
                    }
                    $article_list[$key]['comment_count'] = Db::name('article_comment')->where(['status'=>1,'article_id'=>$val['id']])->count();
                }
                return json(['code'=>1,'list'=>$article_list]);
            }else{
                return json(['code'=>0]);
            }

        }

        $mad['status'] = 1;
        $mad['photo'] = ['neq',''];
        $hot_article_list =  Db::name('article')->field('id,title,photo')->where($mad)->order('views desc')->limit(5)->select();
        foreach($hot_article_list as $kk=>$vv){
            $hot_article_list[$kk]['sort'] = $kk+1;
        }

        $right_ad1 = $this->get_one_ad('29');


        $right_ad2 = $this->get_one_ad('30');

        $web_info = array(
            'web_site_title'=>$this->get_config_v('web_site_title'),
            'web_site_description'=>$this->get_config_v('web_site_description'),
            'web_site_keyword'=>$this->get_config_v('web_site_keyword'),
        );
        $this->assign('ad',$ad);

        $this->assign('left_nav',$this->get_config_v('left_nav'));
        $this->assign('right_ad1',$right_ad1);
        $this->assign('right_ad2',$right_ad2);
        $this->assign('keys',$keys);
        $this->assign('hot_article',$hot_article_list);
        $this->assign('new_instit',$new_instit);
        $this->assign('web_info',$web_info);
        return $this->fetch();
    }



    public function user_index()
    {

//        $keys = input('param.key');
//        if($keys&&$keys!=="")
//        {
//            $map_info['title'] = ['like',"%" . $keys . "%"];
//        }
//        $ad = $this->get_ad('28');

        $user_id =  input('param.user_id');
        if($user_id!=212212){
            $this->redirect('user/center',['user_id' => $user_id ]);

        }
        if(empty($user_id)){
            $user_id = session('user_id');
            if(empty($user_id)){
                $this->redirect('user/login');
            }
        }
        $group = Db::name('member')->field('id,group_id,instit_id')->where('id',$user_id)->find();
        if(input('param.page')){
            $page = input('param.page');
            $map_info['status'] = 1;
            $map_info['user_id'] = $user_id;
            $article_list =  Db::name('article')->field('id,user_id,instit_id,title,create_time,photo')->where($map_info)->order('id desc')->page($page,15)->select();
            if($article_list){
                foreach($article_list as $key=>$val){
                    $author =   get_article_author($val['user_id'],$val['instit_id']);
                    $article_list[$key]['nickname'] =   $author['nickname'];
                    $article_list[$key]['head_img'] =   $author['head_img'];
                    $article_list[$key]['user_id'] =   $author['id'];
                    $today = date('Y-m-d');
                    $day = date('Y-m-d',$val['create_time']);
                    if($today==$day){
                        $article_list[$key]['addtime'] = '今天 '.date('H:i',$val['create_time']);
                    }else{
                        $article_list[$key]['addtime'] = date('Y-m-d H:i',$val['create_time']);
                    }
                    $article_list[$key]['comment_count'] = Db::name('article_comment')->where(['status'=>1,'article_id'=>$val['id']])->count();
                }
                return json(['code'=>1,'list'=>$article_list]);
            }else{
                return json(['code'=>0]);
            }

        }

        $mad['status'] = 1;
        $mad['photo'] = ['neq',''];
        $hot_article_list =  Db::name('article')->field('id,title,photo')->where($mad)->order('views desc')->limit(5)->select();
        foreach($hot_article_list as $kk=>$vv){
            $hot_article_list[$kk]['sort'] = $kk+1;
        }

        $right_ad1 = $this->get_one_ad('29');
        $right_ad2 = $this->get_one_ad('30');
        $user_nav = Db::name('user_nav')->where('user_id',$user_id)->find();
//        $this->assign('ad',$ad);
        $user_info = Db::name('member')->where('id',$user_id)->find();
        $map5 = array(
            'user_id'=>$user_id,
            'status'=>1,
        );
        $article_count = Db::name('article')->where($map5)->count();
        $debate_count = Db::name('debate')->where('user_id',$user_id)->count();
        $essay_count = Db::name('essay')->where('user_id',$user_id)->count();
        $fan_count = Db::name('fans')->where('fans_id',$user_id)->count();
        $focus_count = Db::name('fans')->where('user_id',$user_id)->count();

        $user_info['count'] = $article_count;
        $user_info['fans_count'] = $fan_count;
        $user_info['focus_count'] = $focus_count;


        $map15 = array(
            'user_id' => session('user_id'),
            'fans_id' => $user_id,
        );
        $is_focus = Db::name('fans')->where($map15)->count();

        $info = Db::name('user_nav')->where('user_id',$user_id)->find();
        if($info['jigou']){
            $str1 = '';
            $arr = explode('|',$info['jigou']);
            $i=0;
            foreach($arr as $key=>$val){
                if($i<5){
                    $str1 = $str1.$val.',';
                }
                $i++;
            }
            $str1 = substr($str1,0,-1);
            $ds1['id']= ['in',$str1];
            $new_instit = Db::name('member')->field('id,head_img,nickname,desc')->where($ds1)->order('create_time desc')->select();
        }

        $xiaji = Db::name('article_recommend')->field('article_id')->where('jigou_tui = 1 and senior_instit_id = '.$group['instit_id'])->select();
        $mapdp['jigou_tui']=1;
        $mapdp['status']=1;
        $mapdp['user_id']= $user_id;

        $jieguo = '';
        if($xiaji){
            foreach($xiaji as $vq){
                $jieguo = $jieguo.$vq['article_id'].',';
            }

            $jieguo = substr($jieguo,0,-1);
            $mapdq1['id']= ['in',$jieguo];
            $jg_recommend =  Db::name('article')->field('title,remark,photo,id')->where($mapdp)->whereor($mapdq1)->group('id')->order('create_time desc')->limit(5)->select();
        }else{
            $jg_recommend =  Db::name('article')->field('title,remark,photo,id')->where($mapdp)->order('create_time desc')->limit(5)->select();
        }



        if($user_nav['tuijian1']){

              $article_by1 = Db::name('article')->where(['column_id'=>$user_nav['tuijian1'],'user_id'=>$user_id])->order('id desc')->limit(3)->select();

        }

        if($user_nav['tuijian2']){

            $article_by2 = Db::name('article')->where(['column_id'=>$user_nav['tuijian2'],'user_id'=>$user_id])->order('id desc')->limit(3)->select();

        }

        if($user_nav['tuijian3']){

            $article_by3 = Db::name('article')->where(['column_id'=>$user_nav['tuijian3'],'user_id'=>$user_id])->order('id desc')->limit(3)->select();

        }

        if($user_nav['center']){
           $img_arr = explode('|',$info['center']);
            $cs =0;
           foreach($img_arr as $v){
               $img_arr1[$cs]['images']=$v;
               $cs++;
           }


        }else{
            $img_arr1 = array();
        }


        $this->assign('img_arr',$img_arr1);
        $this->assign('is_focus',$is_focus);
        $this->assign('img_arr',$img_arr);
        $this->assign('article_by1',$article_by1);
        $this->assign('article_by2',$article_by2);
        $this->assign('article_by3',$article_by3);
        $this->assign('user_info',$user_info);
        $this->assign('user_nav',$user_nav);
        $this->assign('jg_recommend',$jg_recommend);
        $this->assign('right_ad1',$right_ad1);
        $this->assign('right_ad2',$right_ad2);
        $this->assign('user_id',$user_id);
        $this->assign('myuser_id',session('user_id'));
//        $this->assign('keys',$keys);
        $this->assign('hot_article',$hot_article_list);
        $this->assign('new_instit',$new_instit);
        return $this->fetch();
    }

public function member()
    {

        $keys = input('param.key');
        $map_info = array();
        if($keys&&$keys!=="")
        {
            $map_info['nickname'] = ['like',"%" . $keys . "%"];
        }


        $map12 = array(
            'group_id'=>8,
            'desc'=>['neq','']
        );
        $new_instit = Db::name('member')->field('id,head_img,nickname,desc')->where($map12)->order('create_time desc')->limit(5)->select();


        if(input('param.page')){
            $page = input('param.page');
//            $map_info['status'] = 1;
            $article_list =  Db::name('member')->field('id,nickname,desc,head_img,create_time')->where($map_info)->order('id desc')->page($page,40)->select();
            if($article_list){
                foreach($article_list as $key=>$val){
//                    $author =   get_article_author($val['user_id'],$val['instit_id']);
//                    $article_list[$key]['nickname'] =   $author['nickname'];
//                    $article_list[$key]['head_img'] =   $author['head_img'];
//                    $article_list[$key]['user_id'] =   $author['id'];
//                    $today = date('Y-m-d');
//                    $day = date('Y-m-d',$val['create_time']);
//                    if($today==$day){
                        $article_list[$key]['create_time1'] = date('Y-m-d H:i:s',$val['create_time']);
                    if(empty($val['desc'])){
                        $article_list[$key]['desc'] ='';
                    }
//                    }else{
//                        $article_list[$key]['addtime'] = date('Y-m-d H:i',$val['create_time']);
//                    }
//                    $article_list[$key]['comment_count'] = Db::name('article_comment')->where(['status'=>1,'article_id'=>$val['id']])->count();
                }
                return json(['code'=>1,'list'=>$article_list]);
            }else{
                return json(['code'=>0]);
            }
        }

        $cate_list = Db::name('article_cate')->field('id,name')->where('status',1)->select();
        $mad['status'] = 1;
        $mad['photo'] = ['neq',''];
        $hot_article_list =  Db::name('article')->field('id,title,photo')->where($mad)->order('views desc')->limit(5)->select();
        foreach($hot_article_list as $kk=>$vv){
            $hot_article_list[$kk]['sort'] = $kk+1;
        }

        $right_ad1 = $this->get_one_ad('29');
        $right_ad2 = $this->get_one_ad('30');
        $this->assign('right_ad1',$right_ad1);
        $this->assign('right_ad2',$right_ad2);
        $this->assign('cate',$cate_list);
        $this->assign('keys',$keys);
        $this->assign('hot_article',$hot_article_list);
        $this->assign('new_instit',$new_instit);
        return $this->fetch();
    }


    public function index_d()
    {
        $ad = $this->get_ad('28');

        $map = array(
            'is_tui'=>1,
            'status'=>1,
        );
        $list = Db::name('article')->field('id,title,photo,create_time,user_id,writer,views,remark')->where($map)->order('id desc')->limit(5)->select();
        foreach($list as $key=>$val){
            if(!empty($val['user_id'])){
                $author = Db::name('member')->field('nickname')->where('id',$val['user_id'])->find();
                $list[$key]['author'] = $author['nickname'];
            }else{
                $list[$key]['author'] = $val['writer'];
            }
            $list[$key]['create_time'] = date('Y-m-d',$val['create_time']);
        }

        $map1 = array(
            'is_tui'=>1,
            'group_id'=>8,

        );
        $instit = Db::name('member')->field('id,account,nickname,head_img,desc,id')->where($map1)->order('id desc')->limit(8)->select();

        $user_id = session('user_id');

        $view = new View();
        if(!empty($user_id)){
            $map['user_id'] = $user_id;
            $map['status'] = 1;

            $mapk['user_id'] = $user_id;
            $mapk['status'] = 1;

            $user_info['debate_num'] = Db::name('debate')->where('user_id',$user_id)->count();
            $user_info['article_num'] = Db::name('article')->where($mapk)->count();

            $user_info['foucs_num'] = Db::name('fans')->where('user_id',$user_id)->count();
            $user_info['fans_num'] = Db::name('fans')->where('fans_id',$user_id)->count();
            $user_info['total'] =  $user_info['debate_num']+$user_info['article_num'];
            $user_info['member'] = Db::name('member')->field('account,nickname,head_img,desc,id,group_id')->where('id',$user_id)->find();
            $user_info['is_login'] =1;

        }else{
            $user_info['is_login'] =0;
        }



        if(session('user_id')){
            $view->assign('is_login',1);
        }else{
            $view->assign('is_login',0);
        }


        $map12 = array(
            'group_id'=>8,
            'desc'=>['neq','']
        );

        $new_instit = Db::name('member')->field('id,head_img,nickname,desc')->where($map12)->order('id desc')->limit(5)->order('id desc')->select();

        $view->assign('ad',$ad);
        $view->assign('list',$list);
        $view->assign('instit',$instit);
        $view->assign('new_instit',$new_instit);
        $view->assign('user_info',$user_info);
        return $view->fetch();
    }


    public function dynamic(){

        $user_id = session('user_id');


        $user  = Db::name('member')->field('nickname')->where('id',$user_id)->find();

        if(input('param.page')){
            $page=input('param.page');
            $dynamic_list =  Db::name('dynamic')->alias('a')->field('a.*,b.nickname,b.head_img')->join('shuren_member b','a.user_id = b.id')->order('id desc')->page($page,10)->select();
            foreach($dynamic_list as $key=>$val){
                if(!empty($val['img'])){
                    $imgs = substr($val['img'],0,-1);
                    $imgarr = explode(",",$imgs);
                    $i = 0;
                    foreach($imgarr as $va){
                        $newimgarr[$i]['imgpath'] = $va;
                        $i++;
                    }
                    $dynamic_list[$key]['imgs'] = $newimgarr ;
                }

                if($val['share_id']>0){
                    $share =  Db::name('dynamic')->alias('a')->field('b.nickname,a.content,a.img')->join('member b','a.user_id = b.id')->where('a.id',$val['share_id'])->find();

                    $dynamic_list[$key]['sharename'] = $share['nickname'];
                    $dynamic_list[$key]['sharecontent'] = $this->replaces($share['content']);
                    $dynamic_list[$key]['share_img'] = $share['img'];

                    if(!empty($share['img'])){
                        $imgs3 = substr($share['img'],0,-1);
                        $imgarr3 = explode(",",$imgs3);
                        $i = 0;
                        foreach($imgarr3 as $vav){
                            $newimgarr3[$i]['imgpath'] = $vav;
                            $i++;
                        }
                        $dynamic_list[$key]['share_imgs'] = $newimgarr3 ;
                    }

                }
                $dynamic_list[$key]['addtime'] = date('m-d H:i',$val['addtime']);
                $at_info = '';
                if(!empty($val['at_user_id'])){
                    $people = substr($val['at_user_id'],0,-1);
                    $peoples = explode(',',$people);
                    foreach($peoples as $kk){
                        $member = Db::name('member')->field('nickname')->where('id',$kk)->find();
                        $at_info = $at_info."<span class='blue'>@".$member['nickname'].'&nbsp;</span>';
                    }
                }
                $dynamic_list[$key]['content'] = $at_info . $this->replaces($val['content']);
                $mapq['dynamic_id'] = $val['id'];
                $dynamic_list[$key]['zan'] = Db::name('dynamic_zan')->where($mapq)->count();
                $dynamic_list[$key]['pinglun'] = Db::name('dynamic_comment')->where($mapq)->count();
                $mapq['user_id'] = $user_id;

                $map4['share_id'] = $val['id'] ;
                $dynamic_list[$key]['shares'] = Db::name('dynamic')->where($map4)->count();

                $dynamic_list[$key]['is_zan'] = Db::name('dynamic_zan')->where($mapq)->count();

                $map = array(
                    'a.dynamic_id'=>$val['id']
                );
                $dynamic_comment =  Db::name('dynamic_comment')->alias('a')->field('a.id,b.nickname,a.content,a.addtime')->join('shuren_member b','a.user_id = b.id')->where($map)->order('a.id asc')->select();
                foreach($dynamic_comment as $kk=>$vv){
                    $dynamic_comment[$kk]['content'] = $this->replaces($vv['content']);
                }
                $dynamic_list[$key]['comment']= $dynamic_comment;
            }
            return json($dynamic_list);
        }

        $map12 = array(
            'group_id'=>8,
            'desc'=>['neq','']
        );
        $new_instit = Db::name('member')->field('id,head_img,nickname,desc')->where($map12)->order('id desc')->limit(5)->order('id desc')->select();


        if(!empty($user_id)){
            $map['user_id'] = $user_id;
            $map['status'] = 1;

            $mapk['user_id'] = $user_id;
            $mapk['status'] = 1;

            $user_info['debate_num'] = Db::name('debate')->where('user_id',$user_id)->count();
            $user_info['article_num'] = Db::name('article')->where($mapk)->count();

            $user_info['foucs_num'] = Db::name('fans')->where('user_id',$user_id)->count();
            $user_info['fans_num'] = Db::name('fans')->where('fans_id',$user_id)->count();
            $user_info['total'] =  $user_info['debate_num']+$user_info['article_num'];
            $user_info['member'] = Db::name('member')->field('account,nickname,head_img,desc,id,group_id')->where('id',$user_id)->find();
            $user_info['is_login'] =1;

        }else{
            $user_info['is_login'] =0;
        }



        $view = new View();
        $view->assign('new_instit',$new_instit);
        $view->assign('is_login',1);
        $view->assign('user_info',$user_info);

        return $view->fetch();

    }

    public function dynamic_zan()
    {
        // return json(['code' => -1,'count'=>1, 'msg' => '点赞已取消']);
        $id=input('param.id');
        $user_id = session('user_id');

        if(empty($user_id)){
            return json(['code' => 11, 'msg' => '您尚未登录！']);
        }
        //$user_id = 14;
        $map = array(
            'user_id'=>$user_id,
            'dynamic_id'=>$id,
        );
        $is_zan = Db::name('dynamic_zan')->where($map)->find();
        if(!$is_zan){
            $is_zan['is_zan'] = 0;
        }
        if($is_zan['is_zan']==1)
        {
            Db::name('dynamic_zan')->where('id',$is_zan['id'])->delete();
            return json(['code' => -1, 'msg' => '点赞已取消']);
        }else{
            $data = array(
                'user_id'=>$user_id,
                'dynamic_id'=>$id,
                'is_zan'=>1,
            );
            $flag = Db::name('dynamic_zan')->insert($data);
            return json(['code' => 1,  'msg' => '点赞成功']);
        }

    }

    public function index1()
    {

        $view = new View();
        return $view->fetch();
    }


    public function loginout(){

            session(null);
        $this->redirect('user/login');

    }




    public function want(){

      $list =   Db::name('want_kebiao')->alias('a')->field('a.*,b.name')->join('instit b','a.instit_id=b.id')->where('a.status',0)->select();

      //echo  Db::name('want_kebiao')->getLastSql();
      if($list){
          return json(['code'=>1,'list'=>$list])  ;
      }else{
          return json(['code'=>0])  ;
      }



    }





}
