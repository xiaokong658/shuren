<?php

namespace app\web\controller;
use think\Controller;
use  think\Request;
use think\Db;
use think\Log;
use think\view;

class Base extends Controller
{

    public function _initialize() {

        $request = Request::instance();
        $mod =  $request->module();
        $con = $request->controller();
        $act= $request->action();
        if($mod=='web'&&$con=='Article'){
            if ($this->ismobile()) {
                if($act=='index'){
                    $arr2=explode("/", $request->path());
                    $thisid=$arr2[count($arr2)-1];
                    $this->redirect('index/index/article_detail',array('id'=>$thisid));
                }
                if($act=='essay_detail'){
                    $arr2=explode("/", $request->path());
                    $thisid=$arr2[count($arr2)-1];
                    $this->redirect('index/index/essay_detail',array('id'=>$thisid));
                }
                if($act=='debatae_detail'){
                    $arr2=explode("/", $request->path());
                    $thisid=$arr2[count($arr2)-1];
                    $this->redirect('index/index/debatae_detail',array('id'=>$thisid));
                }
            }
        }
        if(session('user_id')=='web'){
                session(null);
          //  $this->redirect('user/login');
        }

        $user_id = session('user_id');
        if(empty($user_id)){
            $this->assign('is_login',0);
        }else{
            $this->assign('is_login',1);
        }
        $my_user_info = Db::name('member')->field('id,nickname,head_img')->where('id',$user_id)->find();
        $this->assign('my_user_info',$my_user_info);

    }

    public  function  get_ad($id){
        $ad_position =  Db::name('ad_position')->where('id',$id)->find();
        $map['closed'] = 0;
        $map['status'] = 1;
        $ad =  Db::name('ad')->where($map)->where('ad_position_id',$id)->order('id desc')->select();
        return $ad;
    }

    public  function  get_one_ad($id){
        $ad_position =  Db::name('ad_position')->where('id',$id)->find();
        $map['closed'] = 0;
        $map['status'] = 1;
        $ad =  Db::name('ad')->where($map)->where('ad_position_id',$id)->order('id desc')->find();
        return $ad;
    }

   


    public function  replaces($content){
        $new = array(
            '1'=>'汗',
            '2'=>'白眼',
            '3'=>'鄙视',
            '4'=>'打',
            '5'=>'大哭',
            '6'=>'鼓掌',
            '7'=>'奸笑',
            '8'=>'惊吓',
            '9'=>'纠结',
            '10'=>'可爱',
            '11'=>'可怜',
            '12'=>'酷',
            '13'=>'脸红',
            '14'=>'骂人',
            '15'=>'亲亲',
            '16'=>'傻',
            '17'=>'调皮',
            '18'=>'偷笑',
            '19'=>'委屈',
            '20'=>'无语',
            '21'=>'晕',
            '22'=>'抠鼻');

        for($i=1;$i<=22;$i++){
            $str1 = '<img src="/static/emoji/img/'.$i.'.gif">';
            $str2 = '['.$new[$i].']';
            $content =  str_replace($str2,$str1,$content);
        }

        return $content;


    }


    public function get_config_v($v){
        $info= Db::name('config')->where('name',$v)->find();
        if($info){
            return $info['value'];
        }else
            return '';
    }

    public function  read_article($article_id){
        $view =  Db::name('article')->field('views')->where('id',$article_id)->find();
        if($view){
            $new = $view['views']+1;
            Db::name('article')->where('id',$article_id)->setField('views',$new);

            $map =array(
                'article_id'=>$article_id,
                'user_id'=>session('user_id'),
                'addtime'=>time()
            );
            Db::name('article_read_log')->insert($map);




        }
    }


    public function user_signs($user_id,$every_sign_integral,$sign_cycle){

        $day = strtotime(date('Y-m-d'));
        $last_sign = Db::name('sign_log')->where(['user_id'=>$user_id,'type'=>'sign'])->order('id desc')->find();


        if($last_sign['sign_time']==$day){
            $res['code']=-2;
            return $res;
        }

        $member = Db::name('member')->field('integral,integral_num')->where('id',$user_id)->find();
        $cha = $day-$last_sign['sign_time'];
        if($cha==86400){
            $thisintegral = $member['integral_num']*$every_sign_integral;
            $thisintegral_num = $member['integral_num']+1;
            if($thisintegral_num>$sign_cycle){
                $thisintegral_num =1;
            }


        }else{
            $thisintegral = $member['integral_num']*1;
            $thisintegral_num =1;
        }

        $data = array(
            'integral'=>  $thisintegral+$member['integral'],
            'integral_num'=>  $thisintegral_num,
        );

        $result = Db::name('member')->where('id',$user_id)->update($data);

        $datas = array(
            'user_id'=>$user_id,
            'sign_time'=>$day,
            'integral'=>$thisintegral,
            'add_time'=>time(),
            'type'=>'sign'
        );
        Db::name('sign_log')->insert($datas);




        if($result){
            $res['code']=1;
            $res['integral']=$thisintegral;
        }

        return $res;


    }


    public function ismobile() {
        // 如果有HTTP_X_WAP_PROFILE则一定是移动设备
        if (isset ($_SERVER['HTTP_X_WAP_PROFILE']))
            return true;

        //此条摘自TPM智能切换模板引擎，适合TPM开发
        if(isset ($_SERVER['HTTP_CLIENT']) &&'PhoneClient'==$_SERVER['HTTP_CLIENT'])
            return true;
        //如果via信息含有wap则一定是移动设备,部分服务商会屏蔽该信息
        if (isset ($_SERVER['HTTP_VIA']))
            //找不到为flase,否则为true
            return stristr($_SERVER['HTTP_VIA'], 'wap') ? true : false;
        //判断手机发送的客户端标志,兼容性有待提高
        if (isset ($_SERVER['HTTP_USER_AGENT'])) {
            $clientkeywords = array(
                'nokia','sony','ericsson','mot','samsung','htc','sgh','lg','sharp','sie-','philips','panasonic','alcatel','lenovo','iphone','ipod','blackberry','meizu','android','netfront','symbian','ucweb','windowsce','palm','operamini','operamobi','openwave','nexusone','cldc','midp','wap','mobile'
            );
            //从HTTP_USER_AGENT中查找手机浏览器的关键字
            if (preg_match("/(" . implode('|', $clientkeywords) . ")/i", strtolower($_SERVER['HTTP_USER_AGENT']))) {
                return true;
            }
        }
        //协议法，因为有可能不准确，放到最后判断
        if (isset ($_SERVER['HTTP_ACCEPT'])) {
            // 如果只支持wml并且不支持html那一定是移动设备
            // 如果支持wml和html但是wml在html之前则是移动设备
            if ((strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') !== false) && (strpos($_SERVER['HTTP_ACCEPT'], 'text/html') === false || (strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') < strpos($_SERVER['HTTP_ACCEPT'], 'text/html')))) {
                return true;
            }
        }
        return false;
    }

}