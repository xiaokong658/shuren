<?php
namespace app\web\controller;
use think\Controller;
use think\View;
use think\Db;
class User extends  Base
{

    public function login()
    {

       $user_id = session('user_id');

        if($user_id){
            $this->redirect('index/index');
        }

        if(request()->isAjax()){

            $role = 1;
            $username=input('param.username');
            $password=input('param.password');
            if($role==2){
                $student = Db::name('student')->where('mobile',$username)->find();
                $user = Db::name('member')->where('account',$student['studentCode'])->find();
                if($user){
                    if(empty($user['password'])){
                        return json(['code' => -1, 'data' => '2', 'msg' => '该用户未注册，请先注册']);
                    }else{
                        $pwd =  md5(md5($password) . config('auth_key'));
                        if($user['password']==$pwd ){
                            session('user_id',$user['id']);
                            session('instit_id',$user['instit_id']);
                            session('group_id',$user['group_id']);
                            return json(['code' => 1,'data' => '2','msg' => '登录成功！']);
                        }else{
                            return json(['code' => -1, 'data' => '2', 'msg' => '密码错误！']);
                        }
                    }
                }else{
                    return json(['code' => -1, 'data' => '2', 'msg' => '帐号不存在']);
                }
            }else{
                $user = Db::name('member')->where('account',$username)->find();
                if($user){

                    if(empty($user['password'])){
                        return json(['code' => -1, 'data' => '2', 'msg' => '该用户未注册，请先注册']);
                    }else{
                        $pwd =  md5(md5($password) . config('auth_key'));

                        if($user['password']==$pwd ){
                            session('user_id',$user['id']);
                            session('instit_id',$user['instit_id']);
                            session('group_id',$user['group_id']);
                            return json(['code' => 1]);
                        }else{
                            return json(['code' => -1, 'data' => '2', 'msg' => '密码错误！']);
                        }
                    }
                }else{
                    return json(['code' => -1, 'data' => '2', 'msg' => '帐号不存在']);
                }
            }
        }

        $view = new View();
        return $view->fetch();
    }

    public function center()
    {

       $user_id =  input('param.user_id');
        if(empty($user_id)){
            $user_id = session('user_id');
            if(empty($user_id)){
                $this->redirect('user/login');
            }
        }


        if($user_id==212212){
            $this->redirect('index/user_index',['user_id' => $user_id ]);

        }

        $user_column = Db::name('user_column')->field('*')->where('user_id',$user_id)->select();
        $group = DB::name('member')->field('group_id,instit_id')->where('id',$user_id)->find();

        $type =  input('param.type');
        if(empty($type)){
            $type = 1;
        }

        $user_info = Db::name('member')->where('id',$user_id)->find();

        $map5 = array(
            'user_id'=>$user_id,
            'status'=>1,
        );
        $article_count = Db::name('article')->where($map5)->count();
        $debate_count = Db::name('debate')->where('user_id',$user_id)->count();
        $essay_count = Db::name('essay')->where('user_id',$user_id)->count();
        $fan_count = Db::name('fans')->where('fans_id',$user_id)->count();
        $focus_count = Db::name('fans')->where('user_id',$user_id)->count();

        $user_info['count'] = $article_count+$debate_count+$essay_count;
        $user_info['fans_count'] = $fan_count;
        $user_info['focus_count'] = $focus_count;
        $map['status'] = 1;
        $map['user_id'] =$user_id;


        $Nowpage = input('param.page') ? input('param.page'):1;
        $limits = 10;       // 获取总条数
        $count = Db::name('article')->where($map)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));

       // $instit = Db::name('instit')->where('id',)->find();
        $btype =  input('param.btype');
        if(input('param.page'))
        {



            if($type==1){

                if($btype){
                    $map['column_id'] = $btype;
                }else{
                    $btype=0;
                }

                $list  =  Db::name('article')->field('id,title,create_time,photo,remark')->where($map)->order('my_ding desc,id desc')->page($Nowpage, $limits)->select();


               // $list  =  Db::name('article')->field('id,title,create_time,photo,remark')->where($map)->order('id desc')->page($Nowpage, $limits)->select();
                foreach($list as $key=>$item){
                    $list[$key]['create_time'] = date('Y-m-d',$item['create_time']);
                    $list[$key]['comment_count'] = Db::name('article_comment')->where('article_id',$item['id'])->count();
                    $list[$key]['zan_count'] = Db::name('article_zan')->where('article_id',$item['id'])->count();
                    $list[$key]['collect_count'] = Db::name('article_collect')->where('article_id',$item['id'])->count();

                    $list[$key]['type'] = 1;
                }
            }

            if($type==2){
                $map2 = array(
                    'instit_id'=>$group['instit_id'],
                );

                $list  =  Db::name('essay')->field('id,title,create_time,photo,remark')->where('user_id',$user_id)->order('my_ding desc,id desc')->page($Nowpage, $limits)->select();


                foreach($list as $key=>$item){
                    $list[$key]['create_time'] = date('Y-m-d',$item['create_time']);
                    $list[$key]['comment_count'] = 0;
                    $list[$key]['zan_count'] = Db::name('essay_zan')->where('essay_id',$item['id'])->count();
                    $list[$key]['collect_count'] = Db::name('essay_collect')->where('essay_id',$item['id'])->count();
                    $list[$key]['type'] = 2;
                }
            }




            if($type==3){
                $map3 = array(
                    'user_id'=>$user_id,
                );


                if($group['group_id']==8){

                    $list  =  Db::name('debate')->field('id,title,create_time,photo,remark')->where(" user_id=".$user_id." or ( user_id=0 and instit_id= ".$group['instit_id']." ) ")->order('my_ding desc,id desc')->page($Nowpage, $limits)->select();


                }else{
                    $list  =  Db::name('debate')->field('id,title,create_time,photo,remark')->where($map3)->order('my_ding desc,id desc')->page($Nowpage, $limits)->select();
                }



//                $list  =  Db::name('debate')->field('id,title,create_time,photo,remark')->where($map3)->order('id desc')->page($Nowpage, $limits)->select();
                foreach($list as $key=>$item){
                    $list[$key]['create_time'] = date('Y-m-d',$item['create_time']);
                    $map4 = array(
                        'debate_id'=>$item['id'],
                        'status'=> 1
                    )  ;
                    $list[$key]['comment_count'] = Db::name('debate_list')->where($map4)->count();
                    $list[$key]['zan_count'] = Db::name('debates_zan')->where('debate_id',$item['id'])->count();
                    $list[$key]['collect_count'] = Db::name('debate_collect')->where('debate_id',$item['id'])->count();
                    $list[$key]['type'] = 3;
                }
            }

            return json(['code' => 1, 'list'=>$list,  'msg' => 'ok']);
        }

        $map12 = array(
            'group_id'=>8,
            'desc'=>['neq','']
        );
        $new_instit = Db::name('member')->field('id,head_img,nickname,desc')->where($map12)->order('id desc')->limit(5)->order('create_time desc')->select();


        $map5 = array(
            'user_id' => session('user_id'),
            'fans_id' => $user_id,
        );


        $instit = Db::name('instit')->field('qrcode')->where('id',$group['instit_id'])->find();
        if(empty($instit['qrcode'])){
            $qrcode = '';
        }else{
            $qrcode = $instit['qrcode'];
        }

        if($group['group_id']==8){
            $mapdp['jigou_tui']=1;
            $mapdp['status']=1;
            $mapdp['user_id']= $user_id;


            $xiaji = Db::name('article_recommend')->field('article_id')->where('jigou_tui = 1 and senior_instit_id = '.$group['instit_id'])->select();

            $jieguo = '';
            if($xiaji){
                foreach($xiaji as $vq){
                    $jieguo = $jieguo.$vq['article_id'].',';
                }
                $jieguo = substr($jieguo,0,-1);
                $mapdq1['id']= ['in',$jieguo];
                $jg_recommend =  Db::name('article')->field('title,remark,photo,id')->where($mapdp)->whereor($mapdq1)->group('id')->order('create_time desc')->limit(5)->select();
            }else{
                $jg_recommend =  Db::name('article')->field('title,remark,photo,id')->where($mapdp)->order('create_time desc')->limit(5)->select();
            }
            $this->assign('jg_recommend',$jg_recommend);
        }


        $is_focus = Db::name('fans')->where($map5)->count();
        $this->assign('allpage',$allpage);
        $this->assign('user_column',$user_column);
        $this->assign('new_instit',$new_instit);
        $this->assign('user_info',$user_info);
        $this->assign('is_focus',$is_focus);
        $this->assign('user_id',$user_id);
        $this->assign('myuser_id',session('user_id'));
        $this->assign('group_id',$group['group_id']);
        $this->assign('type',$type);
        $this->assign('btype',$btype);
        $this->assign('qrcode',$qrcode);

        return $this->fetch();


    }


    public function fans()
    {

        $user_id =  input('param.user_id');
        if(empty($user_id)){
            $user_id = session('user_id');
            if(empty($user_id)){
                $this->redirect('user/login');
            }
        }


        $group = DB::name('member')->field('group_id,instit_id')->where('id',$user_id)->find();

        $type =  input('param.type');
        if(empty($type)){
            $type = 1;
        }

        $user_info = Db::name('member')->where('id',$user_id)->find();

        $map5 = array(
            'user_id'=>$user_id,
            'status'=>1,
        );
        $article_count = Db::name('article')->where($map5)->count();
        $debate_count = Db::name('debate')->where('user_id',$user_id)->count();
        $essay_count = Db::name('essay')->where('user_id',$user_id)->count();
        $fan_count = Db::name('fans')->where('fans_id',$user_id)->count();
        $focus_count = Db::name('fans')->where('user_id',$user_id)->count();

        $user_info['count'] = $article_count+$debate_count+$essay_count;
        $user_info['fans_count'] = $fan_count;
        $user_info['focus_count'] = $focus_count;


        $map['status'] = 1;
        $map['user_id'] =$user_id;


        $Nowpage = input('param.page') ? input('param.page'):1;
        $limits = 30;

        $map = array(
            'fans_id'=>  $user_id
        );

        // 获取总条数
        $count = Db::name('fans')->where($map)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));

        // $instit = Db::name('instit')->where('id',)->find();

        if(input('param.page'))
        {

                $map3 = array(
                    'f.fans_id'=>$user_id,
                );

                $list = Db::name('fans')->field('f.create_time,m.nickname,m.head_img,m.id')->alias('f')->join('member m', 'f.user_id = m.id')->where($map3)->order('f.id asc')->page($Nowpage, $limits)->select();

                foreach($list as $key=>$item){
                    $list[$key]['create_time1'] = date('Y-m-d',$item['create_time']);
                }

                return json(['code' => 1, 'list'=>$list,  'msg' => 'ok']);
        }

        $map12 = array(
            'group_id'=>8,
            'desc'=>['neq','']
        );
        $new_instit = Db::name('member')->field('id,head_img,nickname,desc')->where($map12)->order('id desc')->limit(5)->order('create_time desc')->select();


        $map5 = array(
            'user_id' => session('user_id'),
            'fans_id' => $user_id,
        );


        $instit = Db::name('instit')->field('qrcode')->where('id',$group['instit_id'])->find();
        if(empty($instit['qrcode'])){
            $qrcode = '';
        }else{
            $qrcode = $instit['qrcode'];
        }

        if($group['group_id']==8){
            $mapdp['jigou_tui']=1;
            $mapdp['status']=1;
            $mapdp['user_id']= $user_id;


            $xiaji = Db::name('article_recommend')->field('article_id')->where('jigou_tui = 1 and senior_instit_id = '.$group['instit_id'])->select();

            $jieguo = '';
            if($xiaji){
                foreach($xiaji as $vq){
                    $jieguo = $jieguo.$vq['article_id'].',';
                }
                $jieguo = substr($jieguo,0,-1);
                $mapdq1['id']= ['in',$jieguo];
                $jg_recommend =  Db::name('article')->field('title,remark,photo,id')->where($mapdp)->whereor($mapdq1)->group('id')->order('create_time desc')->limit(5)->select();
            }else{
                $jg_recommend =  Db::name('article')->field('title,remark,photo,id')->where($mapdp)->order('create_time desc')->limit(5)->select();
            }
            $this->assign('jg_recommend',$jg_recommend);
        }


        $is_focus = Db::name('fans')->where($map5)->count();
        $this->assign('allpage',$allpage);
        $this->assign('new_instit',$new_instit);
        $this->assign('user_info',$user_info);
        $this->assign('is_focus',$is_focus);
        $this->assign('user_id',$user_id);
        $this->assign('myuser_id',session('user_id'));
        $this->assign('group_id',$group['group_id']);
        $this->assign('type',$type);
        $this->assign('qrcode',$qrcode);

        return $this->fetch();


    }

public function focus()
    {

        $user_id =  input('param.user_id');
        if(empty($user_id)){
            $user_id = session('user_id');
            if(empty($user_id)){
                $this->redirect('user/login');
            }
        }


        $group = DB::name('member')->field('group_id,instit_id')->where('id',$user_id)->find();

        $type =  input('param.type');
        if(empty($type)){
            $type = 1;
        }

        $user_info = Db::name('member')->where('id',$user_id)->find();

        $map5 = array(
            'user_id'=>$user_id,
            'status'=>1,
        );
        $article_count = Db::name('article')->where($map5)->count();
        $debate_count = Db::name('debate')->where('user_id',$user_id)->count();
        $essay_count = Db::name('essay')->where('user_id',$user_id)->count();
        $fan_count = Db::name('fans')->where('fans_id',$user_id)->count();
        $focus_count = Db::name('fans')->where('user_id',$user_id)->count();

        $user_info['count'] = $article_count+$debate_count+$essay_count;
        $user_info['fans_count'] = $fan_count;
        $user_info['focus_count'] = $focus_count;
        $map['status'] = 1;
        $map['user_id'] =$user_id;


        $Nowpage = input('param.page') ? input('param.page'):1;
        $limits = 30;

        $map = array(
            'fans_id'=>  $user_id
        );

        // 获取总条数
        $count = Db::name('fans')->where($map)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));

        // $instit = Db::name('instit')->where('id',)->find();

        if(input('param.page'))
        {

                $map3 = array(
                    'f.user_id'=>$user_id,
                );
                $list = Db::name('fans')->field('f.create_time,m.nickname,m.head_img,m.id')->alias('f')->join('member m', 'f.fans_id = m.id')->where($map3)->order('f.id asc')->page($Nowpage, $limits)->select();
                foreach($list as $key=>$item){
                    $list[$key]['create_time1'] = date('Y-m-d',$item['create_time']);
                }
                return json(['code' => 1, 'list'=>$list,  'msg' => 'ok']);
        }

        $map12 = array(
            'group_id'=>8,
            'desc'=>['neq','']
        );
        $new_instit = Db::name('member')->field('id,head_img,nickname,desc')->where($map12)->order('id desc')->limit(5)->order('create_time desc')->select();


        $map5 = array(
            'user_id' => session('user_id'),
            'fans_id' => $user_id,
        );


        $instit = Db::name('instit')->field('qrcode')->where('id',$group['instit_id'])->find();
        if(empty($instit['qrcode'])){
            $qrcode = '';
        }else{
            $qrcode = $instit['qrcode'];
        }

        if($group['group_id']==8){
            $mapdp['jigou_tui']=1;
            $mapdp['status']=1;
            $mapdp['user_id']= $user_id;


            $xiaji = Db::name('article_recommend')->field('article_id')->where('jigou_tui = 1 and senior_instit_id = '.$group['instit_id'])->select();

            $jieguo = '';
            if($xiaji){
                foreach($xiaji as $vq){
                    $jieguo = $jieguo.$vq['article_id'].',';
                }
                $jieguo = substr($jieguo,0,-1);
                $mapdq1['id']= ['in',$jieguo];
                $jg_recommend =  Db::name('article')->field('title,remark,photo,id')->where($mapdp)->whereor($mapdq1)->group('id')->order('create_time desc')->limit(5)->select();
            }else{
                $jg_recommend =  Db::name('article')->field('title,remark,photo,id')->where($mapdp)->order('create_time desc')->limit(5)->select();
            }
            $this->assign('jg_recommend',$jg_recommend);
        }


        $is_focus = Db::name('fans')->where($map5)->count();
        $this->assign('allpage',$allpage);
        $this->assign('new_instit',$new_instit);
        $this->assign('user_info',$user_info);
        $this->assign('is_focus',$is_focus);
        $this->assign('user_id',$user_id);
        $this->assign('myuser_id',session('user_id'));
        $this->assign('group_id',$group['group_id']);
        $this->assign('type',$type);
        $this->assign('qrcode',$qrcode);

        return $this->fetch();


    }


    public function focus_user()
    {
        // return json(['code' => -1,'count'=>1, 'msg' => '点赞已取消']);


        $fans_id=input('param.fans_id');
        $user_id = session('user_id');

        if(empty($user_id)){
            return json(['code' => 11, 'msg' => '您尚未登录！']);
        }
        //$user_id = 14;
        $map = array(
            'user_id'=>$user_id,
            'fans_id'=>$fans_id,
        );
        $is_focus = Db::name('fans')->where($map)->find();
        if(!$is_focus){
            $focused = 0;
        }else{
            $focused = 1;
        }
        if($focused==1)
        {
            Db::name('fans')->where('id',$is_focus['id'])->delete();
            return json(['code' => -1, 'msg' => '关注已取消']);
        }else{
            $data = array(
                'user_id'=>$user_id,
                'fans_id'=>$fans_id,
                'create_time'=>time(),
            );
            $flag = Db::name('fans')->insert($data);
            return json(['code' => 1,  'msg' => '关注成功']);
        }

    }

    function quitlogin(){
        $user_id = session('user_id');
        if($user_id){
            session(null);
                $this->redirect('user/login');
        }
    }



    public function member_reg(){


        $view = new View();
        $view->assign('is_login',0);
        return $view->fetch();


    }


    public function create(){

        $role=input('param.role');
        $username=input('param.username');
        $name=input('param.name');
        $mobile=input('param.mobile');

        if($role==1){
            $user = Db::name('member')->where('account',$username)->find();
            $info = Db::name('student')->where('studentCode',$username)->find();
        }

        if($role==3){
            $user = Db::name('member')->where('account',$username)->find();
            $info = Db::name('teach')->where('mobile',$username)->find();
        }

        if($role==4){
            $user = Db::name('member')->where('account',$username)->find();
            $info = $user;
        }

        if($role==2){
            $student = Db::name('student')->where('mobile',$username)->find();
            $user = Db::name('member')->where('account',$student['studentCode'])->find();
            $info = $student;
        }

        if($info){
            $instit = Db::name('instit')->field('name')->where('id',$info['instit_id'])->find();
        }

        $view = new View();
        $view->assign('role',$role);
        $view->assign('user',$user);
        $view->assign('info',$info);
        $view->assign('instit_name',$instit['name']);
        $view->assign('is_login',0);
        return $view->fetch();

    }



    public function instit_reg(){

        if(request()->isAjax()){
            $param = input('post.');
            unset($param['file']);

            $param['addtime'] = time();


            $flag = Db::name('instit_apply')->insert($param);

            if($flag){
                return json(['code'=>1,'msg'=>'申请成功']);
            }else{
                return json(['code'=>-1,'msg'=>'申请失败']);
            }




        }
        $view = new View();
        $view->assign('is_login',0);
        return $view->fetch();


    }



    public function reg()
    {
        $role=input('param.role');
        $username=input('param.username');
        $name=input('param.name');
        $mobile=input('param.mobile');

        if($role==1){
            $user = Db::name('member')->where('account',$username)->find();
            if($user){
                if(!empty($user['password'])){
                    return json(['code' => -1, 'data' => '2', 'msg' => '此学生已注册，请直接登录！']);
                }else{
                    $student = Db::name('student')->where('studentCode',$username)->find();
                    if($student['studentName']==$name && $mobile==$student['mobile']){


                        return json(['code' => 1]);
                    }else{
                        return json(['code' => -1, 'data' => '2', 'msg' => '未匹配到学生信息']);
                    }
                }
            }else{
                return json(['code' => -1, 'data' => '2', 'msg' => '未匹配到学生信息']);
            }
        }


        if($role==2){
            $student = Db::name('student')->where('mobile',$username)->find();
            if($student){
                $user = Db::name('member')->where('account',$student['studentCode'])->find();
                if(!empty($user['password'])){
                    return json(['code' => -1, 'data' => '2', 'msg' => '此学生已注册，请直接登录']);
                }else{
                    if($student['studentName']==$name){
                        return json(['code' => 1]);
                    }else{
                        return json(['code' => -1, 'data' => '2', 'msg' => '未匹配到学生信息']);
                    }
                }

            }else{
                return json(['code' => -1, 'data' => '2', 'msg' => '未匹配到学生信息']);
            }
        }



        if($role==3){
            $user = Db::name('member')->where('account',$username)->find();
            if($user){
                if(!empty($user['password'])){
                    return json(['code' => -1, 'data' => '2', 'msg' => '此教师已注册，请直接登录！']);
                }else{
                    $teach = Db::name('teach')->where('mobile',$username)->find();
                    if($teach['name']==$name){
                        return json(['code' => 1]);
                    }else{
                        return json(['code' => -1, 'data' => '2', 'msg' => '此教师学生信息匹配有误！']);
                    }
                }
            }else{
                return json(['code' => -1, 'data' => '2', 'msg' => '未找到该教师信息']);
            }
        }

        if($role==4){
            $user = Db::name('member')->where('account',$username)->find();
            if($user){
                if(empty($user['password'])){
                    if($user['realname']==$name){
                        return json(['code' => 1]);
                    }else{
                        return json(['code' => -1, 'data' => '2', 'msg' => '此社会人士信息匹配有误！']);
                    }

                }else{
                    return json(['code' => -1, 'data' => '2', 'msg' => '此社会人士已注册，请直接登录！']);
                }
            }else{
                return json(['code' => -1, 'data' => '2', 'msg' => '未找到该社会人士信息']);
            }
        }

    }

    public function save_pwd2(){
        $user_id=input('param.user_id');
        $password=input('param.password');
        if($user_id&&$password){
            $pwd =  md5(md5($password) . config('auth_key'));
            $data['password'] = $pwd;
            $res =  Db::name('member')->where('id',$user_id)->update($data);
            if($res){

                $article_review =  $this->get_config_v('reg_integral');
                if($article_review>0){
                    add_integral($res['id'],$article_review,'reg_integral');
                }

                return json(['code' => 1, 'data' => '2', 'msg' => '设置密码成功']);
            }else{
                return json(['code' => -1, 'data' => '2', 'msg' => '未知错误']);
            }
        }else{
            return json(['code' => -1, 'data' => '2', 'msg' => '信息不能为空']);
        }

    }


    public  function  user_sign(){
        $user_id = session('user_id');

        $every_sign_integral =  $this->get_config_v('every_sign_integral');
        $sign_cycle =  $this->get_config_v('sign_cycle');

        $result =  user_sign($user_id,$every_sign_integral,$sign_cycle);
        if($result==1){
            return json(['code' => 1, 'data' => '2', 'msg' => '签到成功！']);
        }
        if($result==-2){
            return json(['code' => 1, 'data' => '2', 'msg' => '请勿重复签到']);
        }

    }











    public function user_reg(){

        if(request()->isAjax()){
            $mobile = input('param.mobile');
            $code= input('param.code');
            $password = input('param.password');

            $map = array(
                'is_used'=>0,
                'mobile'=>$mobile,
                'type'=>'reg'
            );
            $codes = Db::name('user_sms_code')->where($map)->order('id desc')->find();
            if($codes['code']!=$code){
                return json(['code'=>-1,'data'=>'输入的验证码有误！']);
            }

            if($codes['endtime']<time()){
                return json(['code'=>-1,'data'=>'验证码已过期，请重新获取！']);
            }


            $user_count = Db::name('member')->where('account',$mobile)->count();
            if($user_count>0){
                return json(['code'=>-1,'data'=>'该用户已存在！']);
            }

            $data = array(
                'account'=>$mobile,
                'nickname'=>input('param.nickname'),
                'realname'=>input('param.realname'),
                'password' => md5(md5($password) . config('auth_key')),
                'group_id'=>2,
                'instit_id'=>1,
                'create_time'=>time(),
                'status'=>1,
            );
            $id= DB::name('member')->insertGetId($data);


            session('user_id',$id);
            session('group_id',2);
            session('instit_id',1);


            if($id){
                $article_review =  $this->get_config_v('reg_integral');
                if($article_review>0){
                    add_integral($id,$article_review,'reg_integral');
                }

                return json(['code'=>1]);
            }else{
                return json(['code'=>-1,'msg'=>'注册失败']);
            }


        }

        return $this->fetch();
    }




}
