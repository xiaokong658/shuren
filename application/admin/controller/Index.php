<?php

namespace app\admin\controller;
use think\Config;
use think\Loader;

class Index extends Base
{
    public function index()
    {
        return $this->fetch('/index');
    }


    /**
     * [indexPage 后台首页]
     * @return [type] [description]
     * @author [田建龙] [864491238@qq.com]
     */
    public function indexPage()
    {
        $info = array(
            'web_server' => $_SERVER['SERVER_SOFTWARE'],
            'onload'     => ini_get('upload_max_filesize'),
            'think_v'    => THINK_VERSION,
            'phpversion' => phpversion(),
        );

        $this->assign('info',$info);
        return $this->fetch('index');
    }


    /**
     * 清除缓存
     */
    public function clear() {
        if (delete_dir_file(CACHE_PATH) || delete_dir_file(TEMP_PATH)) {
            return json(['code' => 1, 'msg' => '清除缓存成功']);
        } else {
            return json(['code' => 0, 'msg' => '清除缓存失败']);
        }
    }


    public function paike(){
        $this->redirect('http://111.198.38.59:8089/education/rowCourse/rowSchoolPage.html');

    }

    public function read(){
        $this->redirect('http://111.198.38.59:8089/education/rowCourse/totalRowSchoolPage.html');

    }




    public function test(){
       $result =  article_shenhe('oiCz8t1qLwuV40rUPVhshnSVAYes','我爱中国',1);
        print_R($result);
    }

}
