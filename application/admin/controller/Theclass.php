<?php

namespace app\admin\controller;
use app\admin\model\TheclassModel;
use think\Db;

class Theclass extends Base
{

    public function index(){

        $uid= session('uid');
        $instit_id = session('instit_id');
        //print_R($admin);
        $key = input('key');
        $map = [];
        if($key&&$key!==""){
            $map['shuren_theclass.name'] = ['like',"%" . $key . "%"];
        }

        $Nowpage = input('get.page') ? input('get.page'):1;
        $limits = 10;// 获取总条数
        if($uid!=1){
            $map['shuren_theclass.instit_id'] = $instit_id;
        }
        $count = Db::name('theclass')->where($map)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));
        $article = new TheclassModel();
        $lists = $article->getTheclassByWhere($map, $Nowpage, $limits);

        foreach($lists as $k=>$v){
            if(!empty($v['leader'])){
                $teach = Db::name('teach')->where('id',$v['leader'])->find();
                $lists[$k]['leadername'] = $teach['name'];
            }else{
                $lists[$k]['leadername'] = '';
            }
        }


//        echo Db::name('theclass')->getLastSql();
        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('count', $count);
        $this->assign('val', $key);
        $this->assign('list', $lists);
        if(input('get.page')){
            return json($lists);
        }
        return $this->fetch();
    }

    public function theclass_state(){
        $id = input('param.id');
        $status = Db::name('theclass')->where('id',$id)->value('status');//判断当前状态情况
        if($status==1)
        {
            $flag = Db::name('theclass')->where('id',$id)->setField(['status'=>0]);
            return json(['code' => 1, 'data' => $flag['data'], 'msg' => '已禁用']);
        }
        else
        {
            $flag = Db::name('theclass')->where('id',$id)->setField(['status'=>1]);
            return json(['code' => 0, 'data' => $flag['data'], 'msg' => '已开启']);
        }
    }

    /**
     * [add_cate 添加课程]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function theclass_add()
    {
        $edu = new TheclassModel();
        $instit_id = session('instit_id');
        if(request()->isAjax()){
            $param = input('post.');
            $param['instit_id'] = $instit_id;
            if(empty($param['status'])){
                $param['status'] = 0;
            }

//            logResult(json_encode($param));

            $flag = $edu->insertTheclass($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }

        $instit_id = session('instit_id');
        $instit_info = Db::name('instit')->where('id',$instit_id)->find();
        $this->assign('nianji',$edu->getAllnianji($instit_info['school']));
        $this->assign('teach',$edu->getLeaderteach($instit_id));
        return $this->fetch();
    }


    /**
     * [edit_cate 编辑机构]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function theclass_edit()
    {
        $cate = new TheclassModel();
        if(request()->isAjax()){
            $param = input('post.');
            if(empty($param['status'])){
                $param['status']=0;
            }
            $flag = $cate->updateTheclass($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }

        $id = input('param.id');
        $this->assign('theclass',$cate->getOneTheclass($id));

        $instit_id = session('instit_id');
        $instit_info = Db::name('instit')->where('id',$instit_id)->find();
        $this->assign('nianji',$cate->getAllnianji($instit_info['school']));
        $this->assign('teach',$cate->getLeaderteach($instit_id));
        return $this->fetch();
    }


    public function class_student_import()
    {
        $edu = new TheclassModel();
        if(request()->isPost()){

            $param = input('post.');
            require_once getcwd() . '/phpexcel/Classes/PHPExcel.php';     //修改为自己的目录
            require_once getcwd() . '/phpexcel/Classes/PHPExcel/IOFactory.php';     //修改为自己的目录
            require_once getcwd() . '/phpexcel/Classes/PHPExcel/Reader/Excel5.php';     // 用于其他低版本xls
            require_once getcwd() . '/phpexcel/Classes/PHPExcel/Reader/Excel2007.php'; // 用

            echo $filePath = getcwd() . '/uploads/file/'.$param['ufile'];
            $PHPExcel = new \PHPExcel();
            /**默认用excel2007读取excel，若格式不对，则用之前的版本进行读取*/
            $PHPReader = new \PHPExcel_Reader_Excel2007();
            if (!$PHPReader->canRead($filePath)) {
                $PHPReader = new \PHPExcel_Reader_Excel5();
                if (!$PHPReader->canRead($filePath)) {
                    echo 'no Excel';
                    return;
                }
            }
            $PHPExcel = $PHPReader->load($filePath);
            $currentSheet = $PHPExcel->getSheet(0);  //读取excel文件中的第一个工作表
            $allColumn = $currentSheet->getHighestColumn(); //取得最大的列号
            $allRow = $currentSheet->getHighestRow(); //取得一共有多少行
            $erp_orders_id = array();  //声明数组

            /**从第二行开始输出，因为excel表中第一行为列名*/
            for ($currentRow = 2; $currentRow <= $allRow; $currentRow++) {
                /**从第A列开始输出*/
                for ($currentColumn = 'A'; $currentColumn <= $allColumn; $currentColumn++) {
                    $val = $currentSheet->getCellByColumnAndRow(ord($currentColumn) - 65, $currentRow)->getValue();
                    /**ord()将字符转为十进制数*/
                    $excel_data[$currentRow][] = $val;
                }
            }

            $nowyear = date('Y');

            $nowtime = strtotime($nowyear.'-08-25');

            if($nowtime>time()){
                $year = $nowyear-1;
            }else{
                $year = $nowyear;

            }


            foreach($excel_data as $key=>$val) {
                $info = array(
                    'instit_id' => session('instit_id'),
                    'account' => $val[1],
                    'class_id'=>$param['class_id'],
                    'year'=>$year
                );
                $count1 = Db::name('class_student')->where($info)->count();//计算总页面

                if($count1==0){
                    $studut = array(
                        'instit_id' => session('instit_id'),
                        'account' => $val[1],
                        'name' => $val[0],
                        'class_id' => $param['class_id'],
                        'instit_id' => session('instit_id'),
                        'year' => $year,

                    );

                    $flag = $edu->importClassStudent($studut);




                }
            }


            $this->redirect('Theclass/clss_student_list', ['class_id' => $param['class_id'] ]);


        }

        $class_id = input('param.class_id');
        $this->assign('class_id',$class_id);
        return $this->fetch();
    }


    public function class_student_list(){

        $class_id = input('class_id');
        $nowyear = date('Y');
        $nowtime = strtotime($nowyear.'-08-25');
        if($nowtime>time()){
            $year = $nowyear-1;
        }else{
            $year = $nowyear;
        }
        $map5 = array(
            'class_id'=>  $class_id,
            'year'=>$year
        );
        $list = DB::name('class_student')->where($map5)->select();

        if(input('get.page')){

            return json($list);

        }


        $nianji = Db::name('theclass')->where('id',$class_id)->find();

        $this->assign('nianji',$nianji);
        $this->assign('class_id',$class_id);
        return $this->fetch();

    }



}