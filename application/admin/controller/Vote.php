<?php

namespace app\admin\controller;
use app\admin\model\InstitModel;
use app\admin\model\VoteModel;
use think\Db;

class Vote extends Base
{

   
    public function index()
    {
        $instit_id = session('instit_id');
        $key = input('key');
        $map = [];
        if($key&&$key!=="")
        {
            $map['title'] = ['like',"%" . $key . "%"];
        }
        $map['is_closed'] = 0;
        $map['instit_id'] = $instit_id;
        $Nowpage = input('get.page') ? input('get.page') : 1;
        $limits = 12;// 获取总条数
        $count = Db('vote')->where($map)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));
        $ad = new VoteModel();
        $lists = $ad->getvoteAll($map, $Nowpage, $limits);
        foreach($lists as $k => $v){
            $lists[$k]['starttime1'] = date('Y-m-d',$v['starttime']);
            $lists[$k]['endtime1'] = date('Y-m-d',$v['endtime']);
        }

        $this->assign('count', $count); //当前页
        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('val', $key);
        if (input('get.page')) {
            return json($lists);
        }
        return $this->fetch();
    }


    public function choose()
    {
        $id = input('param.id');
        $key = input('key');
        $map = [];
        if($key&&$key!=="")
        {
            $map['desc'] = ['like',"%" . $key . "%"];
        }

        $map['vote_id'] = $id;
        $Nowpage = input('get.page') ? input('get.page') : 1;
        $limits = 12;// 获取总条数
        $count = Db('vote_choose')->where($map)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));
        $ad = new VoteModel();
        $lists = $ad->getvotechooseAll($map, $Nowpage, $limits);

        foreach($lists as $k=>$v){
                $map = [];
                $map['vote_id'] = $id;
                $v_id = $v['id'];
                $map['choose'] = ['like',"%," . $v_id . ",%"];
                $count_vt = Db::name('vote_log')->field('count(id) as num')->where($map)->find();
                $lists[$k]['num'] = $count_vt['num'];
        }


        $this->assign('count', $count); //当前页
        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('val', $key);
        if (input('get.page')) {
            return json($lists);
        }
        $this->assign('id', $id);
        return $this->fetch();
    }


    /**
     * [add_ad 添加投票]
     * @return [type] [description]
     * @author [沈晶晶] [864491238@qq.com]
     */
    public function add()
    {
        $instit_id = session('instit_id');

        if (request()->isAjax()) {

            $param = input('post.');
            $param['instit_id'] = $instit_id;
            $param['addtime'] = time();
            $param['starttime'] = strtotime($param['starttime']);
            $param['endtime'] = strtotime($param['endtime']);

            $vote = new VoteModel();
            $flag = $vote->insertvote($param);

            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }

        return $this->fetch();

    }

    public function edit()
    {
        $id = input('param.id');

        if (request()->isAjax()) {

            $param = input('post.');
            $param['starttime'] = strtotime($param['starttime']);
            $param['endtime'] = strtotime($param['endtime']);
            $vote = new VoteModel();
            $flag = $vote->editvote($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }

        $vote = Db::name('vote')->where('id',$id)->find();
        $vote['starttime1'] = date('Y-m-d',$vote['starttime']);
        $vote['endtime1'] = date('Y-m-d',$vote['endtime']);
        $this->assign('vote',$vote);
        return $this->fetch();

    }




    public function add_choose()
    {
        $id = input('param.id');
        if (request()->isAjax()) {
            $param = input('post.');
            unset($param['file']);
            $vote = new VoteModel();
            $flag = $vote->insertchoose($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }

        $this->assign('id',$id);
        return $this->fetch();

    }


    public function edit_choose()
    {
        $id = input('param.id');
        if (request()->isAjax()) {
            $param = input('post.');
            unset($param['file']);
            $vote = new VoteModel();
            $flag = $vote->editchoose($param);

            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }


        $vote = Db::name('vote_choose')->where('id',$id)->find();

        $this->assign('id',$id);
        $this->assign('vote_id',$vote['vote_id']);
        $this->assign('vote',$vote);
        return $this->fetch();

    }


    public function del_choose()
    {
        $id = input('param.id');
        $ad = new VoteModel();
        $flag = $ad->del_choose($id);
        return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
    }






    /**
     * [del_ad 删除投票]
     * @return [type] [description]
     * @author [沈晶晶] [864491238@qq.com]
     */
    public function del_vote()
    {
        $id = input('param.id');
        $ad = new VoteModel();
        $flag = $ad->del_vote($id);
        return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
    }


    public function del_notice()
    {
        $id = input('param.id');
        $ad = new VoteModel();
        $flag = $ad->delNotice($id);
        return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
    }


    public function notice()
    {
        $key = input('key');
        $map = [];

        $Nowpage = input('get.page') ? input('get.page') : 1;
        $limits = 12;// 获取总条数
        $count = Db('notice')->where($map)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));
        $ad = new VoteModel();
        $lists = $ad->getNoticeAll($map, $Nowpage, $limits);
        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('val', $key);
        if (input('get.page')) {
            return json($lists);
        }
        return $this->fetch();
    }


    public function add_notice()
    {
        if (request()->isAjax()) {
            $param = input('post.');
            $param['addtime'] = time();
            $vote = new VoteModel();
            $flag = $vote->insertNotice($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        } else {
            return $this->fetch();
        }
    }

    public function office()
    {
        $key = input('key');
        $map = [];

        $Nowpage = input('get.page') ? input('get.page') : 1;
        $limits = 12;// 获取总条数
        $map['send_id'] = session('instit_id');
        $map['receive_id'] = session('instit_id');

        $count = Db('office')->whereor($map)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));
        $ad = new VoteModel();
        $lists = $ad->getOfficeAll($map, $Nowpage, $limits);
        foreach($lists as $k=>$v){
            $send_info = Db::name('instit')->field('name')->where('id',$v['send_id'])->find();
            $receive_info = Db::name('instit')->field('name')->where('id',$v['receive_id'])->find();
            $lists[$k]['send_name'] = $send_info['name'];
            $lists[$k]['receive_name'] = $receive_info['name'];
        }
        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('val', $key);
        if (input('get.page')) {
            return json($lists);
        }
        return $this->fetch();
    }



    public function del_office()
    {
        $id = input('param.id');
        $ad = new VoteModel();
        $flag = $ad->del_office($id);
        return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
    }


    public function add_office()
    {
        if (request()->isAjax()) {
            $param = input('post.');
            $param['addtime'] = time();
            $param['send_id'] = session('instit_id');
            $vote = new VoteModel();
            $flag = $vote->insertOffice($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        } else {

            $map = array(
                'pid'=>session('instit_id'),
                'type'=>1
            );

            if(session('instit_id')==1){
                $map['pid'] = 0;
            }


            $instit_list = Db::name('instit')->field('id,name')->where($map)->select();
            $this->assign('instit',$instit_list);
            return $this->fetch();
        }
    }





}