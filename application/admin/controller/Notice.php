<?php

namespace app\admin\controller;
use app\admin\model\NoticeModel;
use think\Db;

class Notice extends Base
{

    //*********************************************广告列表*********************************************//
    /**
     * [index 广告列表]
     * @return [type] [description]
     * @author [田建龙] [864491238@qq.com]
     */
    public function index(){

        $key = input('key');
        $map = [];
        if($key&&$key!=="")
        {
            $map['title'] = ['like',"%" . $key . "%"];
        }
        $Nowpage = input('get.page') ? input('get.page'):1;
        $limits = 10;// 获取总条数
        $count = Db::name('notice')->where($map)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));
        $notice = new NoticeModel();
        $lists = $notice->getnoticeAll($map, $Nowpage, $limits);
        foreach($lists as $k=>$v){
            $lists[$k]['addtime1'] = date('Y-m-d H:i:s',$v['addtime']);
        }

        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('val', $key);
        if(input('get.page'))
        {
            return json($lists);
        }
        return $this->fetch();
    }


    /**
     * [noticed_notice 添加广告]
     * @return [type] [description]
     * @author [田建龙] [864491238@qq.com]
     */
    public function add_notice()
    {
        if(request()->isAjax()){

            $param = input('post.');
            $notice = new NoticeModel();
            $flag = $notice->insertnotice($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }
        return $this->fetch();

    }

    public function edit_notice()
    {
        $notice = new NoticeModel();
        if(request()->isPost()){
            $param = input('post.');
            $flag = $notice->editNotice($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }
        $id = input('param.id');
        $this->assign('notice',$notice->getOneNotice($id));
        return $this->fetch();
    }


    public function del_notice()
    {
        $id = input('param.id');
        $notice = new NoticeModel();
        $flag = $notice->delNotice($id);
        return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
    }


    public function notice_state()
    {
        $id=input('param.id');
        $status = Db::name('notice')->where(array('id'=>$id))->value('status');//判断当前状态情况
        if($status==1)
        {
            $flag = Db::name('notice')->where(array('id'=>$id))->setField(['status'=>0]);
            return json(['code' => 1, 'data' => $flag['data'], 'msg' => '已禁止']);
        }
        else
        {
            $flag = Db::name('notice')->where(array('id'=>$id))->setField(['status'=>1]);
            return json(['code' => 0, 'data' => $flag['data'], 'msg' => '已开启']);
        }

    }




}