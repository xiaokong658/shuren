<?php

namespace app\admin\controller;
use app\admin\model\CourseModel;
use think\Db;

class Course extends Base
{

    public function index(){

        $uid= session('uid');
        $instit_id = session('instit_id');
        //print_R($admin);
        $key = input('key');
        $map = [];
        if($key&&$key!==""){
            $map['shuren_course.name'] = ['like',"%" . $key . "%"];
        }

        $Nowpage = input('get.page') ? input('get.page'):1;
        $limits = 10;// 获取总条数
        if($uid>1){
            $map['shuren_course.instit_id'] = $instit_id;
        }
        $count = Db::name('course')->where($map)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));
        $article = new CourseModel();
        $lists = $article->getCourseByWhere($map, $Nowpage, $limits);
        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('count', $count);
        $this->assign('val', $key);
        $this->assign('list', $lists);
        if(input('get.page')){
            return json($lists);
        }
        return $this->fetch();
    }

    public function course_state(){
        $id = input('param.id');
        $status = Db::name('course')->where('id',$id)->value('status');//判断当前状态情况
        if($status==1)
        {
            $flag = Db::name('course')->where('id',$id)->setField(['status'=>0]);
            return json(['code' => 1, 'data' => $flag['data'], 'msg' => '已禁用']);
        }
        else
        {
            $flag = Db::name('course')->where('id',$id)->setField(['status'=>1]);
            return json(['code' => 0, 'data' => $flag['data'], 'msg' => '已开启']);
        }
    }

    /**
     * [add_cate 添加课程]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function course_add()
    {
        $instit_id = session('instit_id');
        if(request()->isAjax()){
            $param = input('post.');
            $edu = new CourseModel();
            $param['instit_id'] = $instit_id;
            $flag = $edu->insertCourse($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }
        return $this->fetch();
    }


    /**
     * [edit_cate 编辑课程]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function course_edit()
    {
        $cate = new CourseModel();
        if(request()->isAjax()){
            $param = input('post.');
            if(empty($param['status'])){
                $param['status']=0;
            }
            $flag = $cate->updateCourse($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }

        $id = input('param.id');
        $this->assign('course',$cate->getOneCourse($id));
        return $this->fetch();
    }

    public function nianji_course(){
        $uid= session('uid');
        $instit_id = session('instit_id');
        $instit_info = Db::name('instit')->field('school,name')->where('id',$instit_id)->find();
        $map['schoollevel'] = $instit_info['school'];
        $list = Db::name('nianji')->where($map)->order('id asc')->select();//计算总页面
        $this->assign('list', $list);
        $this->assign('instit_info', $instit_info);
        return $this->fetch();
    }


    public function nianji_course_edit(){
        $uid= session('uid');
        $instit_id = session('instit_id');
        $id = input('param.id');
        if(request()->isAjax()){
            $param = input('post.');
            $res = array();
            $map  = array(
                'nianji'=>$param['nianji'],
                'instit_id'=>$instit_id,
            );

            $info = Db::name('theclass_course')->where($map)->find();
            if(empty($param['name'])){
                $result = Db::name('theclass_course')->where('id', $info['id'])
                    ->update(['content' => '']);
                return ['code' => 1, 'data' => '', 'msg' => '年级修改成功'];
            }



            foreach($param['name'] as $key=>$vv){
                $res[$key]['id'] = $key+1;
                $res[$key]['name'] = $vv;
                $res[$key]['num'] = intval($param['num'][$key]);
            }






            if($info){
                $result = Db::name('theclass_course')->where('id', $info['id'])
                    ->update(['content' => json_encode($res)]);
                return ['code' => 1, 'data' => '', 'msg' => '年级修改成功'];
            }else{
                $map['content'] = json_encode($res);
                $result= Db::name('theclass_course')->insert($map);
                return ['code' => 1, 'data' => '', 'msg' => '年级设置成功'];
            }

        }


        $map = array(
            'instit_id'=>$instit_id,
            'status'=>1,
        );
        $map1 = array(
            'instit_id'=>$instit_id,
            'nianji'=>$id,
        );

        $this->assign('course',Db::name('course')->where($map)->select());

        $info  = Db::name('theclass_course')->where($map1)->find();
        $this->assign('info',json_decode($info['content'],true));
        $this->assign('id',$id);

       // $this->assign('course',);
        return $this->fetch();

    }






}