<?php

namespace app\admin\controller;
use app\admin\model\InstitModel;
use app\admin\model\LetterModel;
use think\Db;

class Letter extends Base
{

    //*********************************************站内信列表*********************************************//
    /**
     * [index 站内信列表]
     * @return [type] [description]
     * @author [田建龙] [864491238@qq.com]
     */
    public function index()
    {
        $instit_id = session('instit_id');
        $key = input('key');
        $map = [];
//        if($key&&$key!=="")
//        {
//            $map['title'] = ['like',"%" . $key . "%"];
//        }
        $map['instit_id'] = $instit_id;
        $Nowpage = input('get.page') ? input('get.page') : 1;
        $limits = 12;// 获取总条数
        $count = Db('inside_letter')->where($map)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));
        $ad = new LetterModel();
        $lists = $ad->getLetterAll($map, $Nowpage, $limits);
        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('val', $key);
        if (input('get.page')) {
            return json($lists);
        }
        return $this->fetch();
    }


    /**
     * [add_ad 添加站内信]
     * @return [type] [description]
     * @author [田建龙] [864491238@qq.com]
     */
    public function add_letter()
    {
        $instit_id = session('instit_id');
        $instit_id = 2;
        if (request()->isAjax()) {

            $param = input('post.');

            if ($param['type'] == 0) {
                $param['user_id'] = 0;

            }
            $param['instit_id'] = $instit_id;
            $param['create_time'] = time();

            $letter = new LetterModel();
            $flag = $letter->insertLetter($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }
//        $position = new AdPositionModel();
//        $this->assign('position',$position->getAllPosition());


        $instit_info = Db::name('instit')->where('id', $instit_id)->find();

        $inst = new InstitModel();

        if ($instit_id == 1) {
            $map['id'] = ['neq', 1];
            $instit = $inst->getOpenInstit($map);
            $this->assign('instit', $instit);
        } else {
            if ($instit_info['type'] == 1) {

            }

            if ($instit_info['type'] == 2) {
                $members = Db::name('member')->where('instit_id', $instit_id)->select();
                $this->assign('members', $members);
            }

        }


        $this->assign('instit_info', $instit_info);
        $this->assign('instit_id', $instit_id);
        return $this->fetch();

    }


    /**
     * [edit_ad 编辑站内信]
     * @return [type] [description]
     * @author [田建龙] [864491238@qq.com]
     */
    public function edit_ad()
    {
        $ad = new AdModel();
        if (request()->isPost()) {
            $param = input('post.');
            $flag = $ad->editAd($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }
        $id = input('param.id');
        $this->assign('ad', $ad->getOneAd($id));
        return $this->fetch();
    }


    /**
     * [del_ad 删除站内信]
     * @return [type] [description]
     * @author [田建龙] [864491238@qq.com]
     */
    public function del_letter()
    {
        $id = input('param.id');
        $ad = new LetterModel();
        $flag = $ad->del_letter($id);
        return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
    }


    public function del_notice()
    {
        $id = input('param.id');
        $ad = new LetterModel();
        $flag = $ad->delNotice($id);
        return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
    }


    public function notice()
    {
        $key = input('key');
        $map = [];

        $Nowpage = input('get.page') ? input('get.page') : 1;
        $limits = 12;// 获取总条数
        $count = Db('notice')->where($map)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));
        $ad = new LetterModel();
        $lists = $ad->getNoticeAll($map, $Nowpage, $limits);
        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('val', $key);
        if (input('get.page')) {
            return json($lists);
        }
        return $this->fetch();
    }


    public function add_notice()
    {
        if (request()->isAjax()) {
            $param = input('post.');
            $param['addtime'] = time();
            $letter = new LetterModel();
            $flag = $letter->insertNotice($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        } else {
            return $this->fetch();
        }
    }

    public function office()
    {
        $key = input('key');
        $map = [];

        $Nowpage = input('get.page') ? input('get.page') : 1;
        $limits = 12;// 获取总条数
        $map['send_id'] = session('instit_id');
        $map['receive_id'] = session('instit_id');

        $count = Db('office')->whereor($map)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));
        $ad = new LetterModel();
        $lists = $ad->getOfficeAll($map, $Nowpage, $limits);
        foreach($lists as $k=>$v){
            $send_info = Db::name('instit')->field('name')->where('id',$v['send_id'])->find();
            $receive_info = Db::name('instit')->field('name')->where('id',$v['receive_id'])->find();
            $lists[$k]['send_name'] = $send_info['name'];
            $lists[$k]['receive_name'] = $receive_info['name'];
        }
        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('val', $key);
        if (input('get.page')) {
            return json($lists);
        }
        return $this->fetch();
    }



    public function del_office()
    {
        $id = input('param.id');
        $ad = new LetterModel();
        $flag = $ad->del_office($id);
        return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
    }


    public function add_office()
    {
        if (request()->isAjax()) {
            $param = input('post.');
            $param['addtime'] = time();
            $param['send_id'] = session('instit_id');
            $letter = new LetterModel();
            $flag = $letter->insertOffice($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        } else {

            $map = array(
                'pid'=>session('instit_id'),
                'type'=>1
            );

            if(session('instit_id')==1){
                $map['pid'] = 0;
            }


            $instit_list = Db::name('instit')->field('id,name')->where($map)->select();
            $this->assign('instit',$instit_list);
            return $this->fetch();
        }
    }



}