<?php

namespace app\admin\controller;
use app\admin\model\GongwenModel;
use think\Db;

class Gongwen extends Base
{

    //*********************************************广告列表*********************************************//
    /**
     * [index 广告列表]
     * @return [type] [description]
     * @author [田建龙] [864491238@qq.com]
     */
    public function index(){

        $key = input('key');
        $map = [];
        if($key&&$key!=="")
        {
            $map['title'] = ['like',"%" . $key . "%"];
        }
        $Nowpage = input('get.page') ? input('get.page'):1;
        $limits = 10;// 获取总条数
        $count = Db::name('gongwen')->where($map)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));

        $instit_id= session('instit_id');
        if($instit_id==1){
            $lists = DB::name('gongwen')->where($map)->page($Nowpage,$limits)->order('id desc')->select();
        }else{
            $map1['fa_id'] = $instit_id;
            $map1['shou_id'] = $instit_id;
            $lists = DB::name('gongwen')->where($map)->whereor($map1)->page($Nowpage,$limits)->order('id desc')->select();
        }


        foreach($lists as $k=>$v){
            $lists[$k]['addtime1'] = date('Y-m-d H:i:s',$v['addtime']);

            $inst11 = Db::name('instit')->where('id',$v['fa_id'])->find();
            $inst12 = Db::name('instit')->where('id',$v['shou_id'])->find();
            $lists[$k]['fa_name'] = $inst11['name'];
            $lists[$k]['shou_name'] = $inst12['name'];


        }


        $instit_id = session('instit_id');
        if($instit_id!=1){
            $childcount = Db::name('instit')->where('pid',$instit_id)->count();
            if($childcount>0){
                $can = 1;
            }else{
                $can = 0;
            }
        }else{
            $can = 1;
        }



        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('val', $key);
        $this->assign('can', $can);
        if(input('get.page'))
        {
            return json($lists);
        }
        return $this->fetch();
    }


    /**
     * [gongwend_gongwen 添加广告]
     * @return [type] [description]
     * @author [田建龙] [864491238@qq.com]
     */
    public function add_gongwen()
    {
        if(request()->isAjax()){

            $param = input('post.');
            $param['fa_id'] = session('instit_id');
            $param['addtime'] = time();
            $gongwen = new GongwenModel();
            $flag = $gongwen->insertgongwen($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }

        $instit_id = session('instit_id');
        if($instit_id!=1){
            $instit = Db::name('instit')->where('pid',$instit_id)->select();
        }else{
            $instit = Db::name('instit')->where('id !=1 ')->select();
        }


        $this->assign('instits',$instit);


        return $this->fetch();

    }

    public function edit_gongwen()
    {

        $id = input('param.id');
        $gongwen = new GongwenModel();


        $gongwens = $gongwen->getOneGongwen($id);
        $inst11 = Db::name('instit')->where('id',$gongwens['fa_id'])->find();
        $inst12 = Db::name('instit')->where('id',$gongwens['shou_id'])->find();
        $gongwens['fa_name'] = $inst11['name'];
        $gongwens['shou_name'] = $inst12['name'];
        $this->assign('gongwen',$gongwens);
        return $this->fetch();
    }


    public function del_gongwen()
    {
        $id = input('param.id');
        $gongwen = new GongwenModel();
        $flag = $gongwen->delGongwen($id);
        return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
    }


    public function gongwen_state()
    {
        $id=input('param.id');
        $status = Db::name('gongwen')->where(array('id'=>$id))->value('status');//判断当前状态情况
        if($status==1)
        {
            $flag = Db::name('gongwen')->where(array('id'=>$id))->setField(['status'=>0]);
            return json(['code' => 1, 'data' => $flag['data'], 'msg' => '已禁止']);
        }
        else
        {
            $flag = Db::name('gongwen')->where(array('id'=>$id))->setField(['status'=>1]);
            return json(['code' => 0, 'data' => $flag['data'], 'msg' => '已开启']);
        }

    }




}