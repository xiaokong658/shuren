<?php

namespace app\admin\controller;
use app\admin\model\ProductModel;
use think\Db;

class Product extends Base
{
    /**
     * [index 用户列表]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function product_cate(){
        $key = input('key');
        $map = [];
        if($key&&$key!=="")
        {
            $map['cate'] = ['like',"%" . $key . "%"];
        }
        $Nowpage = input('get.page') ? input('get.page'):1;
        $limits = 10;// 获取总条数
        $count = Db::name('product_cate')->where($map)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));
        $product = new ProductModel();
        $lists = $product->getProductCate($map, $Nowpage, $limits);
        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('val', $key);
        if(input('get.page'))
        {
            return json($lists);
        }

        return $this->fetch();
    }


    /**
     * [index 分类启用/禁用]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function cate_state()
    {
        $id = input('param.id');
        $result =  change_status($id,'product_cate','status');
        return $result;
    }


    public function product_state()
    {
        $id = input('param.id');
        $result =  change_status($id,'product','recommend');
        return $result;
    }

    public function show_state()
    {
        $id = input('param.id');
        $result =  change_status($id,'product','show');
        return $result;
    }




    /**
     * [index 删除分类]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function cateDel()
    {
        $id = input('param.id');
        $result = deletebyid($id,'product_cate');
        return $result;
    }


    /**
     * [index 新增分类]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */

    public function cateAdd()
    {
        if(request()->isAjax()){
            $param = input('post.');
            $accdata = array(
                'cate'=> $param['cate'],
                'sort'=> $param['sort'],
                'status'=> $param['status'],
            );
            try{
                $result = Db::name('product_cate')->insert($accdata);
                if(false === $result){
                    return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
                }else{
                    return ['code' => 1, 'data' => '', 'msg' => '添加产品分类成功'];
                }
            }catch( PDOException $e){
                return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
            }
        }

        return $this->fetch();
    }


    /**
     * [index 修改分类]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function cateEdit()
        {
            $product = new ProductModel();
            if(request()->isAjax()){
                $param = input('post.');
                $accdata = array(
                    'cate'=> $param['cate'],
                    'sort'=> $param['sort'],
                    'status'=> $param['status'],
                );
                $flag = $product->editCate($accdata,$param['id']);
                return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
            }
            $id = input('param.id');
            $this->assign([
                'cate' => $product->getOneCate($id),
            ]);
            return $this->fetch();
        }


    /**
     * [index 添加产品]
     * @return [type] [description]
     * @author [沈晶晶] [864491238@qq.com]
     */
    public function productAdd()
    {
        if(request()->isAjax()){

            $param = input('post.');
            $product = new ProductModel();
            $flag = $product->insertProduct($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }

        $cate = new ProductModel();
        $map = array();
        $this->assign('cate',$cate->getAllProductCate($map));
        return $this->fetch();
    }




    /**
     * [index 用户列表]
     * @return [type] [description]
     * @author [沈晶晶] [864491238@qq.com]
     */
    public function index(){
        $key = input('key');
        $map = [];
        if($key&&$key!=="")
        {
            $map['name'] = ['like',"%" . $key . "%"];
        }
        $Nowpage = input('get.page') ? input('get.page'):1;
        $limits = 10;// 获取总条数
        $count = Db::name('product')->where($map)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));
        $user = new ProductModel();
        $lists = $user->getProductByWhere($map, $Nowpage, $limits);
        foreach($lists as $k=>$v)
        {
            $lists[$k]['addtime']=date('Y-m-d H:i:s',$v['addtime']);
        }
        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('val', $key);
        if(input('get.page'))
        {
            return json($lists);
        }
        return $this->fetch();
    }





    /**
     * [userEdit 编辑产品]
     * @return [type] [description]
     * @author [沈晶晶] [864491238@qq.com]
     */
    public function productEdit()
    {
        $Product = new ProductModel();
        if(request()->isAjax()){

            $param = input('post.');
            $flag = $Product->editProduct($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }
        $id = input('param.id');
        $map= array();
        $this->assign([
            'product' => $Product->getOneProduct($id),
            'cate'=>$Product->getAllProductCate($map),
        ]);
        return $this->fetch();
    }


    /**
     * [UserDel 删除产品]
     * @return [type] [description]
     * @author [沈晶晶] [864491238@qq.com]
     */
    public function productDel()
    {
        $id = input('param.id');
        $Product = new ProductModel();
        $flag = $Product->delProduct($id);
        return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
    }





}