<?php

namespace app\admin\controller;
use app\admin\model\IncomepaymentModel;
use app\admin\model\UserModel;
use think\Db;

class Incomepayment extends Base
{

    /**
     * [index 收支列表]
     * @return [type] [description]
     * @author [沈晶晶] [864491238@qq.com]
     */
    public function index(){
        $key = input('key');
        $map = [];
        if($key&&$key!=="")
        {
            $map['name'] = ['like',"%" . $key . "%"];
        }
        $this->assign('val', $key);
        $Nowpage = input('get.page') ? input('get.page'):1;
        $limits = 10;// 获取总条数
        $count = Db::name('income_payment')->where($map)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));
        $income = new IncomepaymentModel();
        $lists = $income->getincomepaymentByWhere($map, $Nowpage, $limits);
        $income_type = config('income_type');
        $payment_type = config('payment_type');

        foreach($lists as $key=>$val){
            if($val['type']==1){
                $lists[$key]['leixing'] = $income_type[$val['leixing']];
                $lists[$key]['type'] = '收入';
            }elseif($val['type']==2){
                $lists[$key]['leixing'] = $payment_type[$val['leixing']];
                $lists[$key]['type'] = '支出';
            }
            $lists[$key]['addtime']=date('Y-m-d H:i:s',$val['addtime']);
            $lists[$key]['expectedtime']=date('Y-m-d H:i:s',$val['expectedtime']);

        }
        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数

        if(input('get.page'))
        {
            return json($lists);
        }
        return $this->fetch();
    }


    /**
     * [index 添加收支]
     * @return [type] [description]
     * @author [沈晶晶] [864491238@qq.com]
     */
    public function incomeAdd()
    {
        if(request()->isAjax()){
            $param = input('post.');
            $income = new IncomepaymentModel();
            $res = array(
                'name'=>$param['name'],
                'type'=>$param['type'],
                'money'=>$param['money'],
                'content'=>$param['content'],
                'addtime'=>time(),
                'expectedtime'=> strtotime($param['expectedtime'])  ,
                'adduser_id'=>session('uid') ,
                'user_id'=>$param['user_id'],
            );

            if($param['type']==1){
                $res['leixing'] = $param['income_type'];
            }else if($param['type']==2){
                $res['leixing'] = $param['payment_type'];

            }

            $flag = $income->insertIncome($res);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
//            return json(['code' => 1, 'data' =>'', 'msg' =>json_encode($res) ]);
        }

        $user = new UserModel();
        $map = array();
        $this->assign('user',$user->getUsersByWhere($map,0,100));
        return $this->fetch();
    }


    /**
     * [userEdit 编辑收支]
     * @return [type] [description]
     * @author [沈晶晶] [864491238@qq.com]
     */
    public function incomeEdit()
    {
        $income = new IncomepaymentModel();
        if(request()->isAjax()){

            $param = input('post.');

            $res = array(
                'id'=>$param['id'],
                'name'=>$param['name'],
                'type'=>$param['type'],
                'money'=>$param['money'],
                'content'=>$param['content'],
                'expectedtime'=> strtotime($param['expectedtime'])  ,
                'user_id'=>$param['user_id'],
            );

            if($param['type']==1){
                $res['leixing'] = $param['income_type'];
            }else if($param['type']==2){
                $res['leixing'] = $param['payment_type'];

            }

            $flag = $income->editIncome($res);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);

        }
        $id = input('param.id');
        $map= array();
        $info =  $income->getOneIncome($id);
        $info['expectedtime'] = date('Y-m-d',$info['expectedtime']);
        $user = new UserModel();
        $this->assign([
            'info' =>$info,
            'user'=>$user->getUsersByWhere($map,0,100),
        ]);
        return $this->fetch();
    }


    /**
     * [UserDel 删除收支]
     * @return [type] [description]
     * @author [沈晶晶] [864491238@qq.com]
     */
    public function IncomeDel()
    {
        $id = input('param.id');
        $income = new IncomepaymentModel();
        $flag = $income->delIncome($id);
        return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
    }





}