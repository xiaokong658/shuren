<?php

namespace app\admin\controller;
use app\admin\model\InstitModel;
use app\admin\model\MemberModel;
use think\Db;

class Instit extends Base
{

    /**
     * [index_cate 社会机构列表]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function index(){
        $cate = new InstitModel();
        $list = $cate->getAllInstit();
        $this->assign('list',$list);
        return $this->fetch();
    }


    /**
     * [index_cate 教育机构列表]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function edu(){
        $nav = new \org\Leftnav;
        $cate = new InstitModel();
        $list = $cate->getAllEdu();
        $arr = $nav::edu_rule($list);
        $this->assign('list',$arr);
        return $this->fetch();
    }


    /**
     * [add_cate 添加机构]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function add()
    {


        if(request()->isAjax()){
            $param = input('post.');
            $param1 = $param;
            $cate = new InstitModel();
            if(empty($param1['status'])){
                $param1['status']=0;
            }
            $param1['type']=2;
            $param1['create_time'] = time();
            unset($param1['account']);
            $flag = $cate->insertInstit($param1);


            $inst = Db::name('instit')->field('id')->where('name',$param['name'])->find();
            $membermodel = new MemberModel();
            $map5 = array(
                'account'=> $param['account'],
            );
            $count = Db::name('member')->where($map5)->count();//计算总页面

            $member = array(
                'account' => $param['account'],
                'nickname' => $param['name'],
                'realname' => $param['name'],
                'status'=>1,
                'group_id'=>8,
                'create_time'=>time(),
                'instit_id'=>$inst['id']
            );
            if ($count == 0) {
                $member['password'] =  md5(md5('888888') . config('auth_key'));
                $membermodel->insertMember($member);
            }



            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }

        return $this->fetch();
    }


    /**
     * [add_cate 添加教育机构]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function add_edu()
    {
        if(request()->isAjax()){
            $param = input('post.');
            $param1 =$param;
            $edu = new InstitModel();
            $param1['create_time'] = time();
            unset($param1['account']);
            $flag = $edu->insertEdu($param1);

            $inst = Db::name('instit')->field('id')->where('name',$param['name'])->find();
                $membermodel = new MemberModel();
            $map5 = array(
                'account'=> $param['account'],
            );
            $count = Db::name('member')->where($map5)->count();//计算总页面
            $member = array(
                'account' => $param['account'],
                'nickname' => $param['name'],
                'realname' => $param['name'],
                'status'=>1,
                'group_id'=>8,
                'create_time'=>time(),
                'instit_id'=>$inst['id']
            );
            if ($count == 0) {
                $member['password'] =  md5(md5('888888') . config('auth_key'));
                $membermodel->insertMember($member);
            }


            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }
        $pid =  input('pid');
        $edu = new InstitModel();
        if(!empty($pid)){
            $father = $edu->getOneInstit($pid);
            $this->assign('father',$father);
            $this->assign('pid',$pid);
        }else{
            $this->assign('pid',0);
        }
        $nav = new \org\Leftnav;
        $admin_rule = $edu->getAllEdu();
        $arr = $nav::edu_rule($admin_rule);
        $this->assign('list',$arr);
        return $this->fetch();
    }


    public function edu_edit()
    {
        $cate = new InstitModel();
        if(request()->isAjax()){
            $param = input('post.');
            if(empty($param['status'])){
                $param['status']=0;
            }
            $flag = $cate->editinstit($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }
        $edu = new InstitModel();
        $nav = new \org\Leftnav;
        $admin_rule = $edu->getAllEdu();
        $arr = $nav::edu_rule($admin_rule);
        $this->assign('list',$arr);
        $id = input('param.id');


        $this->assign('instit',$cate->getOneInstit($id));
        return $this->fetch();
    }




    /**
     * [edit_cate 编辑机构]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function edit()
    {
        $cate = new InstitModel();
        if(request()->isAjax()){
            $param = input('post.');
            if(empty($param['status'])){
                $param['status']=0;
            }
            $flag = $cate->editinstit($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }

        $id = input('param.id');
        $this->assign('instit',$cate->getOneInstit($id));
        return $this->fetch();
    }


    public function instit_state()
    {
        $id = input('param.id');
        $result =  change_status($id,'instit','status');
        return $result;

    }


    public function del()
    {
        $id = input('param.id');
        $edu = new InstitModel();
        $child = $edu->have_child($id);

        if($child){
            return json(['code' => '-1', 'data' =>'', 'msg' =>'该机构有下属机构，无法直接删除' ]);
        }


        $flag = $edu->delInstit($id);
        return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
    }



    public function instit_apply()
    {
     //   $instit_id = session('instit_id');
        $key = input('key');
        $map = [];
        if($key&&$key!=="")
        {
            $map['title'] = ['name',"%" . $key . "%"];
        }
        $Nowpage = input('get.page') ? input('get.page') : 1;
        $limits = 12;// 获取总条数
        $count = Db('instit_apply')->where($map)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));
        $ad = new InstitModel();
        $lists = $ad->getInstitApply($map, $Nowpage, $limits);

        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('val', $key);
        if (input('get.page')) {
            return json($lists);
        }
        return $this->fetch();
    }



    public  function  instit_apply_status(){
        $id = input('param.id');
        $status = Db::name('instit_apply')->where('id',$id)->value('status');//判断当前状态情况
        if($status==1)
        {
            $flag = Db::name('instit_apply')->where('id',$id)->setField(['status'=>0]);
            return json(['code' => 1, 'data' => $flag['data'], 'msg' => '未处理']);
        }
        else
        {
            $flag = Db::name('instit_apply')->where('id',$id)->setField(['status'=>1]);
            return json(['code' => 0, 'data' => $flag['data'], 'msg' => '已处理']);
        }
    }


    public function qrcode()
    {
        $id = session('instit_id');

        $cate = new InstitModel();
        if(request()->isAjax()){
            $param = input('post.');
           $param['id'] = $id;
            unset($param['file']);
            $flag = $cate->editinstit($param);

            if($flag['code']==1){
                $flag['msg'] = '修改二维码成功';
            }

            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }
        $this->assign('instit',$cate->getOneInstit($id));
        return $this->fetch();
    }



}