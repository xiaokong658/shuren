<?php

namespace app\admin\controller;
use app\admin\model\UserModel;
use app\admin\model\InstitModel;
use app\admin\model\UserType;
use think\Db;

class User extends Base
{

    /**
     * [index 用户列表]
     * @return [type] [description]
     * @author [田建龙] [864491238@qq.com]
     */
    public function index(){


        $key = input('key');
        $map = [];
        $map1 = [];
        if($key&&$key!=="")
        {
            $map['username'] = ['like',"%" . $key . "%"];          
            $map1['username'] = ['like',"%" . $key . "%"];
        }

        $admin_id = session('instit_id');
        if($admin_id>1){
            $map['shuren_admin.instit_id'] = $admin_id;
            $map1['shuren_admin.instit_id'] = $admin_id;
        }

        $Nowpage = input('get.page') ? input('get.page'):1;
        $limits = 10;// 获取总条数
        $count = Db::name('admin')->where($map)->count();//计算总页面

//        echo Db::name('admin')->getLastSql();


        $allpage = intval(ceil($count / $limits));
        $user = new UserModel();



        $lists = $user->getUsersByWhere($map1, $Nowpage, $limits);
//        echo "<br>";
//        echo Db::name('admin')->getLastSql();

        foreach($lists as $k=>$v)
        {
            if($v['last_login_time']>0){
                $lists[$k]['last_login_time1']=date('Y-m-d H:i:s',$v['last_login_time']);
            }else{
                $lists[$k]['last_login_time1']='';
            }

        }
        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数 
        $this->assign('val', $key);
        if(input('get.page'))
        {

//            $admin_id = session('uid');
//            $user = new UserModel();
//            $res = $user->UserCanOperation($admin_id);  //此用户是否允许添加角色
//            if($res==0){
//                return ['code' => -1, 'data' => '', 'msg' => '无权操作'];
//            }
            return json($lists);
        }

      //  print_R($lists);


        return $this->fetch();
    }


    /**
     * [userAdd 添加用户]
     * @return [type] [description]
     * @author [田建龙] [864491238@qq.com]
     */
    public function userAdd()
    {
       $admin_id = session('uid');
        if(request()->isAjax()){
            $user = new UserModel();
//            $res = $user->UserCanOperation($admin_id);  //此用户是否允许添加角色
//            if($res==0){
//                return ['code' => -1, 'data' => '', 'msg' => '无权操作'];
//            }
            $param = input('post.');
            $param['password'] = md5(md5($param['password']) . config('auth_key'));
            $flag = $user->insertUser($param);


           // logResult($flag);

            logResult(json_encode($flag));


            if($flag['code']==1){
                $accdata = array(
                    'uid'=> $flag['uid'],
                    'group_id'=> $param['groupid'],
                );
                $group_access = Db::name('auth_group_access')->insert($accdata);
            }
                return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }
        if($admin_id!=1){
          $admin_info =  Db::name('admin')->field('instit_id')->where('id ='.$admin_id)->find();
            echo $admin_info['instit_id'];
            $this->assign('instit_id',$admin_info['instit_id']);
        }
        $role = new UserType();
        $this->assign('role',$role->getRole());
        $Instit = new InstitModel();
        $this->assign('instit',$Instit->getOpenInstit());
        $this->assign('admin_id',$admin_id);
        return $this->fetch();
    }

    /**
     * [userEdit 编辑用户]
     * @return [type] [description]
     * @author [田建龙] [864491238@qq.com]
     */
    public function userEdit()
    {
        $user = new UserModel();
        if(request()->isAjax()){
         //   $admin_id = session('uid');

//            $res = $user->UserCanOperation($admin_id);  //此用户是否允许添加角色
//            if($res==0){
//                return ['code' => -1, 'data' => '', 'msg' => '无权操作'];
//            }
            $param = input('post.');
            if(empty($param['password'])){
                unset($param['password']);
            }else{
                $param['password'] = md5(md5($param['password']) . config('auth_key'));
            }



            $flag = $user->editUser($param);
            $group_access = Db::name('auth_group_access')->where('uid', $user['id'])->update(['group_id' => $param['groupid']]);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }
        $id = input('param.id');
        $role = new UserType();
        $Instit = new InstitModel();
        $this->assign([
            'user' => $user->getOneUser($id),
            'role' => $role->getRole(),
            'instit' => $Instit->getOpenInstit()
        ]);

       // echo xml($user->getOneUser($id));

        return $this->fetch();
    }


    /**
     * [UserDel 删除用户]
     * @return [type] [description]
     * @author [田建龙] [864491238@qq.com]
     */
    public function UserDel()
    {
//        $admin_id = session('uid');
//        $user = new UserModel();
//        $res = $user->UserCanOperation($admin_id);  //此用户是否允许添加角色
//        if($res==0){
//            return ['code' => -1, 'data' => '', 'msg' => '无权操作'];
//        }
        $id = input('param.id');
        $role = new UserModel();
        $flag = $role->delUser($id);
        return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
    }



    /**
     * [user_state 用户状态]
     * @return [type] [description]
     * @author [田建龙] [864491238@qq.com]
     */
    public function user_state()
    {
        $id = input('param.id');
        $status = Db::name('admin')->where('id',$id)->value('status');//判断当前状态情况
        if($status==1)
        {
            $flag = Db::name('admin')->where('id',$id)->setField(['status'=>0]);
            return json(['code' => 1, 'data' => $flag['data'], 'msg' => '已禁止']);
        }
        else
        {
            $flag = Db::name('admin')->where('id',$id)->setField(['status'=>1]);
            return json(['code' => 0, 'data' => $flag['data'], 'msg' => '已开启']);
        }
    }

    





}