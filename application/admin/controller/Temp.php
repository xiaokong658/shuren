<?php

namespace app\admin\controller;
use app\admin\model\TempModel;
use think\Db;
use think\Log;

class Temp extends Base
{

    public function index(){

        $uid = session('uid');
        $name =  DB::name('admin')->field('username')->where('id',$uid)->find();

        $map2 = array(
            'mobile'=>$name['username'],
            'instit_id'=>session('instit_id'),
            'is_leader'=>1
        );

        $teach =  DB::name('teach')->field('id')->where($map2)->find();
        if($teach){
            $map3 = array(
                'leader'=>$teach['id'],
                'instit_id'=>session('instit_id'),
                'status'=>1
            );
            $banji_info = DB::name('theclass')->where($map3)->find();
        }

        if($teach&&$banji_info){
            $data = array(
                'is_send' => 1,
                'class_id'=>$banji_info['id']
            );
            $auth = 'teach';
        }else{
            $data = array(
                'is_send' => 0,
                'class_id'=>''
            );
        }



        $auth_name = Db::name('auth_group_access')->alias('a')->join('auth_group g','a.group_id = g.id')->where('a.uid',session('uid'))->find();
       // print_R($auth_name);
        if($auth_name['title']=='超级管理员'||$auth_name['title']=='学校管理员'||$auth_name['title']=='测试11'){
            $auth = 'admin';
        }



        $key = input('key');
        $map = [];


        $Nowpage = input('get.page') ? input('get.page'):1;
        $limits = 10;// 获取总条数


        $nowyear = date('Y');
        $nowtime = strtotime($nowyear.'-08-25');
        if($nowtime>time()){
            $year = $nowyear-1;
        }else{
            $year = $nowyear;
        }



        if($auth=='admin'){
            $map['shuren_temp_message.instit_id'] = session('instit_id');
            $map1['instit_id'] = session('instit_id');
            $count = Db::name('temp_message')->where($map1)->count();//计算总页面
            $allpage = intval(ceil($count / $limits));
            $user = new TempModel();
            $lists = $user->getTempByWhere($map, $Nowpage, $limits);

        }elseif($auth=='teach'){
            $map = array();
            $map2= array();
            $map['shuren_temp_message.instit_id'] = session('instit_id');
            $map['shuren_temp_message.year'] = $year;

            $map2['shuren_temp_message.type'] =  ['in','2,3'];
            $map12['type'] = ['in','2,3'];

            $map2['shuren_temp_message.class_id'] =  $data['class_id'];
            $map12['class_id'] = $data['class_id'];

            $map1['instit_id'] = session('instit_id');
            $map1['year'] = $year;

            $count = Db::name('temp_message')->where($map1)->whereor($map12)->count();//计算总页面
            $allpage = intval(ceil($count / $limits));
            $user = new TempModel();
           // $lists = $user->getTempByWhereTeach($map,$map2, $Nowpage, $limits);
            $lists =  Db::name('temp_message')->field('shuren_temp_message.*,shuren_instit.name')->join('shuren_instit','shuren_temp_message.instit_id = shuren_instit.id')->where("shuren_temp_message.instit_id = ". session('instit_id')." and year = ".$year)->where("shuren_temp_message.class_id = ".$data['class_id'] ." or shuren_temp_message.type in (2,3)")->page($Nowpage, $limits)->order('id desc')->select();
        }else{
            $map['shuren_temp_message.instit_id'] = session('instit_id');
            $map['shuren_temp_message.year'] = $year;
            $map['shuren_temp_message.type'] =  ['in','2,3'];
            $map1['instit_id'] = session('instit_id');
            $map1['year'] = $year;
            $map1['type'] = ['in','2,3'];
            $count = Db::name('temp_message')->where($map1)->count();//计算总页面
            $allpage = intval(ceil($count / $limits));
            $user = new TempModel();
            $lists = $user->getTempByWhere($map, $Nowpage, $limits);
        }


        foreach($lists as $k=>$v){
            $contents = json_decode($v['content'],true);
            if($v['type']=='1'){
               $class_info =  Db::name('theclass')->alias('a')->field('n.nianji,a.banji')->join('nianji n','a.nianji = n.id')->where('a.id',$data['class_id'])->find();
                $lists[$k]['duixiang'] = $class_info['nianji'].$class_info['banji'];
                $lists[$k]['message'] = $contents['keyword4'];
            }elseif($v['type']=='2'){
                $lists[$k]['duixiang'] = '全体学生';
                $lists[$k]['message'] = $contents['keyword4'];
            }elseif($v['type']=='3'){
                $lists[$k]['duixiang'] = '全体教师';
                $lists[$k]['message'] =  $contents['keyword3'];
            }





        }

        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('val', $key);
        
        if(input('get.page'))
        {
            return json($lists);
        }


        $this->assign('data',$data);
        $this->assign('auth',$auth);
        return $this->fetch();
    }


    public  function  add(){
        $uid = session('uid');
        $name =  DB::name('admin')->field('username')->where('id',$uid)->find();

        $map2 = array(
            'mobile'=>$name['username'],
            'instit_id'=>session('instit_id'),
            'is_leader'=>1
        );
        $teach =  DB::name('teach')->field('id')->where($map2)->find();
        if($teach){
            $map3 = array(
                'leader'=>$teach['id'],
                'instit_id'=>session('instit_id'),
                'status'=>1
            );
            $banji_info = DB::name('theclass')->where($map3)->find();
        }

        if($teach&&$banji_info){
            $data = array(
                'is_send' => 1,
                'role'=>'teach',
                'class_id'=>$banji_info['id']
            );
        }else{
            $this->redirect('temp/index');
        }

        if(request()->isAjax()){
            $param = input('post.');
            $res['temp'] = $param['temp'];
            $res['class_id'] = $param['class_id'];
            $rv['first'] = $param['first'];
            $rv['keyword1'] = $param['keyword1'];
            $rv['keyword2'] = $param['keyword2'];
            $rv['keyword3'] = $param['keyword3'];
            $rv['keyword4'] = $param['keyword4'];
            $rv['remark'] = $param['remark'];
            $res['is_url'] = $param['is_url'];
            $res['from_username'] = $name['username'];
            $res['instit_id'] = session('instit_id');
            $res['addtime'] = time();
            if($param['temp']=='作业提醒'){
                $res['type'] =1 ;
            }else if($param['temp']=='班级通知'){
                $res['type'] =1;
            }
            $res['content'] = json_encode($rv);
            $nowyear = date('Y');
            $nowtime = strtotime($nowyear.'-08-25');
            if($nowtime>time()){
                $year = $nowyear-1;
            }else{
                $year = $nowyear;
            }
            $res['year'] = $year;
            $flag =   Db::name('temp_message')->insert($res);
            $maps = array(
                'a.class_id'=>$data['class_id'],
                'a.instit_id'=>session('instit_id'),
                'm.open_id'=>['neq',''],
                'a.year'=>$year,
            );
            $list = Db::name('class_student')->alias('a')->field('m.open_id')->join('member m','a.account=m.account')->where($maps)->select();
            if($flag){
                foreach($list as $k => $v){
                    $data['touser']		= $v['open_id'];
                    if($param['temp']=='作业提醒'){
                    $data['template_id']= '62S7ofIhYcIxG3jbVYbMYIjcR2gsz0P3g2A1GJD-VVw';
                    }else if($param['temp']=='班级通知'){
                        $data['template_id']= 'NlMKUksfLQMyKVoabAFn3LWvypZFIFzQSFQktqIfTLI';
                    }
                    // $data['url']		= "http://".$_SERVER["HTTP_HOST"]."/index.php?m=default&c=user&a=order_tracking&order_id=".$order_id;
                    $data['topcolor']	= '#7B68EE';
                    $data['data']['first']['value']	= $param['first'];
                    $data['data']['first']['color']	= '#743A3A';
                    $data['data']['keyword1']['value']	=$param['keyword1'];
                    $data['data']['keyword1']['color']	= '#666666';
                    $data['data']['keyword2']['value']	= $param['keyword2'];
                    $data['data']['keyword2']['color']	=  '#666666';
                    $data['data']['keyword3']['value']	= $param['keyword3'];
                    $data['data']['keyword3']['color']	=  '#666666';
                    $data['data']['keyword4']['value']	= $param['keyword4'];
                    $data['data']['keyword4']['color']	=  '#666666';
                    $data['data']['remark']['value']	=  $param['remark'];
                    $data['data']['remark']['color']	= '#3300CC';
                    $result = send_temp_message(session('instit_id'),$data);
                }

                $msd = array(
                    'instit_id'=>session('instit_id'),
                    'group_id'=>8
                );
                $s_user =  Db::name('member')->where($msd)->find();
                if($s_user['user_id']){
                    $template_integral = $this->get_config_v('template_integral');
                    if($template_integral>0){
                        add_integral($s_user['user_id'],$template_integral,'template_integral');
                    }
                }


                return json(['code' => 1,  'msg' => '模板消息发送成功']);
            }else{
                return json(['code' => -1,  'msg' => '模板消息发送失败']);
            }
        }

        $this->assign('data',$data);
        return $this->fetch();

    }


    public  function  add_admin(){


        $uid = session('uid');
        $name =  DB::name('admin')->field('username')->where('id',$uid)->find();
            $data = array(
                'is_send' => 1,
                'role'=>'teach',
            );

        if(request()->isAjax()){
            $param = input('post.');
            $res['temp'] = $param['temp'];
            $rv['first'] = $param['first'];
            $rv['keyword1'] = $param['keyword1'];
            $rv['keyword2'] = $param['keyword2'];
            $rv['keyword3'] = $param['keyword3'];
            $rv['remark'] = $param['remark'];
            $res['is_url'] = $param['is_url'];
            $res['from_username'] = $name['username'];
            $res['instit_id'] = session('instit_id');
            $res['addtime'] = time();
            if($param['temp']=='员工通知'){
                $res['type'] =3;
            }
            $res['content'] = json_encode($rv);
            $nowyear = date('Y');
            $nowtime = strtotime($nowyear.'-08-25');
            if($nowtime>time()){
                $year = $nowyear-1;
            }else{
                $year = $nowyear;
            }
            $res['year'] = $year;
            $flag =   Db::name('temp_message')->insert($res);

             $maps1 = array(
                            'a.instit_id'=>session('instit_id'),
                            'm.open_id'=>['neq',''],
                        );

            if($param['temp']=='员工通知'){
                $list = Db::name('teach')->alias('a')->field('m.id,m.nickname,m.open_id')->join('member m','a.mobile=m.account')->where($maps1)->select();
                $data['template_id']= 'OJp_jc7_2XCJhlwYf20cXKXFbSdnrz4cXG9yNwBVJVY';
            }

           if($flag){
                foreach($list as $k => $v){
                    $data['touser']		= $v['open_id'];
                    // $data['url']		= "http://".$_SERVER["HTTP_HOST"]."/index.php?m=default&c=user&a=order_tracking&order_id=".$order_id;
                    $data['topcolor']	= '#7B68EE';
                    $data['data']['first']['value']	= $param['first'];
                    $data['data']['first']['color']	= '#743A3A';
                    $data['data']['keyword1']['value']	=$param['keyword1'];
                    $data['data']['keyword1']['color']	= '#666666';
                    $data['data']['keyword2']['value']	= $param['keyword2'];
                    $data['data']['keyword2']['color']	=  '#666666';
                    $data['data']['keyword3']['value']	= $param['keyword3'];
                    $data['data']['keyword3']['color']	=  '#666666';
                    $data['data']['remark']['value']	=  $param['remark'];
                    $data['data']['remark']['color']	= '#3300CC';
                    $result = send_temp_message(session('instit_id'),$data);
                }

               $msd = array(
                    'instit_id'=>session('instit_id'),
                    'group_id'=>8
               );
               $s_user =  Db::name('member')->where($msd)->find();
               if($s_user['id']){
                   $template_integral = $this->get_config_v('template_integral');
                   if($template_integral>0){
                       add_integral($s_user['id'],$template_integral,'template_integral');
                   }
               }

                return json(['code' => 1,  'msg' => '模板消息发送成功']);
            }else{
                return json(['code' => -1,  'msg' => '模板消息发送失败']);
            }
        }

        $this->assign('data',$data);
        return $this->fetch();

    }





}