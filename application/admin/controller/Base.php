<?php

namespace app\admin\controller;
use app\admin\model\Node;
use think\Controller;
use think\Db;

class Base extends Controller
{
    public function _initialize()
    {

        if(!session('uid')||!session('username')){
            $this->redirect(url('login/index'));
        }
        
        $auth = new \com\Auth();   
        $module     = strtolower(request()->module());
        $controller = strtolower(request()->controller());
        $action     = strtolower(request()->action());
        $url        = $module."/".$controller."/".$action;

        //跳过检测以及主页权限
        if(session('uid')!=1){
            if(!in_array($url, ['admin/index/index','admin/index/indexpage'])){
                if(!$auth->check($url,session('uid'))){
                    $this->error('抱歉，您没有操作权限');
                }
            }
        }

        $instit_info = Db::name('instit')->where('id',session('instit_id'))->find();


        $node = new Node();
        $this->assign([
            'username' => session('username'),
            'portrait' => session('portrait'),
            'rolename' => session('rolename'),
            'instit_name' => $instit_info['name'],
            'menu' => $node->getMenu(session('rule'))
        ]);
        
        $config = cache('db_config_data');

        if(!$config){            
            $config = api('Config/lists');                          
            cache('db_config_data',$config);
        }
        config($config); 

        if(config('web_site_close') == 0  ){
            $this->error('站点已经关闭，请稍后访问~');
        }

//        if(config('admin_allow_ip')){
//
//            if(in_array(request()->ip(),explode(',',config('admin_allow_ip')))){
//                $this->error('403:禁止访问');
//            }
//        }

    }

    public function get_config_v($v){
        $info= Db::name('config')->where('name',$v)->find();
        if($info){
            return $info['value'];
        }else
            return '';
    }


}