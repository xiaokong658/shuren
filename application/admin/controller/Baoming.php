<?php

namespace app\admin\controller;
use app\admin\model\InstitModel;
use app\admin\model\BaomingModel;
use think\Db;

class Baoming extends Base
{


    public function index()
    {
        $instit_id = session('instit_id');
        $key = input('key');
        $map = [];
        if($key&&$key!=="")
        {
            $map['title'] = ['like',"%" . $key . "%"];
        }
        $map['is_closed'] = 0;
        $map['instit_id'] = $instit_id;
        $Nowpage = input('get.page') ? input('get.page') : 1;
        $limits = 12;// 获取总条数
        $count = Db('baoming')->where($map)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));
        $ad = new BaomingModel();
        $lists = $ad->getbmAll($map, $Nowpage, $limits);
        foreach($lists as $k => $v){
            $lists[$k]['addtime1'] = date('Y-m-d',$v['addtime']);
        }

        $this->assign('count', $count); //当前页
        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('val', $key);
        if (input('get.page')) {
            return json($lists);
        }
        return $this->fetch();
    }



    public function jieguo()
    {
        $id = input('param.id');

        $map['baoming_id'] = $id;
        $Nowpage = input('get.page') ? input('get.page') : 1;
        $limits = 12;// 获取总条数
        $count = Db('baoming_log')->where($map)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));
        $ad = new BaomingModel();
        $lists = $ad->getlogAll($map, $Nowpage, $limits);

        $baoming = Db::name('baoming')->where('id',$id)->find();
        $arr = explode('|',$baoming['desc']);


        foreach($lists as $k => $v){
            $lists[$k]['addtime1'] = date('Y-m-d',$v['addtime']);

                $choose= json_decode($v['desc'],true);
            foreach($choose as $kk=>$vv){
                $vc = 'tt'.$kk;
                $lists[$k][$vc] = $vv['content'];
                $cccc = $kk;
            }
        }

        $this->assign('count', $count); //当前页
        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        if (input('get.page')) {
            return json($lists);
        }
        $this->assign('id',$id);
        $this->assign('arr',$arr);
        $this->assign('num',$cccc);

        return $this->fetch();
    }



    /**
     * [add_ad 添加报名]
     * @return [type] [description]
     * @author [沈晶晶] [864491238@qq.com]
     */
    public function add()
    {
        $instit_id = session('instit_id');

        if (request()->isAjax()) {

            $param = input('post.');
            $param['instit_id'] = $instit_id;
            $param['addtime'] = time();
            $vote = new BaomingModel();
            $flag = $vote->insertbm($param);

            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }

        return $this->fetch();

    }

    public function edit()
    {
        $id = input('param.id');

        if (request()->isAjax()) {

            $param = input('post.');

            $vote = new BaomingModel();
            $flag = $vote->editbm($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }

        $vote = Db::name('baoming')->where('id',$id)->find();

        $this->assign('vote',$vote);
        return $this->fetch();

    }








    /**
     * [del_ad 删除报名]
     * @return [type] [description]
     * @author [沈晶晶] [864491238@qq.com]
     */
    public function del_baoming()
    {
        $id = input('param.id');
        $ad = new BaomingModel();
        $flag = $ad->del_baoming($id);
        return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
    }


    public function del_notice()
    {
        $id = input('param.id');
        $ad = new BaomingModel();
        $flag = $ad->delNotice($id);
        return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
    }







}