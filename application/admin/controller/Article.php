<?php

namespace app\admin\controller;
use app\admin\model\ArticleModel;
use app\admin\model\ArticleCateModel;
use app\admin\model\EssayModel;
use app\admin\model\DebateModel;
use app\admin\model\Debate_listModel;
use app\admin\model\EssayArticleModel;

use app\admin\model\UserModel;
use think\Db;

class Article extends Base
{

    /**
     * [index 文章列表]
     * @author [沈晶晶] [334554156@qq.com]
     */

    public function cg_add_article()
    {
        if(request()->isAjax()){
            $param = input('post.');

            $mas = array(
                'group_id'=>8,
                'instit_id'=>session('instit_id'),
            );


            $user = Db::name('member')->field('id')->where($mas)->find();



            $article = new ArticleModel();
            $param['instit_id'] = session('instit_id');
            $param['status'] = -1;
            $param['user_id'] = $user['id'];
            $flag = $article->insertArticle($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);

            if($flag){
                $this->redirect('article/cg_index');
            }


        }


    }




    public function index(){

        $key = input('key');
        $map = [];
        $map1= [];
        if($key&&$key!==""){
            $map['shuren_article.title'] = ['like',"%" . $key . "%"];
        }
        $map1['group_id'] = 8;
        if(session('instit_id')!=1){
            $map1['instit_id'] = session('instit_id');
            $map['shuren_article.instit_id'] = session('instit_id');
            $qian_user = Db::name('member')->field('id')->where($map1)->find();
            $map['shuren_article.user_id'] = $qian_user['id'];
        }
        $map['shuren_article.status'] = 1;
        //$map['shuren_article.is_want_tui'] = 1;
        $Nowpage = input('get.page') ? input('get.page'):1;
        $limits = 10;// 获取总条数
        $a = ($Nowpage-1)*10;
        $count = Db::name('article')->where($map)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));
        $article = new ArticleModel();

       // $lists = $article->getArticleByWhere($map, $Nowpage, $limits);

        $lists = Db::name('article')->field('shuren_article.id,shuren_article.is_tui,shuren_article.jigou_tui,shuren_article.status,shuren_article.id,shuren_article.title,shuren_article.views,shuren_article.create_time,shuren_article.photo,name,shuren_member.nickname')->join('shuren_article_cate', 'shuren_article.cate_id = shuren_article_cate.id')->join('shuren_member', 'shuren_article.user_id = shuren_member.id')->where($map)->limit($a, $limits)->order('id desc')->select();

//



        foreach($lists as $k=>$v)
        {
            $map3 = array(
                'instit_id'=>   session('instit_id'),
                'article_id'=> $v['id'],
            );

           $cou = Db::name('article_recommend')->where($map3)->count();

            if($cou>0){
                $lists[$k]['is_recommend'] = 1;
            }else{
                $lists[$k]['is_recommend'] = 0;
            }

            switch($v['status']){
                case '0': $status = '未审核';break;
                case '1': $status = '已发布';break;
                default:$status='';
            }


            $lists[$k]['create_time'] = date('Y-m-d H:i:s',$v['create_time']);
            $lists[$k]['status'] = $status;
        }


        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('count', $count); 
        $this->assign('val', $key);
        $this->assign('instit_id', session('instit_id'));
        if(input('get.page')){
            return json($lists);
        }
        return $this->fetch();
    }


    public function cg_index(){

        $key = input('key');
        $map = [];
        if($key&&$key!==""){
            $map['shuren_article.title'] = ['like',"%" . $key . "%"];
        }
        $map['shuren_article.status'] = -2;
        $map['shuren_article.instit_id'] = session('instit_id');
        $Nowpage = input('get.page') ? input('get.page'):1;
        $limits = 10;// 获取总条数
        $a = ($Nowpage-1)*10;
        $count = Db::name('article')->where($map)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));
        $article = new ArticleModel();
        // $lists = $article->getArticleByWhere($map, $Nowpage, $limits);
        $lists = Db::name('article')->field('shuren_article.id,shuren_article.id,shuren_article.title,shuren_article.views,shuren_article.create_time,shuren_article.photo,name')->join('shuren_article_cate', 'shuren_article.cate_id = shuren_article_cate.id')->where($map)->limit($a, $limits)->order('id desc')->select();
        foreach($lists as $k=>$v)
        {
            $map3 = array(
                'instit_id'=>   session('instit_id'),
                'article_id'=> $v['id'],
            );
            $cou = Db::name('article_recommend')->where($map3)->count();
            if($cou>0){
                $lists[$k]['is_recommend'] = 1;
            }else{
                $lists[$k]['is_recommend'] = 0;
            }
            $lists[$k]['create_time'] = date('Y-m-d H:i:s',$v['create_time']);
        }


        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('count', $count);
        $this->assign('val', $key);
        $this->assign('instit_id', session('instit_id'));
        if(input('get.page')){
            return json($lists);
        }
        return $this->fetch();
    }




    public function recommend_index(){
        $instit_id = session('instit_id');
        $key = input('key');
        $map = [];

       // $map['shuren_article.status'] = array('egt',0);
        $map['shuren_article_recommend.senior_instit_id'] = $instit_id;
        if($key&&$key!==""){
            $map['shuren_article.title'] = ['like',"%" . $key . "%"];
        }
       // $map['shuren_article.is_want_tui'] = 1;

        $Nowpage = input('get.page') ? input('get.page'):1;
        $limits = 10;// 获取总条数
        $a = ($Nowpage-1)*10;
        $count = Db::name('article_recommend')->join('shuren_article','shuren_article_recommend.article_id = shuren_article.id')->where($map)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));
        $article = new ArticleModel();
        // $lists = $article->getArticleByWhere($map, $Nowpage, $limits);

        $lists = Db::name('article_recommend')->field('shuren_article_recommend.addtime,shuren_article_recommend.id as tid,shuren_article_recommend.jigou_tui,shuren_article_recommend.user_id,shuren_article_recommend.instit_id,shuren_article_recommend.senior_instit_id,shuren_article.id,shuren_article.title,shuren_article.views,shuren_article.create_time,shuren_article.photo,name')->join('shuren_article','shuren_article_recommend.article_id = shuren_article.id')->join('shuren_article_cate', 'shuren_article.cate_id = shuren_article_cate.id')->where($map)->limit($a, $limits)->order('shuren_article_recommend.id desc')->select();

        //echo  Db::name('article_recommend')->getLastSql();


       // $lists = Db::name('article')->field('shuren_article.id,shuren_article.id,shuren_article.title,shuren_article.views,shuren_article.create_time,shuren_article.photo,name')->join('shuren_article_cate', 'shuren_article.cate_id = shuren_article_cate.id')->where($map)->limit($a, $limits)->order('id desc')->select();

       // $lists = Db::name('article')->field('shuren_article.id,shuren_article.id,shuren_article.title,shuren_article.views,shuren_article.create_time,shuren_article.photo,name')->join('shuren_article_cate', 'shuren_article.cate_id = shuren_article_cate.id')->where($map)->limit($a, $limits)->order('id desc')->select();

        foreach($lists as $k=>$v)
        {
            $map3 = array(
                'instit_id'=> $instit_id,
                'article_id'=> $v['id'],
            );
            $cou = Db::name('article_recommend')->where($map3)->count();
            if($cou>0){
                $lists[$k]['is_recommend'] = 1;
            }else{
                $lists[$k]['is_recommend'] = 0;
            }

            $lists[$k]['addtime'] = date('Y-m-d H:i:s',$v['addtime']);

            if(!empty($v['instit_id'])){
                $insiti = Db::name('instit')->field('name')->where('id',$v['instit_id'])->find();
                $lists[$k]['tjfrom'] ='机构-'. $insiti['name'];
            }else{
                $insiti = Db::name('member')->field('nickname')->where('id',$v['user_id'])->find();
                $lists[$k]['tjfrom'] ='用户-'. $insiti['nickname'];
            }

        }

       // print_R($lists);


        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('count', $count);
        $this->assign('val', $key);
        $this->assign('instit_id', $instit_id);
        if(input('get.page')){
            return json($lists);
        }
        return $this->fetch();
    }


    public function shenhe_index(){

        $key = input('key');
        $type = input('param.type');
        $map = [];

        if(empty($type)){
            $type=1;
        }

        if($type==1){
            $map['shuren_article.status'] = 0;
        }
        if($type==2){
            $map['shuren_article.status'] = 1;
        }
        if($type==3){
            $map['shuren_article.status'] = -4;
        }



        if($key&&$key!==""){
            $map['shuren_article.title'] = ['like',"%" . $key . "%"];
        }

//        $map['shuren_article.instit_id'] = session('instit_id');
        $Nowpage = input('get.page') ? input('get.page'):1;
        $limits = 10;// 获取总条数
        $count = Db::name('article')->where($map)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));
        $article = new ArticleModel();
        $lists = $article->getArticleByWhere1($map, $Nowpage, $limits);

        foreach($lists as $k=>$v){
            $instit = Db::name('instit')->field('name')->where('id',$v['instit_id'])->find();
            $lists[$k]['instit_name'] = $instit['name'];

            $member = Db::name('member')->field('nickname')->where('id',$v['user_id'])->find();
            $lists[$k]['username'] = $member['nickname'];

            switch($v['status']){
                case '0' : $status = '未审核';break;
                case '1' : $status = '已发布';break;
                case '-4' : $status = '已驳回';break;
                default: $status='';
            }
            $lists[$k]['status1'] = $status;

        }


        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('count', $count);
        $this->assign('val', $key);
        $this->assign('type', $type);
        if(input('get.page')){
            return json($lists);
        }
        return $this->fetch();
    }


    /**
     * [add_article 添加文章]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function add_article()
    {
        if(request()->isAjax()){
            $mas = array(
                'group_id'=>8,
                'instit_id'=>session('instit_id'),
            );
            $user = Db::name('member')->field('id')->where($mas)->find();
            $param = input('post.');
            $article = new ArticleModel();
            $param['instit_id'] = session('instit_id');
            $param['user_id'] = $user['id'];
            $param['status'] = 1;
            $param['is_want_tui'] =1;
            $flag = $article->insertArticle($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }

        $vote = Db::name('vote')->where(['instit_id'=>session('instit_id')])->select();
        $baoming = Db::name('baoming')->where(['instit_id'=>session('instit_id')])->select();



        $cate = new ArticleCateModel();
        $this->assign('cate',$cate->getAllCate(0));

        $this->assign('vote',$vote);
        $this->assign('baoming',$baoming);
        return $this->fetch();
    }


    /**
     * [edit_article 编辑文章]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function edit_article()
        {
            $article = new ArticleModel();
            if(request()->isAjax()){
                $param = input('post.');
                    unset($param['file']);
                    $param['status'] = 1;
                    $article = new ArticleModel();
                    $flag = $article->updateArticle($param);
                    return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
            }
            $id = input('param.id');
            $cate = new ArticleCateModel();
            $this->assign('cate',$cate->getAllCate(0));
            $map5 = array(
                'by_instit_id'=>session('instit_id'),
                'article_id'=>$id,
            );
            $arti_info = Db::name('pt_article_edit')->where($map5)->find();
            $info = $article->getOneArticle($id);
            if($arti_info){
                $info['title'] = $arti_info['title'];
                $info['remark'] = $arti_info['remark'];
                $info['content'] = $arti_info['content'];
            }


            if($info['vote_id']>0){
                $vote= DB::name('vote')->where('id',$info['vote_id'])->find();
                $info['vote'] = $vote['title'];
            }

            if($info['baoming_id']>0){
                $baoming= DB::name('baoming')->where('id',$info['baoming_id'])->find();
                $info['baoming'] = $baoming['title'];
            }

            $author = Db::name('member')->field('nickname')->where('id='.$info['user_id'])->find();
            $info['author'] = $author['nickname'];
            $this->assign('article',$info);
            return $this->fetch();
        }



    /**
     * [del_article 删除文章]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function del_article()
    {
        $id = input('param.id');
        $cate = new ArticleModel();
        $flag = $cate->delArticle($id);
        return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
    }


    /**
     * [del_article 删除文章评论]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function del_article_comment()
    {
        $id = input('param.id');
        $cate = new ArticleModel();
        $flag = $cate->delArticleComment($id);
        return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
    }


 public function del_debate_comment()
    {
        $id = input('param.id');
        $cate = new ArticleModel();
        $flag = $cate->delDebateComment($id);
        return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
    }


 public function del_debate_comment_list()
    {
        $id = input('param.id');
        $cate = new ArticleModel();
        $flag = $cate->delDebateList($id);
        return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
    }



    /**
     * [article_state 文章状态]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function essay_state()
    {
        $id = input('param.id');
        $result =  change_status1($id,'essay','is_tui','取消推荐','成功推荐');
        return $result;

    }
    public function shuren_article_state()
    {
        $id = input('param.id');
        $result =  change_status1($id,'article','is_tui','取消推荐','成功推荐');
        return $result;

    }

    public function article_tui_state()
    {
        $id = input('param.id');
        $result =  change_status1($id,'article','is_tui','已取消置顶','置顶成功');
        return $result;

    }
    public function article_jigoutui_state()
    {
        $id = input('param.id');
        $result =  change_status1($id,'article','jigou_tui','取消推荐','成功推荐');
        return $result;

    }
     public function debate_state()
    {
        $id = input('param.id');
        $result =  change_status1($id,'debate','is_tui','取消推荐','成功推荐');
        return $result;

    }





    public function article_recommend_jigoutui_state()
    {
        $id = input('param.id');
        $result =  change_status1($id,'article_recommend','jigou_tui','取消推荐','成功推荐');
        return $result;

    }


    public function article_status()
    {
        $id = input('param.id');
        $result= input('param.result');
        $note = input('param.note');
        $article =  Db::name('article')->where('id',$id)->find();
        if($result==1){
            $flag = Db::name('article')->where('id',$id)->setField(['status'=>1]);

            $article_review =  $this->get_config_v('article_review');
            if($article_review>0){
                add_integral($article['user_id'],$article_review,'article_review');
            }



        }
        if($result==2){
            $flag = Db::name('article')->where('id',$id)->setField(['status'=>-4,'bohui_why'=>$note]);
        }


                if($article['user_id']){
                    $user =  Db::name('member')->where('id',$article['user_id'])->find();
                    if($article['user_id']) {
                        if($result==1){
                            article_shenhe($user['open_id'], $article['title'],$user['instit_id']);
                        }else{
                            article_shenhe_rejected($user['open_id'], $article['title'],$user['instit_id'],$note);
                        }
                    }
                }


            return json(['code' => 1,  'msg' => '处理成功']);





    }


    //*********************************************分类管理*********************************************//

    /**
     * [index_cate 分类列表]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function index_cate(){

        $cate = new ArticleCateModel();
        $list = $cate->getAllCate(0);
//        foreach($list as $k=>$v){
//            $list[$k]['son'] = $cate->getAllCate($v['id']);
//        }
        $this->assign('list',$list);
        return $this->fetch();
    }


    /**
     * [add_cate 添加分类]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function add_cate()
    {
        if(request()->isAjax()){

            $param = input('post.');
            $cate = new ArticleCateModel();
            if(empty($param['status'])){
                $param['status']=0;
            }
            $flag = $cate->insertCate($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }
        $list = Db::name('article_cate')->where('pid',0)->select();
        $this->assign('list',$list);
        return $this->fetch();
    }


    /**
     * [edit_cate 编辑分类]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function edit_cate()
    {
        $cate = new ArticleCateModel();

        if(request()->isAjax()){

            $param = input('post.');
            if(empty($param['status'])){
                $param['status']=0;
            }
            $flag = $cate->editCate($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }

        $id = input('param.id');
        $list = Db::name('article_cate')->where('pid',0)->select();
        $this->assign('list',$list);
        $this->assign('cate',$cate->getOneCate($id));
        return $this->fetch();
    }


    /**
     * [del_cate 删除分类]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function del_cate()
    {
        $id = input('param.id');
        $cate = new ArticleCateModel();
        $flag = $cate->delCate($id);
        return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
    }



    /**
     * [cate_state 分类状态]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function cate_state()
    {
        $id=input('param.id');
        $status = Db::name('article_cate')->where(array('id'=>$id))->value('status');//判断当前状态情况
        if($status==1)
        {
            $flag = Db::name('article_cate')->where(array('id'=>$id))->setField(['status'=>0]);
            return json(['code' => 1, 'data' => $flag['data'], 'msg' => '已禁止']);
        }
        else
        {
            $flag = Db::name('article_cate')->where(array('id'=>$id))->setField(['status'=>1]);
            return json(['code' => 0, 'data' => $flag['data'], 'msg' => '已开启']);
        }
    
    }

    //*********************************************征文管理*********************************************//
    /**
     * [add_article 征文列表]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */

    public function essay(){
        $key = input('key');
        $map = [];
        if($key&&$key!==""){
            $map['title'] = ['like',"%" . $key . "%"];
        }
        if(session('instit_id')!=1){
            $map['instit_id'] = session('instit_id');
        }



        $Nowpage = input('get.page') ? input('get.page'):1;
        $limits = 10;// 获取总条数
        $count = Db::name('essay')->where($map)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));
        $article = new EssayModel();
        $lists = $article->getArticleByWhere($map, $Nowpage, $limits);
        foreach($lists as $k=>$v)
        {
            $lists[$k]['end_time']=date('Y-m-d H:i:s',$v['end_time']);

            $instit = Db::name('instit')->field('name')->where('id',$v['instit_id'])->find();
            $lists[$k]['nickname'] = $instit['name'];


        }
        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('count', $count);
        $this->assign('instit_id', session('instit_id'));
        $this->assign('val', $key);
        if(input('get.page')){
            return json($lists);
        }
        return $this->fetch();
    }


    /**
     * [add_article 新增征文]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function add_essay()
    {
        if(request()->isAjax()){

//            $user = new UserModel();
//            $user_info =  $user->getUserGroupinfo();

            $param = input('post.');
            $param['user_id'] = session('user_id');
            $param['instit_id'] = session('instit_id');

            $param['end_time'] = strtotime($param['end_time'])+3600*24-1;
            $article = new EssayModel();
            $flag = $article->insertArticle($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }

        $cate = new ArticleCateModel();
        $this->assign('cate',$cate->getAllCate(0));
        return $this->fetch();
    }


    /**
     * [edit_article 编辑征文]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function edit_essay()
    {
        $article = new EssayModel();

        if(request()->isAjax()){
            $param = input('post.');
            $param['end_time'] = strtotime($param['end_time'])+3600*24-1;
            $flag = $article->updateArticle($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }

        $id = input('param.id');
        $cate = new ArticleCateModel();
        $this->assign('cate',$cate->getAllCate(0));

        $info = $article->getOneArticle($id);
        $info['end_time'] = date('Y-m-d', $info['end_time'] );
        $this->assign('article',$info);
        return $this->fetch();
    }

    /**
     * [del_article 删除征文]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function del_essay()
    {
        $id = input('param.id');
        $cate = new EssayModel();
        $flag = $cate->delArticle($id);
        return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
    }



    //*********************************************话题管理*********************************************//
    /**
     * [add_article 辩论列表]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function debate(){
        $key = input('key');
        $map = [];
        if($key&&$key!==""){
            $map['title'] = ['like',"%" . $key . "%"];
        }
        if(session('instit_id') != 1){
            $map['instit_id'] = session('instit_id');
        }

        $Nowpage = input('get.page') ? input('get.page'):1;
        $limits = 10;// 获取总条数

        $instit_id = session('instit_id');
        $qian_user = Db::name('member')->field('id')->where(['group_id'=>8,'instit_id'=>$instit_id])->find();
        if(session('instit_id') != 1) {
            $map['user_id'] = $qian_user['id'];
        }


        $count = Db::name('debate')->where($map)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));
        $article = new DebateModel();
        $lists = $article->getArticleByWhere($map, $Nowpage, $limits);
     //   echo Db::name('debate')->getLastSql();

        foreach($lists as $k=>$v)
        {
            $lists[$k]['end_time']=date('Y-m-d',$v['end_time']);
        }
        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('count', $count);
        $this->assign('val', $key);
        $this->assign('instit_id', session('instit_id'));
        if(input('get.page')){
            return json($lists);
        }
        return $this->fetch();
    }


    /**
     * [add_article 新增辩论]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function add_debate()
    {
        if(request()->isAjax()){

            $user = new UserModel();
            $user_info =  $user->getUserGroupinfo();

            $param = input('post.');
//            $param['user_id'] = $user_info['user_id'];
//            $param['instit_id'] = $user_info['instit_id'];
              $param['instit_id'] = session('instit_id');;
            $mas = array(
                'group_id'=>8,
                'instit_id'=>session('instit_id'),
            );
            $user = Db::name('member')->field('id')->where($mas)->find();
            $param['user_id'] = $user['id'];
            $param['end_time'] = strtotime($param['end_time'])+3600*24-1;
            $article = new DebateModel();
            $flag = $article->insertArticle($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }

        $cate = new ArticleCateModel();
        $this->assign('cate',$cate->getAllCate(0));
        return $this->fetch();
    }


    /**
     * [edit_article 编辑辩论]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function edit_debate()
    {
        $article = new DebateModel();

        if(request()->isAjax()){
            $param = input('post.');
            $param['end_time'] = strtotime($param['end_time'])+3600*24-1;
            $flag = $article->updateArticle($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }

        $id = input('param.id');
        $cate = new ArticleCateModel();
        $this->assign('cate',$cate->getAllCate(0));

        $info = $article->getOneArticle($id);
        $info['end_time'] = date('Y-m-d', $info['end_time'] );
        $this->assign('article',$info);
        return $this->fetch();
    }

    /**
     * [del_article 删除辩论]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function del_debate()
    {
        $id = input('param.id');
        $cate = new DebateModel();
        $flag = $cate->delArticle($id);
        return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
    }



    //*********************************************征文投稿文章管理*********************************************//
    /**
     * [add_article 投稿文章列表]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function essay_article(){
        $essay_id = input('essay_id');
        $val = '';
        $map['essay_id'] = $essay_id;

        $key = input('id');
        if($key&&$key!==""){
            $map['essay_id'] = $key;
        }

        $Nowpage = input('get.page') ;
        $limits = 10;// 获取总条数
        $count = Db::name('essay_article')->where($map)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));
        $essay = new EssayArticleModel();
        $lists = $essay->getAllByWhere($map, $Nowpage, $limits);
        foreach($lists as $k=>$v)
        {
            $lists[$k]['add_time']=date('Y-m-d H:i:s',$v['add_time']);
            if(!empty($v['user_id'])){
                $nickname = Db::name('member')->where('id',$v['user_id'])->find();
                $lists[$k]['nickname']= $nickname['nickname'];
            }else{
                $lists[$k]['nickname']= '';
            }
            switch($v['status']){
                case 0 : $status = '';break;
                case 1 : $status = '已采用';break;
                case 2 : $status = '拟采用';break;
                case 3 : $status = '已退稿';break;
            }
            $lists[$k]['status1']= $v['status'];
            $lists[$k]['status']= $status;
        }

        $article = new EssayModel();
        $info = $article->getOneArticle($essay_id);
        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('info', $info);
        $this->assign('count', $count);
        $this->assign('val', $val);
        $this->assign('essay_id', $essay_id);

        if(input('get.page')){
            return json($lists);
        }
        return $this->fetch();
    }

    /**
     * [add_article 查看征文投稿文章]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */


    public function eassy_articleRead()
    {
        $id = input('param.id');
        $article_id = DB::name('essay_article')->field('article_id,essay_id')->where('id='.$id)->find();
        $article = new ArticleModel();
        $info = $article->getOneArticle($article_id['article_id']);
      //  $info['create_time'] = date('Y-m-d H:i:s',$info['create_time']);
//        $map = array(
//          'article_id'=>  $article_id['article_id'],
//          'essay_id'=>  $article_id['essay_id'],
//        );
        $essay_user = DB::name('essay')->field('instit_id')->where('id='.$article_id['essay_id'])->find();
//        if($essay_user){
//            $map['by_instit_id'] = $essay_user['instit_id'];
//        }
//        $list = Db::table('think_article_edit')->distinct(true)->where($map)->order('id asc')->select();



        $map5 = array(
                'article_id'=>$id,
                'essay_id'=>$article_id['essay_id'],
                'by_instit_id'=>session('instit_id'),
        );


        $list = Db::name('article_edit')->where($map5)->select();


//        $list = Db::query("SELECT * from (SELECT * FROM `shuren_article_edit` WHERE `article_id` = ".$article_id['article_id']." AND `essay_id` = ".$article_id['essay_id']." AND `by_instit_id` = ".$essay_user['instit_id']."  order by id desc  ) as b GROUP BY b.by_admin_id ");

        foreach($list as $k=>$v)
        {
            $lists[$k]['update_time']=date('Y-m-d H:i:s',$v['update_time']);
            $nickname = Db::name('admin')->where('id',$v['by_admin_id'])->find();
            $list[$k]['username']= $nickname['username'];
            $list[$k]['update_time1'] = date('Y-m-d H:i:s',$v['update_time']);
        }

        $this->assign('article',$info);
        $this->assign('list',$list);
        $this->assign('id',$id);
        return $this->fetch('article/article_read');

    }


    /**
     * [edit_article 编辑征文投稿文章]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function eassy_articleEdit()
    {
        $article = new ArticleModel();
        if(request()->isAjax()){
            $param = input('post.');




            if($param['edit_id']==0) {
                $info = $article->getOneArticle($param['article_id']);
                if ($info['title'] != $param['title'] || $info['content'] != $param['content'] || $info['photo'] != $param['photo'] || $info['remark'] != $param['remark']) {
//                    $user = new UserModel();
//                    $user_info = $user->getUserGroupinfo();
                    $param['by_admin_id'] = session('uid');
                    $param['by_instit_id'] = session('instit_id');




                    unset($param['edit_id']);
                    $flag = $article->insertEssayArticle($param);


                    return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
                } else {
                    return json(['code' => -1, 'data' => '', 'msg' => '您未做修改！']);
                }
            }

            if($param['edit_id']>1){
                $edit_id = $param['edit_id'];
                unset($param['edit_id']);
//                    $user = new UserModel();
//                    $user_info = $user->getUserGroupinfo();
                    $param['by_admin_id'] = session('uid');
                    $param['by_instit_id'] = session('instit_id');
                    unset($param['edit_id']);
                    $flag = $article->updateEssayArticle($param,$edit_id);

                if ($flag) {
                    return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
                } else {
                    return json(['code' => -1, 'data' => '', 'msg' => '您未做修改！']);
                }


            }


        }


        $id = input('param.id');
        $article_id = DB::name('essay_article')->field('article_id,essay_id')->where('id='.$id)->find();
        $map =array(
            'essay_id'=>$article_id['essay_id'],
            'article_id'=>$article_id['article_id'],
        );

        $edit_info = Db::name('article_edit')->where($map)->find();

        if($edit_info){
            $info  = $edit_info;
            $info['eidt_id'] = $edit_info['id'];
        }else{
            $info = $article->getOneArticle($article_id['article_id']);
            $info['eidt_id'] = 0;
        }


        $this->assign('article',$info);
        $this->assign('essay_id',$article_id['essay_id']);
        $this->assign('id',$id);
        return $this->fetch('article/eassy_articleEdit');
    }

    /**
     * [edit_article 删除投稿文章]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function eassy_articleDel(){
        $id = input('param.id');
        $cate = new EssayArticleModel();
        $flag = $cate->delArticle($id);
        return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
    }

    public  function essay_status(){
        $id = input('param.id');
        $status = input('param.status');
        $essaynote = input('param.essaynote');
        $map = array(
          'id'=>$id,
          'status'=>$status,
          'note'=>$essaynote,
        );
        $cate = new EssayArticleModel();
        $flag = $cate->essay_status($map);


        $user_info = DB::name('essay_article')->alias('a')->field('a.user_id,b.title as article_title,c.title as essay_title')->join('shuren_article b','a.article_id=b.id')->join('essay c','a.essay_id =c.id')->where('a.id',$id)->find();

        if($status==1){
            $map6 = array(
                'user_id'=>  $user_info['user_id'],
                'instit_id'=>  session('instit_id'),
                'type'=>  1,
                'title'=>'征文采用通知',
                'content'=>'您好！您对于征文《'.$user_info['essay_title'].'》投稿的文章《'.$user_info['article_title'].'》已被采用！',
                'create_time'=>time(),
                'is_template'=>1,
            );
        }else if($status==2){
            $map6 = array(
                'user_id'=>  $user_info['user_id'],
                'instit_id'=>  session('instit_id'),
                'type'=>  1,
                'title'=>'征文拟采用通知',
                'content'=>'您好！您对于征文《'.$user_info['essay_title'].'》投稿的文章《'.$user_info['article_title'].'》已被拟采用！',
                'create_time'=>time(),
                'is_template'=>1,
            );
        }else if($status==3){
            $map6 = array(
                'user_id'=>  $user_info['user_id'],
                'instit_id'=>  session('instit_id'),
                'type'=>  1,
                'title'=>'征文退稿通知',
                'content'=>'您好！您对于征文《'.$user_info['essay_title'].'》投稿的文章《'.$user_info['article_title'].'》已被退稿！',
                'create_time'=>time(),
                'is_template'=>1,
            );
        }

        Db::name('inside_letter')->insert($map6);

        if($status==1){
            $essay_using =  $this->get_config_v('essay_using');
            if($essay_using>0){
                add_integral($user_info['user_id'],$essay_using,'essay_using');
            }
        }
        return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
    }

    public function debate_list(){
        $debate_id = input('debate_id');
        $val = '';
        $map['debate_id'] = $debate_id;

        $key = input('id');
        if($key&&$key!==""){
            logResult("debate_id=".$key);
            $map['debate_id'] = $key;
        }
       // $map['article_type'] = array('eq',1);
        $Nowpage = input('get.page') ;
        $limits = 10;// 获取总条数
        $count = Db::name('debate_list')->where($map)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));
        $essay = new Debate_listModel();
        $lists = $essay->getListByWhere($map, $Nowpage, $limits);
        foreach($lists as $k=>$v)
        {
            $lists[$k]['add_time']=date('Y-m-d H:i:s',$v['add_time']);
            if(!empty($v['user_id'])){
                $nickname = Db::name('member')->where('id',$v['user_id'])->find();
                $lists[$k]['nickname']= $nickname['nickname'];
            }else{
                $lists[$k]['nickname']= $v['writer'];
            }
        }
//        $article = new EssayModel();
//        $info = $article->getOneArticle(debate_id);
        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
//        $this->assign('info', $info);
        $this->assign('count', $count);
        $this->assign('val', $val);
        $this->assign('debate_id', $debate_id);
        if(input('get.page')){
            return json($lists);
        }
        return $this->fetch();
    }


    public function del_debate_list(){
        $id = input('param.id');
        $cate = new Debate_listModel();
        $flag = $cate->delArticle($id);
        return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
    }

    public function debate_list_status()
    {
        $id = input('param.id');
        $result =  change_status($id,'debate_list','status');
        return $result;

    }


    public function article_comment(){

        $map = [];

//      $map['shuren_article.instit_id'] = session('instit_id');
        $Nowpage = input('get.page') ? input('get.page'):1;
        $limits = 10;// 获取总条数
       $count = Db::name('article_comment')->where($map)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));
        $article = new ArticleModel();
        $lists = $article->getArticleComment($map, $Nowpage, $limits);

        foreach($lists as $key=>$val){
            $lists[$key]['addtime'] = date('Y-m-d H:i:s',$val['addtime']);
        }

        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('count', $count);
        if(input('get.page')){
            return json($lists);
        }
        return $this->fetch();
    }

    public function article_comment_status()
    {
        $id = input('param.id');
        $result =  change_status($id,'article_comment','status');
        return $result;

    }



    public function comment_debate_list(){

        $map = [];

//      $map['shuren_article.instit_id'] = session('instit_id');
        $Nowpage = input('get.page') ? input('get.page'):1;
        $limits = 10;// 获取总条数
        $count = Db::name('debate_list')->where($map)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));
        $article = new ArticleModel();
        $lists = $article->getDebateComment($map, $Nowpage, $limits);

        foreach($lists as $key=>$val){
            $lists[$key]['addtime'] = date('Y-m-d H:i:s',$val['add_time']);
        }


        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('count', $count);
        if(input('get.page')){
            return json($lists);
        }
        return $this->fetch();
    }

    public function comment_debate_list_status()
    {
        $id = input('param.id');
        $result =  change_status($id,'debate_list','status');
        return $result;

    }




    public function comment_debate_comment_list(){

        $map = [];

//      $map['shuren_article.instit_id'] = session('instit_id');
        $Nowpage = input('get.page') ? input('get.page'):1;
        $limits = 10;// 获取总条数
        $count = Db::name('debate_comment')->where($map)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));
        $article = new ArticleModel();
        $lists = $article->getDebateCommentList($map, $Nowpage, $limits);

        foreach($lists as $key=>$val){
            $lists[$key]['addtime'] = date('Y-m-d H:i:s',$val['addtime']);
        }


        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('count', $count);
        if(input('get.page')){
            return json($lists);
        }
        return $this->fetch();
    }

    public function comment_debate_status()
    {
        $id = input('param.id');
        $result =  change_status($id,'debate_comment','status');
        return $result;

    }

    public function recommend_article(){

        $id = input('param.id');
        $instit_id = session('instit_id');
        $map = array(
            'instit_id'=>$instit_id,
            'article_id'=>$id,
        );
        $info = Db::name('article_recommend')->where($map)->find();
        if($info){
            Db::name('article_recommend')->where('id',$info['id'])->delete();
            return json(['code' => -1]);
        }else{
            $instit_info =  Db::name('instit')->where('id',$instit_id)->find();
            if($instit_info['pid']==0){
                $instit_info['pid'] = 1;
            }
            $data = array(
                'article_id'=>  $id,
                'instit_id'=>  $instit_id,
                'senior_instit_id'=>  $instit_info['pid'],
                'addtime'=>time(),
            );
            Db::name('article_recommend')->insert($data);
            return json(['code' => 1]);
        }
    }


    public function articleRead()
    {
        $id = input('param.id');

       // $article_id = DB::name('essay_article')->field('article_id,essay_id')->where('id='.$id)->find();

        $article = new ArticleModel();
        $info = $article->getOneArticle($id);
        if($info['vote_id']>0){
            $vote= DB::name('vote')->where('id',$info['vote_id'])->find();
            $info['vote'] = $vote['title'];
        }

        if($info['baoming_id']>0){
            $baoming= DB::name('baoming')->where('id',$info['baoming_id'])->find();
            $info['baoming'] = $baoming['title'];
        }
        //  $info['create_time'] = date('Y-m-d H:i:s',$info['create_time']);
//        $map = array(
//          'article_id'=>  $article_id['article_id'],
//          'essay_id'=>  $article_id['essay_id'],
//        );
       // $essay_user = DB::name('essay')->field('instit_id')->where('id='.$article_id['essay_id'])->find();
//        if($essay_user){
//            $map['by_instit_id'] = $essay_user['instit_id'];
//        }
//        $list = Db::table('think_article_edit')->distinct(true)->where($map)->order('id asc')->select();
        $instit_id = session('instit_id');

        $list = Db::query("SELECT * from (SELECT * FROM `shuren_pt_article_edit` WHERE `article_id` = ".$id." AND `by_instit_id` = ".$instit_id."  order by id desc ) as b GROUP BY b.by_admin_id ");

        foreach($list as $k=>$v)
        {
            $lists[$k]['update_time']=date('Y-m-d H:i:s',$v['update_time']);
            $nickname = Db::name('admin')->where('id',$v['by_admin_id'])->find();
            $list[$k]['username']= $nickname['username'];
            $list[$k]['update_time1'] = date('Y-m-d H:i:s',$v['update_time']);
            $list[$k]['photo'] = $info['photo'];
        }

        $this->assign('article',$info);
        $this->assign('list',$list);
        $this->assign('id',$id);
        return $this->fetch('article/articleread');

    }



}