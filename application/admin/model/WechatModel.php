<?php

namespace app\admin\model;
use think\Model;
use think\Db;

class WechatModel extends Model
{
    protected $name = 'wechat';

    /**
     * [getAllCate 获取全部社会机构]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function getAllWechat()
    {

        $instit_id = session('instit_id');

        if($instit_id!=1){
            return $this->where('instit_id',$instit_id)->select();
        }else{
            return $this->select();
        }


    }

    public function getAllRepay()
    {
        $instit_id = session('instit_id');
        return Db::name('wechat_repay')->where('instit_id',$instit_id)->select();
    }


    /**
     * [getAllCate 获取全部教育机构]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function getAllEdu()
    {
        return $this->where('type=1')->order('sort asc')->select();
    }

    /**
     * [getAllCate 获取全部机构]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function getOpenWechat()
    {
        return $this->where('status =1')->order('sort asc')->select();
    }


    /**
     * [insertCate 添加机构]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function insertWechat($param)
    {
        try{
            $result = $this->allowField(true)->save($param);
            if(false === $result){
                return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '添加公众号成功'];
            }
        }catch( PDOException $e){
            return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
        }
    }


    public function insertWechatMenu($param)
    {
        try{
            $result = Db::name('wechat_menu')->insert($param);
            if(false === $result){
                return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '添加自定义菜单成功'];
            }
        }catch( PDOException $e){
            return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
        }
    }


    public function updateWechatMenu($param)
    {
        try{
            $result = Db::name('wechat_menu')->where('id',$param['id'])->update($param);
            if(false === $result){
                return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '修改自定义菜单成功'];
            }
        }catch( PDOException $e){
            return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
        }
    }


    public function insertRepay($param)
    {
        try{
            $result = Db::name('wechat_repay')->insert($param);
            if(false === $result){
                return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '添加文本回复成功'];
            }
        }catch( PDOException $e){
            return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
        }
    }

    /**
     * [insertCate 添加教育机构]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function insertEdu($param)
    {
        try{
            $result = $this->save($param);
            if(false === $result){
                return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '机构教育添加成功'];
            }
        }catch( PDOException $e){
            return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
        }
    }


    public function editWechat($param)
    {
        try{
            $result =$this->allowField(true)->save($param, ['id' => $param['id']]);
            if(false === $result){
                return ['code' => 0, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '公众号信息修改成功'];
            }
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }


    /**
     * [editMenu 编辑机构]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function editRepay($param)
    {
        try{
            $result = Db::name('wechat_repay')->where('id',$param['id'])->update($param);
            if(false === $result){
                return ['code' => 0, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '文本回复编辑成功'];
            }
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }

    /**
     * [getOneMenu 根据机构id获取一条信息]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function getOneWechat($id)
    {
        return $this->where('id', $id)->find();
    }

    /**
     * [delMenu 删除机构]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function delWechat($id)
    {
        try{
            $this->where('id', $id)->delete();
            return ['code' => 1, 'data' => '', 'msg' => '机构删除成功'];
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }


    public function have_child($id)
    {
        return $this->where('pid', $id)->find();
    }

    public function getAllWechatMenu($pid=0)
    {
        $instit_id = session('instit_id');
        $map = array(
            'instit_id'=>$instit_id,
            'pid'=>$pid,
        );
        return Db::name('wechat_menu')->where($map)->select();
    }


    public function menu_del($id)
    {


        $info = Db::name('wechat_menu')->where('id',$id)->find();

        if($info['pid']==0){
            $count = Db::name('wechat_menu')->where('pid',$info['id'])->count();
            if($count>0){
                return ['code' => -1, 'data' => '', 'msg' => '该菜单存在子菜单无法删除'];
            }
        }

        try{
            Db::name('wechat_menu')->where('id', $id)->delete();
            return ['code' => 1, 'data' => '', 'msg' => '菜单删除成功'];
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }


    public function repay_del($id)
    {


        $info = Db::name('wechat_repay')->where('id',$id)->find();



        try{
            Db::name('wechat_menu')->where('id', $id)->delete();
            return ['code' => 1, 'data' => '', 'msg' => '文本回复删除成功'];
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }


}