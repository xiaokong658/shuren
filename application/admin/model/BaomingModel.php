<?php

namespace app\admin\model;
use think\Model;
use think\Db;

class BaomingModel extends Model
{
    protected $name = 'baoming';

    /**
     * 根据条件获取列表信息
     * @param $where
     * @param $Nowpage
     * @param $limits
     */
    public function getbmAll($map, $Nowpage, $limits)
    {
        //return $this->field('shuren_ad.*,name')->join('shuren_ad_position', 'shuren_ad.ad_position_id = shuren_ad_position.id')->where($map)->page($Nowpage,$limits)->order('orderby desc')->select();
        return $this->field('*')->where($map)->page($Nowpage,$limits)->order('id desc')->select();
    }


    public function getlogAll($map, $Nowpage, $limits)
    {
        //return $this->field('shuren_ad.*,name')->join('shuren_ad_position', 'shuren_ad.ad_position_id = shuren_ad_position.id')->where($map)->page($Nowpage,$limits)->order('orderby desc')->select();
        return Db::name('baoming_log')->field('*')->where($map)->page($Nowpage,$limits)->order('id desc')->select();
    }





    public function insertbm($param)
    {
        try{
            $result = $this->save($param);
            if(false === $result){
                return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '报名添加成功'];
            }
        }catch( PDOException $e){
            return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
        }
    }

    public function editbm($param)
    {
        try{
            //$result = $this->where('id',$param['id'])->save($param);
            $result =  $this->allowField(true)->save($param, ['id' => $param['id']]);
            if(false === $result){
                return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '报名修改成功'];
            }
        }catch( PDOException $e){
            return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
        }
    }





    public function insertchoose($param)
    {
        try{
            $result =Db::name('bm_choose')->insert($param);
            if(false === $result){
                return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '投票选项添加成功'];
            }
        }catch( PDOException $e){
            return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
        }
    }

    public function editchoose($param)
    {
        try{
            $result =Db::name('bm_choose')->where('id',$param['id'])->update($param);
            if(false === $result){
                return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '投票选项修改成功'];
            }
        }catch( PDOException $e){
            return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
        }
    }


    public function del_choose($id)
    {
        try{
            Db::name('bm_choose')->where(['id' => $id])->delete();
            return ['code' => 1, 'data' => '', 'msg' => '删除投票选项成功'];
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }


    public function del_baoming($id)
    {
        try{
            $map['is_closed']=1;
            $this->save($map, ['id' => $id]);
            return ['code' => 1, 'data' => '', 'msg' => '删除报名成功'];
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }




}