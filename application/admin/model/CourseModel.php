<?php

namespace app\admin\model;
use think\Model;
use think\Db;

class CourseModel extends Model
{
    protected $name = 'course';

    /**
     * 根据搜索条件获取用户列表信息
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function getCourseByWhere($map, $Nowpage, $limits)
    {
        return $this->field('shuren_course.*,shuren_instit.name as instit_name')->join('shuren_instit', 'shuren_course.instit_id = shuren_instit.id')->where($map)->page($Nowpage, $limits)->order(' id desc ')->select();
    }

    /**
     * [insertCourse 添加课程]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function insertCourse($param)
    {
        try{
            $result = $this->allowField(true)->save($param);
            if(false === $result){
                return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '课程添加成功'];
            }
        }catch( PDOException $e){
            return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
        }
    }



    /**
     * [updateCourse 编辑课程]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function updateCourse($param)
    {
        try{
            $result = $this->allowField(true)->save($param, ['id' => $param['id']]);
            if(false === $result){
                return ['code' => 0, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '课程编辑成功'];
            }
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }



    /**
     * [getOneCourse 根据课程id获取一条信息]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function getOneCourse($id)
    {
        return $this->where('id', $id)->find();
    }



    /**
     * [delCourse 删除文章]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function delCourse($id)
    {
        try{
            $this->where('id', $id)->delete();
            return ['code' => 1, 'data' => '', 'msg' => '文章删除成功'];
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }


    public function updateEssayCourse($param)
    {
        $param['update_time']=time();
        try{
            $result = Db::table('shuren_course_edit')->insert($param);
            if(false === $result){
                return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '文档修改已保存成功'];
            }
        }catch( PDOException $e){
            return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
        }
    }

}