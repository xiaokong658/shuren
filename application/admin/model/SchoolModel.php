<?php

namespace app\admin\model;
use think\Model;
use think\Db;

class SchoolModel extends Model
{
    protected $name = 'student';

    /**
     * 根据搜索条件获取用户列表信息
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function getSchoolByWhere($map, $Nowpage, $limits)
    {
        return $this->field('shuren_student.*,shuren_instit.name as instit_name')->join('shuren_instit', 'shuren_student.instit_id = shuren_instit.id')->whereOr($map)->page($Nowpage, $limits)->order(' id desc ')->select();
    }

    /**
     * [insertCourse 添加学生]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function insertSchool($param)
    {
        try{
            $result = $this->allowField(true)->save($param);
            if(false === $result){
                return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '学生信息添加成功'];
            }
        }catch( PDOException $e){
            return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
        }
    }


    public function importSchool($param)
    {
            $result = $this->allowField(true)->insert($param);
            if(false === $result){
                return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '学生信息添加成功'];
            }

    }


    /**
     * [updateCourse 编辑学生]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function updateSchool($param)
    {
        try{
            $result = $this->allowField(true)->save($param, ['id' => $param['id']]);
            if(false === $result){
                return ['code' => 0, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '学生信息编辑成功'];
            }
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }

    public function updateTeach($param)
    {
        try{
            $result = Db::name('teach')->where('id', $param['id'])->update($param);
            if(false === $result){
                return ['code' => 0, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '教师信息编辑成功'];
            }
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }



    /**
     * [getOneCourse 根据学生id获取一条信息]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function getOneSchool($id)
    {
        return $this->where('id', $id)->find();
    }

    public function getOneTeach($id)
    {
        return Db::name('teach')->where('id', $id)->find();
    }



    /**
     * [delCourse 删除学生]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function delSchool($id)
    {
        try{
            $this->where('id', $id)->delete();
            return ['code' => 1, 'data' => '', 'msg' => '学生删除成功'];
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }

    public function getTeachByWhere($map, $Nowpage, $limits)
    {
        $lists =$this->name('teach')->field('shuren_teach.*,shuren_instit.name as instit_name')->
        join('shuren_instit', 'shuren_teach.instit_id = shuren_instit.id')->whereOr($map)->page($Nowpage, $limits)->order(' shuren_teach.id desc ')->select();
//        join('shuren_nianji', 'shuren_teach.nianji = shuren_nianji.id')->
//        join('shuren_course', 'shuren_teach.course = shuren_course.id')

        foreach($lists as $key=>$val){
            $nianji = Db::name('nianji')->where('id',$val['nianji'])->find();
            if($nianji){
                $lists[$key]['nianji'] = $nianji['nianji'];
            }else{
                $lists[$key]['nianji'] = '';
            }

            $course = Db::name('course')->where('id',$val['course'])->find();
            if($course){
                $lists[$key]['course'] = $course['name'];
            }else{
                $lists[$key]['course'] = '';
            }
        }

        return $lists;

    }



    public function insertTeach($param)
    {
        try{
            $param['creattime'] = time();
            $result = Db::name('teach')->insert($param);
            if(false === $result){
                return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '老师添加成功'];
            }
        }catch( PDOException $e){
            return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
        }
    }


}