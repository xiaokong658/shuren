<?php

namespace app\admin\model;
use think\Model;
use think\Db;

class LetterModel extends Model
{
    protected $name = 'inside_letter';

    /**
     * 根据条件获取列表信息
     * @param $where
     * @param $Nowpage
     * @param $limits
     */
    public function getLetterAll($map, $Nowpage, $limits)
    {
        //return $this->field('shuren_ad.*,name')->join('shuren_ad_position', 'shuren_ad.ad_position_id = shuren_ad_position.id')->where($map)->page($Nowpage,$limits)->order('orderby desc')->select();
        return $this->field('*')->where($map)->page($Nowpage,$limits)->order('id desc')->select();
    }


    public function getNoticeAll($map, $Nowpage, $limits)
    {
        //return $this->field('shuren_ad.*,name')->join('shuren_ad_position', 'shuren_ad.ad_position_id = shuren_ad_position.id')->where($map)->page($Nowpage,$limits)->order('orderby desc')->select();
        return Db::name('notice')->where($map)->page($Nowpage,$limits)->order('id desc')->select();
    }


    public function getOfficeAll($map, $Nowpage, $limits)
    {
        //return $this->field('shuren_ad.*,name')->join('shuren_ad_position', 'shuren_ad.ad_position_id = shuren_ad_position.id')->where($map)->page($Nowpage,$limits)->order('orderby desc')->select();
        return Db::name('office')->whereor($map)->page($Nowpage,$limits)->order('id desc')->select();
    }



    public function insertLetter($param)
    {
        try{
            $result = $this->save($param);
            if(false === $result){
                return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '站内信添加成功'];
            }
        }catch( PDOException $e){
            return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
        }
    }


    public function insertNotice($param)
    {
        try{
            $result = Db::name('notice')->insert($param);
            if(false === $result){
                return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '系统消息发送成功'];
            }
        }catch( PDOException $e){
            return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
        }
    }


    public function insertOffice($param)
    {
        try{
            $result = Db::name('office')->insert($param);
            if(false === $result){
                return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '公文发送成功'];
            }
        }catch( PDOException $e){
            return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
        }
    }


    /**
     * 删除信息
     * @param $id
     */
    public function del_letter($id)
    {
        try{
            $map['closed']=1;
            $this->save($map, ['id' => $id]);
            return ['code' => 1, 'data' => '', 'msg' => '删除广告成功'];
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }

 public function del_office($id)
    {
        try{
            Db::name('office')->where(['id' => $id])->delete();
            return ['code' => 1, 'data' => '', 'msg' => '删除消息成功'];
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }


    public function delNotice($id)
    {
        try{
            Db::name('notice')->where(['id' => $id])->delete();
            return ['code' => 1, 'data' => '', 'msg' => '删除消息成功'];
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }



}