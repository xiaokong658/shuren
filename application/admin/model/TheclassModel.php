<?php

namespace app\admin\model;
use think\Model;
use think\Db;

class TheclassModel extends Model
{
    protected $name = 'theclass';

    /**
     * 根据搜索条件获取用户列表信息
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function getTheclassByWhere($map, $Nowpage, $limits)
    {
        return $this->field('shuren_theclass.*,shuren_instit.name as instit_name,shuren_nianji.nianji as nianjiname')->join('shuren_instit', 'shuren_theclass.instit_id = shuren_instit.id')->
//        join('shuren_teach', 'shuren_theclass.leader = shuren_teach.id')->
        join('shuren_nianji', 'shuren_theclass.nianji = shuren_nianji.id')->where($map)->page($Nowpage, $limits)->order(' id desc ')->select();
    }

    /**
     * [insertTheclass 添加班级]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function insertTheclass($param)
    {
        try{
            $result = $this->allowField(true)->save($param);
            if(false === $result){
                return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '班级添加成功'];
            }
        }catch( PDOException $e){
            return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
        }
    }



    /**
     * [updateTheclass 编辑班级]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function updateTheclass($param)
    {
        try{
            $result = $this->allowField(true)->save($param, ['id' => $param['id']]);
            if(false === $result){
                return ['code' => 0, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '班级编辑成功'];
            }
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }



    /**
     * [getOneTheclass 根据班级id获取一条信息]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function getOneTheclass($id)
    {
        return $this->where('id', $id)->find();
    }



    /**
     * [delTheclass 删除文章]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function delTheclass($id)
    {
        try{
            $this->where('id', $id)->delete();
            return ['code' => 1, 'data' => '', 'msg' => '文章删除成功'];
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }


    public function updateEssayTheclass($param)
    {
        $param['update_time']=time();
        try{
            $result = Db::table('shuren_theclass_edit')->insert($param);
            if(false === $result){
                return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '文档修改已保存成功'];
            }
        }catch( PDOException $e){
            return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
        }
    }

    public function getAllnianji($school)
    {
        return $this->table('shuren_nianji')->where('schoollevel', $school)->order('id asc')->select();
    }

    public function getAllteach($instit_id)
    {
        return $this->table('shuren_teach')->where('instit_id', $instit_id)->order('id asc')->select();
    }



    public function getLeaderteach($instit_id)
    {
        return $this->table('shuren_teach')->where(['instit_id'=>$instit_id,'is_leader'=>1])->order('id asc')->select();
    }


    public function  importClassStudent($param){
        try{
            $result = Db::name('class_student')->insert($param);
            if(false === $result){
                return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '班级学生添加成功'];
            }
        }catch( PDOException $e){
            return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
        }
    }



}