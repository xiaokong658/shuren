<?php

namespace app\admin\model;
use think\Model;
use think\Db;

class ProductModel extends Model
{
    protected $name = 'product';


    public function getProductByWhere($map, $Nowpage, $limits)
    {
        $list = $this->field('*')->where($map)->page($Nowpage, $limits)->order('id desc')->select();
        foreach($list as $key=>$val){
            $cate = $this->getOneCate($val['cate']);
            $list[$key]['cate'] = $cate['cate'];
        }
        return $list;
    }


    /**
     * 根据搜索条件获取分类信息
     */
    public function getProductCate($map, $Nowpage, $limits)
    {

        return  $this->table('shuren_product_cate')->where($map)->page($Nowpage, $limits)->order('id desc')->select();

    }

    public function getAllProductCate($map)
    {

        return  $this->table('shuren_product_cate')->where($map)->order('id desc')->select();

    }


    /**
     * 插入产品信息
     * @param $param
     */
    public function insertProduct($param)
    {
        try{
            $param['addtime'] = time();
            $result = $this->validate('ProductValidate')->allowField(true)->save($param);
            if(false === $result){
                return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' =>'添加信息成功'];
            }
        }catch( PDOException $e){
            return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
        }
    }


    /**
     * 编辑分类信息
     * @param $param
     */
    public function editCate($param,$id)
    {
        try{
            $result =  $this->table('shuren_product_cate')->where('id',$id)->update($param);
            if(false === $result){
                return ['code' => 0, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '修改分类成功'];
            }
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }


    /**
     * 根据id获取产品分类
     * @param $id
     */
    public function getOneCate($id)
    {
        return $this->table('shuren_product_cate')->where('id', $id)->find();
    }

    /**
     * 根据id获取产品
     * @param $id
     */
    public function getOneProduct($id)
    {
        return $this->where('id', $id)->find();
    }


    /**
     * 修改产品
     * @param $id
     */
    public function editProduct($param)
    {
        try{
            $result =  $this->validate('ProductValidate')->allowField(true)->save($param, ['id' => $param['id']]);
            if(false === $result){
                return ['code' => 0, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '编辑用户信息成功'];
            }
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }



    /**
     * 删除管理员
     * @param $id
     */
    public function delProduct($id)
    {
        try{

            $this->where('id', $id)->delete();
            return ['code' => 1, 'data' => '', 'msg' => '删除产品成功'];

        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }
}