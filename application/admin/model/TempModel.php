<?php

namespace app\admin\model;
use think\Model;
use think\Db;

class TempModel extends Model
{
    protected $name = 'temp_message';


    public function getTempByWhere($map, $Nowpage, $limits)
    {

        return $this->field('shuren_temp_message.*,shuren_instit.name')->join('shuren_instit','shuren_temp_message.instit_id = shuren_instit.id')
            ->where($map)->page($Nowpage, $limits)->order('id desc')->select();

    }



    public function getTempByWhereTeach($map,$map2,$Nowpage, $limits)
    {

        return $this->field('shuren_temp_message.*,shuren_instit.name')->join('shuren_instit','shuren_temp_message.instit_id = shuren_instit.id')->
            where($map)->whereor($map2)->page($Nowpage, $limits)->order('id desc')->select();

    }



}