<?php

namespace app\admin\model;
use think\Model;
use think\Db;

class Debate_listModel extends Model
{
    protected $name = 'debate_list';

    // 开启自动写入时间戳字段
   // protected $autoWriteTimestamp = true;


    /**
     * 根据搜索条件获取用户列表信息
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function getListByWhere($map, $Nowpage, $limits)
    {
        return $this->field('shuren_debate_list.*,shuren_debate.title')->join('shuren_debate', 'shuren_debate_list.debate_id = shuren_debate.id')->where($map)->page($Nowpage, $limits)->order('id desc')->select();
    }


    /**
     * [insertArticle 添加文章]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function insertArticle($param)
    {
        try{
            $result = $this->allowField(true)->save($param);
            if(false === $result){
                return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '文章添加成功'];
            }
        }catch( PDOException $e){
            return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
        }
    }



    /**
     * [updateArticle 编辑文章]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function updateArticle($param)
    {
        try{
            $result = $this->allowField(true)->save($param, ['id' => $param['id']]);
            if(false === $result){
                return ['code' => 0, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '文章编辑成功'];
            }
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }



    /**
     * [getOneArticle 根据文章id获取一条信息]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function getOneArticle($id)
    {
        return $this->where('id', $id)->find();
    }



    /**
     * [delArticle 删除文章]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function delArticle($id)
    {
        try{
            $this->where('id', $id)->delete();
            return ['code' => 1, 'data' => '', 'msg' => '观点删除成功'];
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }

}