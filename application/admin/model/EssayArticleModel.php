<?php

namespace app\admin\model;
use think\Model;
use think\Db;

class EssayArticleModel extends Model
{
    protected $name = 'essay_article';


    /**
     * 根据搜索条件获取用户列表信息
     * @author [田建龙] [864491238@qq.com]
     */
    public function getAllByWhere($map, $Nowpage, $limits)
    {
        return $this->field('shuren_essay_article.*,shuren_article.title')->join('shuren_article', 'shuren_essay_article.article_id = shuren_article.id')
            ->where($map)->page($Nowpage, $limits)->order('id desc')->select();
    }



    public function articleRead($id){
        return $this->field('shuren_essay_article.*,shuren_essay.title')->join('shuren_essay', 'shuren_essay_article.essay_id = shuren_essay.id')
            ->where('shuren_essay_article.id='.$id)->find();

    }


    /**
     * [insertArticle 添加文章]
     * @author [田建龙] [864491238@qq.com]
     */
    public function insertArticle($param)
    {
        try{
            $result = $this->allowField(true)->save($param);
            if(false === $result){
                return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '投稿文章添加成功'];
            }
        }catch( PDOException $e){
            return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
        }
    }



    /**
     * [updateArticle 编辑文章]
     * @author [田建龙] [864491238@qq.com]
     */
    public function updateArticle($param)
    {
        try{
            $result = $this->allowField(true)->save($param, ['id' => $param['id']]);
            if(false === $result){
                return ['code' => 0, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '文章编辑成功'];
            }
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }



    /**
     * [getOneArticle 根据文章id获取一条信息]
     * @author [田建龙] [864491238@qq.com]
     */
    public function getOneArticle($id)
    {
        return $this->where('id', $id)->find();
    }



    /**
     * [delArticle 删除文章]
     * @author [田建龙] [864491238@qq.com]
     */
    public function delArticle($id)
    {
        try{
            $this->where('id', $id)->delete();
            return ['code' => 1, 'data' => '', 'msg' => '文章删除成功'];
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }

    public function essay_status($map)
    {
        try{
            $result = $this->allowField(true)->save($map, ['id' => $map['id']]);
            if(false === $result){
                return ['code' => 0, 'data' => '', 'msg' => $this->getError()];
            }else{
                switch($map['status']){
                    case 1: $msg = '文章采用成功'; break;
                    case 2: $msg = '文章拟采用成功'; break;
                    case 3: $msg = '文章退稿成功'; break;
                }
                return ['code' => 1, 'data' => '', 'msg' => $msg];
            }
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }


}