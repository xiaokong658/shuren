<?php

namespace app\admin\model;
use think\Model;
use think\Db;

class IncomepaymentModel extends Model
{
    protected $name = 'income_payment';

    public function getincomepaymentByWhere($map, $Nowpage, $limits)
    {
        return $this->field('shuren_income_payment.*,shuren_admin.real_name')->join('shuren_admin', 'shuren_income_payment.user_id = shuren_admin.id')->where($map)->page($Nowpage, $limits)->order('id desc')->select();
    }

    /**
     * 插入收支信息
     * @param $param
     */
    public function insertIncome($param)
    {
        try{
            $result = $this->validate('IncomepaymentValidate')->allowField(true)->save($param);
            if(false === $result){
                return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' =>'添加信息成功'];
            }
        }catch( PDOException $e){
            return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
        }
    }


    /**
     * 根据id获取收支记录
     * @param $id
     */
    public function getOneIncome($id)
    {
        return $this->where('id', $id)->find();
    }



    /**
     * 修改收支记录
     * @param $id
     */
    public function editIncome($param)
    {
        try{
            $result =  $this->validate('IncomepaymentValidate')->allowField(true)->save($param, ['id' => $param['id']]);
            if(false === $result){
                return ['code' => 0, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '编辑用户信息成功'];
            }
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }



    /**
     * 删除收支
     * @param $id
     */
    public function delIncome($id)
    {
        try{

            $this->where('id', $id)->delete();
            return ['code' => 1, 'data' => '', 'msg' => '删除收支信息成功'];

        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }
}