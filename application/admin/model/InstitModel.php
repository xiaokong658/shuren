<?php

namespace app\admin\model;
use think\Model;
use think\Db;

class InstitModel extends Model
{
    protected $name = 'instit';

    /**
     * [getAllCate 获取全部社会机构]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function getAllInstit()
    {
        return $this->where('type=2')->order('sort asc')->select();
    }



    public function getInstitApply($map, $Nowpage, $limits)
    {
        return Db::name('instit_apply')->where($map)->page($Nowpage, $limits)->order('status asc,id desc')->select();
    }




    /**
     * [getAllCate 获取全部教育机构]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function getAllEdu()
    {
        return $this->where('type=1')->order('sort asc')->select();
    }

    /**
     * [getAllCate 获取全部机构]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function getOpenInstit()
    {
        return $this->where('status =1')->order('sort asc')->select();
    }


    /**
     * [insertCate 添加机构]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function insertInstit($param)
    {
        try{
            $result = $this->save($param);
            if(false === $result){
                return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '机构添加成功'];
            }
        }catch( PDOException $e){
            return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
        }
    }

    /**
     * [insertCate 添加教育机构]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function insertEdu($param)
    {
        try{
            $result = $this->save($param);
            if(false === $result){
                return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '机构教育添加成功'];
            }
        }catch( PDOException $e){
            return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
        }
    }

    /**
     * [editMenu 编辑机构]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function editinstit($param)
    {
        try{
            $result = $this->save($param, ['id' => $param['id']]);
            if(false === $result){
                return ['code' => 0, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '机构编辑成功'];
            }
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }

    /**
     * [getOneMenu 根据机构id获取一条信息]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function getOneInstit($id)
    {
        return $this->where('id', $id)->find();
    }

    /**
     * [delMenu 删除机构]
     * @return [type] [description]
     * @author [沈晶晶] [334554156@qq.com]
     */
    public function delInstit($id)
    {
        try{
            $this->where('id', $id)->delete();
            return ['code' => 1, 'data' => '', 'msg' => '机构删除成功'];
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }


    public function have_child($id)
    {
        return $this->where('pid', $id)->find();
    }

}