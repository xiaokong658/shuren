<?php
namespace app\index\controller;
use think\Controller;
use think\View;
use think\Db;
use think\Request;
use think\Config;
class Index extends  Base
{
    public function index()
    {
        $user_id = session('user_id');

//        if(!$user_id){
//            $this->redirect('user/login');
//        }
        if(!empty($user_id)){
            $user_info = Db::name('member')->where('id',$user_id)->find();
            if( $this->is_weixin()){
                if(empty($user_info['open_id'])){
                    $open_id = session('open_id');

                    if(empty($open_id)){
                        $wechat_instit = Db::name('wechat')->where('instit_id',$user_info['instit_id'])->find();
                        if(!empty($wechat_instit['appid'])&&!empty($wechat_instit['appid'])){
                             $this->get_openid($wechat_instit['appid'],$wechat_instit['appsecret']);
                        }
                    }else{
                        $data2['open_id'] = $open_id;
                        Db::name('member')->where('id',$user_info['id'])->update($data2);
                    }
                }
            }
            $wechat_instit = Db::name('wechat')->where('instit_id',$user_info['instit_id'])->find();
        }




//        if($wechat_instit&&empty($user_info['open_id'])){
//                      $this->get_openid($wechat_instit['appid'],$wechat_instit['appsecret']);
//        }
      //  header("Location: " . $wechat_url . "\n");

//        $dynamic_list =  Db::name('dynamic')->alias('a')->field('a.*,b.nickname,b.head_img')->join('shuren_member b','a.user_id = b.id')->order('id desc')->select();
//
//       foreach($dynamic_list as $key=>$val){
//           $dynamic_list[$key]['addtime'] = date('m-d H:i',$val['addtime']);
//           $dynamic_list[$key]['content'] = $this->replaces($val['content']);
//       }

        $ad = $this->get_ad('25');

            $map = array(
//                'is_tui'=>1,
                'status'=>1,
            );

            $list = Db::name('article')->field('id,title,photo,create_time,user_id,writer')->where($map)->order('id desc')->limit(5)->select();
            foreach($list as $key=>$val){
                if(!empty($val['user_id'])){
                    $author = Db::name('member')->field('nickname')->where('id',$val['user_id'])->find();
                    $list[$key]['author'] = $author['nickname'];
                }else{
                    $list[$key]['author'] = $val['writer'];
                }
                $list[$key]['create_time'] = date('Y-m-d',$val['create_time']);
            }

        $map1 = array(
//            'is_tui'=>1,
        );
        $essay = Db::name('essay')->field('id,title,photo,create_time,instit_id')->where($map1)->order('id desc')->limit(5)->select();
        foreach($essay as $key=>$val){
                $author = Db::name('instit')->field('name')->where('id',$val['instit_id'])->find();
                $essay[$key]['author'] = $author['name'];
                $essay[$key]['create_time'] = date('Y-m-d',$val['create_time']);
        }
        $map1 = array(
//            'is_tui'=>1,
        );
        $debate = Db::name('debate')->field('id,title,photo,create_time,user_id')->where($map1)->order('id desc')->limit(5)->select();
        foreach($debate as $key=>$val){
            $author = Db::name('member')->field('nickname')->where('id',$val['user_id'])->find();
            $debate[$key]['author'] = $author['nickname'];
            $debate[$key]['create_time'] = date('Y-m-d',$val['create_time']);
        }


        $web_info = array(
                'web_site_title'=>$this->get_config_v('web_site_title'),
                'web_site_description'=>$this->get_config_v('web_site_description'),
                'web_site_keyword'=>$this->get_config_v('web_site_keyword'),
        );

        $view = new View();
        $view->assign('ad',$ad);
        //$view->assign('dynamic',$dynamic_list);
        $view->assign('article',$list);
        $view->assign('debate',$debate);
        $view->assign('essay',$essay);
        $view->assign('web_info',$web_info);
        $view->assign('footfrom',1);
        return $view->fetch();
    }

    

    public function index1()
    {
        $ad = $this->get_ad('25');
        $cate = Db::name('article_cate')->where('status',1)->select();
        foreach($cate as $key=>$val){
            $map = array(
                'cate_id'=>$val['id'],
                'type'=>1
            );
            $list = Db::name('article')->field('id,title,photo')->where($map)->order('id desc')->limit(3)->select();
            $cate[$key]['list'] = $list;
        }
        $view = new View();
        $view->assign('ad',$ad);
        $view->assign('cate',$cate);
        return $view->fetch();
    }

    public function article_detail()
    {
        $id = input('param.id');
        $this->read_article($id);
        $article = Db::name('article')->where('id',$id)->find();


        if(empty($article['user_id'])){
            $map6 = array(
                'instit_id'=>$article['instit_id'],
                'group_id'=>8
            );
            $user_info = Db::name('member')->field('id,nickname,head_img,desc')->where($map6)->find();

            $author = $user_info['nickname'];
        }else{

           // $auth = Db::name('member')->field('nickname')->where('id',$article['user_id'])->find();
            $user_info = Db::name('member')->field('id,nickname,head_img,desc')->where('id',$article['user_id'])->find();
            $author = $user_info['nickname'];
        }






        $user_id = session('user_id');
        //$user_id = 14;
        $zans = Db::name('article_zan')->field('count(*) as num')->where('article_id',$id)->find();
        $map = array(
                'user_id'=>$user_id,
                'article_id'=>$id,
        );
        $myzan = Db::name('article_zan')->field('is_zan')->where($map)->find();
        if($myzan){
            $is_zan = $myzan['is_zan'];
        }else{
            $is_zan = 0;
        }
        $collects = Db::name('article_collect')->where('article_id',$id)->count();
        $map = array(
            'user_id'=>$user_id,
            'article_id'=>$id,
        );
        $mycollect = Db::name('article_collect')->field('is_collect')->where($map)->find();
        if($mycollect){
            $is_collect = $mycollect['is_collect'];
        }else{
            $is_collect = 0;
        }


        $map10 = array(
            'article_id'=>$id,
            'status'=>1,
        );
        $commentall = Db::name('article_comment')->where($map10)->count();
        $commentpage =$commentall/5;
        $commentpage = (int)$commentpage;
        $commentyu = $commentall%5;

        $comment_list =  Db::name('article_comment')->alias('a')->field('a.id,a.content,a.addtime,b.nickname,b.head_img')->join('shuren_member b','a.user_id = b.id')->where('article_id',$id)->select();


        if($article['vote_id']>0){

            $vote = Db::name('vote')->where('id',$article['vote_id'])->find();
            $choose = Db::name('vote_choose')->where('vote_id',$article['vote_id'])->select();
            foreach($choose as $key => $val){
                $map = [];
                $map['vote_id'] = $article['vote_id'];
//            $map['choose'] = ['like','%,'.$v['id'].',%'];
                $v_id = $val['id'];
                $map['choose'] = ['like',"%," . $v_id . ",%"];
                //  print_R($map);
                $count = Db::name('vote_log')->field('count(id) as num')->where($map)->find();
                $choose[$key]['num'] = $count['num'];
            }



            if(empty($choose[0]['img'])){
                $imgs = 0;
            }else{
                $imgs = 1;
            }

        }else{
            $vote = array('max_num'=>0);
            $choose=array();
            $imgs=0;
        }


        if($article['baoming_id']>0){
            $baoming = Db::name('baoming')->where('id',$article['baoming_id'])->find();
//            $instit = Db::name('instit')->field('name')->where('id',$vote['instit_id'])->find();
//            $vote['addtime1'] = date('Y-m-d',$vote['addtime']);
//            $vote['instit_name'] = $instit['name'];

            $xiangmu = explode('|',$baoming['desc']);
            $arr = array();
            foreach($xiangmu as $k=>$v){
                $arr[$k]['name'] = $v;
                $arr[$k]['title'] = 'tt'.$k;
                $i = $k;
            }


        }else{
            $baoming =array();
            $arr =array();
            $i = '';
        }




        $view = new View();


        $view->assign('baoming',$baoming);
        $view->assign('xiangmu',$arr);
        $view->assign('i',$i);


        $view->assign('vote',$vote);
        $view->assign('choose',$choose);
        $view->assign('imgs',$imgs);

        $view->assign('article',$article);    //文章内容

        $view->assign('comment_list',$comment_list);    //文章内容


        $view->assign('author',$author);   //总的点赞数

        if($commentyu>0){
            $view->assign('all',$commentpage+1);   //总的点赞数
        }else{
            $view->assign('all',$commentpage);   //总的点赞数
        }
        $view->assign('zan',$zans['num']);   //总的点赞数
        $view->assign('is_zan',$is_zan);     //我是否赞
        $view->assign('collect',$collects);

        if(empty($user_id)){
            $user_id = 0;
        }





        $tp_article = Db::name('pt_article_edit')->alias('a')->field('a.title,a.remark,a.content,a.update_time,b.name,b.id')->join('instit b','a.by_instit_id=b.id')->where('a.article_id',$id)->select();




        $view->assign('tp_article',$tp_article);
        $view->assign('user_id',$user_id);
        $view->assign('commentall',$commentall);
        $view->assign('user_info',$user_info);
        $view->assign('id',$id);
        $view->assign('is_collect',$is_collect);
        return $view->fetch('index/article_detail');

    }



    public function article_edit()
    {
        $id = input('param.id');
        $this->read_article($id);
        $article = Db::name('article_edit')->where('id',$id)->find();
        $view = new View();
        $view->assign('article',$article);    //文章内容
        return $view->fetch('index/article_edit');

    }


    public function zan_status()
    {
       // return json(['code' => -1,'count'=>1, 'msg' => '点赞已取消']);
        $id=input('param.id');
        $user_id = session('user_id');

        if(empty($user_id)){
            return json(['code' => 11, 'msg' => '您尚未登录！']);
        }
        //$user_id = 14;
        $map = array(
            'user_id'=>$user_id,
            'article_id'=>$id,
        );
        $is_zan = Db::name('article_zan')->where($map)->find();
        if(!$is_zan){
            $is_zan['is_zan'] = 0;
        }
        if($is_zan['is_zan']==1)
        {
            Db::name('article_zan')->where('id',$is_zan['id'])->delete();
            $count = Db::name('article_zan')->where('article_id',$id)->count();
            return json(['code' => -1,'count'=>$count, 'msg' => '点赞已取消']);
        }else{
            $data = array(
                'user_id'=>$user_id,
                'article_id'=>$id,
                'is_zan'=>1,
            );
            $flag = Db::name('article_zan')->insert($data);
            $count = Db::name('article_zan')->where('article_id',$id)->count();
            return json(['code' => 1,'count'=>$count,  'msg' => '点赞成功']);
        }

    }


    public function essay_zan_status()
    {
        // return json(['code' => -1,'count'=>1, 'msg' => '点赞已取消']);
        $id=input('param.id');
        $user_id = session('user_id');

        if(empty($user_id)){
            return json(['code' => 11, 'msg' => '您尚未登录！']);
        }
        //$user_id = 14;
        $map = array(
            'user_id'=>$user_id,
            'essay_id'=>$id,
        );
        $is_zan = Db::name('essay_zan')->where($map)->find();
        if(!$is_zan){
            $is_zan['is_zan'] = 0;
        }
        if($is_zan['is_zan']==1)
        {
            Db::name('essay_zan')->where('id',$is_zan['id'])->delete();
            $count = Db::name('essay_zan')->where('essay_id',$id)->count();
            return json(['code' => -1,'count'=>$count, 'msg' => '点赞已取消']);
        }else{
            $data = array(
                'user_id'=>$user_id,
                'essay_id'=>$id,
                'is_zan'=>1,
            );
            $flag = Db::name('essay_zan')->insert($data);
            $count = Db::name('essay_zan')->where('essay_id',$id)->count();
            return json(['code' => 1,'count'=>$count,  'msg' => '点赞成功']);
        }

    }

    public function debate_zan_status()
    {
        // return json(['code' => -1,'count'=>1, 'msg' => '点赞已取消']);
        $id=input('param.id');
        $user_id = session('user_id');

        if(empty($user_id)){
            return json(['code' => 11, 'msg' => '您尚未登录！']);
        }
        //$user_id = 14;
        $map = array(
            'user_id'=>$user_id,
            'debate_id'=>$id,
        );
        $is_zan = Db::name('debates_zan')->where($map)->find();
        if(!$is_zan){
            $is_zan['is_zan'] = 0;
        }
        if($is_zan['is_zan']==1)
        {
            Db::name('debates_zan')->where('id',$is_zan['id'])->delete();
            $count = Db::name('debates_zan')->where('debate_id',$id)->count();
            return json(['code' => -1,'count'=>$count, 'msg' => '点赞已取消']);
        }else{
            $data = array(
                'user_id'=>$user_id,
                'debate_id'=>$id,
                'is_zan'=>1,
            );
            $flag = Db::name('debates_zan')->insert($data);
            $count = Db::name('debates_zan')->where('debate_id',$id)->count();
            return json(['code' => 1,'count'=>$count,  'msg' => '点赞成功']);
        }

    }

    public function debate_collect_status()
    {

        $id=input('param.id');
        $user_id = session('user_id');
        if(empty($user_id)){
            return json(['code' => 11, 'msg' => '您尚未登录！']);
        }
        $map = array(
            'user_id'=>$user_id,
            'debate_id'=>$id,
        );
        $is_zan = Db::name('debate_collect')->where($map)->find();
        if(!$is_zan){
            $is_zan['is_collect'] = 0;
        }
        if($is_zan['is_collect']==1)
        {
            Db::name('debate_collect')->where('id',$is_zan['id'])->delete();
            $count = Db::name('debate_collect')->where('debate_id',$id)->count();
            return json(['code' => -1,'count'=>$count, 'msg' => '收藏已取消']);
        }else{
            $data = array(
                'user_id'=>$user_id,
                'debate_id'=>$id,
                'is_collect'=>1,
            );
            Db::name('debate_collect')->insert($data);
            $count = Db::name('debate_collect')->where('debate_id',$id)->count();
            return json(['code' => 1,'count'=>$count,  'msg' => '收藏成功']);
        }
    }


    public function article_comment(){

        $article_id=input('param.id');
        $content=input('param.content');
        $user_id = session('user_id');

        $data = array(
            'article_id'=>  $article_id,
            'user_id'=>  $user_id,
            'content'=>  $content,
            'addtime'=>time(),
            'status'=>1
        );

        $flag = Db::name('article_comment')->insert($data);
        if($flag){


            $member = Db::name('member')->where('id',$data['user_id'])->find();
            $data['addtime'] = date('Y-m-d H:i:s',$data['addtime']);
            if(!empty($member['head_img'])){
                $data['img'] = "/uploads/face/".$member['head_img'];
            }else{
                $data['img'] = $member['head_img'];
            }
            $data['nickname'] = $member['nickname'];
            return json(['code' => 1, 'list'=>$data,   'msg' => '评论成功']);
        }else{
            return json(['code' => -1,  'msg' => '评论失败']);
        }
    }


    public function comment_list(){
        $article_id=input('param.id');
        $page=input('param.page');
        $a = ($page-1)*5;

        $map = array(
            'a.article_id'=>  $article_id,
            'a.status'=>  1,
        );

        $list = Db::name('article_comment')->alias('a')->field('a.user_id,.a.id,a.content,a.addtime,b.nickname,b.head_img')->join('shuren_member b','a.user_id = b.id')->where($map)->order('id desc')->limit($a,5)->select();
//      $flag = Db::name('article_comment')->insert($data);
        if($list){
            foreach($list as $key=>$item){
                $list[$key]['addtime'] = date('Y-m-d H:i:s',$item['addtime']);
            }

        }
        return json($list);
    }


    public function collect_status()
    {

        $id=input('param.id');
        $user_id = session('user_id');
        if(empty($user_id)){
            return json(['code' => 11, 'msg' => '您尚未登录！']);
        }
        $map = array(
            'user_id'=>$user_id,
            'article_id'=>$id,
        );

        $is_zan = Db::name('article_collect')->where($map)->find();
        if(!$is_zan){
            $is_zan['is_collect'] = 0;
        }
        if($is_zan['is_collect']==1)
        {
            Db::name('article_collect')->where('id',$is_zan['id'])->delete();
            $count = Db::name('article_collect')->where('article_id',$id)->count();
            return json(['code' => -1,'count'=>$count, 'msg' => '收藏已取消']);
        }else{
            $data = array(
                'user_id'=>$user_id,
                'article_id'=>$id,
                'is_collect'=>1,
                'create_time'=>time()
            );
            Db::name('article_collect')->insert($data);
            $count = Db::name('article_collect')->where('article_id',$id)->count();
            return json(['code' => 1,'count'=>$count,  'msg' => '收藏成功']);
        }
    }

    public function essay_collect_status()
    {

        $id=input('param.id');
        $user_id = session('user_id');
        if(empty($user_id)){
            return json(['code' => 11, 'msg' => '您尚未登录！']);
        }
        $map = array(
            'user_id'=>$user_id,
            'essay_id'=>$id,
        );
        $is_zan = Db::name('essay_collect')->where($map)->find();
        if(!$is_zan){
            $is_zan['is_collect'] = 0;
        }
        if($is_zan['is_collect']==1)
        {
            Db::name('essay_collect')->where('id',$is_zan['id'])->delete();
            $count = Db::name('essay_collect')->where('essay_id',$id)->count();
            return json(['code' => -1,'count'=>$count, 'msg' => '收藏已取消']);
        }else{
            $data = array(
                'user_id'=>$user_id,
                'essay_id'=>$id,
                'is_collect'=>1,
            );
            Db::name('essay_collect')->insert($data);
            $count = Db::name('essay_collect')->where('essay_id',$id)->count();
            return json(['code' => 1,'count'=>$count,  'msg' => '收藏成功']);
        }
    }

    public function essay()
    {
        $ad = $this->get_ad('27');
        $map  = array();
        $list = Db::name('essay')->where($map)->order('id desc')->select();
        $view = new View();
        $view->assign('ad',$ad);
        $view->assign('list',$list);
        return $view->fetch();
    }


    public function debate()
    {
        $ad = $this->get_ad('26');
        $map  = array();
        $list = Db::name('debate')->where($map)->order('id desc')->select();
        $view = new View();
        $view->assign('ad',$ad);
        $view->assign('list',$list);
        return $view->fetch();
    }


    public function debate_detail()
    {
        $id=input('param.id');
        $map  = array();
        $map['a.id'] = $id;
        $info = Db::name('debate')->alias('a')->field('a.*,b.nickname')->join('shuren_member b','a.user_id=b.id')->where($map)->find();

        $info['create_time1'] = date('Y-m-d H:i:s',$info['create_time']);
        $info['end_time1'] = date('Y-m-d H:i:s',$info['end_time']);
        if($info['end_time']<time()){
            $info['can'] = 0;
        }else{
            $info['can'] = 1;
        }
        $comment_num =  Db::name('debate_list')->where('debate_id',$id)->count();

        $view = new View();
        $user_id = session('user_id');
        if(empty($user_id)){
            $user_id = 0;
        }

        $zans = Db::name('debates_zan')->field('count(*) as num')->where('debate_id',$id)->find();
        $map = array(
            'user_id'=>$user_id,
            'debate_id'=>$id,
        );
        $myzan = Db::name('debates_zan')->field('is_zan')->where($map)->find();
        if($myzan){
            $is_zan = $myzan['is_zan'];
        }else{
            $is_zan = 0;
        }
        $collects = Db::name('debate_collect')->field('count(*) as num')->where('debate_id',$id)->find();

        $map = array(
            'user_id'=>$user_id,
            'debate_id'=>$id,
        );
        $mycollect = Db::name('debate_collect')->field('is_collect')->where($map)->find();
        if($mycollect){
            $is_collect = $mycollect['is_collect'];
        }else{
            $is_collect = 0;
        }

        if(empty($info['user_id'])){
            $map6 = array(
                'instit_id'=>$info['instit_id'],
                'group_id'=>8
            );
            $user_info = Db::name('member')->field('id,nickname,head_img,desc')->where($map6)->find();


        }else{

            // $auth = Db::name('member')->field('nickname')->where('id',$article['user_id'])->find();
            $user_info = Db::name('member')->field('id,nickname,head_img,desc')->where('id',$info['user_id'])->find();
        }



        $view->assign('user_id',$user_id);
        $view->assign('info',$info);
        $view->assign('comment_num',$comment_num);
        $view->assign('user_info',$user_info);
        $view->assign('id',$id);
        $view->assign('zan',$zans['num']);   //总的点赞数
        $view->assign('is_zan',$is_zan);     //我是否赞
        $view->assign('collect',$collects['num']);
        $view->assign('is_collect',$is_collect);
        return $view->fetch();
    }


    public function debate_list(){
        $article_id=input('param.id');
        $page=input('param.page');
        $a = ($page-1)*5;

        $user_id = session('user_id');
        $map10 = array(
            'a.debate_id'=>$article_id,
            'a.status'=>1,
        );
        $list =  Db::name('debate_list')->alias('a')->field('a.*,b.nickname,b.head_img')->join('shuren_member b','a.user_id = b.id')->where($map10)->order('id desc')->limit($a,5)->select();

        if($list){
            foreach($list as $key=>$item){
                $list[$key]['add_time'] = date('m-d H:i',$item['add_time']);
                if($item['type']==1){
                    $list[$key]['note'] = "正方";
                }else{
                    $list[$key]['note'] = "反方";
                }
                $map['debate_list_id'] = $item['id'];
                $map['is_zan'] = 1;
                $zan_count = Db::name('debate_zan')->where($map)->count();
                $map1['debate_list_id'] = $item['id'];
                $map1['status'] = 1;
                $comment_count = Db::name('debate_comment')->where($map1)->count();
                $list[$key]['zan_count'] = $zan_count;
                $list[$key]['comment_count'] = $comment_count;

                $map2['user_id'] = $user_id;
                $map2['debate_list_id'] = $item['id'];
                $my_zan = Db::name('debate_zan')->where($map2)->count();
                $list[$key]['is_zan'] = $my_zan;

            }
            return json(['code' => 1, 'list'=>$list,  'msg' => 'ok']);
        }
    }

    public function debate_comment_list(){
        $debate_list_id=input('param.id');
        $page=input('param.page');
        $a = ($page-1)*5;
        $map['a.debate_list_id'] = $debate_list_id;
        $map['a.status'] = 1;
       // $map['status'] = 1;
        $list =  Db::name('debate_comment')->alias('a')->field('a.*,b.nickname,b.head_img')->join('shuren_member b','a.user_id = b.id')->where($map)->order('id desc')->limit($a,5)->select();
        if($list){
            foreach($list as $key=>$item){
                $list[$key]['addtime'] = date('m-d H:i',$item['addtime']);
            }
            return json(['code' => 1, 'list'=>$list,  'msg' => 'ok']);
        }
    }

    public function debate_comment(){

        $article_id=input('param.id');
        $content=input('param.content');
        $type=input('param.type');
        $user_id = session('user_id');
        if(empty($user_id)){
            return json(['code' => 11, 'msg' => '您尚未登录！']);
        }
        $data = array(
            'debate_id'=>  $article_id,
            'user_id'=>  $user_id,
            'content'=>  $content,
            'type'=>  $type,
            'status'=> 1,
            'add_time'=>time()
        );
        $flag = Db::name('debate_list')->insert($data);
        if($flag){
            $member = Db::name('member')->where('id',$data['user_id'])->find();
            $data['add_time'] = date('Y-m-d H:i:s',$data['add_time']);
            if(!empty($member['head_img'])){
                $data['img'] = "/uploads/face/".$member['head_img'];
            }else{
                $data['img'] = $member['head_img'];
            }
            $data['nickname'] = $member['nickname'];
            $data['id'] = $flag;
            return json(['code' => 1, 'list'=>$data,   'msg' => '评论成功']);
        }else{
            return json(['code' => -1,  'msg' => '评论失败']);
        }
    }


    public function debate_list_comment(){

        $article_id=input('param.id');
        $comment=input('param.comment');
        $user_id = input('param.user_id');
        $data = array(
            'debate_list_id'=>  $article_id,
            'user_id'=>  $user_id,
            'comment'=>  $comment,
            'status'=>  1,
            'addtime'=>time()
        );
        $flag = Db::name('debate_comment')->insert($data);
        if($flag){
            $member = Db::name('member')->where('id',$user_id)->find();
            $data['addtime'] = date('m-d H:i',$data['addtime']);
            if(!empty($member['head_img'])){
                $data['img'] = "/uploads/face/".$member['head_img'];
            }else{
                $data['img'] = $member['head_img'];
            }
            $data['nickname'] = $member['nickname'];
            return json(['code' => 1, 'list'=>$data,   'msg' => '评论成功']);
        }else{
            return json(['code' => -1,  'msg' => '评论失败']);
        }
    }

    public function debate_comment_comment(){
        $debate_list_id=input('param.id');
        $debate_id=input('param.debate_id');
        $user_id = session('user_id');
        $view = new View();
        $view->assign('debate_list_id',$debate_list_id);
        $view->assign('debate_id',$debate_id);
        $view->assign('user_id',$user_id);
        return $view->fetch();
    }





    public function essay_detail()
    {
        $id=input('param.id');
        $map  = array();
        $map['id'] = $id;
        $user_id = session('user_id');

        $info = Db::name('essay')->where($map)->find();

        if($info['end_time']>time()){
            $can_tou =1;
        }else{
            $can_tou =0;
        }


        $info['end_time'] = date('Y-m-d H:i:s',$info['end_time']);
        $map1 = array(
           'user_id'=>$user_id,
            'status'=>1
        );
        $myarticle = Db::name('article')->field('id,title')->where($map1)->select();

        $zans = Db::name('essay_zan')->field('count(*) as num')->where('essay_id',$id)->find();
        $map = array(
            'user_id'=>$user_id,
            'essay_id'=>$id,
        );
        $myzan = Db::name('essay_zan')->field('is_zan')->where($map)->find();
        if($myzan){
            $is_zan = $myzan['is_zan'];
        }else{
            $is_zan = 0;
        }
        $collects = Db::name('essay_collect')->field('count(*) as num')->where('essay_id',$id)->find();
        $map = array(
            'user_id'=>$user_id,
            'essay_id'=>$id,
        );
        $mycollect = Db::name('essay_collect')->field('is_collect')->where($map)->find();
        if($mycollect){
            $is_collect = $mycollect['is_collect'];
        }else{
            $is_collect = 0;
        }
        if(empty($user_id)){
            $user_id = 0;
        }

        $map10 = array(
            'a.essay_id'=>$id,
            'a.status'=>1
        );

        $list = Db::name('essay_article')->alias('a')->field('a.id,a.article_id,a.add_time,b.title,b.photo')->
        join('shuren_article b','a.article_id = b.id')->where($map10)->order('id desc')->select();




//        if($list){
//            foreach($list as $key=>$item){
//                $map11['article_id'] = $item['article_id'];
//                $count = Db::name('article_edit')->where($map11)->count();
//                $list[$key]['count'] = $count;
//                if($count>0){
//                    $article_eidt_info = Db::name('article_edit')->where($map11)->find();
//                    $list[$key]['article_eidt_id'] = $article_eidt_info['id'];
//                    $list[$key]['article_eidt_title'] = $article_eidt_info['title'];
//                    $list[$key]['update_time'] = date('m-d H:i',$article_eidt_info['update_time']);
//                }
//                $list[$key]['add_time'] = date('m-d H:i',$item['add_time']);
//            }
//        }



            $map6 = array(
                'instit_id'=>$info['instit_id'],
                'group_id'=>8
            );
            $user_info = Db::name('member')->field('id,nickname,head_img,desc')->where($map6)->find();


        $view = new View();
        $view->assign('zan',$zans['num']);   //总的点赞数
        $view->assign('is_zan',$is_zan);     //我是否赞
        $view->assign('collect',$collects['num']);
        $view->assign('is_collect',$is_collect);
        $view->assign('user_id',$user_id);
        $view->assign('myarticle',$myarticle);
        $view->assign('user_info',$user_info);
        $view->assign('info',$info);
        $view->assign('id',$id);
       $view->assign('list',$list);
        $view->assign('can_tou',$can_tou);
        return $view->fetch();

    }

    public function essay_join(){

        $article_id=input('param.article_id');
        $essay_id=input('param.essay_id');
        $user_id =input('param.user_id');

        $data = array(
            'article_id'=>  $article_id,
            'user_id'=>  $user_id,
            'essay_id'=>  $essay_id,

        );

        $is_part = Db::name('essay_article')->where($data)->find();
        if($is_part){
            return json(['code' => 3, 'list'=>$data,   'msg' => '此文章已参与本次征文！']);
        }
        //'add_time'=>time()
        $data['add_time'] = time();
        $flag = Db::name('essay_article')->insert($data);
        if($flag){
            $article = Db::name('article')->where('id',$data['article_id'])->find();
            $data['add_time'] = date('Y-m-d H:i:s',$data['add_time']);
            $data['title'] = $article['title'];
            $data['photo'] = $article['photo'];
            $data['remark'] = $article['remark'];
            return json(['code' => 1, 'list'=>$data,   'msg' => '参与征文成功']);
        }else{
            return json(['code' => -1,  'msg' => '参与征文失败']);
        }
    }


    public function essay_article_list(){
        $essay_id=input('param.id');


        $page=input('param.page');

        if($page==-1){


            $map10 = array(
                'a.essay_id'=>$essay_id,
                'a.status'=>['<>',1],
            );

            $list = Db::name('essay_article')->alias('a')->field('a.id,b.remark,a.article_id,a.add_time,b.title,b.photo')->
            join('shuren_article b','a.article_id = b.id')->where($map10)->order('id desc')->select();
        }else{
            $a = ($page-1)*5;

            $map10 = array(
                'a.essay_id'=>$essay_id,
                'a.status'=>['<>',1],
            );

            $list = Db::name('essay_article')->alias('a')->field('a.id,b.remark,a.article_id,a.add_time,b.title,b.photo')->
            join('shuren_article b','a.article_id = b.id')->where($map10)->order('id desc')->limit($a,5)->select();
        }





       // $list = Db::name('essay_article')->alias('a')->field('a.id,a.essay_id,a.status,a.article_id,a.add_time,b.title,b.photo')->
       // join('shuren_article b','a.article_id = b.id')->where($map)->order('id desc')->limit($a,5)->select();
//      $flag = Db::name('article_comment')->insert($data);
        if($list){
            foreach($list as $key=>$item){

                $map['article_id'] = $item['article_id'];

                $count = Db::name('article_edit')->where($map)->count();
                $list[$key]['count'] = $count;
                if($count>0){
                    $article_eidt_info = Db::name('article_edit')->where($map)->find();
                    $list[$key]['article_eidt_id'] = $article_eidt_info['id'];
                    $list[$key]['article_eidt_title'] = $article_eidt_info['title'];
                    $list[$key]['update_time'] = date('m-d H:i',$article_eidt_info['update_time']);
                }



                $list[$key]['add_time'] = date('m-d H:i',$item['add_time']);
            }
            return json(['code' => 1, 'list'=>$list,  'msg' => 'ok']);
        }
    }


    public function debate_list_zan_status()
    {
        // return json(['code' => -1,'count'=>1, 'msg' => '点赞已取消']);
        $id=input('param.id');
        $user_id = session('user_id');


        if(empty($user_id)){
            return json(['code' => 11, 'msg' => '您尚未登录！']);
        }
        //$user_id = 14;
        $map = array(
            'user_id'=>$user_id,
            'debate_list_id'=>$id,
        );
        $is_zan = Db::name('debate_zan')->where($map)->find();
        if(!$is_zan){
            $is_zan['is_zan'] = 0;
        }
        if($is_zan['is_zan']==1)
        {
            Db::name('debate_zan')->where('id',$is_zan['id'])->delete();

        }else{
            $data = array(
                'user_id'=>$user_id,
                'debate_list_id'=>$id,
                'is_zan'=>1,
            );
            $flag = Db::name('debate_zan')->insert($data);

        }
        $count = Db::name('debate_zan')->where('debate_list_id',$id)->count();
        return json(['code' => 1,'count'=>$count,  'msg' => '点赞成功']);
    }



        public function  region(){

//            $map['pid'] = 0;
//            $map['id'] = ['neq','810000'];
//            $map['id'] = ['neq','820000'];
//            $map['id'] = ['neq','710000'];
//
//
//


           $list['province'] = Db::table('areas')->field('id,name')->where("pid =0 and id!='810000' and id!='820000' and id!='710000'")->order('id asc')->select();
            foreach($list['province'] as  $key=>$val){
                $list1= Db::table('areas')->field('id,name')->where('pid',$val['id'])->order('id asc')->select();
                $list['province'][$key]['city'] = $list1;
                foreach($list1 as $k=>$v){
                    $list3= Db::table('areas')->field('id,name')->where('pid',$v['id'])->order('id asc')->select();
                    $list['province'][$key]['city'][$k]['district'] = $list3;
//                        foreach($list3 as $kk=>$vv){
//                            $list4= Db::table('areas')->field('id,name')->where('pid',$vv['id'])->order('id asc')->select();
//                            $list['province'][$key]['city'][$k]['district'][$kk]['street'] = $list4;
//                        }
                }
            }

            $fp = fopen("log.txt", "a");
            flock($fp, LOCK_EX);
            fwrite($fp, "执行日期：" . strftime("%Y%m%d%H%M%S", time()) . "\n" . json_encode($list) . "\n");
            flock($fp, LOCK_UN);
            fclose($fp);

        }


    public function wechatlogin()
    {
        $code = $_GET['code'];
        $parent_id = $_GET['u'];
        if ($code == "") {
            return;
        }
//		$redirect_uri = $_GET['redirect_uri'];
//	    	echo $redirect_uri;exit;
        $web_access_token = $this->get_web_access_token($code);
        $url = "https://api.weixin.qq.com/sns/userinfo?access_token=" . $web_access_token['access_token'] . "&openid=" . $web_access_token['openid'] . "&lang=zh_CN";

        $retk = $web_access_token['refresh_token'];

        $_SESSION['refresh_token_hzhus'] = $retk;



        // 1. 初始化
        $ch = curl_init();
        // 2. 设置选项，包括URL
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        // 3. 执行并获取HTML文档内容
        $output = curl_exec($ch);
        // 4. 释放curl句柄
        curl_close($ch);
        $output_arr = json_decode($output, true);

    }



    public function get_web_access_token($code,$appid,$appsecret)
    {
        $url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=".$appid."&secret=".$appsecret."&code=" . $code . "&grant_type=authorization_code";
        $data = json_decode(file_get_contents($url), true);
        return $data;
    }

    public function get_openid($appid,$appsecret){
        $referer = input('get.referer') ? input('get.referer') : "0";
        if ($referer == "0") {
            // $last_url = "http://" . $_SERVER["HTTP_HOST"];
            $request = Request::instance();
            $url = $request->url();

            //$last_url = "http://".$url."/index/index/index";
            $last_url = urlencode($url);
        } else {
            $last_url =input('get.referer');
        }
        $wechat_url ="https://open.weixin.qq.com/connect/oauth2/authorize?appid=".$appid."&redirect_uri=" . $last_url . "&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect";

        $code = input('code');
        if($code){
            $access_tokens = $this->get_web_access_token($code,$appid,$appsecret);
            session('open_id',$access_tokens['openid']);
        }else{
            header("Location: " . $wechat_url . "\n");
        }

    }





}
