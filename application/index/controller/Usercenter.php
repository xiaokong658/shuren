<?php
namespace app\index\controller;
use think\Controller;
use think\View;
use think\Db;
use app\admin\model\GongwenModel;
use think\Cookie;

class Usercenter extends  Base
{

    public function index()
    {

       $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }

        $user_info = Db::name('member')->where('id',$user_id)->find();

        if(empty($user_info['nickname'])||empty($user_info['desc'])||empty($user_info['realname'])||empty($user_info['head_img'])){
            //$this->redirect('usercenter/user_edit');
        }
        $map['user_id'] = $user_id;
        $map['status'] = 1;
        $user_info['debate_num'] = Db::name('debate')->where('user_id',$user_id)->count();
        $user_info['essay_num'] = Db::name('essay')->where('user_id',$user_id)->count();
        $user_info['article_num'] = Db::name('article')->where($map)->count();

        $user_info['foucs_num'] = Db::name('fans')->where('user_id',$user_id)->count();
        $user_info['fans_num'] = Db::name('fans')->where('fans_id',$user_id)->count();
        $user_info['total'] =  $user_info['debate_num']+$user_info['article_num']+ $user_info['essay_num'];


        $day = strtotime(date('Y-m-d'));
        $is_sign = Db::name('sign_log')->where(['user_id'=>$user_id,'sign_time'=>$day])->count();


        $view = new View();
        $view->assign('info',$user_info);
        $view->assign('footfrom',3);
        $view->assign('is_sign',$is_sign);
        return $view->fetch();
    }

    public function myessay()
    {
        $user_id=input('param.user_id');
        $page=input('param.page');
        if(empty($page)){
            $page = 1;
        }
        $a = ($page-1)*5;

        $map['a.user_id'] = $user_id;
        // $map['status'] = 1;
        $list =  Db::name('essay_article')->alias('a')->field('a.add_time,a.status,b.title,c.title as article_title,c.photo')
            ->join('shuren_essay b','a.essay_id = b.id')->join('shuren_article c','a.article_id = c.id')->where($map)->order('a.id desc')->limit($a,5)->select();
        if($list){
            foreach($list as $key=>$item){
                $list[$key]['add_time'] = date('Y-m-d H:i:s',$item['add_time']);
            }
            return json(['code' => 1, 'list'=>$list,  'msg' => 'ok']);
        }else{
            return json(['code' => 0, 'msg' => 'end']);
        }
    }


    public function myarticle()
    {

        $user_id = session('user_id');
        $map['user_id'] = $user_id ;
      //  $map['status'] = array('egt',0);

//         $map['status'] = 1;

        $member = Db::name('member')->field('group_id')->where('id',$user_id)->find();


       // echo $member['group_id'];

        if($member['group_id']==8){

            $article_list  =  Db::name('article')->field('id,user_id,title,create_time,photo,status,remark,my_ding')->where(" status !=-2 and ( user_id=".$user_id." or ( user_id=0 and instit_id= ".session('instit_id')." )) ")->order('my_ding desc,id desc')->select();

        }else{
            $article_list  =  Db::name('article')->field('id,user_id,title,create_time,photo,status,remark,my_ding')->where($map)->order('my_ding desc,id desc')->select();
        }

            foreach($article_list as $key=>$item){
                $article_list[$key]['create_time'] = date('Y-m-d',$item['create_time']);
                $mapss = array();
                $mapss['user_id'] = $user_id;
                $mapss['article_id'] = $item['id'];
                $count = Db::name('article_recommend')->field('count(id) as num')->where(['user_id'=>$user_id,'article_id'=>$item['id']])->find();
                $article_list[$key]['is_want_tui'] = $count['num'];
            }

        $map1['user_id'] = $user_id ;

        $debate_list  =  Db::name('debate')->where('user_id',$user_id)->order('my_ding desc,id desc')->select();

        if($debate_list){
            foreach($debate_list as $key=>$item){
                $debate_list[$key]['create_time'] = date('Y-m-d',$item['create_time']);
            }
        }
        $view = new View();
        if($member['group_id']==8){
            $essay_list  =  Db::name('essay')->where('user_id',$user_id)->order('my_ding desc,id desc')->select();
            foreach($essay_list as $kv=>$vs){
                $essay_list[$kv]['create_time'] = date('Y-m-d',$vs['create_time']);
            }
            $view->assign('essay',$essay_list);
        }


        $user = Db::name('member')->field('nickname')->where('id',$user_id)->find();
        $view->assign('group_id',$member['group_id']);
        $view->assign('article',$article_list);
        $view->assign('debate',$debate_list);
        $view->assign('nickname',$user['nickname']);
        return $view->fetch();
    }



    public function myarticle1()
    {
        $user_id = session('user_id');

        $map['user_id'] =$user_id ;
        $map['status'] = 1;


        $user = Db::name('member')->field('nickname')->where('id',$user_id)->find();

        $article = Db::name('article_cate')->field('id as cate_id,name')->order('orderby asc')->select();

        $i = 0;
        foreach($article as $key=>$val){
            $map['cate_id'] = $val['cate_id'];
            $count  =  Db::name('article')->field("id")->where($map)->order('id desc')->count();

            if($count>0){
                $list  =  Db::name('article')->field("id,title,FROM_UNIXTIME(create_time,'%Y-%m-%d') as create_time,photo")->where($map)->order('id desc')->select();
                $articles[$key]['cate_id'] = $val['cate_id'];
                $articles[$key]['name'] = $val['name'];
                $articles[$key]['article_list'] = $list;
                if($i==0){
                    $articles[$key]['selected'] = 1;
                }else{
                    $articles[$key]['selected'] = 0;
                }
                $i++;
            }
        }


        $view = new View();
        $view->assign('articles',$articles);
        $view->assign('nickname',$user['nickname']);
        return $view->fetch();

    }

    public function mypartessay()
    {
        $user_id = session('user_id');
        $page = input('param.page');
        if(empty($page)){
            $page = 1;
        }
        $a = ($page-1)*5;

        $map['a.user_id'] = $user_id;
      //  $map['status'] = 1;

        if(input('param.page'))
        {
            //$list  =  Db::name('article')->field('id,title,create_time,photo')->where($map)->order('id desc')->limit($a,5)->select();
            $list = Db::name('essay_article')->alias('a')->field('a.status,a.essay_id,c.id,b.photo,a.add_time,b.title as essay_title,c.title as article_title')->join('essay b','a.essay_id=b.id')->join('article c','a.article_id=c.id')->where($map)->order('a.id desc')->limit($a,5)->select();
            foreach($list as $key=>$item){
                $list[$key]['add_time'] = date('Y-m-d H:i:s',$item['add_time']);
                if($item['status']==1){
                    $list[$key]['note'] = '已采用';
                }if($item['status']==2){
                    $list[$key]['note'] = '拟采用';
                }if($item['status']==3){
                    $list[$key]['note'] = '已退稿';
                }else{
                    $list[$key]['note'] = '';
                }
            }
            return json(['code' => 1, 'list'=>$list,  'msg' => 'ok']);
        }
        $view = new View();
        return $view->fetch();

    }



    public function mydraft()
    {
        $user_id = session('user_id');
        $page = input('param.page');
        if(empty($page)){
            $page = 1;
        }
        $a = ($page-1)*5;

        $map['user_id'] = $user_id;
        $map['status'] = -2;

        if(input('param.page'))
        {
            $list  =  Db::name('article')->field('id,title,create_time,photo')->where($map)->order('id desc')->limit($a,5)->select();

            foreach($list as $key=>$item){
                $list[$key]['create_time'] = date('Y-m-d H:i:s',$item['create_time']);
            }
            return json(['code' => 1, 'list'=>$list,  'msg' => 'ok']);
        }
        $view = new View();
        return $view->fetch();

    }


    public function myfocus()
    {

        $user_id = session('user_id');
        $page=input('param.page');
        if(empty($page)){
            $page = 1;
        }
        $a = ($page-1)*5;

        $map['a.user_id'] = $user_id;
        // $map['status'] = 1;
        if(input('param.page')){
              $list  =  Db::name('fans')->alias('a')->field('*')->join('shuren_member b','a.fans_id = b.id')->where($map)->order('a.id desc')->limit($a,5)->select();
            return json(['code' => 1, 'list'=>$list,  'msg' => 'ok']);
        }

        $view = new View();
        return $view->fetch();

    }


    public function myfans()
    {

        $user_id = session('user_id');
        $page=input('param.page');
        if(empty($page)){
            $page = 1;
        }
        $a = ($page-1)*5;

        $map['a.fans_id'] = $user_id;
        // $map['status'] = 1;
         if(input('param.page')){
             $list  =  Db::name('fans')->alias('a')->field('*')->join('shuren_member b','a.user_id = b.id')->where($map)->order('a.id desc')->limit($a,5)->select();
             return json(['code' => 1, 'list'=>$list,  'msg' => 'ok']);
        }
        $view = new View();
        return $view->fetch();
    }


    public function collect_article()
    {
        $user_id = input('param.user_id');
        $page=input('param.page');
        if(empty($page)){
            $page = 1;
        }
        $a = ($page-1)*5;
        $map['a.user_id'] = $user_id;
        $list  =  Db::name('article_collect')->alias('a')->field('*')->join('shuren_article b','a.article_id = b.id')->where($map)->order('a.id desc')->limit($a,5)->select();
        if($list){
            return json(['code' => 1, 'list'=>$list,  'msg' => 'ok']);
        }else{
            return json(['code' => 0, 'msg' => 'end']);
        }
    }


    public function collect_essay()
    {
        $user_id = input('param.user_id');
        $page=input('param.page');
        if(empty($page)){
            $page = 1;
        }
        $a = ($page-1)*5;
        $map['a.user_id'] = $user_id;
        $list  =  Db::name('essay_collect')->alias('a')->field('*')->join('shuren_essay b','a.essay_id = b.id')->where($map)->order('a.id desc')->limit($a,5)->select();
        if($list){
            return json(['code' => 1, 'list'=>$list,  'msg' => 'ok']);
        }else{
            return json(['code' => 0, 'msg' => 'end']);
        }
    }


    public function collect_debate()
    {
        $user_id = input('param.user_id');
        $page=input('param.page');
        if(empty($page)){
            $page = 1;
        }
        $a = ($page-1)*5;
        $map['a.user_id'] = $user_id;
        $list  =  Db::name('debate_collect')->alias('a')->field('*')->join('shuren_debate b','a.debate_id = b.id')->where($map)->order('a.id desc')->limit($a,5)->select();
        if($list){
            return json(['code' => 1, 'list'=>$list,  'msg' => 'ok']);
        }else{
            return json(['code' => 0, 'msg' => 'end']);
        }
    }


    public function send_info(){

        $user_id = session('user_id');

        if(request()->isPost()){
            $content = input('param.content');
            $share_id = input('param.share_id');
            $imgs = input('param.imgs');
            $at_user_id = input('param.at_user_id');

            $new = array(
                '1'=>'汗',
                '2'=>'白眼',
                '3'=>'鄙视',
                '4'=>'打',
                '5'=>'大哭',
                '6'=>'鼓掌',
                '7'=>'奸笑',
                '8'=>'惊吓',
                '9'=>'纠结',
                '10'=>'可爱',
                '11'=>'可怜',
                '12'=>'酷',
                '13'=>'脸红',
                '14'=>'骂人',
                '15'=>'亲亲',
                '16'=>'傻',
                '17'=>'调皮',
                '18'=>'偷笑',
                '19'=>'委屈',
                '20'=>'无语',
                '21'=>'晕',
                '22'=>'抠鼻');
            for($i=1;$i<=22;$i++){
                //<img src="/static/emoji/img/3.gif">
                $str1 = '<img src="/static/emoji/img/'.$i.'.gif">';
                $str2 = '['.$new[$i].']';
                $content =  str_replace($str1,$str2,$content);
            }


            $data = array(
                'user_id' =>$user_id,
                'content' =>$content,
                'img'=>$imgs,
                'share_id'=>$share_id,
                'addtime'=>time(),
                'at_user_id'=>$at_user_id

            );

            $people = substr($at_user_id,0,-1);
            $people_arr = explode(",",$people);




            $flag = Db::name('dynamic')->insertGetId($data);

            foreach($people_arr as $val){
                    $datas = array(
                      'user_id'=>$user_id,
                      'at_user_id'=>$val,
                      'dynamic_id'=>$flag,
                      'create_time'=>time(),
                    );
                Db::name('at_message')->insert($datas);
            }

            if($flag){
                return json(['code'=>1,'msg'=>'发布动态成功']);
            }else{
                return json(['code'=>0,'msg'=>'发布动态失败！']);
            }


        }
        $view = new View();
        $share_id = input('param.share_id');
        if($share_id){
            $map = array(
                'a.id'=>$share_id
            );
            $share =  Db::name('dynamic')->alias('a')->field('a.id,a.img,b.head_img,b.nickname,a.content')->join('member b','a.user_id = b.id')->where($map)->find();

        }else{
            $share = array(
                'head_img'=>'',
                'nickname'=>'',
                'content'=>'',
                'id'=>0,
                'img'=>'',
            );
        }

        $myfocus =  Db::name('fans')->alias('a')->field('b.nickname,a.fans_id,b.head_img')->join('member b','a.fans_id = b.id')->where('a.user_id',$user_id)->select();
//        print_R($myfocus);

        $view->assign('info',$share);
        $view->assign('myfocus',$myfocus);
        return $view->fetch();
    }



    public function  send_comment(){
        $content = input('param.content');
        $id = input('param.id');
        $new = array(
            '1'=>'汗',
            '2'=>'白眼',
            '3'=>'鄙视',
            '4'=>'打',
            '5'=>'大哭',
            '6'=>'鼓掌',
            '7'=>'奸笑',
            '8'=>'惊吓',
            '9'=>'纠结',
            '10'=>'可爱',
            '11'=>'可怜',
            '12'=>'酷',
            '13'=>'脸红',
            '14'=>'骂人',
            '15'=>'亲亲',
            '16'=>'傻',
            '17'=>'调皮',
            '18'=>'偷笑',
            '19'=>'委屈',
            '20'=>'无语',
            '21'=>'晕',
            '22'=>'抠鼻');
        for($i=1;$i<=22;$i++){
            //<img src="/static/emoji/img/3.gif">
            $str1 = '<img src="/static/emoji/img/'.$i.'.gif">';
            $str2 = '['.$new[$i].']';
            $content =  str_replace($str1,$str2,$content);
        }

        $user_id = session('user_id');
        $data = array(
            'user_id' =>$user_id,
            'content' =>$content,
            'dynamic_id' =>$id,
            'addtime'=>time(),
        );

        $flag = Db::name('dynamic_comment')->insert($data);

        if($flag){
            return json(['code'=>1,'msg'=>'评论成功']);
        }else{
            return json(['code'=>0,'msg'=>'评论失败！']);
        }
    }



    public function article_collect()    {

        $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }
        $map['a.user_id'] = $user_id;
        $list = Db::name('article_collect')->alias('a')->field('a.id as collect_id,b.id,b.title,b.photo,b.create_time,b.user_id,b.writer')->join('article b','a.article_id = b.id')->where($map)->order('a.id desc')->select();

        foreach($list as $key=>$val){
            if(!empty($val['user_id'])){
                $author = Db::name('member')->field('nickname')->where('id',$val['user_id'])->find();
                $list[$key]['author'] = $author['nickname'];
            }else{
                $list[$key]['author'] = $val['writer'];
            }
            $list[$key]['create_time'] = date('Y-m-d',$val['create_time']);
        }



        $essay = Db::name('essay_collect')->alias('a')->field('a.id as collect_id,b.id,b.title,b.photo,b.create_time,b.user_id,b.writer,b.instit_id')->join('essay b','a.essay_id = b.id')->where($map)->order('a.id desc')->select();
        foreach($essay as $key=>$val){
            $author = Db::name('instit')->field('name')->where('id',$val['instit_id'])->find();
            $essay[$key]['author'] = $author['name'];
            $essay[$key]['create_time'] = date('Y-m-d',$val['create_time']);
        }

        $debate = Db::name('debate_collect')->alias('a')->field('a.id as collect_id,b.id,b.title,b.photo,b.create_time,b.user_id,b.writer')->join('debate b','a.debate_id = b.id')->where($map)->order('a.id desc')->select();
        foreach($debate as $key=>$val){
            $author = Db::name('member')->field('nickname')->where('id',$val['user_id'])->find();
            $debate[$key]['author'] = $author['nickname'];
            $debate[$key]['create_time'] = date('Y-m-d',$val['create_time']);
        }

        $view = new View();
        $view->assign('article',$list);
        $view->assign('essay',$essay);
        $view->assign('debate',$debate);
        return $view->fetch();
    }


    public function dynamic()
    {

        $user_id = session('user_id');
        $user  = Db::name('member')->field('nickname')->where('id',$user_id)->find();
        if(input('param.page')){
            $page=input('param.page');
            $a = ($page-1)*5;
            $dynamic_list =  Db::name('dynamic')->alias('a')->field('a.*,b.nickname,b.head_img')->join('shuren_member b','a.user_id = b.id')->order('id desc')->limit($a,5)->select();
            foreach($dynamic_list as $key=>$val){
                if(!empty($val['img'])){
                     $imgs = substr($val['img'],0,-1);
                    $imgarr = explode(",",$imgs);
                    $i = 0;
                    foreach($imgarr as $va){
                            $newimgarr[$i]['imgpath'] = $va;
                        $i++;
                    }
                    $dynamic_list[$key]['imgs'] = $newimgarr ;
                }

                if($val['share_id']>0){
                    $share =  Db::name('dynamic')->alias('a')->field('b.nickname,a.content,a.img')->join('member b','a.user_id = b.id')->where('a.id',$val['share_id'])->find();

                    $dynamic_list[$key]['sharename'] = $share['nickname'];
                    $dynamic_list[$key]['sharecontent'] = $this->replaces($share['content']);
                    $dynamic_list[$key]['share_img'] = $share['img'];

                        if(!empty($share['img'])){
                            $imgs3 = substr($share['img'],0,-1);
                            $imgarr3 = explode(",",$imgs3);
                            $i = 0;
                            foreach($imgarr3 as $vav){
                                $newimgarr3[$i]['imgpath'] = $vav;
                                $i++;
                            }
                            $dynamic_list[$key]['share_imgs'] = $newimgarr3 ;
                        }


                }




                $dynamic_list[$key]['addtime'] = date('m-d H:i',$val['addtime']);


                $at_info = '';
                if(!empty($val['at_user_id'])){
                    $people = substr($val['at_user_id'],0,-1);
                    $peoples = explode(',',$people);
                    foreach($peoples as $kk){
                        $member = Db::name('member')->field('nickname')->where('id',$kk)->find();
                        $at_info = $at_info."<span class='blue'>@".$member['nickname'].'&nbsp;</span>';
                    }
                }
                $dynamic_list[$key]['content'] = $at_info . $this->replaces($val['content']);


                $mapq['dynamic_id'] = $val['id'];
                $dynamic_list[$key]['zan'] = Db::name('dynamic_zan')->where($mapq)->count();
                $dynamic_list[$key]['pinglun'] = Db::name('dynamic_comment')->where($mapq)->count();
                $mapq['user_id'] = $user_id;

                $map4['share_id'] = $val['id'] ;
                $dynamic_list[$key]['shares'] = Db::name('dynamic')->where($map4)->count();



                $dynamic_list[$key]['is_zan'] = Db::name('dynamic_zan')->where($mapq)->count();

                $map = array(
                    'a.dynamic_id'=>$val['id']
                );
                $dynamic_comment =  Db::name('dynamic_comment')->alias('a')->field('a.id,b.nickname,a.content,a.addtime')->join('shuren_member b','a.user_id = b.id')->where($map)->order('a.id asc')->select();
                foreach($dynamic_comment as $kk=>$vv){
                    $dynamic_comment[$kk]['content'] = $this->replaces($vv['content']);
                }
                $dynamic_list[$key]['comment']= $dynamic_comment;
            }

            if($dynamic_list){
                return json(['code' => 1, 'list' =>$dynamic_list]);
            }

        }
        $view = new View();
        $view->assign('nickname',$user['nickname']);
        $view->assign('footfrom',2);
        return $view->fetch();
    }


    public function dynamic_zan()
    {
        // return json(['code' => -1,'count'=>1, 'msg' => '点赞已取消']);
        $id=input('param.id');
        $user_id = session('user_id');

        if(empty($user_id)){
            return json(['code' => 11, 'msg' => '您尚未登录！']);
        }
        //$user_id = 14;
        $map = array(
            'user_id'=>$user_id,
            'dynamic_id'=>$id,
        );
        $is_zan = Db::name('dynamic_zan')->where($map)->find();
        if(!$is_zan){
            $is_zan['is_zan'] = 0;
        }
        if($is_zan['is_zan']==1)
        {
            Db::name('dynamic_zan')->where('id',$is_zan['id'])->delete();
            return json(['code' => -1, 'msg' => '点赞已取消']);
        }else{
            $data = array(
                'user_id'=>$user_id,
                'dynamic_id'=>$id,
                'is_zan'=>1,
            );
            $flag = Db::name('dynamic_zan')->insert($data);
            return json(['code' => 1,  'msg' => '点赞成功']);
        }

    }

    public function message_info(){
        $user_id = session('user_id');
        $map = array(
            'a.at_user_id'=>$user_id,
            'a.is_read'=>0
        );

       $result = Db::name('at_message')->alias('a')->field('a.*,b.nickname,b.head_img,from_unixtime(b.create_time) as create_time')->join('member b','a.user_id = b.id')->where($map)->group('a.user_id') ->select();
//       print_R($result);
        $view = new View();
       $view->assign('result',$result);
       return $view->fetch();
    }

    public function message_infos(){
        $at_id=input('param.at_id');
        $user_id = session('user_id');
        $user = Db::name('member')->where('id',$user_id)->find();
        $view = new View();
        $view->assign('at_id',$at_id);
        $view->assign('user',$user);
        $view->assign('nickname',$user['nickname']);
        return $view->fetch();
    }



    public function at_dynamic()
    {
        $user_id = session('user_id');
        $at_id=input('param.at_id');

        $arr = array(
            'a.user_id'=>$at_id,
            'a.at_user_id'=>$user_id,
        );

        $dynamic_list = Db::name('at_message')->alias('a')->field('c.*,b.nickname,b.head_img')->join('member b','a.user_id=b.id')->join('dynamic c','a.dynamic_id = c.id')->where($arr)->order('a.id desc')->select();


          //  $dynamic_list =  Db::name('dynamic')->alias('a')->field('a.*,b.nickname,b.head_img')->join('shuren_member b','a.user_id = b.id')->order('id desc')->select();






            foreach($dynamic_list as $key=>$val){
                if(!empty($val['img'])){
                    $imgs = substr($val['img'],0,-1);
                    $imgarr = explode(",",$imgs);
                    $i = 0;
                    foreach($imgarr as $va){
                        $newimgarr[$i]['imgpath'] = $va;
                        $i++;
                    }
                    $dynamic_list[$key]['imgs'] = $newimgarr ;
                }

                if($val['share_id']>0){
                    $share =  Db::name('dynamic')->alias('a')->field('b.nickname,a.content,a.img')->join('member b','a.user_id = b.id')->where('a.id',$val['share_id'])->find();

                    $dynamic_list[$key]['sharename'] = $share['nickname'];
                    $dynamic_list[$key]['sharecontent'] = $this->replaces($share['content']);
                    $dynamic_list[$key]['share_img'] = $share['img'];

                    if(!empty($share['img'])){
                        $imgs3 = substr($share['img'],0,-1);
                        $imgarr3 = explode(",",$imgs3);
                        $i = 0;
                        foreach($imgarr3 as $vav){
                            $newimgarr3[$i]['imgpath'] = $vav;
                            $i++;
                        }
                        $dynamic_list[$key]['share_imgs'] = $newimgarr3 ;
                    }


                }




                $dynamic_list[$key]['addtime'] = date('m-d H:i',$val['addtime']);


                $at_info = '';
                if(!empty($val['at_user_id'])){
                    $people = substr($val['at_user_id'],0,-1);
                    $peoples = explode(',',$people);
                    foreach($peoples as $kk){
                        $member = Db::name('member')->field('nickname')->where('id',$kk)->find();
                        $at_info = $at_info.'@'.$member['nickname'].'&nbsp;';
                    }
                }
                $dynamic_list[$key]['content'] = $at_info . $this->replaces($val['content']);


                $mapq['dynamic_id'] = $val['id'];
                $dynamic_list[$key]['zan'] = Db::name('dynamic_zan')->where($mapq)->count();
                $dynamic_list[$key]['pinglun'] = Db::name('dynamic_comment')->where($mapq)->count();
                $mapq['user_id'] = $user_id;

                $map4['share_id'] = $val['id'] ;
                $dynamic_list[$key]['shares'] = Db::name('dynamic')->where($map4)->count();



                $dynamic_list[$key]['is_zan'] = Db::name('dynamic_zan')->where($mapq)->count();

                $map = array(
                    'a.dynamic_id'=>$val['id']
                );
                $dynamic_comment =  Db::name('dynamic_comment')->alias('a')->field('a.id,b.nickname,a.content,a.addtime')->join('shuren_member b','a.user_id = b.id')->where($map)->order('a.id asc')->select();
                foreach($dynamic_comment as $kk=>$vv){
                    $dynamic_comment[$kk]['content'] = $this->replaces($vv['content']);
                }
                $dynamic_list[$key]['comment']= $dynamic_comment;
            }


            $arr11 = array(
                'user_id'=>$at_id,
                'at_user_id'=>$user_id,
            );

            Db::name('at_message')->where($arr11)->update(['is_read' =>1]);



            return json($dynamic_list);

    }


    public  function inside_letter_list(){

        if(input('param.page')) {

            $page =  input('param.page');

            $a = ($page-1)*5;

            $user_id= session('user_id');
            $user = Db::name('member')->field('group_id,account,instit_id')->where('id',$user_id)->find();
            if(!empty($user['instit_id'])){
                //$where = " instit_id =".$user['instit_id'];
                if($user['group_id']==1){
                     $where = " a.instit_id =".$user['instit_id'] ." and a.type in (0,1)";
                }elseif($user['group_id']==2){
                  $teach =  Db::name('teach')->field('is_leader')->where('mobile',$user['account'])->find();
                  if($teach['is_leader']==0){
                      $where = " a.instit_id =".$user['instit_id'] ." and a.type in (0,2)";
                  }  else{
                       $where = " a.instit_id =".$user['instit_id'] ." and a.type in (0,2,3)";
                  }
                }

            }

            $sql = "select a.*,b.name from shuren_inside_letter as a left JOIN shuren_instit as b on a.instit_id = b.id where a.user_id = " .$user_id." OR ( ".$where.") order by a.id desc limit ".$a.",5";
            $list = Db::query($sql);

            return json($list);


            }


        $view = new View();
        return $view->fetch();

    }


    public  function inside_letter_detail(){

        $id = input('param.id');

        $list =  Db::name('inside_letter')->alias('a')->field('a.*,b.name')
            ->join('instit b','a.instit_id = b.id')->where('a.id',$id)->find();
        $list['create_time'] = date('Y-m-d H:i', $list['create_time']);
        $view = new View();
        $view->assign('info',$list);
        return $view->fetch();

    }

    public function want_recommend_article(){

        $id = input('param.id');
        $nowtime = date('Y-m-d');
        $user_id = session('user_id');
        $map = array(
            'time'=>$nowtime,
            'user_id'=>$user_id,
        );


        $member = Db::name('member')->field('group_id,instit_id')->where('id',$user_id)->find();
        $instit_info =  Db::name('instit')->where('id',$member['instit_id'])->find();




        if($member['group_id']==8){
            $data = array(
                'article_id'=>  $id,
                'instit_id'=>  $member['instit_id'],
                'senior_instit_id'=>  $instit_info['pid'],
                'addtime'=>time(),
            );
        }else{
            $data = array(
                'article_id'=>  $id,
                'user_id'=>  $user_id,
                'senior_instit_id'=>  $member['instit_id'],
                'addtime'=>time(),
            );
        }
        Db::name('article_recommend')->insert($data);
        $recommend =  Db::name('user_recommend')->where($map)->find();

        if($recommend){
                if($recommend['counts']<2){
                    $total = $recommend['counts']+1;
                    Db::name('user_recommend')->where('id',$recommend['id'])->update(['counts' => $total]);
//                    Db::name('article')->where('id', $id)
//                        ->update(['is_want_tui' => 1]);
                    return json(['code' => 1,   'msg' => '推荐成功']);
                }else{
                    return json(['code' => 5,   'msg' => '每天最多推荐2篇文章']);
                }
        }else{
                $map['counts']= 1;
                Db::name('user_recommend')->insert($map);
//                Db::name('article')->where('id', $id)
//                    ->update(['is_want_tui' => 1]);
                return json(['code' => 1,   'msg' => '推荐成功']);
        }

    }




    function quitlogin(){

        $user_id = session('user_id');

        Db::name('member')->where('id',$user_id)->update(['open_id'=>'','shuren_open_id'=>'']);

        session(null);
        Cookie::delete('user_id');
        return json(['code' => 1,'msg' => '退出登录成功']);

    }

    public function user_edit(){
        $user_id= session('user_id');
        if(request()->isPost()){
            $password = input('param.password');
            if($password){
                $data['password'] = md5(md5($password) . config('auth_key'));
            }
            $data['nickname'] = input('param.nickname');
            $data['desc']  = input('param.desc');
            if(input('param.realname')){
            $data['realname'] = input('param.realname');
            }

            $data['peopleid'] = input('param.peopleid');

            $file = request()->file('img');
            if($file){
                $info = $file->move(ROOT_PATH . 'public_html' . DS . 'uploads/face');
                if($info){
                    $result = $info->getSaveName();
                }else{
                    $result = $file->getError();
                }
                $data['head_img']  =$result;
            }

            $flag= DB::name('member')->where('id',$user_id)->update($data);

            if($flag){
                return json(['code'=>1,'msg'=>'信息修改成功']);
            }else{
                return json(['code'=>-1,'msg'=>'信息修改失败']);
            }

          //  $this->success('修改成功','usercenter/index');

        }
        $member = Db::name('member')->field('realname,peopleid,account,nickname,head_img,desc,group_id')->where('id',$user_id)->find();
        $view = new View();
        $view->assign('info',$member);
        return $view->fetch();
    }

    public  function  user_sign(){
        $user_id = session('user_id');

        $integral = $this->get_config_v('every_sign_integral');
        $cycle = $this->get_config_v('sign_cycle');


        $result =  $this->user_signs($user_id,$integral,$cycle);
        if($result['code']==1){
            return json(['code' => 1, 'data' => '2', 'msg' => '签到成功！','jifen'=>$result['integral']]);
        }
        if($result['code']==-2){
            return json(['code' => 5, 'data' => '2', 'msg' => '请勿重复签到']);
        }

    }


    public function vote()
    {
        $id = input('param.id');
        $vote = Db::name('vote')->where('id',$id)->find();
        $choose = Db::name('vote_choose')->where('vote_id',$id)->select();
        foreach($choose as $key => $val){
            $map = [];
            $map['vote_id'] = $id;
//            $map['choose'] = ['like','%,'.$v['id'].',%'];
            $v_id = $val['id'];
            $map['choose'] = ['like',"%," . $v_id . ",%"];
          //  print_R($map);
            $count = Db::name('vote_log')->field('count(id) as num')->where($map)->find();
            $choose[$key]['num'] = $count['num'];
        }

        $instit = Db::name('instit')->field('name')->where('id',$vote['instit_id'])->find();

        if(empty($choose[0]['img'])){
            $imgs = 0;
        }else{
            $imgs = 1;
        }
        $vote['addtime1'] = date('Y-m-d',$vote['addtime']);
        $vote['instit_name'] = $instit['name'];

        $this->assign('vote',$vote);
        $this->assign('choose',$choose);
        $this->assign('imgs',$imgs);
        return $this->fetch();
    }


    public function real_vote()
    {

        $user_id = session('user_id');
        if(empty($user_id)){
            return json(['code'=>-11,'msg'=>'请先登录']);
        }

        $id = input('param.id');
        $choose = input('param.choose');

        $vote = Db::name('vote')->where('id',$id)->find();
        if(!empty($vote['starttime'])){
            if($vote['starttime']>time()){
                return json(['code'=>-1,'msg'=>'投票活动尚未开始!']);
            }
        }

        if(!empty($vote['endtime'])){
            if($vote['endtime']<time()){
                return json(['code'=>-1,'msg'=>'投票活动已经结束!']);
            }
        }
        $map = array(
            'user_id'=>session('user_id'),
            'vote_id'=>$id,
        );
        if($vote['cycle']==1){
                $starttime = strtotime(date('Y-m-d'));
                $endtime = $starttime+3600*24-1;
            $map['addtime'] = ['>=',$starttime];
            $map1['addtime'] = ['<=',$endtime];
                $count = Db::name('vote_log')->where($map)->where($map1)->count();
            $msg = '今天已投过票了!';
        }

        if($vote['cycle']==2){
            $count = Db::name('vote_log')->where($map)->count();
            $msg = '您已参与过本次投票!';
        }

        if($count>=1){
            return json(['code'=>-1,'msg'=>$msg]);
        }

        $data =  array(
            'user_id'=>session('user_id'),
            'vote_id'=>$id,
            'choose'=>$choose,
            'addtime'=>time()
        );
        $thi =   Db::name('vote_log')->insertGetId($data);

        if($thi){
            return json(['code'=>1,'msg'=>'投票成功！']);
        }

    }


    public function baoming()
    {


        if (request()->isPost()) {

            $param = input('post.');
            $vote = Db::name('baoming')->where('id',$param['baoming_id'])->find();
            $xiangmu = explode('|',$vote['desc']);

            $arr = array();
            foreach($xiangmu as $k=>$v){
                $c = $k;
            }

            for($i=0;$i<=$c;$i++){
                $cv = 'tt'.$i;
                $arr[$i]['content'] = $param[$cv];
            }

            $data = array(
                'user_id'=>session('user_id'),
                'addtime'=>time(),
                'baoming_id'=>$param['baoming_id'],
                'desc'=>json_encode($arr)
            );

            Db::name('baoming_log')->insert($data);

            return $this->fetch('usercenter/baoming1');


        }



        $id = input('param.id');
        $vote = Db::name('baoming')->where('id',$id)->find();
        $instit = Db::name('instit')->field('name')->where('id',$vote['instit_id'])->find();
        $vote['addtime1'] = date('Y-m-d',$vote['addtime']);
        $vote['instit_name'] = $instit['name'];

        $xiangmu = explode('|',$vote['desc']);
        $arr = array();
        foreach($xiangmu as $k=>$v){
            $arr[$k]['name'] = $v;
            $arr[$k]['title'] = 'tt'.$k;
            $i = $k;
        }

        $this->assign('vote',$vote);
        $this->assign('xiangmu',$arr);
        $this->assign('i',$i);
        return $this->fetch();
    }


    public function baomingajax()
    {


        if (request()->isAjax()) {

            $user_id = session('user_id');
            if(empty($user_id)){
                return json(['code'=>-11,'msg'=>'请先登录']);
            }

            $param = input('post.');
            $vote = Db::name('baoming')->where('id',$param['baoming_id'])->find();
            $xiangmu = explode('|',$vote['desc']);
            $arr = array();
            foreach($xiangmu as $k=>$v){
                $c = $k;
            }
            for($i=0;$i<=$c;$i++){
                $cv = 'tt'.$i;
                $arr[$i]['content'] = $param[$cv];
            }


            $data1 = array(
                'user_id'=>$user_id,
                'baoming_id'=>$param['baoming_id'],
                'desc'=>json_encode($arr)
            );

            $cdsd = Db::name('baoming_log')->where($data1)->find();
            if($cdsd){
                return json(['code'=>-1,'msg'=>'您已成功报名，请勿重复报名！']);
            }

            $data = array(
                'user_id'=>$user_id,
                'addtime'=>time(),
                'baoming_id'=>$param['baoming_id'],
                'desc'=>json_encode($arr)
            );
            Db::name('baoming_log')->insert($data);
            return json(['code'=>1,'msg'=>'报名成功！']);

        }




    }


    public function gongwen(){

        $key = input('key');
        $map = [];
        if($key&&$key!=="")
        {
            $map['title'] = ['like',"%" . $key . "%"];
        }
        $Nowpage = input('get.page') ? input('get.page'):1;
        $limits = 6;// 获取总条数
        $count = Db::name('gongwen')->where($map)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));

        $instit_id= session('instit_id');
        if($instit_id==1){
            $lists = DB::name('gongwen')->where($map)->page($Nowpage,$limits)->order('id desc')->select();
        }else{
            $map1['fa_id'] = $instit_id;
            $map1['shou_id'] = $instit_id;
            $lists = DB::name('gongwen')->where($map)->whereor($map1)->page($Nowpage,$limits)->order('id desc')->select();
        }


        foreach($lists as $k=>$v){
            $lists[$k]['addtime1'] = date('Y-m-d H:i:s',$v['addtime']);

            $inst11 = Db::name('instit')->where('id',$v['fa_id'])->find();
            $inst12 = Db::name('instit')->where('id',$v['shou_id'])->find();
            $lists[$k]['fa_name'] = $inst11['name'];
            $lists[$k]['shou_name'] = $inst12['name'];


        }


        $instit_id = session('instit_id');
        if($instit_id!=1){
            $childcount = Db::name('instit')->where('pid',$instit_id)->count();
            if($childcount>0){
                $can = 1;
            }else{
                $can = 0;
            }
        }else{
            $can = 1;
        }



        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('val', $key);
        $this->assign('can', $can);
        if(input('get.page'))
        {
            return json($lists);
        }
        return $this->fetch();
    }

    public function gongwen_read()
    {
        $id = input('param.id');
        $gongwen = new GongwenModel();
        $gongwens = $gongwen->getOneGongwen($id);
        $inst11 = Db::name('instit')->where('id',$gongwens['fa_id'])->find();
        $inst12 = Db::name('instit')->where('id',$gongwens['shou_id'])->find();
        $gongwens['fa_name'] = $inst11['name'];
        $gongwens['shou_name'] = $inst12['name'];
        $this->assign('gongwen',$gongwens);
        return $this->fetch();
    }


    public function gongwen_add()
    {
        if(request()->isAjax()){
            $param = input('post.');
            $param['fa_id'] = session('instit_id');
            $param['addtime'] = time();
            $gongwen = new GongwenModel();
            $flag = $gongwen->insertgongwen($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }
        $instit_id = session('instit_id');
        if($instit_id!=1){
            $instit = Db::name('instit')->where('pid',$instit_id)->select();
        }else{
            $instit = Db::name('instit')->where('id !=1 ')->select();
        }
        $this->assign('instits',$instit);
        return $this->fetch();

    }


}
