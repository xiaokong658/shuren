<?php
namespace app\index\controller;
use think\Controller;
use think\View;
use think\Db;
use think\Cookie;

class User extends  Base
{
    public function login()
    {
        $user_id = session('user_id');

        if($user_id){
            $this->redirect('index/index');
        }



        if(request()->isAjax()){

                $role= 1;
                $username=input('param.username');
                $password=input('param.password');
                if($role==2){
                    $student = Db::name('student')->where('mobile',$username)->find();

                    $user = Db::name('member')->where('account',$student['studentCode'])->find();
                    if($user){
                        if(empty($user['password'])){
                            return json(['code' => -1, 'data' => '2', 'msg' => '该用户未注册，请先注册']);
                        }else{
                            $pwd =  md5(md5($password) . config('auth_key'));

                            if($user['password']==$pwd ){

                                session('user_id',$user['id']);
                                session('instit_id',$user['instit_id']);
                                session('group_id',$user['group_id']);

                                return json(['code' => 1,'data' => '2','msg' => '登录成功！']);
                            }else{
                                return json(['code' => -1, 'data' => '2', 'msg' => '密码错误！']);
                            }
                        }
                    }else{
                        return json(['code' => -1, 'data' => '2', 'msg' => '帐号不存在']);
                    }
                }else{
                    $user = Db::name('member')->where('account',$username)->find();
                    if($user){

                        if(empty($user['password'])){
                            return json(['code' => -1, 'data' => '2', 'msg' => '该用户未注册，请先注册']);
                        }else{
                            $pwd =  md5(md5($password) . config('auth_key'));

                            if($user['password']==$pwd ){
                                session('user_id',$user['id']);
                                session('instit_id',$user['instit_id']);
                                session('group_id',$user['group_id']);

                                Cookie::set('user_id',$user['id'],3600);
                                if(empty($user['shuren_open_id'])){
                                    $shuren_open_id = session('shuren_open_id');
                                    if(!empty($shuren_open_id)){
                                        $data1['shuren_open_id'] = $shuren_open_id;
                                        Db::name('member')->where('id',$user['id'])->update($data1);
                                    }
                                }






                                return json(['code' => 1]);
                            }else{
                                return json(['code' => -1, 'data' => '2', 'msg' => '密码错误！']);
                            }
                        }
                    }else{
                        return json(['code' => -1, 'data' => '2', 'msg' => '帐号不存在']);
                    }
                }
        }

        $view = new View();
        return $view->fetch();
    }

    public function reg1()
    {
        $view = new View();
        return $view->fetch();
    }

    public function reg()
    {
        $role=input('param.role');
        $username=input('param.username');
        $name=input('param.name');
        $mobile=input('param.mobile');


        if($role==1){
           $user = Db::name('member')->where('account',$username)->find();
            if($user){
                if(!empty($user['password'])){
                    return json(['code' => -1, 'data' => '2', 'msg' => '此学生已注册，请直接登录！']);
                }else{
                    $student = Db::name('student')->where('studentCode',$username)->find();
                    if($student['studentName']==$name && $mobile==$student['mobile']){

                        return json(['code' => 1]);
                    }else{
                        return json(['code' => -1, 'data' => '2', 'msg' => '未匹配到学生信息']);
                    }
                }
            }else{
                return json(['code' => -1, 'data' => '2', 'msg' => '未匹配到学生信息']);
            }
        }


        if($role==2){
            $student = Db::name('student')->where('mobile',$username)->find();
            if($student){
                $user = Db::name('member')->where('account',$student['studentCode'])->find();
                if(!empty($user['password'])){
                    return json(['code' => -1, 'data' => '2', 'msg' => '此学生已注册，请直接登录']);
                }else{
                    if($student['studentName']==$name){
                        return json(['code' => 1]);
                    }else{
                        return json(['code' => -1, 'data' => '2', 'msg' => '未匹配到学生信息']);
                    }
                }

            }else{
                return json(['code' => -1, 'data' => '2', 'msg' => '未匹配到学生信息']);
            }
        }



        if($role==3){
            $user = Db::name('member')->where('account',$username)->find();
            if($user){
                if(!empty($user['password'])){
                    return json(['code' => -1, 'data' => '2', 'msg' => '此教师已注册，请直接登录！']);
                }else{
                    $teach = Db::name('teach')->where('mobile',$username)->find();
                    if($teach['name']==$name){
                        return json(['code' => 1]);
                    }else{
                        return json(['code' => -1, 'data' => '2', 'msg' => '此教师学生信息匹配有误！']);
                    }
                }
            }else{
                return json(['code' => -1, 'data' => '2', 'msg' => '未找到该教师信息']);
            }
        }

        if($role==4){
            $user = Db::name('member')->where('account',$username)->find();
            if($user){
                if(empty($user['password'])){
                    if($user['nickname']==$name){
                        return json(['code' => 1]);
                    }else{
                        return json(['code' => -1, 'data' => '2', 'msg' => '此社会人士信息匹配有误！']);
                    }

                }else{
                    return json(['code' => -1, 'data' => '2', 'msg' => '此社会人士已注册，请直接登录！']);
                }
            }else{
                return json(['code' => -1, 'data' => '2', 'msg' => '未找到该社会人士信息']);
            }
        }

    }



    public function reg2()
    {
        $view = new View();
        return $view->fetch();
    }


    public function save_pwd1(){
        $role=input('param.role');
        $username=input('param.username');
        $name=input('param.name');
        $mobile=input('param.mobile');

        if($role==1){
            $user = Db::name('member')->where('account',$username)->find();
            $info = Db::name('student')->where('studentCode',$username)->find();
        }

        if($role==3){
            $user = Db::name('member')->where('account',$username)->find();
            $info = Db::name('teach')->where('mobile',$username)->find();
        }

        if($role==4){
            $user = Db::name('member')->where('account',$username)->find();
            $info = $user;
        }

        if($role==2){
            $student = Db::name('student')->where('mobile',$username)->find();
            $user = Db::name('member')->where('account',$student['studentCode'])->find();
            $info = $student;
        }

        if($info){
            $instit = Db::name('instit')->field('name')->where('id',$info['instit_id'])->find();
        }

        $view = new View();
        $view->assign('role',$role);
        $view->assign('user',$user);
        $view->assign('info',$info);
        $view->assign('instit_name',$instit['name']);
        return $view->fetch();
    }

    public function save_pwd2(){
        $user_id=input('param.user_id');
        $password=input('param.password');
        if($user_id&&$password){
           $pwd =  md5(md5($password) . config('auth_key'));
            $data['password'] = $pwd;
           $res =  Db::name('member')->where('id',$user_id)->update($data);
            if($res){
                $article_review =  $this->get_config_v('reg_integral');
                if($article_review>0){
                    add_integral($user_id,$article_review,'reg_integral');
                }
                return json(['code' => 1, 'data' => '2', 'msg' => '设置密码成功']);
            }else{
                return json(['code' => -1, 'data' => '2', 'msg' => '未知错误']);
            }
        }else{
            return json(['code' => -1, 'data' => '2', 'msg' => '信息不能为空']);
        }

    }


    public function upload(){
        $view = new View();
        return $view->fetch();
    }

    public function user_info()
    {

      $user_id=input('param.user_id');

        $myuser_id = session('user_id');
        if(empty($user_id)){
            $user_id = $myuser_id;
        }

        $user_column = Db::name('user_column')->field('*')->where('user_id',$user_id)->select();

        $user  = Db::name('member')->field('*')->where('id',$user_id)->find();
        $dynamic_list =  Db::name('dynamic')->alias('a')->field('a.*,b.nickname,b.head_img')->join('shuren_member b','a.user_id = b.id')->order('id desc')->select();
        foreach($dynamic_list as $key=>$val){
            $dynamic_list[$key]['addtime'] = date('m-d H:i',$val['addtime']);
            $dynamic_list[$key]['content'] = $this->replaces($val['content']);
        }
        $ad = $this->get_ad('25');
        $map = array(
            'status'=>1,
            'user_id'=>$user_id,
        );
        $list = Db::name('article')->field('id,title,photo,create_time,user_id,writer')->where($map)->order('my_ding desc,id desc')->select();
        foreach($list as $key=>$val){
            $list[$key]['author'] = $user['nickname'];
            $list[$key]['create_time'] = date('Y-m-d',$val['create_time']);
        }
        $essay = array();
        if($user['group_id']==8){
            $map1 = array(
                'user_id'=>$user_id,
            );
            $essay = Db::name('essay')->field('id,title,photo,create_time,user_id')->where($map1)->order('my_ding desc,id desc')->select();
            foreach($essay as $key=>$val){
                $essay[$key]['author'] = $user['nickname'];
                $essay[$key]['create_time'] = date('Y-m-d',$val['create_time']);
            }
        }



        $map1 = array(
            'user_id'=>$user_id,
        );
        $debate = Db::name('debate')->field('id,title,photo,create_time,user_id')->where($map1)->order('my_ding desc,id desc')->limit(5)->select();
        foreach($debate as $key=>$val){
            $author = Db::name('member')->field('nickname')->where('id',$val['user_id'])->find();
            $debate[$key]['author'] = $author['nickname'];
            $debate[$key]['create_time'] = date('Y-m-d',$val['create_time']);
        }


        $user['focus'] = Db::name('fans')->where('user_id',$user_id)->count();
        $user['fans'] = Db::name('fans')->where('fans_id',$user_id)->count();

        $map5 = array(
          'user_id' => $myuser_id,
          'fans_id' => $user_id,
        );

        $user['is_focus'] = Db::name('fans')->where($map5)->count();


        $view = new View();
        $view->assign('ad',$ad);
        $view->assign('dynamic',$dynamic_list);
        $view->assign('article',$list);
        $view->assign('debate',$debate);
        $view->assign('essay',$essay);
        $view->assign('user_column',$user_column);
        $view->assign('myuser_id',$myuser_id);
        $view->assign('user',$user);
        $view->assign('footfrom',1);
        return $view->fetch();
    }


    public function article_column()
    {

        $user_id=input('param.user_id');
        $type=input('param.type');


        $myuser_id = session('user_id');
        if(empty($user_id)){
            $user_id = $myuser_id;
        }

        $user_column = Db::name('user_column')->field('*')->where('user_id',$user_id)->select();

        $user  = Db::name('member')->field('*')->where('id',$user_id)->find();


        if(input('param.page')){

            if($type==0){
                $map = array(
                    'status'=>1,
                    'user_id'=>$user_id,
                );
            }else{
                $map = array(
                    'status'=>1,
                    'user_id'=>$user_id,
                    'column_id'=>$type
                );
            }

            $list = Db::name('article')->field('id,title,photo,create_time,user_id,writer')->where($map)->order('my_ding desc,id desc')->select();
            foreach($list as $key=>$val){
                $list[$key]['author'] = $user['nickname'];
                $list[$key]['create_time'] = date('Y-m-d',$val['create_time']);
            }
            return json(['code'=>1,'list'=>$list]);
        }



        $view = new View();
        $view->assign('user_column',$user_column);
        $view->assign('myuser_id',$myuser_id);
        $view->assign('type',$type);
        $view->assign('user',$user);
        $view->assign('footfrom',1);
        return $view->fetch();
    }

    public function mydynamic()
    {
        $user_id=input('param.user_id');
       // $user_id = 212082;
        $user  = Db::name('member')->field('nickname')->where('id',$user_id)->find();
        if(input('param.page')){
            $page=input('param.page');
            $a = ($page-1)*3;
            $dynamic_list =  Db::name('dynamic')->alias('a')->field('a.*,b.nickname,b.head_img')->join('shuren_member b','a.user_id = b.id')->where('a.user_id',$user_id)->order('id desc')->limit($a,3)->select();
            foreach($dynamic_list as $key=>$val){
                if(!empty($val['img'])){
                    $imgs = substr($val['img'],0,-1);
                    $imgarr = explode(",",$imgs);
                    $i = 0;
                    foreach($imgarr as $va){
                        $newimgarr[$i]['imgpath'] = $va;
                        $i++;
                    }
                    $dynamic_list[$key]['imgs'] = $newimgarr ;
                }

                if($val['share_id']>0){
                    $share =  Db::name('dynamic')->alias('a')->field('b.nickname,a.content,a.img')->join('member b','a.user_id = b.id')->where('a.id',$val['share_id'])->find();

                    $dynamic_list[$key]['sharename'] = $share['nickname'];
                    $dynamic_list[$key]['sharecontent'] = $this->replaces($share['content']);
                    $dynamic_list[$key]['share_img'] = $share['img'];

                    if(!empty($share['img'])){
                        $imgs3 = substr($share['img'],0,-1);
                        $imgarr3 = explode(",",$imgs3);
                        $i = 0;
                        foreach($imgarr3 as $vav){
                            $newimgarr3[$i]['imgpath'] = $vav;
                            $i++;
                        }
                        $dynamic_list[$key]['share_imgs'] = $newimgarr3 ;
                    }


                }


                $dynamic_list[$key]['addtime'] = date('m-d H:i',$val['addtime']);
                $dynamic_list[$key]['content'] = $this->replaces($val['content']);
                $mapq['dynamic_id'] = $val['id'];
                $dynamic_list[$key]['zan'] = Db::name('dynamic_zan')->where($mapq)->count();
                $dynamic_list[$key]['pinglun'] = Db::name('dynamic_comment')->where($mapq)->count();
                $mapq['user_id'] = $user_id;

                $map4['share_id'] = $val['id'] ;
                $dynamic_list[$key]['shares'] = Db::name('dynamic')->where($map4)->count();



                $dynamic_list[$key]['is_zan'] = Db::name('dynamic_zan')->where($mapq)->count();

                $map = array(
                    'a.dynamic_id'=>$val['id']
                );
                $dynamic_comment =  Db::name('dynamic_comment')->alias('a')->field('a.id,b.nickname,a.content,a.addtime')->join('shuren_member b','a.user_id = b.id')->where($map)->order('a.id asc')->select();
                foreach($dynamic_comment as $kk=>$vv){
                    $dynamic_comment[$kk]['content'] = $this->replaces($vv['content']);
                }
                $dynamic_list[$key]['comment']= $dynamic_comment;
            }
            return json($dynamic_list);
        }
        $view = new View();
        $view->assign('nickname',$user['nickname']);
        $view->assign('footfrom',2);
        return $view->fetch();
    }

    public function user_info1(){
        $view = new View();
        return $view->fetch();
    }


    public function focus_user()
    {
        // return json(['code' => -1,'count'=>1, 'msg' => '点赞已取消']);


        $fans_id=input('param.fans_id');
        $user_id = session('user_id');

        if(empty($user_id)){
            return json(['code' => 11, 'msg' => '您尚未登录！']);
        }
        //$user_id = 14;
        $map = array(
            'user_id'=>$user_id,
            'fans_id'=>$fans_id,
        );
        $is_focus = Db::name('fans')->where($map)->find();
        if(!$is_focus){
            $focused = 0;
        }else{
            $focused = 1;
        }
        if($focused==1)
        {
            Db::name('fans')->where('id',$is_focus['id'])->delete();
            return json(['code' => -1, 'msg' => '关注已取消']);
        }else{
            $data = array(
                'user_id'=>$user_id,
                'fans_id'=>$fans_id,
                'create_time'=>time(),
            );
            $flag = Db::name('fans')->insert($data);
            return json(['code' => 1,  'msg' => '关注成功']);
        }

    }


    public function user_reg(){
//        echo session('shuren_open_id');
        if(request()->isAjax()){
            $mobile = input('param.mobile');
            $code= input('param.code');
            $password = input('param.password');

            $map = array(
                'is_used'=>0,
                'mobile'=>$mobile,
                'type'=>'reg'
            );
            $codes = Db::name('user_sms_code')->where($map)->order('id desc')->find();
            if($codes['code']!=$code){
                return json(['code'=>-1,'data'=>'输入的验证码有误！']);
            }

            if($codes['endtime']<time()){
                return json(['code'=>-1,'data'=>'验证码已过期，请重新获取！']);
            }


            $user_count = Db::name('member')->where('account',$mobile)->count();
            if($user_count>0){
                return json(['code'=>-1,'data'=>'该用户已存在！']);
            }

            $shuren_open_id = session('shuren_open_id');
            $data = array(
                'account'=>$mobile,
                'nickname'=>input('param.nickname'),
                'realname'=>input('param.realname'),
                'password' => md5(md5($password) . config('auth_key')),
                'group_id'=>2,
                'instit_id'=>1,
                'create_time'=>time(),
                'status'=>1,

            );
            if(!empty($shuren_open_id)){
                $data['shuren_open_id'] = $shuren_open_id;
            }

            $id= DB::name('member')->insertGetId($data);
            session('user_id',$id);
            session('group_id',2);
            session('instit_id',1);
            if($id){
                $article_review =  $this->get_config_v('reg_integral');
                if($article_review>0){
                    add_integral($id,$article_review,'reg_integral');
                }

                return json(['code'=>1]);
            }else{
                return json(['code'=>-1,'msg'=>'注册失败']);
            }
        }

        return $this->fetch();
    }



}
