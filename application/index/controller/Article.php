<?php
namespace app\index\controller;
use think\Controller;
use app\admin\model\ArticleCateModel;
use think\View;
use think\Db;
use app\admin\model\EssayModel;
class Article extends  Controller
{

    public function index()
    {

        $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }
        $user_info = Db::name('member')->where('id',$user_id)->find();
        $user_info['article_num'] = Db::name('article')->where('user_id',$user_id)->count();
        $user_info['foucs_num'] = Db::name('fans')->where('user_id',$user_id)->count();
        $user_info['fans_num'] = Db::name('fans')->where('fans_id',$user_id)->count();
        $view = new View();
        $view->assign('info',$user_info);
        return $view->fetch();

    }

    public function essay()
    {
        $page = input('param.page');
        if(empty($page)){
            $page = 1;
        }
        $a = ($page-1)*8;
        $map = array();
        if(input('param.page'))
        {
            $list  =  Db::name('essay')->field('id,title,create_time,photo')->where($map)->order('id desc')->limit($a,8)->select();
            foreach($list as $key=>$item){
                $list[$key]['create_time'] = date('Y-m-d H:i:s',$item['create_time']);
            }
            return json(['code' => 1, 'list'=>$list,  'msg' => 'ok']);
        }
        $view = new View();
        return $view->fetch();

    }

    public function debate()
    {
        $page = input('param.page');
        if(empty($page)){
            $page = 1;
        }
        $a = ($page-1)*8;
        $map = array();
        if(input('param.page'))
        {
            $list  =  Db::name('debate')->field('id,title,create_time,photo')->where($map)->order('id desc')->limit($a,8)->select();
            foreach($list as $key=>$item){
                $list[$key]['create_time'] = date('Y-m-d H:i:s',$item['create_time']);
            }
            return json(['code' => 1, 'list'=>$list,  'msg' => 'ok']);
        }
        $view = new View();
        return $view->fetch();

    }


    public function article()
    {
        $page = input('param.page');
        if(empty($page)){
            $page = 1;
        }

        $keyword = input('param.keyword');
        if(!empty($keyword)){
            $map['title'] = ['like','%'.$keyword.'%'];
            $map1['content'] = ['like','%'.$keyword.'%'];
        }

        $a = ($page-1)*8;
        $map['status'] = 1;
        if(input('param.page'))
        {
            if(!empty($keyword)) {
                $list  =  Db::name('article')->field('id,title,create_time,photo')->where($map)->whereor($map1)->order('id desc')->limit($a,8)->select();
            }else{
                $list  =  Db::name('article')->field('id,title,create_time,photo')->where($map)->order('id desc')->limit($a,8)->select();
            }

            foreach($list as $key=>$item){
                $list[$key]['create_time'] = date('Y-m-d H:i:s',$item['create_time']);
            }
            return json(['code' => 1, 'list'=>$list,  'msg' => 'ok']);
        }
        $view = new View();
        $view->assign('keyword',$keyword);
        return $view->fetch();

    }


    public function write()
    {
        if(request()->isAjax()){
            $page['title'] = input('param.title');
            $op = input('param.op');
            $page['type']  = input('param.type');
            $page['desc']  = input('param.desc');
            $page['content']  = input('param.content');
            $page['column_id']  = input('param.column_id');
            $page['is_want_tui'] = input('param.is_want_tui');
//            if($page['is_want_tui']!=1){
                $page['is_want_tui'] = 0;
//            }

            if($op==1){
                $file = request()->file('img');
                if($file){
                    $info = $file->move(ROOT_PATH . 'public_html' . DS . 'uploads/images');
                    if($info){
                        $result = $info->getSaveName();
                    }else{
                        $result = $file->getError();
                    }
                }else{
//                    return json(['code'=>-1,'msg'=>'请上传封面']);
                    $result = '';
                }
                $user_id = session('user_id');

                $data = array(
                    'title'=>  $page['title'],
                    'cate_id'=>  $page['type'],
                    'remark'=>  $page['desc'],
                    'user_id'=> $user_id,
                    'content'=>  $page['content'],
                    'status'=> 0,
                    'column_id'=> $page['column_id'],
                    'create_time'=> time(),
                    'photo'=>$result,
                    'instit_id' => session('instit_id'),
                );

                $group_id = session('group_id');
                if($group_id==8){
                    $data['status'] = 1;
                }


                $flay =  Db::name('article')->insert($data);
                if($flay){
                    //$this->success('文章发布成功','usercenter/index');

                    return json(['code'=>1,'msg'=>'文章发布成功']);

                }
            }else if($op==2){
                $user_id = session('user_id');

                $data = array(
                    'title'=>  $page['title'],
                    'cate_id'=>  $page['type'],
                    'remark'=>  $page['desc'],
                    'user_id'=> $user_id,
                    'content'=>  $page['content'],
                    'status'=> -2,
                    'column_id'=> $page['column_id'],
                    'create_time'=>time(),
                    'is_want_tui'=>  $page['is_want_tui'],
                );

                $id = input('param.id');
                if($id){
                    $flay =  Db::name('article')->where('id',$id)->update($data);
                }else{
                    $flay =  Db::name('article')->insertGetId($data);
                    $id = $flay;
                }
//                    $this->success('','/index/article/write/id/'.$id.'.html');
                return json(['code'=>1,'msg'=>'保存草稿成功']);
            }

        }

        $id = input('param.id');
        $article = Db::name('article')->field('id,title,remark,content,is_want_tui,cate_id,column_id')->where('id',$id)->find();
        $lanmu = Db::name('user_column')->where('user_id',session('user_id'))->select();
        //$article_cate = Db::name('article_cate')->field('id,name')->where('status',1)->select();
        $cate = new ArticleCateModel();
        $group_id = session('group_id');
        $view = new View();
        $view->assign('article_cate',$cate->getAllCate(0));
        $view->assign('article',$article);
        $view->assign('group_id',$group_id);
        $view->assign('lanmu',$lanmu);
        return $view->fetch();

    }

    public function essay_write()
    {

        $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }


        if(request()->isAjax()){


            $param = input('post.');
            //  $article = new ArticleModel();
            $param['instit_id'] = session('instit_id');
            $param['user_id'] = session('user_id');
            $param['end_time'] = strtotime($param['end_time'])+3600*24-1;
            //  $flag = $article->insertArticle($param);
            $article = new EssayModel();
            $flag = $article->insertArticle($param);

            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }




        $cate = new ArticleCateModel();
        $this->assign('cate',$cate->getAllCate(0));
        return $this->fetch();

    }






    public function debate_write()
    {
        if(request()->isPost()){
            $page['title'] = input('param.title');
            $page['zhengfang']  = input('param.zhengfang');
            $page['fanfang']  = input('param.fanfang');
            $page['end_time']  = input('param.end_time');
            $page['content']  = input('param.content');

                $file = request()->file('img');
                if($file){
                    $info = $file->move(ROOT_PATH . 'public_html' . DS . 'uploads/images');
                    if($info){
                        $result = $info->getSaveName();
                    }else{
                        $result = $file->getError();
                    }
                }else{
                    $result = '';
                }
                $user_id = session('user_id');
                $data = array(
                    'title'=>  $page['title'],
                    'zhengfang'=>  $page['zhengfang'],
                    'fanfang'=>  $page['fanfang'],
                    'user_id'=> $user_id,
                    'content'=>  $page['content'],
                    'end_time'=>strtotime($page['end_time'])+24*3600-1,
                    'create_time'=> time(),
                    'photo'=>$result,
                );

                $flay =  Db::name('debate')->insert($data);
                if($flay){
                    $this->success('话题发布成功','usercenter/index');
                }
        }
        $view = new View();
        return $view->fetch();

    }


    public function user_column()
    {

        $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }
        $map1['user_id'] = $user_id;
        $list = Db::name('user_column')->where($map1)->order('id asc')->select();
        $vs = 0;
        foreach($list as $kk=>$vv){
            $list[$kk]['sort'] = $kk+1;
            $vs=$kk+1;
        }
        $this->assign('lists',$list);
        $this->assign('vs',$vs);
        return $this->fetch();

    }


    public function add_column()
    {

        $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }

        if(request()->isAjax()){
            $param = input('post.');
            $count =  DB::name('user_column')->where('user_id',$user_id)->count();
            if($count>=5){
                return json(['code' => -1,  'msg' =>'每人最多添加5个菜单']);
            }
            $param['user_id'] = $user_id;
            $new_id = DB::name('user_column')->insertGetId($param);
            if($new_id){
                return json(['code' => 1,  'msg' =>'新增栏目成功']);
            }else{
                return json(['code' => -1,  'msg' =>'新增栏目失败']);
            }
        }


        return $this->fetch();
    }


    public function edit_column()
    {

        $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }


        if(request()->isAjax()){
            $param = input('post.');

            $new_id = DB::name('user_column')->where('id',$param['id'])->update($param);
            if($new_id){
                return json(['code' => 1,  'msg' =>'修改栏目成功']);
            }else{
                return json(['code' => -1,  'msg' =>'修改栏目失败']);
            }
        }

        $id= input('param.id');
        $map = array(
            'user_id'=>$user_id,
            'id'=>$id
        );
        $column = Db::name('user_column')->where($map)->find();



        $this->assign('column',$column);
        return $this->fetch();
    }

    public function del_column()
    {
        $id = input('param.id');
        $count = Db::name('article')->where('column_id',$id)->count();
        if($count>0){
            return json(['code' =>-1,  'msg' =>'该文章栏目有文章存在，无法删除']);
        }

        $id= Db::name('user_column')->where('id',$id)->delete();

        return json(['code' =>1,'msg'=>'删除成功']);
    }




}
