<?php

namespace app\index\controller;
use think\Controller;
use think\File;
use think\Request;

class Upload extends Base
{
    //图片上传
    public function upload(){
        $file = request()->file('file');

        $imgName = request()->post('imgpath');

//        $date = date('Ymd');


        $info = $file->move(ROOT_PATH . 'public_html' . DS . 'uploads/images',$imgName);
        if($info){
            echo $info->getSaveName();
        }else{
            echo $file->getError();
        }
    }



}