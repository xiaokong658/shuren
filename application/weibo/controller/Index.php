<?php
namespace app\weibo\controller;
use think\Controller;
use think\View;
use think\Db;
class Index extends  Base
{
    public function index()
    {
        $ad = $this->get_ad('25');
        $view = new View();
        $view->assign('ad',$ad);
        return $view->fetch();
    }

}
