<?php

namespace app\mobile\controller;
use think\Controller;
use think\Db;

class Base extends Controller
{



    public  function  get_ad($id){
        $ad_position =  Db::name('ad_position')->where('id',$id)->find();
        $map['closed'] = 0;
        $map['status'] = 1;
        $ad =  Db::name('ad')->where($map)->where('ad_position_id',$id)->order('id desc')->select();
        return $ad;
    }

    public  function  read_article($article_id){
        $view =  Db::name('article')->field('views')->where('id',$article_id)->find();
        if($view){
            $new = $view['views']+1;
            Db::name('article')->where('id',$article_id)->setField('views',$new);
        }
    }


    public function  replaces($content){
        $new = array(
            '1'=>'汗',
            '2'=>'白眼',
            '3'=>'鄙视',
            '4'=>'打',
            '5'=>'大哭',
            '6'=>'鼓掌',
            '7'=>'奸笑',
            '8'=>'惊吓',
            '9'=>'纠结',
            '10'=>'可爱',
            '11'=>'可怜',
            '12'=>'酷',
            '13'=>'脸红',
            '14'=>'骂人',
            '15'=>'亲亲',
            '16'=>'傻',
            '17'=>'调皮',
            '18'=>'偷笑',
            '19'=>'委屈',
            '20'=>'无语',
            '21'=>'晕',
            '22'=>'抠鼻');

        for($i=1;$i<=22;$i++){
            $str1 = '<img src="/static/emoji/img/'.$i.'.gif">';
            $str2 = '['.$new[$i].']';
            $content =  str_replace($str2,$str1,$content);
        }

        return $content;


    }






}