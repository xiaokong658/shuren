<?php
namespace app\mobile\controller;
use think\Controller;
use think\View;
use think\Db;
class User extends  Base
{

    public function login()
    {
        $user_id = session('user_id');

        if($user_id){
            $this->redirect('index/index');
        }

        if(request()->isAjax()){
            $role=input('param.role');
            $username=input('param.username');
            $password=input('param.password');
            if($role==2){
                $student = Db::name('student')->where('mobile',$username)->find();
                $user = Db::name('member')->where('account',$student['studentCode'])->find();
                if($user){
                    if(empty($user['password'])){
                        return json(['code' => -1, 'data' => '2', 'msg' => '该用户未注册，请先注册']);
                    }else{
                        $pwd =  md5(md5($password) . config('auth_key'));
                        if($user['password']==$pwd ){
                            session('user_id',$user['id']);
                            session('instit_id',$user['instit_id']);
                            session('group_id',$user['group_id']);
                            return json(['code' => 1,'data' => '2','msg' => '登录成功！']);
                        }else{
                            return json(['code' => -1, 'data' => '2', 'msg' => '密码错误！']);
                        }
                    }
                }else{
                    return json(['code' => -1, 'data' => '2', 'msg' => '帐号不存在']);
                }
            }else{
                $user = Db::name('member')->where('account',$username)->find();
                if($user){

                    if(empty($user['password'])){
                        return json(['code' => -1, 'data' => '2', 'msg' => '该用户未注册，请先注册']);
                    }else{
                        $pwd =  md5(md5($password) . config('auth_key'));

                        if($user['password']==$pwd ){
                            session('user_id',$user['id']);
                            session('instit_id',$user['instit_id']);
                            session('group_id',$user['group_id']);
                            return json(['code' => 1]);
                        }else{
                            return json(['code' => -1, 'data' => '2', 'msg' => '密码错误！']);
                        }
                    }
                }else{
                    return json(['code' => -1, 'data' => '2', 'msg' => '帐号不存在']);
                }
            }
        }

        $view = new View();
        return $view->fetch();
    }

    public function center()
    {
       
        $user_id =  input('param.user_id');
        if(empty($user_id)){
            $user_id = session('user_id');
            if(empty($user_id)){
                $this->redirect('user/login');
            }
        }
        $user_info = Db::name('member')->where('id',$user_id)->find();
        $map5 = array(
            'user_id'=>$user_id,
            'status'=>1,
        );
        $article_count = Db::name('article')->where($map5)->count();
        $debate_count = Db::name('debate')->where('user_id',$user_id)->count();
        $fan_count = Db::name('fans')->where('fans_id',$user_id)->count();
        $user_info['count'] = $article_count+$debate_count;
        $user_info['fans_count'] = $fan_count;
        $map['status'] = 1;
        $Nowpage = input('param.page') ? input('param.page'):1;
        $limits = 10;// 获取总条数
        $count = Db::name('article')->where($map)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));
        if(input('param.page'))
        {
            $list  =  Db::name('article')->field('id,title,create_time,photo,remark')->where($map)->order('id desc')->page($Nowpage, $limits)->select();
            foreach($list as $key=>$item){
                $list[$key]['create_time'] = date('Y-m-d',$item['create_time']);
                $list[$key]['comment_count'] = Db::name('article_comment')->where('article_id',$item['id'])->count();
                $list[$key]['zan_count'] = Db::name('article_zan')->where('article_id',$item['id'])->count();
                $list[$key]['collect_count'] = Db::name('article_collect')->where('article_id',$item['id'])->count();
            }
            return json(['code' => 1, 'list'=>$list,  'msg' => 'ok']);
        }
        $view = new View();
        $view->assign('allpage',$allpage);
        if(session('user_id')){
            $view->assign('is_login',1);
        }else{
            $view->assign('is_login',0);
        }
        $view->assign('user_info',$user_info);
        return $view->fetch();

    }


    function quitlogin(){
        $user_id = session('user_id');
        if($user_id){
            session(null);
                $this->redirect('user/login');
        }
    }



}
