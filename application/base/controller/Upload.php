<?php

namespace app\mobile\controller;
use think\Controller;
use think\File;
use think\Request;

class Upload extends Base
{
    //图片上传
    public function upload(){
        $file = request()->file('file');
        $info = $file->move(ROOT_PATH . 'public_html' . DS . 'uploads/images');
        if($info){
            echo $info->getSaveName();
        }else{
            echo $file->getError();
        }
    }


    //会员头像上传
    public function uploadface(){
        $file = request()->file('file');
        $info = $file->move(ROOT_PATH . 'public_html' . DS . 'uploads/face');
        if($info){
            echo $info->getSaveName();
        }else{
            echo $file->getError();
        }
    }


    //会员头像上传
    public function uploadfile(){
        $file = request()->file('file');
        $info = $file->move(ROOT_PATH . 'public_html' . DS . 'uploads/file');
        if($info){
            echo $info->getSaveName();
        }else{
            echo $file->getError();
        }
    }

}