<?php
namespace app\mobile\controller;
use app\admin\model\ArticleModel;
use app\admin\model\DebateModel;
use app\admin\model\TempModel;
use app\admin\model\ArticleCateModel;
use think\Controller;
use think\View;
use think\Db;
class Article extends  Base
{

    public function article()
    {

        $type = input('param.type');
        $keyword = input('param.keyword');
       $cate_list = Db::name('article_cate')->field('id,name')->where('status',1)->select();

        $page = input('param.page');
        if(empty($page)){
            $page = 1;
        }
        $a = ($page-1)*8;
        $map['status'] = 1;
        if($type){
            $map['cate_id'] = $type;
        }

        if($keyword){
            $map['title'] = ['like','%'.$keyword.'%'];
        }




        if(input('param.page'))
        {
            $list  =  Db::name('article')->field('id,title,create_time,photo,remark,views')->where($map)->order('id desc')->limit($a,8)->select();
            foreach($list as $key=>$item){
                $list[$key]['create_time'] = date('Y-m-d',$item['create_time']);
                $list[$key]['zan']  = Db::name('article_zan')->where('article_id',$item['id'])->count();
                $list[$key]['collect']  = Db::name('article_collect')->where('article_id',$item['id'])->count();
                $list[$key]['comment']  = Db::name('article_comment')->where('article_id',$item['id'])->count();
            }
            return json(['code' => 1, 'list'=>$list,  'msg' => 'ok']);
        }


        $view = new View();

        if(session('user_id')){
            $view->assign('is_login',1);
        }else{
            $view->assign('is_login',0);
        }


        $view->assign('cate',$cate_list);
        $view->assign('keyword',$keyword);
        $view->assign('type',$type);
        return $view->fetch();

    }

    public function essay()
    {

        $type = input('param.type');
        $keyword = input('param.keyword');

        $cate_list = Db::name('article_cate')->field('id,name')->where('status',1)->select();


        $page = input('param.page');
        if(empty($page)){
            $page = 1;
        }
        $a = ($page-1)*8;
        $map =array();
//        $map['status'] = 1;
        if($type){
            $map['cate_id'] = $type;
        }

        if($keyword){
            $map['title'] = ['like','%'.$keyword.'%'];
        }




        if(input('param.page'))
        {
            $list  =  Db::name('essay')->field('id,title,create_time,photo,remark,views')->where($map)->order('id desc')->limit($a,8)->select();
            foreach($list as $key=>$item){
                $list[$key]['create_time'] = date('Y-m-d',$item['create_time']);
                $list[$key]['zan']  = Db::name('essay_zan')->where('essay_id',$item['id'])->count();
                $list[$key]['collect']  = Db::name('essay_collect')->where('essay_id',$item['id'])->count();
//                $list[$key]['comment']  = Db::name('essay_comment')->where('essay_id',$item['id'])->count();
            }
            return json(['code' => 1, 'list'=>$list,  'msg' => 'ok']);
        }


        $view = new View();
        if(session('user_id')){
            $view->assign('is_login',1);
        }else{
            $view->assign('is_login',0);
        }
        $view->assign('cate',$cate_list);
        $view->assign('keyword',$keyword);
        $view->assign('type',$type);
        return $view->fetch();

    }


    public function debate()
    {
        $page = input('param.page');
        if(empty($page)){
            $page = 1;
        }

        $keyword = input('param.keyword');

        $a = ($page-1)*8;
        $map = array();
        if($keyword){
            $map['title'] = ['like','%'.$keyword.'%'];
        }
        if(input('param.page'))
        {
            $list  =  Db::name('debate')->field('id,title,create_time,photo')->where($map)->order('id desc')->limit($a,8)->select();
            foreach($list as $key=>$item){
                $list[$key]['create_time'] = date('Y-m-d H:i:s',$item['create_time']);
                $list[$key]['zan']  = Db::name('debates_zan')->where('debate_id',$item['id'])->count();
                $list[$key]['collect']  = Db::name('debate_collect')->where('debate_id',$item['id'])->count();
                $list[$key]['comment']  = Db::name('debate_list')->where('debate_id',$item['id'])->count();


            }
            return json(['code' => 1, 'list'=>$list,  'msg' => 'ok']);
        }
        $view = new View();
        if(session('user_id')){
            $view->assign('is_login',1);
        }else{
            $view->assign('is_login',0);
        }
        $view->assign('keyword',$keyword);
        return $view->fetch();

    }


    public function index()
    {

        $id = input('param.id');
        $this->read_article($id);
        $article = Db::name('article')->where('id',$id)->find();

        $article['create_time1'] = date('Y-m-d H:i:s',$article['create_time']);


        if(empty($article['user_id'])){
            $map6 = array(
                'instit_id'=>$article['instit_id'],
                'group_id'=>8
            );
            $user_info = Db::name('member')->field('nickname,head_img,desc')->where($map6)->find();
            $author = $user_info['nickname'];
        }else{

            // $auth = Db::name('member')->field('nickname')->where('id',$article['user_id'])->find();
            $user_info = Db::name('member')->field('nickname,head_img,desc')->where('id',$article['user_id'])->find();
            $author = $user_info['nickname'];
        }

        $user_id = session('user_id');
        //$user_id = 14;
        $zans = Db::name('article_zan')->field('count(*) as num')->where('article_id',$id)->find();
        $map = array(
            'user_id'=>$user_id,
            'article_id'=>$id,
        );
        $myzan = Db::name('article_zan')->field('is_zan')->where($map)->find();
        if($myzan){
            $is_zan = $myzan['is_zan'];
        }else{
            $is_zan = 0;
        }
        $collects = Db::name('article_collect')->where('article_id',$id)->count();
        $map = array(
            'user_id'=>$user_id,
            'article_id'=>$id,
        );
        $mycollect = Db::name('article_collect')->field('is_collect')->where($map)->find();
        if($mycollect){
            $is_collect = $mycollect['is_collect'];
        }else{
            $is_collect = 0;
        }
        $map10 = array(
            'article_id'=>$id,
            'status'=>1,
        );
        $commentall = Db::name('article_comment')->where($map10)->count();
        $commentpage =$commentall/5;
        $commentpage = (int)$commentpage;
        $commentyu = $commentall%5;

        $comment_list =  Db::name('article_comment')->alias('a')->field('a.id,a.content,a.addtime,b.nickname,b.head_img')->join('shuren_member b','a.user_id = b.id')->where('article_id',$id)->select();

        $view = new View();
        //print_R($article);
        $view->assign('article',$article);    //文章内容
        $view->assign('comment_list',$comment_list);    //文章内容
        $view->assign('author',$author);   //总的点赞数
        if($commentyu>0){
            $view->assign('all',$commentpage+1);   //总的点赞数
        }else{
            $view->assign('all',$commentpage);   //总的点赞数
        }
        $view->assign('zan',$zans['num']);   //总的点赞数
        $view->assign('is_zan',$is_zan);     //我是否赞
        $view->assign('collect',$collects);

        if(empty($user_id)){
            $user_id = 0;
        }

        $tp_article = Db::name('pt_article_edit')->alias('a')->field('a.title,a.remark,a.content,a.update_time,b.name,b.id')->join('instit b','a.by_instit_id=b.id')->where('a.article_id',$id)->select();




        $view->assign('tp_article',$tp_article);
        $view->assign('user_id',$user_id);
        $view->assign('commentall',$commentall);
        $view->assign('user_info',$user_info);
        $view->assign('id',$id);
        if(session('user_id')){
            $view->assign('is_login',1);
        }else{
            $view->assign('is_login',0);
        }
        $view->assign('is_collect',$is_collect);
        return $view->fetch();

    }

    public function essay_detail()
    {
        $id=input('param.id');
        $map  = array();
        $map['id'] = $id;
        $user_id = session('user_id');

        $info = Db::name('essay')->where($map)->find();

        $info['create_time1'] = date('Y-m-d H:i:s',$info['create_time']);

        if($info['end_time']>time()){
            $can_tou =1;
        }else{
            $can_tou =0;
        }


        $info['end_time'] = date('Y-m-d H:i:s',$info['end_time']);
        $map1 = array(
            'user_id'=>$user_id,
            'status'=>1
        );
        $myarticle = Db::name('article')->field('id,title')->where($map1)->select();

        $zans = Db::name('essay_zan')->field('count(*) as num')->where('essay_id',$id)->find();
        $map = array(
            'user_id'=>$user_id,
            'essay_id'=>$id,
        );
        $myzan = Db::name('essay_zan')->field('is_zan')->where($map)->find();
        if($myzan){
            $is_zan = $myzan['is_zan'];
        }else{
            $is_zan = 0;
        }
        $collects = Db::name('essay_collect')->field('count(*) as num')->where('essay_id',$id)->find();
        $map = array(
            'user_id'=>$user_id,
            'essay_id'=>$id,
        );
        $mycollect = Db::name('essay_collect')->field('is_collect')->where($map)->find();
        if($mycollect){
            $is_collect = $mycollect['is_collect'];
        }else{
            $is_collect = 0;
        }
        if(empty($user_id)){
            $user_id = 0;
        }

        $map10 = array(
            'a.essay_id'=>$id,
            'a.status'=>1
        );

        $list = Db::name('essay_article')->alias('a')->field('a.id,a.article_id,a.add_time,b.title,b.photo')->
        join('shuren_article b','a.article_id = b.id')->where($map10)->order('id desc')->select();
        if($list){
            foreach($list as $key=>$item){
                $map11['article_id'] = $item['article_id'];
                $count = Db::name('article_edit')->where($map11)->count();
                $list[$key]['count'] = $count;
                if($count>0){
                    $article_eidt_info = Db::name('article_edit')->where($map11)->find();
                    $list[$key]['article_eidt_id'] = $article_eidt_info['id'];
                    $list[$key]['article_eidt_title'] = $article_eidt_info['title'];
                    $list[$key]['update_time'] = date('m-d H:i',$article_eidt_info['update_time']);
                }
                $list[$key]['add_time'] = date('m-d H:i',$item['add_time']);
            }
        }


        $map6 = array(
            'instit_id'=>$info['instit_id'],
            'group_id'=>8
        );
        $user_info = Db::name('member')->field('nickname,head_img,desc')->where($map6)->find();


        $view = new View();
        $view->assign('zan',$zans['num']);   //总的点赞数
        $view->assign('is_zan',$is_zan);     //我是否赞
        $view->assign('collect',$collects['num']);
        $view->assign('is_collect',$is_collect);
        $view->assign('user_id',$user_id);
        $view->assign('myarticle',$myarticle);
        $view->assign('user_info',$user_info);
        $view->assign('info',$info);
        $view->assign('id',$id);
        $view->assign('list',$list);
        if(session('user_id')){
            $view->assign('is_login',1);
        }else{
            $view->assign('is_login',0);
        }
        $view->assign('can_tou',$can_tou);
        return $view->fetch();
    }


    public function debate_detail()
    {
        $id=input('param.id');
        $map  = array();
        $map['a.id'] = $id;
        $info = Db::name('debate')->alias('a')->field('a.*,b.nickname')->join('shuren_member b','a.user_id=b.id')->where($map)->find();

        $comment_num =  Db::name('debate_list')->where('debate_id',$id)->count();

        $view = new View();
        $user_id = session('user_id');
        if(empty($user_id)){
            $user_id = 0;
        }

        $zans = Db::name('debates_zan')->field('count(*) as num')->where('debate_id',$id)->find();
        $map = array(
            'user_id'=>$user_id,
            'debate_id'=>$id,
        );
        $myzan = Db::name('debates_zan')->field('is_zan')->where($map)->find();
        if($myzan){
            $is_zan = $myzan['is_zan'];
        }else{
            $is_zan = 0;
        }
        $collects = Db::name('debate_collect')->field('count(*) as num')->where('debate_id',$id)->find();

        $map = array(
            'user_id'=>$user_id,
            'debate_id'=>$id,
        );
        $mycollect = Db::name('debate_collect')->field('is_collect')->where($map)->find();
        if($mycollect){
            $is_collect = $mycollect['is_collect'];
        }else{
            $is_collect = 0;
        }

        if(empty($info['user_id'])){
            $map6 = array(
                'instit_id'=>$info['instit_id'],
                'group_id'=>8
            );
            $user_info = Db::name('member')->field('nickname,head_img,desc')->where($map6)->find();


        }else{

            // $auth = Db::name('member')->field('nickname')->where('id',$article['user_id'])->find();
            $user_info = Db::name('member')->field('nickname,head_img,desc')->where('id',$info['user_id'])->find();
        }

        $info['create_time1'] = date('Y-m-d H:i:s',$info['create_time']);

        $view->assign('user_id',$user_id);
        $view->assign('info',$info);
        $view->assign('comment_num',$comment_num);
        $view->assign('user_info',$user_info);
        $view->assign('id',$id);
        $view->assign('zan',$zans['num']);   //总的点赞数
        $view->assign('is_zan',$is_zan);     //我是否赞
        $view->assign('collect',$collects['num']);
        $view->assign('is_collect',$is_collect);
        if(session('user_id')){
            $view->assign('is_login',1);
        }else{
            $view->assign('is_login',0);
        }
        return $view->fetch();
    }




    public function write()
    {

        $user_id = session('user_id');
        if(empty($user_id)){
                $this->redirect('user/login');
        }

        $member = Db::name('member')->field('nickname,head_img,account')->where('id',$user_id)->find();




        if(request()->isAjax()){
            $param = input('post.');
            $article = new ArticleModel();
            $param['instit_id'] = session('instit_id');
            $param['user_id'] = session('user_id');
            if(empty($param['id'])){
                unset($param['id']);
                $flag = $article->insertArticle($param);
            }else{
                $param['status'] = 0;
                $flag = $article->updateArticle($param);
            }
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }


        $id = input('param.id');
        if($id){

            $article = new ArticleModel();
            $info = $article->getOneArticle($id);
        }else{
            $info = array(
                'title'=>'',
                'remark'=>'',
                'id'=>'',
                'content'=>'',
                'photo'=>'',
                'cate_id'=>'',
            );
        }

        $cate = new ArticleCateModel();
        $view = new View();
        $view->assign('cate',$cate->getAllCate());
        $view->assign('info',$info);
        $view->assign('member',$member);
        if(session('user_id')){
            $view->assign('is_login',1);
        }else{
            $view->assign('is_login',0);
        }
        return $view->fetch();

    }

    public function cg_add_article()
    {
        if(request()->isAjax()){
            $param = input('post.');
            $article = new ArticleModel();
            $param['instit_id'] = session('instit_id');
            $param['user_id'] = session('user_id');
            $param['create_time'] = time();
            $param['status'] = -1;

            if(empty($param['id'])){
                unset($param['id']);
                $flag = $article->insertArticle($param);
            }else{
                $flag = $article->updateArticle($param);
            }



            if($flag['code']==1){
                $flag['msg'] = '草稿保存成功';
            }

            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);

            if($flag){
                $this->redirect('article/draft');
            }
        }
    }



    public function write_debate()
    {
        $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }

        $member = Db::name('member')->field('nickname,head_img,account')->where('id',$user_id)->find();


        if(request()->isAjax()){

//            $user = new UserModel();
//            $user_info =  $user->getUserGroupinfo();

            $param = input('post.');



            $param['user_id'] = session('user_id');
            $param['instit_id'] =  session('instit_id');
            $article = new DebateModel();
            $flag = $article->insertArticle($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }

        $view = new View();
        $view->assign('member',$member);
        if(session('user_id')){
            $view->assign('is_login',1);
        }else{
            $view->assign('is_login',0);
        }
        return $view->fetch();

    }


    public function collect()
    {



        $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }

        $member = Db::name('member')->field('nickname,head_img,account')->where('id',$user_id)->find();

        $Nowpage = input('param.page') ? input('param.page'):1;
        $type = input('param.type') ? input('param.type'):1;
        $limits = 10;// 获取总条数

            $map['a.user_id'] = $user_id;
            if($type==1){
                $count = Db::name('article_collect')->where('user_id',$user_id)->count();//计算总页面


                $list = Db::name('article_collect')->alias('a')->field('a.id as collect_id,b.id,b.title,b.photo,b.create_time,b.user_id,b.writer')->join('article b', 'a.article_id = b.id')->where($map)->order('a.id desc')->select();

                foreach ($list as $key => $val) {
                    if (!empty($val['user_id'])) {
                        $author = Db::name('member')->field('nickname')->where('id', $val['user_id'])->find();
                        $list[$key]['author'] = $author['nickname'];
                    } else {
                        $list[$key]['author'] = $val['writer'];
                    }
                    $list[$key]['create_time'] = date('Y-m-d', $val['create_time']);
                }
                if(input('param.page')) {
                    return json(['code' => 1, 'list'=>$list,  'msg' => 'ok']);
                }
            }elseif($type==2){
                $count = Db::name('essay_collect')->where('user_id',$user_id)->count();//计算总页面
                $essay = Db::name('essay_collect')->alias('a')->field('a.id as collect_id,b.id,b.title,b.photo,b.create_time,b.user_id,b.writer,b.instit_id')->join('essay b', 'a.essay_id = b.id')->where($map)->order('a.id desc')->select();
                foreach ($essay as $key => $val) {
                    $author = Db::name('instit')->field('name')->where('id', $val['instit_id'])->find();
                    $essay[$key]['author'] = $author['name'];
                    $essay[$key]['create_time'] = date('Y-m-d', $val['create_time']);
                }
                if(input('param.page')) {
                    return json(['code' => 1, 'list'=>$essay,  'msg' => 'ok']);
                }
            }elseif($type==3) {
                $count = Db::name('debate_collect')->where('user_id',$user_id)->count();//计算总页面
                $debate = Db::name('debate_collect')->alias('a')->field('a.id as collect_id,b.id,b.title,b.photo,b.create_time,b.user_id,b.writer')->join('debate b', 'a.debate_id = b.id')->where($map)->order('a.id desc')->select();
                foreach ($debate as $key => $val) {
                    $author = Db::name('member')->field('nickname')->where('id', $val['user_id'])->find();
                    $debate[$key]['author'] = $author['nickname'];
                    $debate[$key]['create_time'] = date('Y-m-d', $val['create_time']);
                }
                if(input('param.page')) {
                    return json(['code' => 1, 'list'=>$debate,  'msg' => 'ok']);
                }
            }



        $allpage = intval(ceil($count / $limits));

        $view = new View();
        $view->assign('Nowpage', $Nowpage); //当前页
        $view->assign('allpage', $allpage); //总页数
        $view->assign('member',$member);
        $view->assign('type', $type); //总页数
        if(session('user_id')){
            $view->assign('is_login',1);
        }else{
            $view->assign('is_login',0);
        }
        return $view->fetch();







    }

    public function myessay()
    {


        $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }

        $member = Db::name('member')->field('nickname,head_img,account')->where('id',$user_id)->find();

        $map['a.user_id'] = $user_id;
        $map1['user_id'] = $user_id;
        //  $map['status'] = 1;


        $Nowpage = input('param.page') ? input('param.page'):1;
        $limits = 10;// 获取总条数
        $count = Db::name('essay_article')->where($map1)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));

        if(input('param.page'))
        {
            //$list  =  Db::name('article')->field('id,title,create_time,photo')->where($map)->order('id desc')->limit($a,5)->select();
            $list = Db::name('essay_article')->alias('a')->field('a.article_id,a.status,a.essay_id,c.id,b.photo,a.add_time,b.title as essay_title,c.title as article_title')->join('essay b','a.essay_id=b.id')->join('article c','a.article_id=c.id')->where($map)->page($Nowpage,$limits)->order('a.id desc')->select();
            foreach($list as $key=>$item){
                $list[$key]['add_time'] = date('Y-m-d H:i:s',$item['add_time']);
                if($item['status']==1){
                    $list[$key]['note'] = '已采用';
                }if($item['status']==2){
                    $list[$key]['note'] = '拟采用';
                }if($item['status']==3){
                    $list[$key]['note'] = '已退稿';
                }else{
                    $list[$key]['note'] = '';
                }
            }
            return json(['code' => 1, 'list'=>$list,  'msg' => 'ok']);
        }
        $view = new View();

        $view->assign('Nowpage', $Nowpage); //当前页
        $view->assign('allpage', $allpage); //总页数
        $view->assign('member',$member);
        if(session('user_id')){
            $view->assign('is_login',1);
        }else{
            $view->assign('is_login',0);
        }

        return $view->fetch();

    }

    public function draft()
    {
        $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }

        $member = Db::name('member')->field('nickname,head_img,account')->where('id',$user_id)->find();


        $map['user_id'] = $user_id;
        $map['status'] = -2;

        $Nowpage = input('param.page') ? input('param.page'):1;
        $limits = 10;// 获取总条数
        $count = Db::name('article')->where($map)->count();//计算总页面
        $allpage = intval(ceil($count / $limits));

        if(input('param.page'))
        {
            $list  =  Db::name('article')->field('id,title,create_time,photo')->where($map)->order('id desc')->page($Nowpage,10)->select();
            foreach($list as $key=>$item){
                $list[$key]['create_time'] = date('Y-m-d H:i:s',$item['create_time']);
            }
            return json(['code' => 1, 'list'=>$list,  'msg' => 'ok']);
        }
        $view = new View();
        $view->assign('Nowpage', $Nowpage); //当前页
        $view->assign('allpage', $allpage); //总页数
        $view->assign('member',$member);
        if(session('user_id')){
            $view->assign('is_login',1);
        }else{
            $view->assign('is_login',0);
        }
        return $view->fetch();


    }

    public function temp_message()
    {

        $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }

        $member = Db::name('member')->field('nickname,head_img,account')->where('id',$user_id)->find();


        $user =  DB::name('member')->field('account,group_id')->where('id',$user_id)->find();

        if($user['group_id']==9){
            $map2 = array(
                'mobile'=>$user['account'],
                'instit_id'=>session('instit_id'),
                'is_leader'=>1
            );
            $teach =  DB::name('teach')->field('id')->where($map2)->find();

            if($teach){
                $map3 = array(
                    'leader'=>$teach['id'],
                    'instit_id'=>session('instit_id'),
                    'status'=>1
                );
                $banji_info = DB::name('theclass')->where($map3)->find();
            }

            if($teach&&$banji_info){
                $data = array(
                    'is_send' => 1,
                    'class_id'=>$banji_info['id']
                );
                $auth = 'teach';
            }else{
                $data = array(
                    'is_send' => 0,
                    'class_id'=>''
                );
                $auth = 'teach2';
            }
        }


        if($user['group_id']==8){
            $auth = 'admin';
        }

        if($user['group_id']==1){
            $auth = 'student';
        }

        $key = input('key');
        $map = [];


        $Nowpage = input('get.page') ? input('get.page'):1;
        $limits = 10;// 获取总条数


        $nowyear = date('Y');
        $nowtime = strtotime($nowyear.'-08-25');
        if($nowtime>time()){
            $year = $nowyear-1;
        }else{
            $year = $nowyear;
        }

        if($auth=='admin'){
            $map['shuren_temp_message.instit_id'] = session('instit_id');
            $map1['instit_id'] = session('instit_id');
            $count = Db::name('temp_message')->where($map1)->count();//计算总页面
            $allpage = intval(ceil($count / $limits));
            $user = new TempModel();
            $lists = $user->getTempByWhere($map, $Nowpage, $limits);

        }elseif($auth=='teach'){
            $map = array();
            $map2= array();
            $map['shuren_temp_message.instit_id'] = session('instit_id');
            $map['shuren_temp_message.year'] = $year;

            $map2['shuren_temp_message.type'] =  ['in','2,3'];
            $map12['type'] = ['in','2,3'];

            $map2['shuren_temp_message.class_id'] =  $data['class_id'];
            $map12['class_id'] = $data['class_id'];

            $map1['instit_id'] = session('instit_id');
            $map1['year'] = $year;

            $count = Db::name('temp_message')->where($map1)->whereor($map12)->count();//计算总页面
            $allpage = intval(ceil($count / $limits));
            $user = new TempModel();
            // $lists = $user->getTempByWhereTeach($map,$map2, $Nowpage, $limits);
            $lists =  Db::name('temp_message')->field('shuren_temp_message.*,shuren_instit.name')->join('shuren_instit','shuren_temp_message.instit_id = shuren_instit.id')->where("shuren_temp_message.instit_id = ". session('instit_id')." and year = ".$year)->where("shuren_temp_message.class_id = ".$data['class_id'] ." or shuren_temp_message.type in (2,3)")->page($Nowpage, $limits)->order('id desc')->select();
        }elseif($auth=='student'){

            $cla_arr = Db::name('class_student')->where('account',$user['account'])->select();

            if($cla_arr){
                $all_class_str= '';
                foreach($cla_arr as $key=>$v){
                    $all_class_str =$all_class_str . $v['class_id'].',';
                }

                $all_class_str = substr($all_class_str,0,-1);


                $map = array();
              //  $map2= array();
               // $map1['shuren_temp_message.instit_id'] = session('instit_id');
               // $map['shuren_temp_message.year'] = $year;

               // $map2['shuren_temp_message.type'] =  ['in','2'];
                $map12['type'] = 2;

               // $map2['shuren_temp_message.class_id'] = ['in','2'];
                $map12['class_id'] = ['in',$all_class_str];

                $map1['instit_id'] = session('instit_id');
                //$map1['year'] = $year;

                $count = Db::name('temp_message')->where($map1)->whereor($map12)->count();//计算总页面
                $allpage = intval(ceil($count / $limits));
                $user = new TempModel();

                // $lists = $user->getTempByWhereTeach($map,$map2, $Nowpage, $limits);
                $lists =  Db::name('temp_message')->field('shuren_temp_message.*,shuren_instit.name')->join('shuren_instit','shuren_temp_message.instit_id = shuren_instit.id')->where("shuren_temp_message.instit_id = ". session('instit_id')." and year = ".$year)->where("shuren_temp_message.class_id in (".$all_class_str .") or shuren_temp_message.type =2 ")->page($Nowpage, $limits)->order('id desc')->select();

//
//               $sql =   Db::name('temp_message')->getLastSql();
//                echo $sql;
            }


        }elseif($auth=='teach2'){
            $map['shuren_temp_message.instit_id'] = session('instit_id');
            $map['shuren_temp_message.year'] = $year;
            $map['shuren_temp_message.type'] =  ['in','2','3'];
            $map1['instit_id'] = session('instit_id');
            $map1['year'] = $year;
            $map1['type'] = ['in','2','3'];
            $count = Db::name('temp_message')->where($map1)->count();//计算总页面
            $allpage = intval(ceil($count / $limits));
            $user = new TempModel();
            $lists = $user->getTempByWhere($map, $Nowpage, $limits);
        }





        foreach($lists as $k=>$v){
            if($v['type']=='1'){
                $class_info =  Db::name('theclass')->alias('a')->field('n.nianji,a.banji')->join('nianji n','a.nianji = n.id')->where('a.id',$v['class_id'])->find();
                $lists[$k]['duixiang'] = $class_info['nianji'].$class_info['banji'];
            }elseif($v['type']=='2'){
                $lists[$k]['duixiang'] = '全体学生';
            }elseif($v['type']=='3'){
                $lists[$k]['duixiang'] = '全体教师';
            }
            $contents = json_decode($v['content'],true);
            $lists[$k]['message'] = $contents['keyword4'];
            $lists[$k]['addtime1'] = date('Y-m-d H:i:s',$v['addtime']);
        }

        $this->assign('Nowpage', $Nowpage); //当前页
        $this->assign('allpage', $allpage); //总页数
        $this->assign('val', $key);

        if(input('get.page'))
        {
            return json(['list'=>$lists]);
        }


        $view = new View();
        $view->assign('Nowpage', $Nowpage); //当前页
        $view->assign('auth', $auth); //当前页
        $view->assign('allpage', $allpage); //总页数
        $view->assign('member',$member);
        if(session('user_id')){
            $view->assign('is_login',1);
        }else{
            $view->assign('is_login',0);
        }
        return $view->fetch();

    }



    public function temp_detail()
    {
        $user_id = session('user_id');
        if(empty($user_id)){
            $this->redirect('user/login');
        }

        $member = Db::name('member')->field('nickname,head_img,account')->where('id',$user_id)->find();

        $id = input('param.id');

        $info =  Db::name('temp_message')->field('shuren_temp_message.*,shuren_instit.name')->join('shuren_instit','shuren_temp_message.instit_id = shuren_instit.id')->where("shuren_temp_message.id",$id)->find();

        if($info['type']=='1'){
            $class_info =  Db::name('theclass')->alias('a')->field('n.nianji,a.banji')->join('nianji n','a.nianji = n.id')->where('a.id',$info['class_id'])->find();
            $info['duixiang'] = $class_info['nianji'].$class_info['banji'];
        }elseif($info['type']=='2'){
            $info['duixiang'] = '全体学生';
        }elseif($info['type']=='3'){
            $info['duixiang'] = '全体教师';
        }
        $contents = json_decode($info['content'],true);
        $info['message'] = $contents['keyword4'];
        $info['addtime1'] = date('Y-m-d H:i:s',$info['addtime']);

  //  print_R($info);
        $view = new View();
        $view->assign('info',$info);
        $view->assign('member',$member);
        $view->assign('content',$contents);
        if(session('user_id')){
            $view->assign('is_login',1);
        }else{
            $view->assign('is_login',0);
        }
        return $view->fetch();

    }

}
