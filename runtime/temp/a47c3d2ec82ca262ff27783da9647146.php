<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:79:"/data/www/html/web2017/public_html/../application/admin/view/article/index.html";i:1498729788;s:79:"/data/www/html/web2017/public_html/../application/admin/view/public/header.html";i:1484102488;s:79:"/data/www/html/web2017/public_html/../application/admin/view/public/footer.html";i:1487735376;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo config('WEB_SITE_TITLE'); ?></title>
    <link href="/static/admin/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/admin/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/static/admin/css/animate.min.css" rel="stylesheet">
    <link href="/static/admin/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="/static/admin/css/plugins/chosen/chosen.css" rel="stylesheet">
    <link href="/static/admin/css/plugins/switchery/switchery.css" rel="stylesheet">
    <link href="/static/admin/css/style.min.css?v=4.1.0" rel="stylesheet">
    <link href="/static/admin/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <style type="text/css">
    .long-tr th{
        text-align: center
    }
    .long-td td{
        text-align: center
    }
    </style>
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <!-- Panel Other -->
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>文章列表</h5>
        </div>
        <div class="ibox-content">
            <!--搜索框开始-->           
            <div class="row">
                <div class="col-sm-12">   
                <div  class="col-sm-2" style="width: 100px">
                    <div class="input-group" >
                        <a href="<?php echo url('add_article'); ?>"><button class="btn btn-outline btn-primary" type="button">添加文章</button></a>
                    </div>
                </div>
                    <form name="admin_list_sea" class="form-search" method="post" action="<?php echo url('index'); ?>">
                        <div class="col-sm-3">
                            <div class="input-group">
                                <input type="text" id="key" class="form-control" name="key" value="<?php echo $val; ?>" placeholder="输入需查询的文章名称" />   
                                <span class="input-group-btn"> 
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> 搜索</button> 
                                </span>
                            </div>
                        </div>
                    </form>                         
                </div>
            </div>
            <!--搜索框结束-->
            <div class="hr-line-dashed"></div>
            <div class="example-wrap">
                <div class="example">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr class="long-tr">
                                <th width="3%">ID</th>
                                <th width="15%">标题</th>
                                <th width="5%">作者</th>
                                <th width="5%">所属分类</th>
                                <th width="5%">文章封面</th>
                                <th width="4%">浏览量</th>
                                <!--<th width="4%">转发数</th>-->
                                <!--<th width="4%">收藏数</th>-->
                                <!--<th width="4%">关注数</th>-->
                                <th width="10%">提交时间</th>
                                <!--<th width="10%">更新时间</th>-->
                                <!--<th width="5%">状态</th>-->
                                <th width="5%">公告栏推荐</th>
                                <th width="5%">状态</th>
                                <?php if($instit_id ==1): ?>
                                <th width="5%">首页推荐</th>
                                <?php endif; ?>
                                <th width="12%">操作</th>
                            </tr>
                        </thead>
                        <script id="list-template" type="text/html">
                            {{# for(var i=0;i<d.length;i++){  }}
                                <tr class="long-td">
                                    <td>{{d[i].id}}</td>
                                    <td>{{d[i].title}}</td>
                                    <td>{{d[i].nickname}}</td>
                                    <td>{{d[i].name}}</td>
                                    <td><img src="/uploads/images/{{d[i].photo}}" style="height: 30px" onerror="this.src='/static/admin/images/no_img.jpg'"/></td> 
                                    <td>{{d[i].views}}</td>
                                    <!--<td>{{d[i].share}}</td>-->
                                    <!--<td>{{d[i].collect}}</td>-->
                                    <!--<td>{{d[i].fans}}</td>-->
                                    <td>{{d[i].create_time}}</td>
                                    <td>
                                        {{# if(d[i].jigou_tui==1){ }}
                                        <a class="red" href="javascript:;" onclick="article_state({{d[i].id}});">
                                            <div id="zt{{d[i].id}}"><span class="label label-info">是</span></div>
                                        </a>
                                        {{# }else{ }}
                                        <a class="red" href="javascript:;" onclick="article_state({{d[i].id}});">
                                            <div id="zt{{d[i].id}}"><span class="label label-danger">否</span></div>
                                        </a>
                                        {{# } }}
                                    </td>


                                    <!--<td>-->
                                        <!--{{# if(d[i].status==0){ }}-->
                                        <!--<a class="red" href="javascript:;" onclick="article_status({{d[i].id}});">-->
                                            <!--<div id="zk{{d[i].id}}"><span class="label label-danger">未审核</span></div>-->
                                        <!--</a>-->
                                        <!--{{# } }}-->
                                        <!--{{# if(d[i].status==1){ }}-->
                                        <!--<a class="red" href="javascript:;" onclick="article_status({{d[i].id}});">-->
                                            <!--<div id="zk{{d[i].id}}"><span class="label label-info">已审核</span></div>-->
                                        <!--</a>-->
                                        <!--{{# } }}-->
                                    <!--</td>-->
                                    <td>{{d[i].status}}</td>


                                    <?php if($instit_id ==1): ?>
                                    <td>
                                        {{# if(d[i].is_tui==1){ }}
                                        <a href="javascript:;" onclick="shuren_article_state({{d[i].id}});">
                                            <div id="ztk{{d[i].id}}"><span class="label label-info">是</span></div>
                                        </a>
                                        {{# }else{ }}
                                        <a href="javascript:;" onclick="shuren_article_state({{d[i].id}});">
                                            <div id="ztk{{d[i].id}}"><span class="label label-danger">否</span></div>
                                        </a>
                                        {{# } }}
                                    </td>
                                    <?php endif; ?>

                                    <td>

                                        <a href="/web/article/index/id/{{d[i].id}}.html"  target="_blank" class="btn btn-primary btn-xs btn-outline">
                                            <i class="fa fa-paste"></i>预览</a>&nbsp;&nbsp;

                                        <a href="javascript:;" onclick="read_article({{d[i].id}})" class="btn btn-primary btn-xs btn-outline">
                                            <i class="fa fa-paste"></i>查看文章</a>&nbsp;&nbsp;

                                        <a href="javascript:;" onclick="edit_article({{d[i].id}})" class="btn btn-primary btn-xs btn-outline">
                                            <i class="fa fa-paste"></i>编辑</a>&nbsp;&nbsp;




                                        <?php if($instit_id != 1): ?>
                                        <a href="javascript:;" onclick="recommend_article({{d[i].id}})" class="btn btn-primary btn-xs btn-outline">
                                            <i class="fa fa-bullhorn" id="tj{{d[i].id}}">
                                                {{# if(d[i].is_recommend==1){ }}
                                                已推荐
                                                {{# } }}
                                                {{# if(d[i].is_recommend==0){ }}
                                                推荐
                                                {{# } }}
                                            </i>
                                        </a>
                                        <?php endif; ?>

                                            <a href="javascript:;" onclick="del_article({{d[i].id}})" class="btn btn-danger btn-xs btn-outline">
                                            <i class="fa fa-trash-o"></i> 删除</a>

                                    </td>
                                </tr>
                            {{# } }}
                        </script>
                        <tbody id="list-content"></tbody>
                    </table>
                    <div id="AjaxPage" style="text-align:right;"></div>
                    <div style="text-align: right;">
                        共<?php echo $count; ?>条数据，<span id="allpage"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!-- 加载动画 -->
<div class="spiner-example">
    <div class="sk-spinner sk-spinner-three-bounce">
        <div class="sk-bounce1"></div>
        <div class="sk-bounce2"></div>
        <div class="sk-bounce3"></div>
    </div>
</div>

<script src="__JS__/jquery.min.js?v=2.1.4"></script>
<script src="__JS__/bootstrap.min.js?v=3.3.6"></script>
<script src="__JS__/content.min.js?v=1.0.0"></script>
<script src="__JS__/plugins/chosen/chosen.jquery.js"></script>
<script src="__JS__/plugins/iCheck/icheck.min.js"></script>
<script src="__JS__/plugins/layer/laydate/laydate.js"></script>
<script src="__JS__/plugins/switchery/switchery.js"></script><!--IOS开关样式-->
<script src="__JS__/jquery.form.js"></script>
<script src="__JS__/layer/layer.js"></script>
<script src="__JS__/laypage/laypage.js"></script>
<script src="__JS__/laytpl/laytpl.js"></script>
<script src="__JS__/lunhui.js"></script>
<script>
    $(document).ready(function(){$(".i-checks").iCheck({checkboxClass:"icheckbox_square-green",radioClass:"iradio_square-green",})});
</script>

<script type="text/javascript">
   
    /**
     * [Ajaxpage laypage分页]
     * @param {[type]} curr [当前页]
     */
    Ajaxpage();

    function Ajaxpage(curr){
        var key=$('#key').val();
        $.getJSON('<?php echo url("article/index"); ?>', {
            page: curr || 1,key:key
        }, function(data){      //data是后台返回过来的JSON数据
            console.log(data);
			$(".spiner-example").css('display','none'); //数据加载完关闭动画
            if(data==''){
                $("#list-content").html('<td colspan="20" style="padding-top:10px;padding-bottom:10px;font-size:16px;text-align:center">暂无数据</td>');
            }else{
                var tpl = document.getElementById('list-template').innerHTML;
                laytpl(tpl).render(data, function(html){
                    document.getElementById('list-content').innerHTML = html;
                });
                laypage({
                    cont: $('#AjaxPage'),//容器。值支持id名、原生dom对象，jquery对象,
                    pages:'<?php echo $allpage; ?>',//总页数
                    skip: true,//是否开启跳页
                    skin: '#1AB5B7',//分页组件颜色
                    curr: curr || 1,
                    groups: 3,//连续显示分页数
                    jump: function(obj, first){
                        if(!first){
                            Ajaxpage(obj.curr)
                        }
                        $('#allpage').html('第'+ obj.curr +'页，共'+ obj.pages +'页');
                    }
                });
            }
        });
    }


    function read_article(id){
        location.href = './articleread/id/'+id+'.html';
    }

//编辑文章
function edit_article(id){
    location.href = './edit_article/id/'+id+'.html';
}

//删除文章
function del_article(id){
    lunhui.confirm(id,'<?php echo url("del_article"); ?>');
}

//文章状态
function article_state(id){
    lunhui.status(id,'<?php echo url("article_jigoutui_state"); ?>','是','否');
}
function shuren_article_state(id){
    lunhui.status(id,'<?php echo url("shuren_article_state"); ?>','是','否','ztk');
}

function article_status(id){
    lunhui.status(id,'<?php echo url("article_status"); ?>','已审核','未审核','zk');
}

function recommend_article(id){

    $.post('<?php echo url("Article/recommend_article"); ?>',
            {id:id},
            function(data){
                var ccc = '#tj'+id;
                if(data.code==1) {
                    $(ccc).html('已推荐');
                }else{
                    $(ccc).html('推荐');
                }
            });
}

</script>
</body>
</html>