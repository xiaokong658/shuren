<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:84:"/data/www/html/web2017/public_html/../application/admin/view/wechat/wechat_edit.html";i:1494488022;s:79:"/data/www/html/web2017/public_html/../application/admin/view/public/header.html";i:1484102488;s:79:"/data/www/html/web2017/public_html/../application/admin/view/public/footer.html";i:1487735376;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo config('WEB_SITE_TITLE'); ?></title>
    <link href="/static/admin/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/admin/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/static/admin/css/animate.min.css" rel="stylesheet">
    <link href="/static/admin/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="/static/admin/css/plugins/chosen/chosen.css" rel="stylesheet">
    <link href="/static/admin/css/plugins/switchery/switchery.css" rel="stylesheet">
    <link href="/static/admin/css/style.min.css?v=4.1.0" rel="stylesheet">
    <link href="/static/admin/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <style type="text/css">
    .long-tr th{
        text-align: center
    }
    .long-td td{
        text-align: center
    }
    </style>
</head>
<link rel="stylesheet" type="text/css" href="/static/admin/webupload/webuploader.css">
<link rel="stylesheet" type="text/css" href="/static/admin/webupload/style.css">
<style>
    .file-item{float: left; position: relative; width: 110px;height: 110px; margin: 0 20px 20px 0; padding: 4px;}
    .file-item .info{overflow: hidden;}
    .uploader-list{width: 100%; overflow: hidden;}
</style>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>添加微信公众号</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="form_basic.html#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" name="add" id="add" method="post" action="<?php echo url('wechat_edit'); ?>">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Token：</label>
                            <div class="input-group col-sm-4">
                                <input id="token" type="text" class="form-control" name="token" value="<?php echo $wechat['token']; ?>">
                            </div>
                        </div>


                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">公众号名称：</label>
                            <div class="input-group col-sm-4">
                                <input id="wechat_name" type="text" class="form-control" name="wechat_name"  value="<?php echo $wechat['wechat_name']; ?>"   >
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">公众号原始id：</label>
                            <div class="input-group col-sm-4">
                                <input id="original_id" type="text" class="form-control" name="original_id"  value="<?php echo $wechat['original_id']; ?>" >
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">微信号：</label>
                            <div class="input-group col-sm-4">
                                <input id="wechat_id" type="text" class="form-control" name="wechat_id"  value="<?php echo $wechat['wechat_id']; ?>" >
                            </div>
                        </div>






                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">头像地址：</label>
                            <div class="input-group col-sm-4">
                                <input type="hidden" id="data_photo" name="head_img"  value="<?php echo $wechat['head_img']; ?>">
                                <div id="fileList" class="uploader-list" style="float:right"></div>
                                <div id="imgPicker" style="float:left">选择头像</div>
                                <img id="img_data" class="img-circle" height="80px" width="80px" style="float:left;margin-left: 50px;margin-top: -10px;"    src="/uploads/face/<?php echo $wechat['head_img']; ?>" onerror="this.src='/static/admin/images/head_default.gif'"  />
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">appid：</label>
                            <div class="input-group col-sm-4">
                                <input id="appid" type="text" class="form-control" name="appid"  value="<?php echo $wechat['appid']; ?>" >

                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">encodingaeskey：</label>
                            <div class="input-group col-sm-4">
                                <input id="encodingaeskey" type="text" class="form-control" name="encodingaeskey"  value="<?php echo $wechat['encodingaeskey']; ?>" >
                            </div>
                        </div>


                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">appsecret：</label>
                            <div class="input-group col-sm-4">
                                <input id="appsecret" type="text" class="form-control" name="appsecret"  value="<?php echo $wechat['appsecret']; ?>" >
                            </div>
                        </div>


                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">二维码：</label>
                            <div class="input-group col-sm-4">
                                <input type="hidden" id="data_photo1" name="erweima"  value="<?php echo $wechat['erweima']; ?>" >
                                <div id="fileList1" class="uploader-list" style="float:right"></div>
                                <div id="imgPicker1" style="float:left">选择头像</div>
                                <img id="img_data1" class="img-circle" height="80px" width="80px" style="float:left;margin-left: 50px;margin-top: -10px;"  src="/uploads/face/<?php echo $wechat['erweima']; ?>" onerror="this.src='/static/admin/images/head_default.gif'"  />
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">微信号类型：</label>
                            <div class="input-group col-sm-4">
                                <select class="form-control m-b chosen-select" name="type" id="type">
                                    <option <?php if($wechat['type'] ==1): ?>selected<?php endif; ?> value="1">订阅号</option>
                                    <option <?php if($wechat['type'] ==2): ?>selected<?php endif; ?> value="2">认证订阅号</option>
                                    <option <?php if($wechat['type'] ==3): ?>selected<?php endif; ?> value="3">服务号</option>
                                    <option <?php if($wechat['type'] ==4): ?>selected<?php endif; ?> value="4">认证服务号</option>
                                </select>
                            </div>
                        </div>



                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">关注回复：</label>
                            <div class="input-group col-sm-4">
                                <textarea name="focus_repay" class="form-control" rows="5" style="width: 300px;"><?php echo $wechat['focus_repay']; ?></textarea>
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">默认回复：</label>
                            <div class="input-group col-sm-4">
                                <textarea name="default_repay" class="form-control" rows="5" style="width: 300px;"><?php echo $wechat['default_repay']; ?></textarea>
                            </div>
                        </div>


                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">接入状态：</label>
                            <div class="col-sm-6">
                                <div class="radio ">
                                    <input type="checkbox" name='status'  value="<?php echo $wechat['status']; ?>" class="js-switch" checked />&nbsp;&nbsp;默认开启
                                </div>
                            </div>
                        </div>


                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-3">
                                <input  type="hidden" class="form-control" name="id"  value="<?php echo $wechat['id']; ?>" >
                                <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> 保存</button>&nbsp;&nbsp;&nbsp;
                                <a class="btn btn-danger" href="javascript:history.go(-1);"><i class="fa fa-close"></i> 返回</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<script src="__JS__/jquery.min.js?v=2.1.4"></script>
<script src="__JS__/bootstrap.min.js?v=3.3.6"></script>
<script src="__JS__/content.min.js?v=1.0.0"></script>
<script src="__JS__/plugins/chosen/chosen.jquery.js"></script>
<script src="__JS__/plugins/iCheck/icheck.min.js"></script>
<script src="__JS__/plugins/layer/laydate/laydate.js"></script>
<script src="__JS__/plugins/switchery/switchery.js"></script><!--IOS开关样式-->
<script src="__JS__/jquery.form.js"></script>
<script src="__JS__/layer/layer.js"></script>
<script src="__JS__/laypage/laypage.js"></script>
<script src="__JS__/laytpl/laytpl.js"></script>
<script src="__JS__/lunhui.js"></script>
<script>
    $(document).ready(function(){$(".i-checks").iCheck({checkboxClass:"icheckbox_square-green",radioClass:"iradio_square-green",})});
</script>
<script type="text/javascript" src="/static/admin/webupload/webuploader.min.js"></script>

<script type="text/javascript">
    var $list = $('#fileList');
    //上传图片,初始化WebUploader
    var uploader = WebUploader.create({

        auto: true,// 选完文件后，是否自动上传。
        swf: '/static/admin/webupload/Uploader.swf',// swf文件路径
        server: "<?php echo url('Upload/uploadface'); ?>",// 文件接收服务端。
        duplicate :true,// 重复上传图片，true为可重复false为不可重复
        pick: '#imgPicker',// 选择文件的按钮。可选。

        accept: {
            title: 'Images',
            extensions: 'gif,jpg,jpeg,bmp,png',
            mimeTypes: 'image/jpg,image/jpeg,image/png'
        },

        'onUploadSuccess': function(file, data, response) {
            $("#data_photo").val(data._raw);
            $("#img_data").attr('src', '/uploads/face/' + data._raw).show();
        }
    });

    uploader.on( 'fileQueued', function( file ) {
//        $list.html( '<div id="' + file.id + '" class="item">' +
//                '<h4 class="info">' + file.name + '</h4>' +
//                '<p class="state">正在上传...</p>' +
//                '</div>' );
    });

    // 文件上传成功
    uploader.on( 'uploadSuccess', function( file ) {
//        $( '#'+file.id ).find('p.state').text('上传成功！');
    });

    // 文件上传失败，显示上传出错。
    uploader.on( 'uploadError', function( file ) {
        $( '#'+file.id ).find('p.state').text('上传出错!');
    });




    var uploader1 = WebUploader.create({

        auto: true,// 选完文件后，是否自动上传。
        swf: '/static/admin/webupload/Uploader.swf',// swf文件路径
        server: "<?php echo url('Upload/uploadface'); ?>",// 文件接收服务端。
        duplicate :true,// 重复上传图片，true为可重复false为不可重复
        pick: '#imgPicker1',// 选择文件的按钮。可选。

        accept: {
            title: 'Images',
            extensions: 'gif,jpg,jpeg,bmp,png',
            mimeTypes: 'image/jpg,image/jpeg,image/png'
        },

        'onUploadSuccess': function(file, data, response) {
            $("#data_photo1").val(data._raw);
            $("#img_data1").attr('src', '/uploads/face/' + data._raw).show();
        }
    });

    uploader1.on( 'fileQueued', function( file ) {
//        $list.html( '<div id="' + file.id + '" class="item">' +
//                '<h4 class="info">' + file.name + '</h4>' +
//                '<p class="state">正在上传...</p>' +
//                '</div>' );
    });

    // 文件上传成功
    uploader1.on( 'uploadSuccess', function( file ) {
//        $( '#'+file.id ).find('p.state').text('上传成功！');
    });

    // 文件上传失败，显示上传出错。
    uploader1.on( 'uploadError', function( file ) {
        $( '#'+file.id ).find('p.state').text('上传出错!');
    });



    //提交
    $(function(){
        $('#add').ajaxForm({
            beforeSubmit: checkForm,
            success: complete,
            dataType: 'json'
        });

        function checkForm(){
//            if( '' == $.trim($('#username').val())){
//                layer.msg('请输入用户名',{icon:2,time:1500,shade: 0.1}, function(index){
//                    layer.close(index);
//                });
//                return false;
//            }
//
//            if( '' == $.trim($('#groupid').val())){
//                layer.msg('请选择用户角色',{icon:2,time:1500,shade: 0.1}, function(index){
//                    layer.close(index);
//                });
//                return false;
//            }
//
//            if( '' == $.trim($('#password').val())){
//                layer.msg('请输入登录密码',{icon:2,time:1500,shade: 0.1}, function(index){
//                    layer.close(index);
//                });
//                return false;
//            }
//
//            if( '' == $.trim($('#real_name').val())){
//                layer.msg('请输入真实姓名',{icon:2,time:1500,shade: 0.1}, function(index){
//                    layer.close(index);
//                });
//                return false;
//            }
        }


        function complete(data){

            if(data.code==11){
                layer.msg(data.msg, {icon: 5,time:1500,shade: 0.1});
                return false;
            }else{
                if(data.code==1){
                    layer.msg(data.msg, {icon: 6,time:1500,shade: 0.1}, function(index){
                        window.location.href="<?php echo url('wechat/index'); ?>";
                    });
                }else{
                    layer.msg(data.msg, {icon: 5,time:1500,shade: 0.1});
                    return false;
                }
            }



        }

    });



    //IOS开关样式配置
    var elem = document.querySelector('.js-switch');
    var switchery = new Switchery(elem, {
        color: '#1AB394'
    });
    var config = {
        '.chosen-select': {},
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

</script>
</body>
</html>