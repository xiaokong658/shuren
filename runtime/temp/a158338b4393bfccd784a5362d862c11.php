<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:87:"/data/www/html/web2017/public_html/../application/admin/view/article/essay_article.html";i:1493947368;s:79:"/data/www/html/web2017/public_html/../application/admin/view/public/header.html";i:1484102488;s:79:"/data/www/html/web2017/public_html/../application/admin/view/public/footer.html";i:1487735376;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo config('WEB_SITE_TITLE'); ?></title>
    <link href="/static/admin/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/admin/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/static/admin/css/animate.min.css" rel="stylesheet">
    <link href="/static/admin/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="/static/admin/css/plugins/chosen/chosen.css" rel="stylesheet">
    <link href="/static/admin/css/plugins/switchery/switchery.css" rel="stylesheet">
    <link href="/static/admin/css/style.min.css?v=4.1.0" rel="stylesheet">
    <link href="/static/admin/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <style type="text/css">
    .long-tr th{
        text-align: center
    }
    .long-td td{
        text-align: center
    }
    </style>
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <!-- Panel Other -->
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5><?php echo $info['title']; ?>---投稿文章列表</h5>
        </div>
        <div class="ibox-content">
            <!--<div class="row">-->
                <!--&lt;!&ndash;<div class="col-sm-12">&ndash;&gt;-->
                    <!--&lt;!&ndash;<div  class="col-sm-2" style="width: 100px">&ndash;&gt;-->
                        <!--&lt;!&ndash;<div class="input-group" >&ndash;&gt;-->
                            <!--&lt;!&ndash;<a href="<?php echo url('essay_article_add',array('essay_id'=>$essay_id)); ?>"><button class="btn btn-outline btn-primary" type="button">添加投稿</button></a>&ndash;&gt;-->
                        <!--&lt;!&ndash;</div>&ndash;&gt;-->
                    <!--&lt;!&ndash;</div>&ndash;&gt;-->
                    <!--&lt;!&ndash;&lt;!&ndash;<form name="admin_list_sea" class="form-search" method="post" action="<?php echo url('essay_article'); ?>">&ndash;&gt;&ndash;&gt;-->
                        <!--&lt;!&ndash;&lt;!&ndash;<div class="col-sm-3">&ndash;&gt;&ndash;&gt;-->
                            <!--&lt;!&ndash;&lt;!&ndash;<div class="input-group">&ndash;&gt;&ndash;&gt;-->
                                <!--&lt;!&ndash;&lt;!&ndash;<input type="text" id="key" class="form-control" name="key" value="<?php echo $val; ?>" placeholder="输入需查询的用户名" />&ndash;&gt;&ndash;&gt;-->
                                <!--&lt;!&ndash;&lt;!&ndash;<span class="input-group-btn">&ndash;&gt;&ndash;&gt;-->
                                    <!--&lt;!&ndash;&lt;!&ndash;<button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> 搜索</button>&ndash;&gt;&ndash;&gt;-->
                                <!--&lt;!&ndash;&lt;!&ndash;</span>&ndash;&gt;&ndash;&gt;-->
                            <!--&lt;!&ndash;&lt;!&ndash;</div>&ndash;&gt;&ndash;&gt;-->
                        <!--&lt;!&ndash;&lt;!&ndash;</div>&ndash;&gt;&ndash;&gt;-->
                    <!--&lt;!&ndash;&lt;!&ndash;</form>&ndash;&gt;&ndash;&gt;-->
                <!--&lt;!&ndash;</div>&ndash;&gt;-->
            <!--</div>-->
            <!--搜索框结束-->
            <div class="hr-line-dashed"></div>

            <div class="example-wrap">
                <div class="example">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr class="long-tr">
                            <th>ID</th>
                            <th>标题</th>
                            <th>投稿人</th>
                            <th>投稿时间</th>
                            <th>文章状态</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <script id="list-template" type="text/html">
                            {{# for(var i=0;i<d.length;i++){  }}

                            <tr class="long-td">
                                <td>{{d[i].id}}</td>
                                <td>{{d[i].title}}</td>
                                <td>{{d[i].nickname}}</td>
                                <td>{{d[i].add_time}}</td>
                                <td>{{d[i].status}}</td>
                                <td>
                                    <a href="javascript:;" onclick="eassy_articleRead({{d[i].id}})" class="btn btn-primary btn-xs">
                                        <i class="fa fa-paste"></i> 查看文章</a>&nbsp;&nbsp;

                                    {{# if(d[i].status1==0){ }}

                                    <a href="javascript:;" onclick="essay_status({{d[i].id}},2)" class="btn btn-primary btn-xs">
                                        <i class="fa fa-paste"></i> 拟采用</a>&nbsp;&nbsp;
                                    <a href="javascript:;" onclick="essay_status({{d[i].id}},3)" class="btn btn-danger btn-xs">
                                        <i class="fa fa-paste"></i> 退稿</a>&nbsp;&nbsp;

                                    {{# } }}

                                    {{# if(d[i].status1==1){ }}
                                    <a href="javascript:;" onclick="eassy_articleEdit({{d[i].id}})" class="btn btn-primary btn-xs">
                                        <i class="fa fa-paste"></i> 编辑</a>&nbsp;&nbsp;
                                    {{# } }}

                                    {{# if(d[i].status1==2){ }}
                                    <a href="javascript:;" onclick="eassy_articleEdit({{d[i].id}})" class="btn btn-primary btn-xs">
                                        <i class="fa fa-paste"></i> 编辑</a>&nbsp;&nbsp;
                                    <a href="javascript:;" onclick="essay_status({{d[i].id}},1)" class="btn btn-primary btn-xs">
                                        <i class="fa fa-paste"></i> 采用</a>&nbsp;&nbsp;
                                    <a href="javascript:;" onclick="essay_status({{d[i].id}},3)" class="btn btn-danger btn-xs">
                                        <i class="fa fa-paste"></i> 退稿</a>&nbsp;&nbsp;
                                    {{# } }}




                                    <!--<a href="javascript:;" onclick="eassy_articleDel({{d[i].id}})" class="btn btn-danger btn-xs">-->
                                        <!--<i class="fa fa-trash-o"></i> 删除</a>-->
                                </td>
                            </tr>
                            {{# } }}
                        </script>
                        <tbody id="list-content"></tbody>
                    </table>
                    <div id="AjaxPage" style=" text-align: right;"></div>
                    <div id="allpage" style=" text-align: right;"></div>
                </div>
            </div>
            <!-- End Example Pagination -->
        </div>
    </div>
</div>
<!-- End Panel Other -->
</div>

<!-- 加载动画 -->
<div class="spiner-example">
    <div class="sk-spinner sk-spinner-three-bounce">
        <div class="sk-bounce1"></div>
        <div class="sk-bounce2"></div>
        <div class="sk-bounce3"></div>
    </div>
</div>

<script src="__JS__/jquery.min.js?v=2.1.4"></script>
<script src="__JS__/bootstrap.min.js?v=3.3.6"></script>
<script src="__JS__/content.min.js?v=1.0.0"></script>
<script src="__JS__/plugins/chosen/chosen.jquery.js"></script>
<script src="__JS__/plugins/iCheck/icheck.min.js"></script>
<script src="__JS__/plugins/layer/laydate/laydate.js"></script>
<script src="__JS__/plugins/switchery/switchery.js"></script><!--IOS开关样式-->
<script src="__JS__/jquery.form.js"></script>
<script src="__JS__/layer/layer.js"></script>
<script src="__JS__/laypage/laypage.js"></script>
<script src="__JS__/laytpl/laytpl.js"></script>
<script src="__JS__/lunhui.js"></script>
<script>
    $(document).ready(function(){$(".i-checks").iCheck({checkboxClass:"icheckbox_square-green",radioClass:"iradio_square-green",})});
</script>

<script type="text/javascript">

    //laypage分页
    Ajaxpage();
    function Ajaxpage(curr){
        var key=$('#key').val();
        var id= <?php echo $essay_id; ?>;
        $.getJSON('<?php echo url("Article/essay_article"); ?>', {page: curr || 1,key:key,id:id}, function(data){
            $(".spiner-example").css('display','none'); //数据加载完关闭动画
            if(data==''){
                $("#list-content").html('<td colspan="20" style="padding-top:10px;padding-bottom:10px;font-size:16px;text-align:center">暂无数据</td>');
            }else{
                var tpl = document.getElementById('list-template').innerHTML;
                laytpl(tpl).render(data, function(html){
                    document.getElementById('list-content').innerHTML = html;
                });
                laypage({
                    cont: $('#AjaxPage'),//容器。值支持id名、原生dom对象，jquery对象,
                    pages:'<?php echo $allpage; ?>',//总页数
                    skip: true,//是否开启跳页
                    skin: '#1AB5B7',//分页组件颜色
                    curr: curr || 1,
                    groups: 3,//连续显示分页数
                    jump: function(obj, first){
                        if(!first){
                            Ajaxpage(obj.curr)
                        }
                        $('#allpage').html('第'+ obj.curr +'页，共'+ obj.pages +'页');
                    }
                });
            }
        });
    }

    //编辑用户
    function eassy_articleRead(id){
        location.href = '__ROOT__/admin/article/eassy_articleRead/id/'+id+'.html';
    }

    function eassy_articleEdit(id){
        location.href = '__ROOT__/admin/article/eassy_articleEdit/id/'+id+'.html';
    }

    //删除用户
    function eassy_articleDel(id){
        lunhui.confirm(id,'<?php echo url("eassy_articleDel"); ?>');
    }


    function essay_status(id,status){
        lunhui.confirm_essay(id,status,'<?php echo url("essay_status"); ?>');
    }



</script>
</body>
</html>