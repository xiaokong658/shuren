<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:75:"/data/www/html/web2017/public_html/../application/web/view/index/index.html";i:1504258148;s:78:"/data/www/html/web2017/public_html/../application/web/view/public/header4.html";i:1502875458;}*/ ?>


<!DOCTYPE html>

<html lang="zh-CN">

<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">

    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1">

    <meta name="description" content="">

    <title>树人教育平台</title>

    <link rel="icon" href="favicon.ico">

    <link href="/static/admin/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">

    <link rel="stylesheet" href="/static/pc/swiper.3.2.0.min.css">

    <!-- 最新版本的 Bootstrap 核心 CSS 文件 -->



    <link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <script src="/static/admin/js/jquery.min.js?v=2.1.4"> </script>

    <script src="/static/admin/js/jquery.form.js"></script>

    <!--<script src="/static/admin/js/plugins/layer/laydate/laydate.js"></script>-->

    <!--<script src="/static/pc/js/layer/layer.js"></script>-->

    <script src="/static/admin/js/laypage/laypage.js"></script>

    <script src="/static/admin/js/laytpl/laytpl.js"></script>

    <link href="/static/pc/style.css" rel="stylesheet">

    <style>

        .swiper-slide a img {width: 100%;}

    </style>

</head>



<body>

<header id="main-header">
    <div class="area">
        <div class="head-nav left">
            <ul>
                <li class="index"><a href="<?php echo url('index/index'); ?>"><img src="/static/pc/image/logo.png" STYLE="height: 40px;padding-right: 20px;"></a></li>
                <li><a href="<?php echo url('index/index'); ?>">树人网</a></li>             
                <li><a href="<?php echo url('article/essay'); ?>" target="_blank">征文活动</a></li>
                <li><a href="http://club.shuren100.com/"  target="_blank">树人论坛</a></li>
				<li><a href="http://baozhi.shuren100.com/" target="_blank">电子报纸</a></li>
				<li><a href="http://qikan.shuren100.com/" target="_blank">电子期刊</a></li>
				<li><a href="http://www.shuren100.com//indexold.html" target="_blank">返回老版</a></li>

            </ul>
        </div>
        <div id="head-login" class="right login">
            <div class="login">
                <?php if($is_login==1): ?>
                <a href="<?php echo url('article/user_index'); ?>" data-role="login-btn" class="login-sohu"><img src="/static/pc/image/user.png" ><?php echo $my_user_info['nickname']; ?></a>
                <a href="<?php echo url('user/center'); ?>" data-role="login-btn" class="login-sohu"><img src="/static/pc/image/home.png" >我的主页</a>
                <a href="<?php echo url('index/loginout'); ?>" data-role="login-btn" class="login-sohu">退出</a>
                <?php else: ?>
                <a href="<?php echo url('user/login'); ?>"  data-role="login-btn" class="login-sohu"><img src="/static/pc/image/user.png" >用户登录</a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</header>

<div class="area clear" id="main-container">

    <div id="left-nav" class="column" style="visibility: visible;">

            <?php echo $left_nav; ?>

    </div>

    <div class="main left">

            <div id="search" class="search">

                    <select id="lei" name="lei" class="search-input left" style="width: 70px;">

                        <option value="article">文章</option>

                        <option value="user">用户</option>

                    </select>

                    <input type="text" id="key" class="search-input left"  placeholder="搜一搜" name="key"  value="<?php echo $keys; ?>" style="width:549px;">

                    <!--<a href=""  class="search-btn" id="search_info"><i class="fa fa-search"></i></a>-->

                    <button  type="submit"  class="search-btn" id="search_info"><i class="fa fa-search"></i></button>

                <!--<form target="" style="display:none;width:0;height:0"></form>-->

            </div>

            <div>

                <div class="jumbotron_swiper">

                <div class="swiper-container">

                    <div class="swiper-wrapper">

                        <?php if(is_array($ad) || $ad instanceof \think\Collection || $ad instanceof \think\Paginator): if( count($ad)==0 ) : echo "" ;else: foreach($ad as $key=>$ad): ?>

                        <div class="swiper-slide"><a href="<?php echo $ad['link_url']; ?>" target="_blank"><img src="/uploads/images/<?php echo $ad['images']; ?>"></a></div>

                        <?php endforeach; endif; else: echo "" ;endif; ?>

                    </div>

                    <div class="swiper-pagination"></div>

                </div>

                <div class="news-wrapper">

                    <script id="list-template" type="text/html">

                        {{# for(var i=0;i<d.length;i++){  }}

                                <div data-role="news-item" class="news-box clear   news-box-aa " data-media-id="{{d[i].id}}" data-loc="">



                                    {{# if(d[i].photo!=''){ }}

                                    <div class="pic img-do left">

                                        <a href="/web/article/index/id/{{d[i].id}}.html" target="_blank">

                                            <img src="/uploads/images/{{d[i].photo}}"  alt="">

                                        </a>

                                    </div>

                                    {{# } }}

                                    <h4><a href="/web/article/index/id/{{d[i].id}}.html" target="_blank" style="font-weight: 700;">

                                        {{d[i].title}}

                                    </a></h4>

                                    <div class="other">

                                        <span class="img"><a href="/web/user/center/user_id/{{d[i].user_id}}.html" target="_blank"><img src="/uploads/face/{{d[i].head_img}}" onerror="this.src='/static/admin/images/head_default.gif'" alt=""></a></span>



                                        <span class="name"><a href="/web/user/center/user_id/{{d[i].user_id}}.html" target="_blank">{{d[i].nickname}}</a></span><span class="dot">·</span>

                                        <span class="time"  data-role="time"> {{d[i].addtime}}</span>

                                        <a class="com" href="/web/article/index/id/{{d[i].id}}.html" target="_blank"><i class="fa fa-comment-o"></i><span data-id="{{d[i].id}}" data-role="">{{d[i].comment_count}}</span></a>

                                    </div>

                                </div>

                        {{# } }}

                    </script>

                    <div id="list-content"></div>

                </div>

                    </div>









            <!-- 如果需要分页器 -->



        </div>



    </div>





    <div class="right sidebar">



        <?php if($right_ad1['images']!=''): ?>

        <div class="ad_right" >

            <a href=""  target=""> <img src="/uploads/images/<?php echo $right_ad1['images']; ?>"></a>

        </div>

        <?php endif; ?>



        <div class="bord clear recommend" id="recommend-writer">

            <div class="titleR"><span class="tt">树人推荐</span>

            </div>

            <?php if(is_array($new_instit) || $new_instit instanceof \think\Collection || $new_instit instanceof \think\Paginator): if( count($new_instit)==0 ) : echo "" ;else: foreach($new_instit as $key=>$vt): ?>

            <div class="pic-txt clear">

                <div class="pic img-do"><a href="<?php echo url('user/center',array('user_id'=>$vt['id'])); ?>" target="_blank"><img src="/uploads/face/<?php echo $vt['head_img']; ?>" alt=""></a></div>

                <h4><a href="<?php echo url('user/center',array('user_id'=>$vt['id'])); ?>" target="_blank"><?php echo $vt['nickname']; ?></a></h4>

                <p>

                    <?php echo $vt['desc']; ?>.

                </p>

            </div>

            <?php endforeach; endif; else: echo "" ;endif; ?>

        </div>

        <?php if($right_ad2['images']!=''): ?>

        <div class="ad_right" style="margin-top:10px;">

            <a href=""  target=""> <img src="/uploads/images/<?php echo $right_ad2['images']; ?>"></a>

        </div>

        <?php endif; ?>



        <div class="hot-article clear bord" id="hot-news">

            <div class="titleR">

            <span class="tt">24小时热点</span>

            </div>



            <?php if(is_array($hot_article) || $hot_article instanceof \think\Collection || $hot_article instanceof \think\Paginator): if( count($hot_article)==0 ) : echo "" ;else: foreach($hot_article as $key=>$vv): ?>

                    <div class="pic-txt clear ">

                        <div class="pic img-do">

                            <a target="_blank" href="/web/article/index/id/<?php echo $vv['id']; ?>.html">





                                <img alt="" src="/uploads/images/<?php echo $vv['photo']; ?>">



                                <span class="sn"><?php echo $vv['sort']; ?></span>



                            </a>

                        </div>

                        <p><a target="_blank" href="/web/article/index/id/<?php echo $vv['id']; ?>.html"><?php echo $vv['title']; ?></a>

                        </p>

                    </div>

            <?php endforeach; endif; else: echo "" ;endif; ?>







        </div>





        <div class="cooperation clear bord">

            <div class="titleR"><span class="tt">地市教育局导航</span></div>

            <div class="cooper">

                <p>

                    <a target="_blank" href="http://www.zzjy.gov.cn/">郑州市</a> 丨


					<a target="_blank" href="http://www.xcsjyw.com/">许昌市</a> 丨
					<a target="_blank" href="http://www.xinyangedu.gov.cn/">信阳市</a> 丨
					<a target="_blank" href="http://www.nyedu.net/">南阳市</a> 丨
					<a target="_blank" href="http://www.xxjy.gov.cn/">新乡市</a> 丨
					<a target="_blank" href="http://www.pxx.cn/">濮阳市</a> 丨
					<a target="_blank" href="http://www.zkedu.gov.cn/">周口市</a> 丨
					<a target="_blank" href="http://www.anyangedu.com/">安阳市</a> 丨
                    <a target="_blank" href="http://www.kfedu.com.cn/">开封市</a> 丨

					
<a target="_blank" href="http://www.smxjy.cn/">三门峡市</a> 丨
                    <a target="_blank" href="http://www.lysdw.com/">洛阳市</a> 丨

					<a target="_blank" href="http://www.jzedu.cn/">焦作市</a> 丨
					<a target="_blank" href="http://www.lhjy.net/">漯河市</a> 丨
					<a target="_blank" href="http://www.jyjy.gov.cn/">济源市</a> 丨
					<a target="_blank" href="http://www.hnhbedu.net/">鹤壁市</a> 丨
					<a target="_blank" href="http://www.sqedu.gov.cn/index.action">商丘市</a> 丨
					<a target="_blank" href="http://www.zmdedu.net/index.html">驻马店市</a> 丨
					<a target="_blank" href="http://www.pdsedu.gov.cn/">平顶山市</a>
                </p>

            </div>

        </div>
		
		
		 <!-- 如果需要分页器 





        <div class="cooperation clear bord">

            <div class="titleR"><span class="tt">更多</span></div>

            <div class="cooper">

                <p>

                    <a target="_blank" href="">媒体合作&nbsp;&nbsp;&nbsp;</a> 丨



                    <a target="_blank" href="">广告合作&nbsp;&nbsp;&nbsp;</a> 丨



                    <a target="_blank" href="">产品合作&nbsp;&nbsp;&nbsp;</a> 丨



                    <a target="_blank" href="">用户协议&nbsp;&nbsp;&nbsp;</a> 丨



                    <a target="_blank" href="">广告投放&nbsp;&nbsp;&nbsp;</a> 丨



                    <a target="_blank" href="">侵权投诉&nbsp;&nbsp;&nbsp;</a> 丨



                </p>

            </div>

        </div>
		
		 -->





        <div class="cooperation clear bord">

            <div class="titleR"><span class="tt">@关于河南教育报刊社</span></div>

            <div class="cooper">

                <p class="cinfo"> <span>地址：</span>河南省郑州市金水区顺河路11号</p>
				<p class="cinfo"> <span>邮编：</span>450004</p>

                <p class="cinfo"> <span>办公室联系电话：</span>0371-66320712</p>

                <p class="cinfo"> <span>报刊订阅联系电话：</span>400-659-7019</p>

				 <!--<p class="cinfo"> <span>报刊订阅热线</span>800-883-6878</p>-->
                <p class="cinfo"> <span>树人网联系电话：</span>0371-66383059</p>
				<p class="cinfo"> <span>树人网顾问律师：</span></p>
				<p class="cinfo">魏涛 13837133211 杨玉果 13503719039</p>

                <p class="cinfo"> <span> Copyright @ 2003-2017 Shuren100.com </span> </p>
				<p class="cinfo"> <span>电信与信息服务业务经营许可证编号：</span> </p>
				<p class="cinfo"> <span>豫ICP备05022387号</span>  </p>
				

            </div>

        </div>

		<script src='http://s121.cnzz.com/stat.php?id=680266&web_id=680266&show=pic1' language='JavaScript' ></script>
		<script src="http://s16.cnzz.com/stat.php?id=2779275&web_id=2779275&show=pic" language="JavaScript"></script>
		



        <div class="cooperation clear bord">

            <div class="titleR"><span class="tt">友情链接</span></div>

            <div class="cooper">

                <p>

                    <a target="_blank" href="http://www.moe.edu.cn/">教育部</a>丨

					<a target="_blank" href="http://www.haedu.gov.cn/">河南省教育厅</a>丨
					<a target="_blank" href="http://www.haedu.cn/">河南省教育网</a>丨
					<a target="_blank" href="http://learning.sohu.com/">搜狐教育</a>丨
					<a target="_blank" href="http://www.jyb.cn/">中国教育新闻网</a>丨
					<a target="_blank" href="http://www.chinaxwcb.com/">中国新闻出版网</a>丨
					<a target="_blank" href="http://henan.youth.cn/">中国青年网河南频道</a>丨
					<a target="_blank" href="http://www.cac.gov.cn/">中国网信办</a>丨
					<a target="_blank" href="http://www.12377.cn/">中国互联网违法和不良信息举报中心</a>丨
                    <a target="_blank" href="http://www.henanjubao.com/main">河南省互联网违法和不良信息举报中心</a>丨

                </p>

            </div>

        </div>





    </div>



</div>

<script>

    $('.column').height($(window).height());



    var wid = $(window).width();

    var widths = (parseInt(wid) - 1200 )/2

    console.log(widths);



    var ccc =1;

    Ajaxpage();

    $(window).scroll(

            function() {



                $('.column').css('position','fixed');

                $('.column').css('left',widths);





                var scrollTop = $(this).scrollTop();

                var scrollHeight = $(document).height();

                var windowHeight = $(this).height();

                if (scrollTop + windowHeight == scrollHeight) {

                    Ajaxpage();

                }

            });



    function Ajaxpage(){

        key = '<?php echo $keys; ?>';

        $.post('<?php echo url("index/index"); ?>', {page:ccc,key:key}, function(data){

            //  $(".spiner-example").css('display','none'); //数据加载完关闭动画

            console.log(data);

            if(data.code==1){

                var tpl = document.getElementById('list-template').innerHTML;

                laytpl(tpl).render(data.list, function(html){

                    // document.getElementById('list-content').innerHTML = html;

                    $('#list-content').append(html);

//                    $('.weui-grid__icon').css('height',$('.weui-grid__icon').css('width'));

                    ccc++;

                });

                // $("#list-content").html('<td colspan="20" style="padding-top:10px;padding-bottom:10px;font-size:16px;text-align:center">暂无数据</td>');

            }else{





            }

        });

    }





    $('#search_info').click(function(){

                var  key =  $('#key').val();

                var  lei =  $('#lei').val();



                if(lei=='article'){

                    if(key==''){

                        location.href = '/web/index/index.html';

                    }else{



                        location.href = '/web/article/article/keyword/'+key+'.html';



                    }

                }



                if(lei=='user'){

                    if(key==''){

                        location.href = '/web/index/member.html';

                    }else{



                        location.href = '/web/index/member/key/'+key+'.html';



                    }

                }









            }

    );





</script>



<script src="/static/pc/swiper.3.2.0.min.js"></script>

<script>

    var mySwiper = new Swiper ('.swiper-container', {

        direction: 'horizontal',

        loop: true,

        autoplay : 3000,

        // 如果需要分页器

        pagination: '.swiper-pagination',

    })

    var myHot = new Swiper ('.hot-container', {

        direction: 'horizontal',

        loop: true,

        autoplay : 6000,

        // 如果需要分页器

        pagination: '.hot-pagination',

    })

    var mymiddleAdd = new Swiper ('.middle-container', {

        direction: 'horizontal',

        loop: true,

        autoplay : 3000,

        // 如果需要分页器

        pagination: '.middle-pagination',

    })

    var mymiddleAdd = new Swiper ('.middle2-container', {

        direction: 'horizontal',

        loop: true,

        autoplay : 3000,

        // 如果需要分页器

        pagination: '.middle-pagination',

    })

</script>



</body>

</html>

