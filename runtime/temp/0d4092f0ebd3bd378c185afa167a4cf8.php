<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:74:"/data/www/html/web2017/public_html/../application/admin/view/vote/add.html";i:1498742970;s:79:"/data/www/html/web2017/public_html/../application/admin/view/public/header.html";i:1484102488;s:79:"/data/www/html/web2017/public_html/../application/admin/view/public/footer.html";i:1487735376;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo config('WEB_SITE_TITLE'); ?></title>
    <link href="/static/admin/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/admin/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/static/admin/css/animate.min.css" rel="stylesheet">
    <link href="/static/admin/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="/static/admin/css/plugins/chosen/chosen.css" rel="stylesheet">
    <link href="/static/admin/css/plugins/switchery/switchery.css" rel="stylesheet">
    <link href="/static/admin/css/style.min.css?v=4.1.0" rel="stylesheet">
    <link href="/static/admin/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <style type="text/css">
    .long-tr th{
        text-align: center
    }
    .long-td td{
        text-align: center
    }
    </style>
</head>
<link rel="stylesheet" type="text/css" href="/static/admin/webupload/webuploader.css">
<link rel="stylesheet" type="text/css" href="/static/admin/webupload/style.css">
<style>
    .file-item{float: left; position: relative; width: 110px;height: 110px; margin: 0 20px 20px 0; padding: 4px;}
    .file-item .info{overflow: hidden;}
    .uploader-list{width: 100%; overflow: hidden;}
</style>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>新增投票</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="form_basic.html#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" name="userAdd" id="userAdd" method="post" action="<?php echo url('vote/add'); ?>">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">标题：</label>
                            <div class="input-group col-sm-4">
                                <input id="title" type="text" class="form-control" name="title" >
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">开始时间：</label>
                            <div class="input-group col-sm-4">
                                <input type="text" name="starttime" onclick="laydate()" id="starttime" class="form-control layer-date" placeholder="开始时间"/>
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">截至时间：</label>
                            <div class="input-group col-sm-4">
                                <input type="text" name="endtime" onclick="laydate()" id="endtime" class="form-control layer-date" placeholder="截至时间"/>
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">投票周期：</label>
                            <div class="input-group col-sm-4">

                                <select name="cycle"  id="cycle" class="form-control layer-date" >
                                    <option value="1">每天一票</option>
                                    <option value="2">该投票项目仅能投一票</option>
                                </select>

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">每次能给几人投票：</label>
                            <div class="input-group col-sm-4">
                                <input id="max_num" type="text" class="form-control" name="max_num" >
                            </div>
                        </div>

                        <!--<div class="hr-line-dashed"></div>-->
                        <!--<div class="form-group">-->
                            <!--<label class="col-sm-3 control-label " for="myEditor">内容：</label>-->
                            <!--<div class="input-group col-sm-9">-->
                                <!--<script src="/static/admin/ueditor/ueditor.config.js" type="text/javascript"></script>-->
                                <!--<script src="/static/admin/ueditor/ueditor.all.js" type="text/javascript"></script>-->
                                <!--<textarea name="content" style="width:90%" id="myEditor"></textarea>-->
                                <!--<script type="text/javascript">-->
                                    <!--var editor = UE.getEditor('myEditor',{-->
                                        <!--//这里可以选择自己需要的工具按钮名称,此处仅选择如下五个-->
                                        <!--toolbars:[['undo', 'redo', '|',-->
                                            <!--'bold', 'italic', 'underline', 'formatmatch', 'forecolor','paragraph', 'fontfamily', 'fontsize', '|',-->
                                            <!--'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|', 'simpleupload', 'insertimage', 'insertvideo','attachment'-->
<!--//                                            'directionalityltr', 'directionalityrtl', 'indent', '|', 'touppercase', 'tolowercase', '|',  'link', 'unlink', 'anchor', '|',  'imagenone', 'imageleft', 'imageright', 'imagecenter', '|',-->
<!--//                                            'emotion', 'scrawl', 'music','map', 'gmap', 'insertframe', 'insertcode', 'webapp','template', 'background', '|','fullscreen',   'splittocols', 'charts', '|',-->
<!--//                                            'horizontal', 'date', 'time', 'spechars', 'snapscreen', 'wordimage', '|','print', 'preview', 'searchreplace', 'drafts', 'help',  'pagebreak', 'splittorows',-->
<!--//                                            'inserttable', 'deletetable', 'insertparagraphbeforetable', 'insertrow', 'deleterow', 'insertcol', 'deletecol', 'mergecells', 'mergeright', 'mergedown', 'splittocells',-->
                                        <!--]],-->
                                        <!--//      autoClearinitialContent:true,-->
                                        <!--wordCount:false,-->
                                        <!--elementPathEnabled:false,-->
                                    <!--});-->
                                <!--</script>-->
                            <!--</div>-->
                        <!--</div>-->




                        <!--<div class="hr-line-dashed"></div>-->
                        <!--<div class="form-group">-->
                            <!--<label class="col-sm-3 control-label">头像：</label>-->
                            <!--<div class="input-group col-sm-4">-->
                                <!--<input type="hidden" id="data_photo" name="portrait" >-->
                                <!--<div id="fileList" class="uploader-list" style="float:right"></div>-->
                                <!--<div id="imgPicker" style="float:left">选择头像</div>-->
                                <!--<img id="img_data" class="img-circle" height="80px" width="80px" style="float:left;margin-left: 50px;margin-top: -10px;" src="/static/admin/images/head_default.gif"/>-->
                            <!--</div>-->
                        <!--</div>-->



                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-3">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> 保存</button>&nbsp;&nbsp;&nbsp;
                                <a class="btn btn-danger" href="javascript:history.go(-1);"><i class="fa fa-close"></i> 返回</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<script src="__JS__/jquery.min.js?v=2.1.4"></script>
<script src="__JS__/bootstrap.min.js?v=3.3.6"></script>
<script src="__JS__/content.min.js?v=1.0.0"></script>
<script src="__JS__/plugins/chosen/chosen.jquery.js"></script>
<script src="__JS__/plugins/iCheck/icheck.min.js"></script>
<script src="__JS__/plugins/layer/laydate/laydate.js"></script>
<script src="__JS__/plugins/switchery/switchery.js"></script><!--IOS开关样式-->
<script src="__JS__/jquery.form.js"></script>
<script src="__JS__/layer/layer.js"></script>
<script src="__JS__/laypage/laypage.js"></script>
<script src="__JS__/laytpl/laytpl.js"></script>
<script src="__JS__/lunhui.js"></script>
<script>
    $(document).ready(function(){$(".i-checks").iCheck({checkboxClass:"icheckbox_square-green",radioClass:"iradio_square-green",})});
</script>
<script type="text/javascript" src="/static/admin/webupload/webuploader.min.js"></script>

<script type="text/javascript">
    var $list = $('#fileList');
    //上传图片,初始化WebUploader
    var uploader = WebUploader.create({

        auto: true,// 选完文件后，是否自动上传。
        swf: '/static/admin/webupload/Uploader.swf',// swf文件路径
        server: "<?php echo url('Upload/uploadface'); ?>",// 文件接收服务端。
        duplicate :true,// 重复上传图片，true为可重复false为不可重复
        pick: '#imgPicker',// 选择文件的按钮。可选。

        accept: {
            title: 'Images',
            extensions: 'gif,jpg,jpeg,bmp,png',
            mimeTypes: 'image/jpg,image/jpeg,image/png'
        },

        'onUploadSuccess': function(file, data, response) {
            $("#data_photo").val(data._raw);
            $("#img_data").attr('src', '/uploads/face/' + data._raw).show();
        }
    });

    uploader.on( 'fileQueued', function( file ) {
        $list.html( '<div id="' + file.id + '" class="item">' +
                '<h4 class="info">' + file.name + '</h4>' +
                '<p class="state">正在上传...</p>' +
                '</div>' );
    });

    // 文件上传成功
    uploader.on( 'uploadSuccess', function( file ) {
        $( '#'+file.id ).find('p.state').text('上传成功！');
    });

    // 文件上传失败，显示上传出错。
    uploader.on( 'uploadError', function( file ) {
        $( '#'+file.id ).find('p.state').text('上传出错!');
    });

    //提交
    $(function(){
        $('#userAdd').ajaxForm({
            beforeSubmit: checkForm,
            success: complete,
            dataType: 'json'
        });

        function checkForm(){
//            if( '' == $.trim($('#username').val())){
//                layer.msg('请输入用户名',{icon:2,time:1500,shade: 0.1}, function(index){
//                    layer.close(index);
//                });
//                return false;
//            }
//
//            if( '' == $.trim($('#groupid').val())){
//                layer.msg('请选择用户角色',{icon:2,time:1500,shade: 0.1}, function(index){
//                    layer.close(index);
//                });
//                return false;
//            }
//
//            if( '' == $.trim($('#password').val())){
//                layer.msg('请输入登录密码',{icon:2,time:1500,shade: 0.1}, function(index){
//                    layer.close(index);
//                });
//                return false;
//            }
//
//            if( '' == $.trim($('#real_name').val())){
//                layer.msg('请输入真实姓名',{icon:2,time:1500,shade: 0.1}, function(index){
//                    layer.close(index);
//                });
//                return false;
//            }
        }


        function complete(data){
            if(data.code==1){
                layer.msg(data.msg, {icon: 6,time:1500,shade: 0.1}, function(index){
                    window.location.href="<?php echo url('vote/index'); ?>";
                });
            }else{
                layer.msg(data.msg, {icon: 5,time:1500,shade: 0.1});
                return false;
            }
        }

    });



    //IOS开关样式配置
    var elem = document.querySelector('.js-switch');
    var switchery = new Switchery(elem, {
        color: '#1AB394'
    });
    var config = {
        '.chosen-select': {},
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

</script>
</body>
</html>