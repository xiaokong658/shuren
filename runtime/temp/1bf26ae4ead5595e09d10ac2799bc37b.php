<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:84:"/data/www/html/web2017/public_html/../application/web/view/article/essay_detail.html";i:1498577520;s:77:"/data/www/html/web2017/public_html/../application/web/view/public/header.html";i:1496220118;}*/ ?>
<html lang="zh-CN"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $info['title']; ?>-<?php echo $user_info['nickname']; ?>-树人号</title>

    <!-- Bootstrap core CSS -->
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="http://v3.bootcss.com/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/static/pc/cmgd1.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="http://v3.bootcss.com/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="http://v3.bootcss.com/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="/static/admin/js/jquery.min.js?v=2.1.4"></script>
    <script src="/static/admin/js/jquery.form.js"></script>
    <script src="/static/admin/js/plugins/layer/laydate/laydate.js"></script>
    <script src="/static/pc/js/layer/layer.js"></script>
    <script src="/static/admin/js/laypage/laypage.js"></script>
    <script src="/static/admin/js/laytpl/laytpl.js"></script>
    <link href="/static/admin/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/static/pc/style.css" rel="stylesheet">
</head>
<style>
    .fa-thumbs-up{color:#2fab87;}
    .fa-heart{color:#2fab87;}
    .fa-heart-o{color:#000;}
    .fa-thumbs-o-up{color:#000;}
    .c-head-wrapper{width:100px;height:63px; border-radius: 0px;}
    .c-item-comment {
        height:90px;padding-top: 13px;
    }
    .c-item-head{width:115px;}
    .cv1{color: #000; text-decoration: none;}
</style>
<body>


<nav class="navbar navbar-fixed-top navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <a  href="#"><img src="/static/pc/image/logo.png" style="height: 50px;padding-right: 20px;"></a>
        </div>
        <div id="navbar" class="collapse navbar-collapse" style="font-size:16px;">
            <ul class="nav navbar-nav">
                <li class="active"><a href="<?php echo url('index/index'); ?>">首页</a></li>
                <li><a href="<?php echo url('user/center'); ?>">个人主页</a></li>
                <!--<li><a href="#contact">Contact</a></li>-->
            </ul>
            <div class="nav navbar-nav navbar-right">
                <!--<form class="navbar-form navbar-left">-->
                <!--<div class="form-group">-->
                <!--<input type="text" class="form-control" placeholder="搜索内容">-->
                <!--</div>-->
                <!--<button type="submit" class="btn btn-default">确定</button>-->
                <!--</form>-->
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="<?php echo url('article/user_index'); ?>">用户中心</a></li>
                    <?php if($is_login==1): ?>
                    <li><a href="<?php echo url('user/quitlogin'); ?>">退出</a></li> <?php endif; ?>
                    <!--<li class="dropdown">-->
                    <!--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">方法 <span class="caret"></span></a>-->
                    <!--<ul class="dropdown-menu">-->
                    <!--<li><a href="#">订单</a></li>-->
                    <!--<li><a href="#">嗯嗯</a></li>-->
                    <!--<li><a href="#">尺寸</a></li>-->
                    <!--<li role="separator" class="divider"></li>-->
                    <!--<li><a href="#">嗷嗷</a></li>-->
                    <!--</ul>-->
                    <!--</li>-->
                </ul>
            </div>
        </div>
        <!-- /.nav-collapse -->
    </div>
    <!-- /.container -->
</nav>

<div class="container">
    <div class="blog-header">
        <ol class="breadcrumb">
            <li><a href="#">首页</a></li>
            <li><a href="#">征文</a></li>
            <li class="active">正文</li>
        </ol>

    </div>

    <div class="row">

        <div class="col-sm-3 ">
            <div class="column left">
                <div class="user-info" id="user-info">
                    <div class="user-pic">
                        <a href="<?php echo url('user/center',array('user_id'=>$user_info['id'])); ?>" ><img src="/uploads/face/<?php echo $user_info['head_img']; ?>" alt="" style="width: 100%;"></a>
                    </div>
                    <h4><a href="<?php echo url('user/center',array('user_id'=>$user_info['id'])); ?>" ><?php echo $user_info['nickname']; ?></a></h4>
                    <dl class="user-num">
                        <dd><span class="value" data-value="2729" data-role="info-article-num"><span class="num"><?php echo $article_count; ?></span></span><p>文章</p></dd>
                        <dd><span class="value" data-value="" data-role="info-read-num"><span class="num"><?php echo $fans_count; ?></span></span>粉丝</dd>
                    </dl>
                    <div class="user-more"><a href="<?php echo url('user/center',array('user_id'=>$user_info['id'])); ?>" >查看TA的文章&gt;</a></div>
                </div>
                <!--<div class="article-do" id="article-do" style="">-->
                <!--<div class="article-done">-->
                <!--<dl>-->
                <!--<dd class="comment-do"><a href="#comment_area"><span class="num" data-role="comment-count">0</span><em class="comment-icon icon"></em></a></dd>-->
                <!--</dl>-->
                <!--</div>-->
                <!--</div>-->

            </div>
        </div>
        <div class="col-sm-8 blog-main">
            <div class="blog-post">
                <h2 class="blog-post-title"><?php echo $info['title']; ?></h2>

                <p><?php echo $info['create_time1']; ?></p>
                <p>征文对象:<?php echo $info['object_people']; ?></p>
                <hr>
                <p><img src="/uploads/images/<?php echo $info['photo']; ?>" style="width: 100%;"></p>
                <?php echo $info['content']; ?>
            </div>

            <div style="text-align: center; font-size: 22px; font-weight: 0;"  >
                   <span style="padding: 13px 60px;border:1px #c2ccd1 solid;  margin-right: 20px; border-radius: 40px;">
                    <a class="cv1"  href="javascript:;"  onclick="dianzan(<?php echo $info['id']; ?>);"  >   <i id="zan" class="fa <?php if($is_zan == 1): ?>fa-thumbs-up<?php else: ?>fa-thumbs-o-up<?php endif; ?>"></i> </a>
                       <span id="zan1"><?php echo $zan; ?></span>
                       <b style="width:1px; padding:0 40px 0 40px; font-weight: 0;"><i style="border-left:1px #c2ccd1 solid;"></i></b>
                  <a class="cv1" href="javascript:;"  onclick="shoucang(<?php echo $info['id']; ?>);"  >  <i id="collect" class="fa <?php if($is_collect == 1): ?>fa-heart<?php else: ?>fa-heart-o<?php endif; ?>"></i> </a><span id="collect1"><?php echo $collect; ?></span>
                   </span>
            </div>


            <!--<div style="padding-top: 50px;">-->


                <!--<div class="c-comment-header clear">-->
                    <!--<div class="left c-opinion"><em class="c-line"></em>我来说两句</div>-->
                    <!--<div class="right"><span class="c-num-red">4</span>人参与，<span class="c-num-red">4</span>条评论</div>-->
                <!--</div>-->

                <!--<div style="margin-top: 18px;">-->
                    <!--<textarea rows="5" style="width:757px; border-bottom: 0px;" id="content_msg" ></textarea>-->
                <!--</div>-->
                <!--<div style="text-align: center; padding-left: 645px; border-bottom: 1px rgb(169, 169, 169) solid;border-left: 1px rgb(169, 169, 169) solid;border-right: 1px rgb(169, 169, 169) solid;">-->
                    <!--<a href="javascript:;" style="text-decoration: none;" onclick="send_pinglun(<?php echo $user_id; ?>,<?php echo $id; ?>)">  <p style="width: 110px;background: red; padding:5px 0 5px 0; margin: 0; font-size:14px; color: #fff; font-family: 'Microsoft Yahei'; "> 发表</p></a>-->
                <!--</div>-->
            <!--</div>-->

            <div style="padding-top: 50px;text-align: center;width: 100%;">
                <div id="join" style="background:#2fab87; width:200px;text-align: center;margin: 0 auto;padding: 5px 0; color:#fff;  cursor:pointer ;">我要参与</div>

                <div class="form-group" style="padding-top: 20px; height:60px;line-height:27px; display:none;" id="pts">
                    <label class="col-sm-4 control-label">选择你要投递的文章：</label>
                    <div class="input-group col-sm-6">
                        <select class="form-control m-b chosen-select" name="article_id" id="article_id" style="width: 85%;">
                            <option value="">==请选择==</option>
                            <?php if(is_array($myarticle) || $myarticle instanceof \think\Collection || $myarticle instanceof \think\Paginator): if( count($myarticle)==0 ) : echo "" ;else: foreach($myarticle as $key=>$vv): ?>
                            <option value="<?php echo $vv['id']; ?>"><?php echo $vv['title']; ?></option>
                            <?php endforeach; endif; else: echo "" ;endif; ?>
                        </select>
                        <a href="javascript:;"  id="showTooltips" style="background:#2fab87; color:#fff; padding: 5px 6px; text-decoration: none;">提交</a>


                        <input type="hidden" name="essay_id" id="essay_id" value="<?php echo $id; ?>">
                        <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>">
                    </div>


                </div>

            </div>


            <div class="c-comment-content"><div>


                <div class="c-comment-title c-new" style="margin-top: 40px;">
                    <span><em class="c-line"></em>征文参与记录</span>
                </div>
                <div class="c-comment-main">

                    <script id="list-template" type="text/html">
                        {{# for(var i=0;i<d.length;i++){  }}
                        <div class="c-item-comment clear">
                            <div class="c-item-head left">
                                <div class="c-head-wrapper"><img src="/uploads/images/{{d[i].photo}}" style="width: 100%;" alt=""></div>
                            </div>
                            <div class="c-item-content">
                                <div class="c-item-info clear">
                                 <a onclick="gourl({{d[i].article_id}})" style="text-decoration: none; cursor: pointer;  ">   <div class="c-username">
                                        {{d[i].title}}
                                    </div></a>
                                </div>
                                <div class="c-user-content">
                                    <div class="c-discuss" style="height:26px;overflow: hidden;">{{d[i].remark}}</div>
                                    <div class="c-controller" >
                                        <div class="c-date left" >{{d[i].add_time}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{# } }}
                    </script>

                    <div id="list-content"></div>
                </div>

            </div></div>

        </div>



    </div>
</div>

<script type="text/javascript">

    AjaxPage();
    function AjaxPage(){
        var article_id = '<?php echo $id; ?>';
            $.post('<?php echo url("article/essay_article_list"); ?>',
                    {id:article_id,page:-1},
                    function(data){
                    console.log(data);
                        if(data=='') {

                        }else{
                            var tpl = document.getElementById('list-template').innerHTML;
                            //   console.log(tpl);
                            laytpl(tpl).render(data.list, function(html){
                                // document.getElementById('list-content').innerHTML = html;
                                $('#list-content').append(html);
                            });

                        }
                    });



    }


    function dianzan(id){

        zan_count = $('#zan1').html();
        zan_count = parseInt(zan_count);
        $.post('<?php echo url("article/essay_zan_status"); ?>',
                {id:id},
                function(data){
                    if(data.code==11){
                        layer.msg(data.msg,{icon:5,time:1500,shade: 0.1,});
                        window.location.href="<?php echo url('user/login'); ?>";
                    }else {
                        if(data.code==1){
                            $('#zan').removeClass('fa-thumbs-o-up');
                            $('#zan').addClass('fa-thumbs-up');

                            zan_count = zan_count+1;
                            $('#zan1').html(zan_count);

//                            layer.msg(data.msg,{icon:1,time:1500,shade: 0.1,});
                        }else{
                            $('#zan').removeClass('fa-thumbs-up');
                            $('#zan').addClass('fa-thumbs-o-up');
                            zan_count = zan_count-1;
                            $('#zan1').html(zan_count);
//                            layer.msg(data.msg,{icon:5,time:1500,shade: 0.1,});



                        }
                    }

                });
    }
    function shoucang(id){
        collect_count = $('#collect1').html();
        collect_count = parseInt(collect_count);
        $.post('<?php echo url("article/essay_collect_status"); ?>',
                {id:id},
                function(data){
                    if(data.code==11){
                        layer.msg(data.msg,{icon:5,time:1500,shade: 0.1,});
                        window.location.href="<?php echo url('user/login'); ?>";
                    }else{
                        if(data.code==1){
                            $('#collect').removeClass('fa-heart-o');
                            $('#collect').addClass('fa-heart');
                            collect_count = collect_count+1;
                            $('#collect1').html(collect_count);

//                            layer.msg(data.msg, {icon: 5,time:1500,shade: 0.2}, function(index){
//                                layer.close(index);
//                            });


                        }else{

                            $('#collect').removeClass('fa-heart');
                            $('#collect').addClass('fa-heart-o');
                            collect_count = collect_count-1;
                            $('#collect1').html(collect_count);

//                            layer.msg(data.msg, {icon: 5,time:1500,shade: 0.2}, function(index){
//                                layer.close(index);
//                            });
                        }
                    }
                });
    }


//    function send_pinglun(user_id,article_id){
//        var content = document.getElementById('content_msg').value;
//        if($.trim(content)==''){
//            alert('评论内容不能为空');
//            return false;
//        }
//
//        console.log(article_id+'|'+user_id+'|'+content);
//        $.post('<?php echo url("index/index/article_comment"); ?>',
//                {id:article_id,user_id:user_id,content:content},
//                function(data){
//                    if(data.code==11){
//                        layer.msg(data.msg,{icon:5,time:1500,shade: 0.1,});
//                        window.location.href="<?php echo url('user/login'); ?>";
//                    }else {
//                        if (data.code == 1) {
//                            layer.msg(data.msg, {icon: 1, time: 1500, shade: 0.1,});
//                            // $html ="<div class='comm' ><div class='c_img'><img src='"+data.list.img+"' width='30px'></div><div class='c_left'><p class='p1' style=''>"+data.list.nickname+"</p><p class='p2' style=''>"+data.list.content+"</p><p class='p3' style=''>"+data.list.addtime+"</p></div></div>";
//
//                            $html = "<div class='c-item-comment clear'><div class='c-item-head left'><div class='c-head-wrapper'><img src='/uploads/face/" + data.list.img + "' alt=''></div> </div><div class='c-item-content'><div class='c-item-info clear'><div class='c-username'>" + data.list.nickname + "</div></div><div class='c-user-content'><div class='c-discuss'>" + data.list.content + "</div><div class='c-controller' ><div class='c-date left' >" + data.list.addtime + "</div></div></div></div></div>";
//
//                            $("#list-content").prepend($html);
//                            document.getElementById('content_msg').value='';
//                        } else {
//                            layer.msg(data.msg, {icon: 5, time: 1500, shade: 0.1,});
//                        }
//                    }
//                });
//    }


    function gourl(id){
        window.location.href="/web/article/index/id/"+id+".html";
    }



    $('#join').on('click',function(){
        $('#pts').show();
    });

    $('#showTooltips').on('click', function(){
        var article_id = $('#article_id').val();
        var user_id = $('#user_id').val();
        var essay_id = $('#essay_id').val();
        if(article_id==0){
           // layer.msg('请选择您要投稿的文章',{icon:5,time:1500,shade: 0.1,});
//            layer.msg('请选择您要投稿的文章', {icon: 5,time:1111500,shade: 0.1,offset: 't',}, function(index){

//tips层-右
                    //tips层-上
                    layer.tips('请选择您要投稿的文章', '#showTooltips', {
                        tips: [1, 'red'] //还可配置颜色
                    });

//                layer.close(index);
//            });
            return false;
        }

        $.post('<?php echo url("article/essay_join"); ?>',
                {essay_id:essay_id,user_id:user_id,article_id:article_id},
                function(data){
                    if(data.code==11){
//                        layer.msg(data.msg, {icon: 5,time:1500,shade: 0.1}, function(index){
//                            layer.close(index);
//                        });
                        window.location.href="<?php echo url('user/login'); ?>";
                    }else{
                        if(data.code==1) {

                            //$html =  "<span  class='weui-media-box weui-media-box_appmsg'><div class='weui-media-box__hd'> <img class='weui-media-box__thumb' src='/uploads/images/"+data.list.photo+"' alt=''></div><div class='weui-media-box__bd'><h5 class='weui-media-box__desc'>"+data.list.title+"</h5><small class='weui-media-box__desc minTime'>"+data.list.add_time+"</small> </div></span>";


                            $html= "<div class='c-item-comment clear'><div class='c-item-head left'><div class='c-head-wrapper'><img src='/uploads/images/"+data.list.photo+"' style='width: 100%;' alt=''></div></div><div class='c-item-content'><div class='c-item-info clear'><a onclick='gourl("+data.list.id+")' style='text-decoration: none; cursor:pointer;'>   <div class='c-username'>"+data.list.title+"</div></a></div><div class='c-user-content'><div class='c-discuss' style='height:26px;overflow: hidden;'>"+data.list.remark+"</div><div class='c-controller' ><div class='c-date left' >"+data.list.add_time+"</div></div></div></div></div>";

                            $("#list-content").prepend($html);
                            $('#pts').hide();

//                            layer.msg(data.msg, {icon: 5,time:1500,shade: 0.1}, function(index){
//                                layer.close(index);
//                            });

                        }else{


//                            layer.msg(data.msg, {icon: 5,time:1500,shade: 0.1}, function(index){
//                                layer.close(index);
//                            });



                        }
                    }
                });

    });
</script>

</body></html>