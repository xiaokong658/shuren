<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:92:"/data/www/html/web2017/public_html/../application/index/view/usercenter/article_collect.html";i:1498228530;}*/ ?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>我的收藏</title>
    <!-- 最新版本的 Bootstrap 核心 CSS 文件 -->
    <!--<link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">-->
    <link rel="stylesheet" href="__STATIC__/style/weui.min.css">
    <link rel="stylesheet" href="__STATIC__/style/cmgd.css">
    <script src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
    <!--奥森图标-->
    <link rel="stylesheet" href="__STATIC__/style/FontAwesome_4.2.0/css/font-awesome.min.css">
    <style>
        .text{padding-left:9px; }
        .fa-list-ul{padding-right:3px;}
        .weui-media-box__desc img{width:22px;height:22px;}
        .weui-navbar{top:5px;}
        .weui-navbar+.weui-tab__panel{
            padding-top: 62px;
            padding-bottom: 60px;
        }
    </style>
</head>
<body>

<div class="container" id="container">

    <!--顶部搜索-->
    <div class="page home js_show">

        <div class="page__bd" style="height: 100%;">
            <!--选择导航-->
            <div class="weui-tab" id="navbar" >
                <div class="weui-navbar" style="top: 0px;">
                    <div class="weui-navbar__item weui-bar__item_on">普通文章</div>
                    <div class="weui-navbar__item">征文</div>
                    <div class="weui-navbar__item">话题</div>
                </div>

                <div class="weui-tab__panel">
                    <div id="wenzhang">
                        <div class="weui-panel articlelist">
                            <div class="page__bd" >
                                <div class="weui-panel weui-panel_access">
                                    <div class="weui-panel__hd" style="font-size:16px;color: #000;"><i class="fa fa-list-ul"></i>普通文章列表</div>
                                    <div class="weui-panel__bd">
                                        <?php if(is_array($article) || $article instanceof \think\Collection || $article instanceof \think\Paginator): if( count($article)==0 ) : echo "" ;else: foreach($article as $key=>$article): ?>
                                        <a href="<?php echo url('index/article_detail',array('id'=>$article['id'])); ?>" class="weui-media-box weui-media-box_appmsg">
                                            <?php if($article['photo'] !=''): ?>
                                            <div class="weui-media-box__hd">
                                                <img class="weui-media-box__thumb" src="/uploads/images/<?php echo $article['photo']; ?>" height="100%" alt="">
                                            </div>
                                            <?php endif; ?>
                                            <div class="weui-media-box__bd">
                                                <h4 class="weui-media-box__title"><?php echo $article['title']; ?></h4>
                                                <p class="weui-media-box__desc" ><?php echo $article['author']; ?> &nbsp;&nbsp;&nbsp;&nbsp; <?php echo $article['create_time']; ?></p>
                                            </div>
                                        </a>
                                        <?php endforeach; endif; else: echo "" ;endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div id="zhengwen">
                        <div class="weui-panel articlelist">
                            <div class="page__bd" >
                                <div class="weui-panel weui-panel_access">
                                    <div class="weui-panel__hd" style="font-size:16px;color: #000;"><i class="fa fa-list-ul"></i>征文列表</div>
                                    <div class="weui-panel__bd">
                                        <?php if(is_array($essay) || $essay instanceof \think\Collection || $essay instanceof \think\Paginator): if( count($essay)==0 ) : echo "" ;else: foreach($essay as $key=>$essay): ?>
                                        <a href="<?php echo url('index/essay_detail',array('id'=>$essay['id'])); ?>" class="weui-media-box weui-media-box_appmsg">
                                            <?php if($essay['photo'] !=''): ?>
                                            <div class="weui-media-box__hd">
                                                <img class="weui-media-box__thumb" src="/uploads/images/<?php echo $essay['photo']; ?>" alt="">
                                            </div>
                                            <?php endif; ?>
                                            <div class="weui-media-box__bd">
                                                <h4 class="weui-media-box__title"><?php echo $essay['title']; ?></h4>
                                                <p class="weui-media-box__desc"><?php echo $essay['author']; ?> &nbsp;&nbsp;&nbsp;&nbsp; <?php echo $essay['create_time']; ?></p>
                                            </div>
                                        </a>
                                        <?php endforeach; endif; else: echo "" ;endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="huati">
                        <div class="weui-panel articlelist">
                            <div class="page__bd" >
                                <div class="weui-panel weui-panel_access">
                                    <div class="weui-panel__hd" style="font-size:16px;color: #000;"><i class="fa fa-list-ul"></i>话题列表</div>
                                    <div class="weui-panel__bd">
                                        <?php if(is_array($debate) || $debate instanceof \think\Collection || $debate instanceof \think\Paginator): if( count($debate)==0 ) : echo "" ;else: foreach($debate as $key=>$debate): ?>
                                        <a href="<?php echo url('index/debate_detail',array('id'=>$debate['id'])); ?>" class="weui-media-box weui-media-box_appmsg">
                                            <?php if($debate['photo'] !=''): ?>
                                            <div class="weui-media-box__hd">
                                                <img class="weui-media-box__thumb" src="/uploads/images/<?php echo $debate['photo']; ?>" height="100%" alt="">
                                            </div>
                                            <?php endif; ?>
                                            <div class="weui-media-box__bd">
                                                <h4 class="weui-media-box__title"><?php echo $debate['title']; ?></h4>
                                                <p class="weui-media-box__desc" ><?php echo $debate['author']; ?> &nbsp;&nbsp;&nbsp;&nbsp; <?php echo $debate['create_time']; ?></p>
                                            </div>
                                        </a>
                                        <?php endforeach; endif; else: echo "" ;endif; ?>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
</body>

<script type="text/javascript">
    //        顶部搜索
    $("#zhengwen").hide();
    $("#huati").hide();

    //顶部导航
    $(function(){
        $('.weui-navbar__item').on('click', function () {
            $(this).addClass('weui-bar__item_on').siblings('.weui-bar__item_on').removeClass('weui-bar__item_on');

            var cc = $(this).text();
            if(cc=='普通文章'){
                $("#zhengwen").hide();
                $("#huati").hide();
                $("#wenzhang").show();
            }
            if(cc=='征文'){
                $("#wenzhang").hide();
                $("#huati").hide();
                $("#zhengwen").show();

            }
            if(cc=='话题'){
                $("#wenzhang").hide();
                $("#zhengwen").hide();
                $("#huati").show();

            }
        });
    });
    //底部导航
    $(function(){
        $('.weui-tabbar__item').on('click', function () {
            $(this).addClass('weui-bar__item_on').siblings('.weui-bar__item_on').removeClass('weui-bar__item_on');
        });
    });


</script>

</html>