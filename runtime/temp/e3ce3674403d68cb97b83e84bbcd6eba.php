<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:82:"/data/www/html/web2017/public_html/../application/index/view/usercenter/index.html";i:1501834262;s:72:"/data/www/html/web2017/public_html/../application/index/view/footer.html";i:1505293149;}*/ ?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>个人中心</title>
    <!-- 最新版本的 Bootstrap 核心 CSS 文件 -->
    <!--<link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">-->
    <link rel="stylesheet" href="__STATIC__/style/weui.min.css">
    <link rel="stylesheet" href="__STATIC__/style/cmgd.css">

    <script src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="/static/admin/js/layer/layer.js"></script>
    <!--奥森图标-->
    <link rel="stylesheet" href="__STATIC__/style/FontAwesome_4.2.0/css/font-awesome.min.css">
    <style>
        .text{padding-left:9px; }
        .placeholder{text-align: center;}
        .fl{text-align: left;}
        .black{color:#000; font-weight: 600;font-size:17px; line-height: 1.8;}
        .color1{color:#30ab88;}
        .color2{color:#1aafe7;}
        .color3{color:#ffc242;}
        .edit{font-size:13px;color:#999;padding-left: 20px;}
        .color4{color:#eb6877;}
        .weui-cell__bd p i{padding: 0 10px 0 2px; font-size: 20px;}
        .weui-cell__bd p{font-size:16px;}
    </style>
</head>
<body>


<!--顶部搜索-->
<div class="page home js_show">

        <!--选择导航-->
        <div class="page__bd ">
            <div class="weui-flex"  style="background: #fff;margin-top:10px;">
                <div >
                    <div class="placeholder">
                        <img src="/uploads/face/<?php echo $info['head_img']; ?>" width="65px" height="65px" style="border-radius: 33.5px; border: 0.5px #ccc solid;" >
                    </div>
                </div>
                <div class="weui-flex__item" style="padding-left: 10px;">
                    <div class="placeholder">
                        <p class="fl"><?php echo $info['nickname']; ?></p>
                        <p class="weui-media-box__desc fl" style="font-size:12px;"><?php echo $info['desc']; ?></p>

                    </div>
                </div>


                <div class="placeholder">
                    <p>

                        <a href="javascript:;" onclick="user_sign()" >

                        <span style="padding: 3px 5px;color: #fff;background: #1ee0a5;border-radius: 3px;font-size: 12px;" id="daysign">
                         <?php if($is_sign==1): ?>已签到<?php else: ?>签到 <?php endif; ?>
                        </span>
                        </a>
                    </p>

                    <!--<a href="<?php echo url('user_edit'); ?>">-->
                   <!--<p><span class="edit">修改<i class="fa fa-angle-right" style="font-size:16px;padding-left: 2px;"></i></span></p></a>-->
                </div>

            </div>

            <div class="weui-flex" style="background: #fff; padding: 8px; margin-top: 8px;">
                <div class="weui-flex__item">
                    <a href="<?php echo url('myarticle'); ?>">
                        <div class="placeholder">
                            <p class="weui-media-box__desc  black"><?php echo $info['total']; ?></p>
                            <p class="weui-media-box__desc">文章</p>
                        </div>
                    </a>
                </div>
                <div class="weui-flex__item">
                    <a href="<?php echo url('myfocus'); ?>">
                        <div class="placeholder">
                            <p class="weui-media-box__desc  black"><?php echo $info['foucs_num']; ?></p>
                            <p class="weui-media-box__desc">关注</p>
                        </div>
                    </a>
                </div>
                <div class="weui-flex__item">
                    <a href="<?php echo url('myfans'); ?>">
                        <div class="placeholder">
                            <p class="weui-media-box__desc  black"><?php echo $info['fans_num']; ?></p>
                            <p class="weui-media-box__desc">粉丝</p>
                        </div>
                    </a>
                </div>
                <div class="weui-flex__item">

                        <div class="placeholder">
                            <p class="weui-media-box__desc  black" id ='inte'><?php echo $info['integral']; ?></p>
                            <p class="weui-media-box__desc">积分</p>
                        </div>

                </div>
            </div>
            <div class="weui-cells" style="margin-top: 10px;">

                <a class="weui-cell weui-cell_access" href="<?php echo url('user/user_info',array('user_id'=>$info['id'])); ?>">
                    <div class="weui-cell__bd">
                        <p><i class="fa fa-home color4"></i>我的主页</p>
                    </div>
                    <div class="weui-cell__ft">
                    </div>
                </a>

                <a class="weui-cell weui-cell_access" href="<?php echo url('article/write'); ?>">
                    <div class="weui-cell__bd">
                        <p><i class="fa fa-pencil-square color1"></i>写文章</p>
                    </div>
                    <div class="weui-cell__ft">
                    </div>
                </a>

                <a class="weui-cell weui-cell_access" href="<?php echo url('article/debate_write'); ?>">
                    <div class="weui-cell__bd">
                        <p><i class="fa fa-comments color2"></i>论话题</p>
                    </div>
                    <div class="weui-cell__ft">
                    </div>
                </a>



                <?php if($info['group_id']==8): ?>

                <a class="weui-cell weui-cell_access" href="<?php echo url('article/essay_write'); ?>">
                                    <div class="weui-cell__bd">
                                        <p><i class="fa fa-file-text color3"></i>发征文</p>
                                    </div>
                                    <div class="weui-cell__ft">
                                    </div>
                                </a>
                <?php endif; ?>


                <a class="weui-cell weui-cell_access" href="<?php echo url('usercenter/mypartessay'); ?>">
                    <div class="weui-cell__bd">
                        <p><i class="fa fa-paper-plane color4"></i>已投征文</p>
                    </div>
                    <div class="weui-cell__ft">
                    </div>
                </a>

                <a class="weui-cell weui-cell_access" href="<?php echo url('usercenter/mydraft'); ?>">
                    <div class="weui-cell__bd">
                        <p><i class="fa fa-folder-open color1"></i>我的草稿</p>
                    </div>
                    <div class="weui-cell__ft">
                    </div>
                </a>

                <a class="weui-cell weui-cell_access" href="<?php echo url('usercenter/article_collect'); ?>">
                    <div class="weui-cell__bd">
                        <p><i class="fa fa-heart color2"></i>我的收藏</p>
                    </div>
                    <div class="weui-cell__ft">
                    </div>
                </a>

                <a class="weui-cell weui-cell_access" href="<?php echo url('usercenter/gongwen'); ?>">
                    <div class="weui-cell__bd">
                        <p><i class="fa  fa-file-audio-o color3"></i>我的公文</p>
                    </div>
                    <div class="weui-cell__ft">
                    </div>
                </a>
                <a class="weui-cell weui-cell_access" href="<?php echo url('usercenter/message_info'); ?>">
                    <div class="weui-cell__bd">
                        <p><i class="fa fa-at color4"></i>@我的人</p>
                    </div>
                    <div class="weui-cell__ft">
                    </div>
                </a>

                <a class="weui-cell weui-cell_access"href="<?php echo url('usercenter/inside_letter_list'); ?>">
                    <div class="weui-cell__bd">
                        <p><i class="fa fa-envelope color1"></i>站内信</p>
                    </div>
                    <div class="weui-cell__ft">
                    </div>
                </a>

                <a class="weui-cell weui-cell_access"href="<?php echo url('article/user_column'); ?>">
                    <div class="weui-cell__bd">
                        <p><i class="fa fa-bars color2"></i>栏目管理</p>
                    </div>
                    <div class="weui-cell__ft">
                    </div>
                </a>


                <a class="weui-cell weui-cell_access" href="<?php echo url('user_edit'); ?>">
                    <div class="weui-cell__bd">
                        <p><i class="fa fa-cog color3"></i>我的资料</p>
                    </div>
                    <div class="weui-cell__ft">
                    </div>
                </a>


                <a href="javascript:;" onclick="quitlogin()" class="weui-btn weui-btn_warn" style="width: 90%;margin-top: 20px;margin-bottom: 50px;">退出登录</a>
            </div>


        </div>

</div>
<div class="weui-tabbar footer" >
    <a href="<?php echo url('index/index'); ?>" class="weui-tabbar__item <?php if($footfrom ==1): ?>weui-bar__item_on <?php endif; ?>">
                        <span style="display: inline-block;position: relative;">
                            <i class="fa fa-home weui-tabbar__icon   "></i>
                        </span>
        <p class="weui-tabbar__label">首页</p>
    </a>
    <a href="<?php echo url('usercenter/dynamic'); ?>" class="weui-tabbar__item <?php if($footfrom ==2): ?>weui-bar__item_on <?php endif; ?>">
                        <span style="display: inline-block;position: relative;">
                            <i class="fa fa-comments weui-tabbar__icon " ></i>
                            <!--<span class="weui-badge weui-badge_dot" style="position: absolute;top: 0;right: -6px;"></span>-->
                        </span>
        <p class="weui-tabbar__label">我的圈子</p>
    </a>
    <a href="<?php echo url('usercenter/index'); ?>" class="weui-tabbar__item <?php if($footfrom==3): ?>weui-bar__item_on <?php endif; ?>">
                        <span style="display: inline-block;position: relative;">
                            <i class="fa fa-user weui-tabbar__icon"></i>
                            <!--<span class="weui-badge" style="position: absolute;top: -2px;right: -6px;">8</span>-->
                        </span>
        <p class="weui-tabbar__label">我的</p>

    </a>
</div>   

<script type="text/javascript">
    $(function(){
        $('.weui-tabbar__item').on('click', function () {
            $(this).addClass('weui-bar__item_on').siblings('.weui-bar__item_on').removeClass('weui-bar__item_on');
        });
    });
</script>
</body>
    <script type="text/javascript">
        function quitlogin(){
            $.post('<?php echo url("usercenter/quitlogin"); ?>',
                    {id:1},
                    function(data){
                        console.log(data);
                        if(data.code==1) {
                            layer.msg(data.msg,{icon:1,time:1000,shade: 0.1,});

                            window.location.href="<?php echo url('index/index'); ?>";
                        }else{
                            layer.msg(data.msg,{icon:5,time:1000,shade: 0.1,});
                        }

//
//
//                    $("#myform").html('');
//                    $('#emoji').hide();
                    });
        }

        function user_sign(){
            $.post('<?php echo url("usercenter/user_sign"); ?>',
                    {id:1},
                    function(data){
//                        console.log(data);
//

                        if(data.code==1) {
                            jifen1 = parseInt($('#inte').html());
                            jifen2 =parseInt(data.jifen);
                            jifen3 = jifen1+jifen2;
                            $('#daysign').html('已签到');
                            $('#inte').html(jifen3)
                            layer.msg(data.msg,{icon:1,time:1000,shade: 0.1,});
                        }else{
                            layer.msg(data.msg,{icon:5,time:1000,shade: 0.1,});
                        }
                    });
        }


    </script>


</html>