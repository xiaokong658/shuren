<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:85:"/data/www/html/web2017/public_html/../application/index/view/index/debate_detail.html";i:1498579644;}*/ ?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title><?php echo $info['title']; ?>-<?php echo $user_info['nickname']; ?>-树人号</title>
    <!-- 最新版本的 Bootstrap 核心 CSS 文件 -->
    <!--<link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">-->
    <link rel="stylesheet" href="__STATIC__/style/weui.min.css">
    <link rel="stylesheet" href="__STATIC__/style/cmgd.css">
    <script src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
    <!--奥森图标-->
    <link rel="stylesheet" href="__STATIC__/style/FontAwesome_4.2.0/css/font-awesome.min.css">
    <script type="text/javascript" src="__STATIC__/admin/js/layer/layer.js" ></script>
    <style>
        .weui-article h1 {margin-bottom:0px;}

        .weui-tabbar__i{font-family: "Microsoft Yahei";}
        #zan{font-size:16px;width: 50px;padding-top:7px;}
        #collect{font-size:16px;width:60px;padding-top:7px;}
        #fabiao{font-size:16px;width:60px;padding-top:7px;}
        .weui-tab__panel {padding-bottom: 5px;}
        .weui-media-box_appmsg .weui-media-box__thumb{width:45px;hegiht:45px;border: 1px #ccc solid;border-radius: 22.5px;}
        .black{color:#000;}
        .thumbup{color:#999;}
        .fa-thumbs-up{color:#2fab87;}
        .fa-heart{color:#2fab87;}
    </style>
</head>
<body>

<div class="container" id="container">




    <!--顶部搜索-->
    <div class="page home js_show">

        <div class="page__bd" style="height: 100%;">

            <div class="weui-tab" id="navbar" >
                <div class="weui-tab__panel">
                    <div class="weui-panel articlelist">
                        <div class="weui-panel__hd weui-panel__hd_detail">

                            <img src="/uploads/images/<?php echo $info['photo']; ?>" height="100%;" class="detailPic">

                            <div class="detailTitle">
                                <h4 class="weui-media-box__desc " style="height: 30px;"><?php echo $info['title']; ?></h4>
                                <!--<p class="weui-form-preview__label "><span>阅读量：</span><?php echo $info['views']; ?></p>-->
                                <p class="weui-form-preview__label "><span>参与讨论：</span><?php echo $comment_num; ?></p>
                                <p class="weui-form-preview__label "><span>发起人：</span><?php echo $info['nickname']; ?></p>
                                <p class="weui-form-preview__label "><span>开始时间：</span><?php echo $info['create_time1']; ?></p>
                                <p class="weui-form-preview__label "><span>截止时间：</span><?php echo $info['end_time1']; ?></p>
                            </div>

                        </div>

                    </div>
                </div>
                <?php if($info['is_zf'] ==1): ?>
                <div class="weui-cells">
                    <div class="weui-cell weui-cell_access">
                        <div class="weui-cell__bd">
                            <span style="vertical-align: middle">正方:<?php echo $info['zhengfang']; ?></span>
                        </div>
                    </div>
                    <div class="weui-cell weui-cell_access">
                        <div class="weui-cell__bd">
                            <span style="vertical-align: middle">反方:<?php echo $info['fanfang']; ?></span>
                        </div>
                    </div>
                </div>
                <?php endif; ?>

                <div class="weui-cells" style="margin-top:5px;">
                    <div class="weui-panel__bd" style="height: 100%;">
                        <article class="weui-article">
                            <section>
                                <section>
                                    <p>
                                        描述:<?php echo $info['content']; ?>
                                    </p>
                                </section>
                            </section>
                        </article>


                    </div>
                </div>


                <div class="weui-panel weui-panel_access">
                    <div>作者信息：</div>
                    <div class="weui-panel__bd">

                        <a href="<?php echo url('user/user_info',array('user_id'=>$user_info['id'])); ?>" class="weui-media-box weui-media-box_appmsg">
                            <div class="weui-media-box__hd">
                                <img class="weui-media-box__thumb" src="/uploads/face/<?php echo $user_info['head_img']; ?>" style="height:45px;" alt="">
                            </div>
                            <div class="weui-media-box__bd">
                                <h4 class="weui-media-box__title"><?php echo $user_info['nickname']; ?></h4>
                                <p class="weui-media-box__desc"><?php echo $user_info['desc']; ?></p>
                            </div>
                        </a>
                    </div>
                </div>



                <div class="weui-cells"  style="margin-top:5px;">
                    <article class="weui-article" style="padding: 10px 8px;"> <h1>全部评论</h1></article>
                    <div id="pingluns"> </div>

                </div>

                <div class="weui-tabbar footer" id="foot" style="z-index: 999;">
                    <a href="javascript:;" class="weui-tabbar__item" onclick="dianzan(<?php echo $info['id']; ?>);" style=" border-right:1px #ccc solid;" >
                    <span style="display: inline-block;position: relative;vertical-align: middle;">
                            <i id="zan" class="fa <?php if($is_zan == 1): ?>fa-thumbs-up<?php else: ?>fa-thumbs-o-up<?php endif; ?> ">&nbsp;&nbsp;<span style="color:#999;">赞</span></i>
                        </span>

                        <!--<p class="weui-tabbar__label" id="zan1"><?php echo $zan; ?></p>-->
                    </a>
                    <a href="javascript:;" class="weui-tabbar__item" onclick="shoucang(<?php echo $info['id']; ?>);" >

                   <span style="display: inline-block;position: relative;vertical-align: middle;">
                            <i id="collect" class="fa <?php if($is_collect == 1): ?>fa-heart<?php else: ?>fa-heart-o<?php endif; ?> ">&nbsp;&nbsp;<span style="color:#999;">收藏</span></i>
            </span>
                        <!--<p class="weui-tabbar__label" id="collect1">收藏</p>-->

                    </a>
                    <a  href="javascript:;" class="weui-tabbar__item" onclick="pinglun(<?php echo $user_id; ?>)" style=" border-left:1px #ccc solid;">


            <span style="display: inline-block;position: relative;vertical-align: middle;">
                            <i id="fabiao" class="fa fa-comment-o weui-tabbar__icon">&nbsp;&nbsp;评论</i>
        </span>
                    </a>
                </div>



            </div>

            <div class="weui-tab" id="navbar1" style="display: none;" >
                <div class="weui-cells__title">您支持的观点：</div>
                <div class="weui-cells weui-cells_radio">
                    <?php if($info['is_zf'] ==1): ?>
                        <label class="weui-cell weui-check__label" for="x11">
                            <div class="weui-cell__bd">
                                <p>正方</p>
                            </div>
                            <div class="weui-cell__ft">
                                <input type="radio" class="weui-check" name="type" id="x11" value="1">
                                <span class="weui-icon-checked"></span>
                            </div>
                        </label>

                    <label class="weui-cell weui-check__label" for="x12">
                        <div class="weui-cell__bd">
                            <p>反方</p>
                        </div>
                        <div class="weui-cell__ft">
                            <input type="radio" name="type" class="weui-check" id="x12" value="2" >
                            <span class="weui-icon-checked"></span>
                        </div>
                    </label>
                    <?php endif; ?>
                </div>

                <div class="weui-cells__title">观点简述</div>
                <div class="weui-cells weui-cells_form">
                    <div class="weui-cell">
                        <div class="weui-cell__bd">
                            <textarea class="weui-textarea" id="content" name="content" placeholder="请输入描述" rows="5"></textarea>
                            <div class="weui-textarea-counter"></div>
                        </div>
                    </div>
                </div>
                <div style="width:80%;margin:0 auto;padding-top: 30px;">
                    <input type="hidden" name="debate_id" id="debate_id" value="<?php echo $id; ?>">
                    <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>">
                    <input type="hidden" name="now" id="now" value="">
                    <a href="javascript:;" class="weui-btn weui-btn_primary" id="showTooltips">提交</a>

                    <a href="javascript:;" class="weui-btn weui-btn_default" id="returnbutton" onclick="cancel_pinglun()" >返回</a>




                </div>
            </div>
        </div>
    </div>
</div>
</body>

<script type="text/javascript">

    AjaxPage();


    $(window).scroll(
            function() {
                var scrollTop = $(this).scrollTop();
                var scrollHeight = $(document).height();
                var windowHeight = $(this).height();
                if (scrollTop + windowHeight == scrollHeight) {
                    AjaxPage();
                }
            });



    function AjaxPage(){
        var now =  $('#now').val();
        var info_id = '<?php echo $id; ?>';
        if(now==''){
            now =1 ;
        }

        console.log(info_id+'|'+now);

        $.post('<?php echo url("index/debate_list"); ?>',
                {id:info_id,page:now},
                function(data){
                    if(data.code==1) {
                         console.log(data);
                        $html = '';
                        for(c in data.list){
                            //data.list[c].addtime

                            if(data.list[c].is_zan==1){
                                var zanstyle = 'fa fa-thumbs-up';
                            }else{
                                var zanstyle = 'fa fa-thumbs-o-up';
                            }


                           var zffang = data.list[c].note;
                            if(zffang=='正方'){
                                var types = "<span style='padding: 4px;background:#0d8ddb;color: #fff;'>正方</span>&nbsp;&nbsp;&nbsp;";
                            }else if(zffang=='反方'){
                                var types ="<span style='padding: 4px;background:#0d8ddb;color: #fff;'>反方</span>&nbsp;&nbsp;&nbsp;";
                            }else{
                                var types = '';
                            }



                            //  $html = $html + "<span class='weui-media-box weui-media-box_appmsg'><div class='weui-media-box__hd'><img class='weui-media-box__thumb' src='/uploads/face/"+data.list[c].head_img+"' alt=''></div><div class='weui-media-box__bd'><h5 class='weui-media-box__title'>"+data.list[c].nickname+"</h5><p class='weui-media-box__desc commentlist'>十六大解放螺丝钉解放了随即大幅拉开距离"+data.list[c].content+"</p><small class='weui-media-box__desc '> <span style='padding: 4px;background:#0d8ddb;color: #fff;'>"+data.list[c].note+"</span>"+data.list[c].add_time+"</small></div></span><div class='weui-form-preview__ft'><a class='weui-form-preview__btn weui-form-preview__btn_default' id='comment1' onclick='comment('"+data.list[c].id+"',this)'><i class='fa fa-comment-o' id='pl_"+data.list[c].id+"'>"+data.list[c].comment_count+"</i></a><a class='weui-form-preview__btn weui-form-preview__btn_default no' onclick='thumbsUp('"+data.list[c].id+"',this)'><i class='fa fa-thumbs-o-up' id='zan_"+data.list[c].id+"'> "+data.list[c].zan_count+"</i></a></div>";
                            $html = $html + "<span  class='weui-media-box weui-media-box_appmsg'><div class='weui-media-box__hd'><a href='/index/user/user_info/user_id/"+data.list[c].user_id+".html'> <img class='weui-media-box__thumb' src='/uploads/face/"+data.list[c].head_img+"' alt='' style='width:45px;height:45px;'></a></div><div class='weui-media-box__bd'><h5 class='weui-media-box__desc black'>"+data.list[c].nickname+"</h5><p class='weui-media-box__desc commentlist'>"+data.list[c].content+"</p><small class='weui-media-box__desc minTime'> "+types+data.list[c].add_time+"</small><small class='thumbup'> <a id='comment1' onclick='comment("+data.list[c].id+",this)'><i class='fa fa-comment-o' id='pl_"+data.list[c].id+"'>"+data.list[c].comment_count+"</i></a><i class='"+zanstyle+"' onclick='thumbsUp("+data.list[c].id+",this)'> <span id='zan_"+data.list[c].id+"'>"+data.list[c].zan_count+"</span></span></i></small> </div></span>";
                        }

                        $("#pingluns").append($html);

                    }
                });
        next = parseInt(now)  + 1;
        $('#now').val(next) ;
    }

    //        顶部搜索
    $(function () {

        $('#showTooltips').on('click', function(){
            var content = $('#content').val();
            var user_id = $('#user_id').val();
            var id = $('#debate_id').val();
            var is_zf = '<?php echo $info['is_zf']; ?>';
            if(is_zf==1){
                var type = $('input:radio[name="type"]:checked').val();
                if(type=='undefined'){
                    layer.msg('请选择支持的观点！',{icon:5,time:1500,shade: 0.1,});
                    return false;
                }
                if(content==''){
                    layer.msg('请填写您的观点描述！',{icon:5,time:1500,shade: 0.1,});
                    return false;
                }
            }else{
                type = -1;
            }



            $.post('<?php echo url("index/debate_comment"); ?>',
                    {id:id,user_id:user_id,content:content,type:type},
                    function(data){
                        if(data.code==11){
                            layer.msg(data.msg,{icon:5,time:1500,shade: 0.1,});
                            window.location.href="<?php echo url('user/login'); ?>";
                        }else{
                            if(data.code==1) {
                                if(type==1){
                                    var types = "<span style='padding: 4px;background:#0d8ddb;color: #fff;'>正方</span>&nbsp;&nbsp;&nbsp;";
                                }else if(type==0){
                                    var types ="<span style='padding: 4px;background:#0d8ddb;color: #fff;'>反方</span>&nbsp;&nbsp;&nbsp;";
                                }else{
                                    var types = '';
                                }
                                $html =  "<span  class='weui-media-box weui-media-box_appmsg'><div class='weui-media-box__hd'> <img class='weui-media-box__thumb' src='"+data.list.img+"' alt=''></div><div class='weui-media-box__bd'><h5 class='weui-media-box__desc'>"+data.list.nickname+"</h5><p class='weui-media-box__desc commentlist'>"+data.list.content+"</p><small class='weui-media-box__desc minTime'> "+types+data.list.add_time+"</small><small class='thumbup'> <a id='comment1' onclick='comment("+data.list.id+",this)'><i class='fa fa-comment-o' id='pl_"+data.list.id+"'>0</i></a><i class='fa fa-thumbs-o-up' onclick='thumbsUp("+data.list.id+",this)'> <span id='zan_"+data.list.id+"'>0</span></span></i></small> </div></span>";
                                $("#pingluns").prepend($html);
                                $('#navbar1').hide();
                                $('#navbar').show();
                                $('#foot').show();
                            }
                        }
                    });
        });

    });
    //顶部导航
    $(function () {
        $('.weui-navbar__item').on('click', function () {
            $(this).addClass('weui-bar__item_on').siblings('.weui-bar__item_on').removeClass('weui-bar__item_on');
        });
    });
    //底部导航
//    $(function () {
//        $('.weui-tabbar__item').on('click', function () {
//            $(this).addClass('weui-bar__item_on').siblings('.weui-bar__item_on').removeClass('weui-bar__item_on');
//        });
//    });
    //点赞
    function thumbsUp(id, obj) {
        $.post('<?php echo url("index/debate_list_zan_status"); ?>',
                {id:id},
                function(data){
                    if(data.code==11){
                        window.location.href="<?php echo url('user/login'); ?>";
                    }else {
                        if ($(obj).hasClass('fa-thumbs-o-up')) {
                            $(obj).removeClass('fa-thumbs-o-up').addClass('fa-thumbs-up');
                            $(obj).html(data.count);
                        }else{
                            $(obj).removeClass('fa-thumbs-up').addClass('fa-thumbs-o-up');
                            $(obj).html(data.count);
                        }

                    }

                });

    }
    function comment(id, obj) {
        var debate_id = '<?php echo $id; ?>';
        location.href = '/index/index/debate_comment_comment/debate_id/'+debate_id+'/id/'+id+'.html';
    }


    function dianzan(id){
        $.post('<?php echo url("index/debate_zan_status"); ?>',
                {id:id},
                function(data){
                    if(data.code==11){
                        layer.msg(data.msg,{icon:5,time:1500,shade: 0.1,});
                        window.location.href="<?php echo url('user/login'); ?>";
                    }else {
                        if(data.code==1){

                            $('#zan').removeClass('fa-thumbs-o-up');
                            $('#zan').addClass('fa-thumbs-up');


//                            $('#zan').attr('src','__STATIC__/index/images/zan.png') ;
//                            document.getElementById('zan1').innerHTML = data.count;
                            layer.msg(data.msg,{icon:1,time:1500,shade: 0.1,});
                        }else{
                            $('#zan').removeClass('fa-thumbs-up');
                            $('#zan').addClass('fa-thumbs-o-up');
//                            $('#zan').attr('src','__STATIC__/index/images/zan1.png') ;
//                            document.getElementById('zan1').innerHTML = data.count;
                            layer.msg(data.msg,{icon:5,time:1500,shade: 0.1,});
                        }
                    }

                });
    }
    function shoucang(id){
        $.post('<?php echo url("index/debate_collect_status"); ?>',
                {id:id},
                function(data){
                    if(data.code==11){
                        layer.msg(data.msg,{icon:5,time:1500,shade: 0.1,});
                        window.location.href="<?php echo url('user/login'); ?>";
                    }else{
                        if(data.code==1){
                            $('#collect').removeClass('fa-heart-o');
                            $('#collect').addClass('fa-heart');
                            layer.msg(data.msg,{icon:1,time:1500,shade: 0.1,});
                        }else{

                            $('#collect').removeClass('fa-heart');
                            $('#collect').addClass('fa-heart-o');
                            layer.msg(data.msg,{icon:5,time:1500,shade: 0.1,});
                        }
                    }
                });
    }



    function pinglun(i){
        if(i==0){
            layer.msg('您尚未登录！',{icon:5,time:1500,shade: 0.1,});
            window.location.href="<?php echo url('user/login'); ?>";
        }


        var df = '<?php echo $info['can']; ?>';
        if(df=='0'){
            layer.msg('本话题评论时间已过，无法评论！',{icon:5,time:1500,shade: 0.1,});
        }

        if(df=='1'){
            $('#navbar').hide();
            $('#foot').hide();
            $('#navbar1').show();
        }

    }


    function cancel_pinglun(){

        $('#navbar1').hide();
        $('#navbar').show();
        $('#foot').show();



    }


</script>

</html>