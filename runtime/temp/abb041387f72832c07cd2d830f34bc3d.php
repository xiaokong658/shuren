<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:73:"/data/www/html/web2017/public_html/../application/web/view/user/fans.html";i:1498354692;s:78:"/data/www/html/web2017/public_html/../application/web/view/public/header4.html";i:1502875458;}*/ ?>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <meta name="description" content="">
    <meta name="author" content="">
    <title>个人主页</title>
    <!-- Bootstrap core CSS -->
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="http://v3.bootcss.com/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="/static/pc/cmgd.css" rel="stylesheet">
    <link href="/static/pc/style.css" rel="stylesheet">
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="http://v3.bootcss.com/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="http://v3.bootcss.com/assets/js/ie-emulation-modes-warning.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <script src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="/static/admin/js/layer/layer.js"></script>
    <script src="/static/admin/js/laypage/laypage.js"></script>
    <script src="/static/admin/js/laytpl/laytpl.js"></script>
    <link href="/static/admin/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">

    <style>
        .article_right {
            float: right;
            width:90%;
            padding-left: 40px;
            border-left: 1px solid #efefef;
        }

        .profile_all img {
            float: left;
            width: 80px;
            height: 80px;
            -webkit-border-radius: 40px;
            border-radius: 40px;
        }

        .profile_info {
            margin-left:90px;
            width: 200px;
            padding-top: 8px;
            min-height: 105px;
        }

        .data_all {
            width: 100%;;
            height: 97px;
            border: 1px solid #efefef;
            margin: 30px 0;
            padding: 18px 0;
        }

        .data_acticle {
            float: left;
            width: 33%;
            height: 100%;
            border-right: 1px solid #efefef;
            margin: 0 auto;
            text-align: center;
            float: left;
        }

        .data_read {
            margin: 0 auto 0 50%;
            text-align: center;
            width: 50%;
            height: 100%;
        }
        .pager > li > a { width:120px; margin-right: 10px;}
        #ff{font-size:24px;font-weight: 600;}

        .media{border-bottom:1px #f0f0f0 solid; padding: 25px 0;margin-top: 0px; }
        .cat{ text-decoration: none;}

        .media-heading{height: 42px; overflow: hidden; font-family: Microsoft YaHei; font-size:20px;font-weight: 500; }
        .media-content {height:40px;overflow: hidden;color: #878787;}
        .media-time{margin-top: 26px;color: #878787;}
        .fa-bookmark-o{color:#fdd000;margin-right: 10px;}
        .article_right{padding-left: 20px; width: 280px;}
        .col-sm-9{padding-right: 35px;}
        .col-sm-3{padding-left: 35px;}

        .c-line {
            float: left;
            margin-top: 3px;
            width: 4px;
            height: 19px;
            background: #fdd000;
            border-radius: 2px;
            margin-right: 12px;
        }
        .hovers:hover{
            color:#2fab87;
            text-decoration: none;
        }
        .hovers{
            color:#000;
            text-decoration: none;
        }
        .profile_content{ overflow: hidden;height:50px;line-height: 25px;}

    </style>
</head>

<body>

<header id="main-header">
    <div class="area">
        <div class="head-nav left">
            <ul>
                <li class="index"><a href="<?php echo url('index/index'); ?>"><img src="/static/pc/image/logo.png" STYLE="height: 40px;padding-right: 20px;"></a></li>
                <li><a href="<?php echo url('index/index'); ?>">树人网</a></li>             
                <li><a href="<?php echo url('article/essay'); ?>" target="_blank">征文活动</a></li>
                <li><a href="http://club.shuren100.com/"  target="_blank">树人论坛</a></li>
				<li><a href="http://baozhi.shuren100.com/" target="_blank">电子报纸</a></li>
				<li><a href="http://qikan.shuren100.com/" target="_blank">电子期刊</a></li>
				<li><a href="http://www.shuren100.com//indexold.html" target="_blank">返回老版</a></li>

            </ul>
        </div>
        <div id="head-login" class="right login">
            <div class="login">
                <?php if($is_login==1): ?>
                <a href="<?php echo url('article/user_index'); ?>" data-role="login-btn" class="login-sohu"><img src="/static/pc/image/user.png" ><?php echo $my_user_info['nickname']; ?></a>
                <a href="<?php echo url('user/center'); ?>" data-role="login-btn" class="login-sohu"><img src="/static/pc/image/home.png" >我的主页</a>
                <a href="<?php echo url('index/loginout'); ?>" data-role="login-btn" class="login-sohu">退出</a>
                <?php else: ?>
                <a href="<?php echo url('user/login'); ?>"  data-role="login-btn" class="login-sohu"><img src="/static/pc/image/user.png" >用户登录</a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</header>

<div class="container" style="padding-top:110px;">


    <div class="row" >



        <div class="col-sm-9 blog-main" >

            <!--<div class="blog-header" style="padding-top: 0px;margin-top: 0px;">-->
                <!--<nav>-->
                    <!--<ul class="pager" style="margin:0;margin-bottom:20px;">-->


                        <!--<li><a href="<?php echo url('center',array('user_id'=>$user_id,'type'=>1)); ?>"  <?php if($type ==1): ?>style="background: #2fab87;color: #fff;"  <?php endif; ?> >普通文章</a></li>-->
                        <!--<?php if($group_id ==8): ?>      <li><a href="<?php echo url('center',array('user_id'=>$user_id,'type'=>2)); ?>" <?php if($type ==2): ?>style="background: #2fab87;color: #fff;"  <?php endif; ?> >征文</a></li>   <?php endif; ?>-->
                        <!--<li><a href="<?php echo url('center',array('user_id'=>$user_id,'type'=>3)); ?>"  <?php if($type ==3): ?> style="background: #2fab87;color: #fff;"  <?php endif; ?> >话题</a></li>-->



                    <!--</ul>-->
                <!--</nav>-->
            <!--</div>-->


            <div style="    font-size: 24px;    font-weight: 600; padding-bottom: 20px;"> 我的粉丝 </div>

            <script id="list-template" type="text/html">
                {{# for(var i=0;i<d.length;i++){  }}
                <div class="media" style="width: 200px;float: left; padding: 15px;">
                    <div class="media-left" >

                        <a href='/web/user/center/user_id/{{d[i].id}}.html'>
                                    <img class="media-object"  alt="" src="/uploads/face/{{d[i].head_img}}"   onerror="this.src='/static/admin/images/head_default.gif'" data-holder-rendered="true" style="width:150px; height: 150px;">
                        </a>

                        <p style="font-size: 14px;padding: 5px 0px;">{{d[i].nickname}}</p>
                        <p style="font-size: 13px;">关注时间:{{d[i].create_time1}}</p>
                        </a>
                    </div>

                    <!--<div class="media-body">-->
                        <!--<h3 class="media-heading">-->
                            <!--<a class="hovers" href='/web/user/center/user_id/{{d[i].id}}.html'>-->
                                        <!--{{d[i].nickname}}  </a></h3>-->

                        <!--<div class="media-content" style="height:48px;overflow: hidden;">-->


                                    <!--{{# if(d[i].type==3){ }}-->
                                    <!--<a class="hovers" href='/web/user/center/user_id/{{d[i].id}}.html'>-->
                                        <!--{{# } }}-->

                                        <!--<small style="font-size:16px;">{{d[i].create_time1}}</small>-->
                                    <!--</a>-->
                        <!--</div>-->
                        <!--&lt;!&ndash;<div class="media-time">&ndash;&gt;-->
                            <!--&lt;!&ndash;<small>&ndash;&gt;-->
                                <!--&lt;!&ndash;<span class="media-time-low">{{d[i].create_time}}&nbsp;&nbsp;&nbsp;&nbsp;</span>&ndash;&gt;-->
                                <!--&lt;!&ndash;<i class="fa fa-comment-o"></i>&ndash;&gt;-->
                                <!--&lt;!&ndash;<span  class="media-time-low">{{d[i].comment_count}}</span>&ndash;&gt;-->
                                <!--&lt;!&ndash;<i class="fa fa-thumbs-o-up"></i>&ndash;&gt;-->
                                <!--&lt;!&ndash;<span class="media-time-low">{{d[i].zan_count}}</span>&ndash;&gt;-->
                                <!--&lt;!&ndash;<i class="fa fa-heart-o "></i>&ndash;&gt;-->
                                <!--&lt;!&ndash;<span class="media-time-low">{{d[i].collect_count}}</span>&ndash;&gt;-->
                            <!--&lt;!&ndash;</small>&ndash;&gt;-->
                        <!--&lt;!&ndash;</div>&ndash;&gt;-->
                    <!--</div>-->
                </div>

                {{# } }}
            </script>

            <div id="list-content"></div>
            <div id="AjaxPage" style=" text-align: right; margin-top: 30px;"></div>
            <div id="allpage" style=" text-align: right;"></div>
        </div><!-- /.blog-main -->

        <div class="col-sm-3 blog-sidebar" >

            <div class="article_right" style="padding-left:30px;">
                <div class="profile_all">
                    <a href="<?php echo url('user/center',array('user_id'=>$user_info['id'])); ?>" > <img id='img11' src="/uploads/face/<?php echo $user_info['head_img']; ?>"   onerror="this.src='/static/admin/images/head_default.gif'"  /> </a>
                    <div class="profile_info" style="width: 205px;">
                        <div class="profile_title">
                            <p id="ff"><?php echo $user_info['nickname']; ?></p>
                        </div>
                        <div class="profile_content">
                            <p><?php echo $user_info['desc']; ?></p>
                        </div>
                    </div>
                </div>
                <div class="data_all" style="width: 288px;">
                    <A href="<?php echo url('user/center',array('user_id'=>$user_id)); ?>">
                        <div class="data_acticle">
                            <p style="font-size: 22px;"><?php echo $user_info['count']; ?></p>
                            <p>文章数</p>
                        </div>
                    </A>

                    <A href="<?php echo url('user/fans',array('user_id'=>$user_id)); ?>">
                        <div class="data_acticle">
                            <p style="font-size: 22px;"><?php echo $user_info['fans_count']; ?></p>
                            <p>粉丝数</p>
                        </div>
                    </A>

                    <A href="<?php echo url('user/focus',array('user_id'=>$user_id)); ?>">
                        <div class="data_acticle">
                            <p style="font-size: 22px;"><?php echo $user_info['focus_count']; ?></p>
                            <p>关注数</p>
                        </div>
                    </A>
                </div>


            </div>


            <?php if($myuser_id != $user_id): ?>
            <div style="font-size:18px;text-align:center; margin-top: 30px;margin-bottom: 10px; height:40px;line-height: 40px;border:1px #2fab87 solid;color: #2fab87; clear: both;margin-left:30px; cursor:pointer "   onclick="isfoucs(<?php echo $user_id; ?>)"  id="guanzhu" >

                <?php if($is_focus==1): ?>已<i style="width: 90px;">&nbsp;</i>关<i style="width: 90px;">&nbsp;</i>注<?php else: ?>关<i style="width: 90px;">&nbsp;</i>注<?php endif; ?>


            </div>
            <?php endif; ?>
            <hr>

            <?php if($qrcode !=''): ?>
            <div class="article_right" style="padding-left:30px; padding-top:50px;">
                <div class="panel-heading" style="font-size:16px;font-weight: 600;word-spacing:10px; padding-bottom: 0px; margin-bottom: 0px;    letter-spacing: 2px;">
                    <em class="c-line"></em> 我的二维码
                </div>
                <div class="panel-body" style="padding: 5px;">

                    <img src="/uploads/face/<?php echo $qrcode; ?>" style="width: 100%;">

                </div>
            </div>
            <?php endif; if($group_id ==8): ?>
            <div class="article_right" style="padding-left:30px; padding-top:50px;">
                <div class="panel-heading" style="font-size:16px;font-weight: 600;word-spacing:10px; padding-bottom: 0px; margin-bottom: 0px;    letter-spacing: 2px;">
                    <em class="c-line"></em> 我的公告栏
                </div>
                <div class="panel-body" style="padding: 5px;">

                    <?php if(is_array($jg_recommend) || $jg_recommend instanceof \think\Collection || $jg_recommend instanceof \think\Paginator): if( count($jg_recommend)==0 ) : echo "" ;else: foreach($jg_recommend as $key=>$vt): ?>
                    <div style="padding: 10px 0;height: 70px;">
                        <div style="float: left ;width: 25%;">
                            <a href="<?php echo url('article/index',array('id'=>$vt['id'])); ?>" >  <img src="/uploads/images/<?php echo $vt['photo']; ?>" style="width:50px;height:50px;border-radius: 50%; "></a>
                        </div>
                        <div style="float: left;width: 75%;">
                            <a href="<?php echo url('article/index',array('id'=>$vt['id'])); ?>" >   <div style="height:25px;line-height:25px; overflow: hidden;"><?php echo $vt['title']; ?></div>
                                <div  style="height:25px;line-height:25px;overflow: hidden;color:#878787;font-size: 12px;"><?php echo $vt['remark']; ?></div></a>
                        </div>
                    </div>
                    <?php endforeach; endif; else: echo "" ;endif; ?>

                </div>
            </div>
            <?php endif; ?>

            <div class="article_right" style="padding-left:30px; padding-top:50px;">
                <div class="panel-heading" style="font-size:16px;font-weight: 600;word-spacing:10px; padding-bottom: 0px; margin-bottom: 0px;    letter-spacing: 2px;">
                    <em class="c-line"></em> 最新机构
                </div>
                <div class="panel-body" style="padding: 5px;">

                    <?php if(is_array($new_instit) || $new_instit instanceof \think\Collection || $new_instit instanceof \think\Paginator): if( count($new_instit)==0 ) : echo "" ;else: foreach($new_instit as $key=>$vt): ?>
                    <div style="padding: 10px 0;height: 70px;">
                        <div style="float: left ;width: 25%;">
                            <a href="<?php echo url('user/center',array('user_id'=>$vt['id'])); ?>" >  <img src="/uploads/face/<?php echo $vt['head_img']; ?>" style="width:50px;height:50px;border-radius: 50%; "></a>
                        </div>
                        <div style="float: left;width: 75%;">
                            <div style="height:25px;line-height:25px; overflow: hidden;"><?php echo $vt['nickname']; ?></div>
                            <div  style="height:25px;line-height:25px;overflow: hidden;color:#878787;font-size: 12px;"><?php echo $vt['desc']; ?></div>
                        </div>
                    </div>
                    <?php endforeach; endif; else: echo "" ;endif; ?>

                </div>
            </div>


        </div><!-- /.blog-sidebar -->

    </div><!-- /.row -->

</div><!-- /.container -->


<script type="text/javascript">

    Ajaxpage();


    function Ajaxpage(curr){
        console.log(1);
        var user_id = '<?php echo $user_id; ?>';
        $.post('<?php echo url("user/fans"); ?>', {page: curr || 1,user_id:user_id}, function(data){
            //  $(".spiner-example").css('display','none'); //数据加载完关闭动画
            console.log(data);
            if(data==''){
                // $("#list-content").html('<td colspan="20" style="padding-top:10px;padding-bottom:10px;font-size:16px;text-align:center">暂无数据</td>');
            }else{




                var tpl = document.getElementById('list-template').innerHTML;
                laytpl(tpl).render(data.list, function(html){
                    console.log(data.list);
                    // document.getElementById('list-content').innerHTML = html;
                    $('#list-content').html(html);
                    // $('.weui-grid__icon').css('height',$('.weui-grid__icon').css('width'));
                    laypage({
                        cont: $('#AjaxPage'),//容器。值支持id名、原生dom对象，jquery对象,
                        pages:'<?php echo $allpage; ?>',//总页数
                        skip: true,//是否开启跳页
                        skin: '#1AB5B7',//分页组件颜色
                        curr: curr || 1,
                        groups: 3,//连续显示分页数
                        jump: function(obj, first){


                            console.log(obj.curr);

                            if(!first){
                                Ajaxpage(obj.curr)
                            }
                            //  $('#allpage').html('第'+ obj.curr +'页，共'+ obj.pages +'页');
                        }
                    });
                });
            }
        });
    }

    function isfoucs(id){

        console.log(id);

        $.post('<?php echo url("web/user/focus_user"); ?>',
                {fans_id:id},
                function(data){

                    console.log(data);

                    if(data.code==11){
                        layer.msg(data.msg,{icon:5,time:1500,shade: 0.1,});
                        window.location.href="<?php echo url('user/login'); ?>";
                    }else{
                        if(data.code==1){
//                            $('#collect').removeClass('fa-heart-o');
//                            $('#collect').addClass('fa-heart');
                            $('#guanzhu').html('已<i style="width: 90px;">&nbsp;</i>关<i style="width: 90px;">&nbsp;</i>注');
//                            layer.msg(data.msg,{icon:1,time:1500,shade: 0.1,});
                        }else{

                            $('#collect').removeClass('fa-heart');
                            $('#guanzhu').html('关<i style="width: 90px;">&nbsp;</i>注');
//                            layer.msg(data.msg,{icon:5,time:1500,shade: 0.1,});
                        }
                    }
                });
    }



</script>

</body></html>