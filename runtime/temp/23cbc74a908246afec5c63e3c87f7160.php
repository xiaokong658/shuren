<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:78:"/data/www/html/web2017/public_html/../application/web/view/article/debate.html";i:1498703948;s:78:"/data/www/html/web2017/public_html/../application/web/view/public/header4.html";i:1502875458;}*/ ?>
<html lang="zh-CN"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <meta name="description" content="">
    <meta name="author" content="">
    <title>话题-树人号</title>
    <!-- Bootstrap core CSS -->
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="http://v3.bootcss.com/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="/static/pc/cmgd1.css" rel="stylesheet">
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="http://v3.bootcss.com/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="http://v3.bootcss.com/assets/js/ie-emulation-modes-warning.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <script src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="/static/admin/js/layer/layer.js"></script>
    <script src="/static/admin/js/laypage/laypage.js"></script>
    <script src="/static/admin/js/laytpl/laytpl.js"></script>
    <link href="/static/admin/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/static/pc/style.css" rel="stylesheet">
    <style>
        .sidebar-module{padding:0;margin:0;}
        .list-unstyled{ background: #f3f3f3;}
        .list-unstyled li{
            font-size: 16px;
            line-height: 36px;
            padding-left: 25px;
            height: 36px;
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
            word-spacing:10px; letter-spacing: 2px;
        }
        .list-unstyled li:hover{
            font-size: 16px;
            line-height: 36px;
            padding-left: 25px;
            height: 36px;
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
            word-spacing:10px; letter-spacing: 2px;
            background: #ddd;
        }

        .tag-title {
            height: 80px;
            border-bottom: 2px #fdd000 solid;
        }

        .area {
            width: 1180px;
            margin: 0 auto;
        }

        .tag-title .tag-name {
            padding-top: 20px;
        }


        .tag-title .tag-name .tt {
            color: #191919;
            font-size: 30px;
            font-weight: 600;
            line-height: 40px;
            word-spacing:22px;
            letter-spacing: 5px;
        }
        .media{border-bottom:1px #f0f0f0 solid; padding: 15px 0;margin-top: 0px; }
        .cat{ text-decoration: none;}

        .list-unstyled li a{color:#000;}
        .list-unstyled li a:hover{color:red; text-decoration: none;}

        .media-heading{height: 38px; overflow: hidden; font-family: Microsoft YaHei; font-size:18px;font-weight: 500;color:#000; }
        .media-content {height:40px;overflow: hidden;color: #878787;}
        .media-time{margin-top: 32px;color: #878787;}
        .fa-bookmark-o{color:#fdd000;margin-right: 10px;}

    </style>
</head>

<body>
<header id="main-header">
    <div class="area">
        <div class="head-nav left">
            <ul>
                <li class="index"><a href="<?php echo url('index/index'); ?>"><img src="/static/pc/image/logo.png" STYLE="height: 40px;padding-right: 20px;"></a></li>
                <li><a href="<?php echo url('index/index'); ?>">树人网</a></li>             
                <li><a href="<?php echo url('article/essay'); ?>" target="_blank">征文活动</a></li>
                <li><a href="http://club.shuren100.com/"  target="_blank">树人论坛</a></li>
				<li><a href="http://baozhi.shuren100.com/" target="_blank">电子报纸</a></li>
				<li><a href="http://qikan.shuren100.com/" target="_blank">电子期刊</a></li>
				<li><a href="http://www.shuren100.com//indexold.html" target="_blank">返回老版</a></li>

            </ul>
        </div>
        <div id="head-login" class="right login">
            <div class="login">
                <?php if($is_login==1): ?>
                <a href="<?php echo url('article/user_index'); ?>" data-role="login-btn" class="login-sohu"><img src="/static/pc/image/user.png" ><?php echo $my_user_info['nickname']; ?></a>
                <a href="<?php echo url('user/center'); ?>" data-role="login-btn" class="login-sohu"><img src="/static/pc/image/home.png" >我的主页</a>
                <a href="<?php echo url('index/loginout'); ?>" data-role="login-btn" class="login-sohu">退出</a>
                <?php else: ?>
                <a href="<?php echo url('user/login'); ?>"  data-role="login-btn" class="login-sohu"><img src="/static/pc/image/user.png" >用户登录</a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</header>

<div class="container">
    <div class="blog-header">
        <div class="tag-title-bg">
            <div class="area tag-title">
                <div class="tag-name"><span class="tt"><i class="fa fa-bookmark-o"></i>话题</span></div>
            </div>
        </div>
    </div>

    <div class="row">



        <div class="col-sm-12 blog-main">
            <script id="list-template" type="text/html">
                {{# for(var i=0;i<d.length;i++){  }}
                <div class="media">
                    {{# if(d[i].photo!=''){ }}
                    <div class="media-left">
                        <a href='/web/article/debate_detail/id/{{d[i].id}}.html'>
                            <img class="media-object"  alt="" src="/uploads/images/{{d[i].photo}}" data-holder-rendered="true" style="width: 225px; height: 150px;">
                        </a>
                    </div>
                    {{#} }}

                    <div class="media-body">
                        <a class="cat" href='/web/article/debate_detail/id/{{d[i].id}}.html' style="text-decoration: none;">
                          <h3 class="media-heading"  style="font-weight:700;">{{d[i].title}}</h3></a>
                        <div class="media-content" ><small>
                            <!--{{d[i].remark}}-->
                        </small></div>
                        <div class="media-time">
                            <small>
                                <span class="media-time-low">{{d[i].create_time}}</span>
                                <!--<span  class="media-time-low">阅读量:{{d[i].views}}</span>-->
                                <i class="fa fa-thumbs-o-up"></i>
                                <span class="media-time-low">{{d[i].zan}}</span>
                                <i class="fa fa-heart-o"></i>
                                <span class="media-time-low">{{d[i].collect}}</span>
                                <i class="fa fa-comment-o"></i>
                                <span class="media-time-low">{{d[i].comment}}</span>
                            </small>
                        </div>
                    </div>
                </div>

                {{# } }}
            </script>

            <div id="list-content"></div>

        </div><!-- /.blog-main -->



    </div><!-- /.row -->

</div><!-- /.container -->

<!--<footer class="blog-footer">-->
<!--<p>Blog template built for <a href="http://getbootstrap.com">Bootstrap</a> by <a href="https://twitter.com/mdo">@mdo</a>.</p>-->
<!--<p>-->
<!--<a href="#">Back to top</a>-->
<!--</p>-->
<!--</footer>-->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="http://v3.bootcss.com/assets/js/ie10-viewport-bug-workaround.js"></script>
<script type="text/javascript">






    var ccc =1;
    Ajaxpage();
    $(window).scroll(
            function() {
                var scrollTop = $(this).scrollTop();
                var scrollHeight = $(document).height();
                var windowHeight = $(this).height();
                if (scrollTop + windowHeight  == scrollHeight) {
                    Ajaxpage();
                }
            });

    function Ajaxpage(){

        var keyword ='<?php echo $keyword; ?>';
        $.post('<?php echo url("article/debate"); ?>',
                {page:ccc,keyword:keyword}
                , function(data){
                    //  $(".spiner-example").css('display','none'); //数据加载完关闭动画
                    console.log(data);
                    if(data==''){
                        // $("#list-content").html('<td colspan="20" style="padding-top:10px;padding-bottom:10px;font-size:16px;text-align:center">暂无数据</td>');
                    }else{

                        var tpl = document.getElementById('list-template').innerHTML;
                        laytpl(tpl).render(data.list, function(html){
                            // document.getElementById('list-content').innerHTML = html;
                            console.log(1);
                            console.log(html);
                            $('#list-content').append(html);
                            // $('.weui-grid__icon').css('height',$('.weui-grid__icon').css('width'));
                            ccc++;
                        });
                    }
                });
    }

</script>

</body></html>