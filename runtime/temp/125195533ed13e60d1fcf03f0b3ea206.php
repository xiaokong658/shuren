<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:84:"/data/www/html/web2017/public_html/../application/web/view/article/gongwen_read.html";i:1498799386;s:78:"/data/www/html/web2017/public_html/../application/web/view/public/header2.html";i:1496220126;s:82:"/data/www/html/web2017/public_html/../application/web/view/public/user_center.html";i:1498798586;}*/ ?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <meta name="description" content="">
    <title>Off Canvas Template for Bootstrap</title>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="http://v3.bootcss.com/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <link href="/static/pc/cmgd.css" rel="stylesheet">
    <link rel="stylesheet" href="/static/pc/swiper.3.2.0.min.css">

    <link rel="stylesheet" type="text/css" href="/static/admin/webupload/webuploader.css">
    <link rel="stylesheet" type="text/css" href="/static/admin/webupload/style.css">

    <!--[if lt IE 9]><script src="http://v3.bootcss.com/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="http://v3.bootcss.com/assets/js/ie-emulation-modes-warning.js"></script>
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="/static/admin/js/jquery.min.js?v=2.1.4"></script>
    <script src="/static/admin/js/jquery.form.js"></script>
    <script src="/static/admin/js/plugins/layer/laydate/laydate.js"></script>
    <script src="/static/pc/js/layer/layer.js"></script>
    <script src="/static/admin/js/laypage/laypage.js"></script>
    <script src="/static/admin/js/laytpl/laytpl.js"></script>
    <link href="/static/admin/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <style>
        .list-group-item.active, .list-group-item.active:focus, .list-group-item.active:hover{
            background: #fff;
            color: #2fab87;
            border-bottom: 0px;
            border-color:#f5f5f5;
        }
        .list-group-item:first-child {border-radius: 0px;   }
        .fa{padding-right: 15px;font-size:18px;}
        .list-group-item{font-size:16px; border-top:0px;border-bottom: 1px #f5f5f5 solid;padding-top:20px;padding-bottom:20px; padding-left: 40px;
            border-left:0px;            border-right:0px;}
        .smallpic{padding-right: 5px;font-size:22px;}

        .panel-heading{background: #f5f5f5;}
        .panel-primary>.panel-heading {
            color: #fff;
            background-color: #2fab87;
            border-color: #2fab87;
        }

        .panel-primary {
            border-color: #2fab87;
        }
        body{background:#f5f5f5;}
        .col-xs-12, .col-sm-9{padding-left: 0px;}
    </style>
</head>

<body>
<nav class="navbar navbar-fixed-top navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <a  href="#"><img src="/static/pc/image/logo.png" style="height: 50px;padding-right: 20px;"></a>
        </div>
        <div id="navbar" class="collapse navbar-collapse" style="font-size:16px;">
            <ul class="nav navbar-nav">
                <li><a href="<?php echo url('index/index'); ?>">首页</a></li>
                <li><a href="<?php echo url('user/center'); ?>">个人主页</a></li>
                <!--<li><a href="#contact">Contact</a></li>-->
            </ul>
            <div class="nav navbar-nav navbar-right">
                <!--<form class="navbar-form navbar-left">-->
                <!--<div class="form-group">-->
                <!--<input type="text" class="form-control" placeholder="搜索内容">-->
                <!--</div>-->
                <!--<button type="submit" class="btn btn-default">确定</button>-->
                <!--</form>-->
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="<?php echo url('article/user_index'); ?>">用户中心</a></li>
                    <?php if($is_login==1): ?>
                    <li><a href="<?php echo url('user/quitlogin'); ?>">退出</a></li> <?php endif; ?>
                    <!--<li class="dropdown">-->
                    <!--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">方法 <span class="caret"></span></a>-->
                    <!--<ul class="dropdown-menu">-->
                    <!--<li><a href="#">订单</a></li>-->
                    <!--<li><a href="#">嗯嗯</a></li>-->
                    <!--<li><a href="#">尺寸</a></li>-->
                    <!--<li role="separator" class="divider"></li>-->
                    <!--<li><a href="#">嗷嗷</a></li>-->
                    <!--</ul>-->
                    <!--</li>-->
                </ul>
            </div>
        </div>
        <!-- /.nav-collapse -->
    </div>
    <!-- /.container -->
</nav>
<!-- /.navbar -->

<div class="container body_bg" style="margin-top:80px;">

    <div class="row row-offcanvas row-offcanvas-right">
        <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">
            <!--<div style="width: 100%;height:100px;padding-top: 20px;padding-left: 10px;background: #fff;" >-->
            <!--<div style="width:60px;float: left;margin-right:10px;">-->
            <!--<img src="/uploads/face/<?php echo $member['head_img']; ?>" style="width: 60px;height: 60px;">-->
            <!--</div>-->
            <!--<div style="float: left;width: 178px;">-->
            <!--<div style="width: 178px;overflow: hidden;padding-top: 3px;"><?php echo $member['nickname']; ?></div>-->
            <!--<div style="width: 178px;overflow: hidden;padding-top: 12px;">ID:<?php echo $member['account']; ?></div>-->
            <!--</div>-->
            <!--</div>-->
            <div style="width: 100%;height:120px;padding-top: 20px;padding-top: 20px;padding-left: 10px; background: #fff;" >
    <div style="width:60px;float: left;margin-right:10px;">
        <img src="/uploads/face/<?php echo $member['head_img']; ?>" id="img11"  onerror="this.src='/static/admin/images/head_default.gif'"  style="width: 60px;height: 60px;">
    </div>
    <div style="float: left;width: 178px;">
        <div style="width: 178px;overflow: hidden;padding-top: 3px;"><?php echo $member['nickname']; ?></div>
        <div style="width: 178px;overflow: hidden;padding-top: 12px;">帐号:<?php echo $member['account']; ?></div>
        <div style="width: 178px;overflow: hidden;padding-top: 12px;">积分:<span id="jifen"><?php echo $member['integral']; ?></span> <span style="float: right;padding-right: 20px;">  <a href="javascript:;" onclick="user_sign()" >

                        <span style="padding: 3px 5px;color: #fff;background: #1ee0a5;border-radius: 3px;font-size: 12px;" id="daysign">
                         <?php if($is_sign==0): ?>签到<?php else: ?>已签到 <?php endif; ?>
                        </span>
        </a></span></div>
    </div>
</div>

<div class="list-group">
    <a href="<?php echo url('user_index'); ?>" class="list-group-item <?php if($m==1): ?>active<?php endif; ?>" style="border-bottom: 2px #f5f5f5 solid;"><i class="fa fa-home "></i>首页</a>

    <a class="list-group-item " ><i class="fa fa-tags"></i>文章</a>
    <a href="<?php echo url('write'); ?>" class="list-group-item <?php if($m==2): ?>active<?php endif; ?>" style="padding-left: 85px;padding-top:0px;padding-bottom: 10px;">写文章</a>
    <a href="<?php echo url('user_column'); ?>" class="list-group-item <?php if($m==18): ?>active<?php endif; ?>"  style="padding-left: 85px;padding-top:10px;padding-bottom: 20px; ">栏目管理</a>
    <a href="<?php echo url('myarticle'); ?>" class="list-group-item <?php if($m==3): ?>active<?php endif; ?>"  style="padding-left: 85px;border-bottom: 2px #f5f5f5 solid;padding-top:10px;padding-bottom: 20px; ">文章列表</a>
    <a class="list-group-item" ><i class="fa fa-file-text-o"></i>征文</a>
    <?php if($member['group_id'] == 8): ?>
    <a href="<?php echo url('write_essay'); ?>" class="list-group-item <?php if($m==4): ?>active<?php endif; ?>"  style="padding-left: 85px;padding-top:0px;padding-bottom: 10px;">发征文</a>
    <a href="<?php echo url('myessay'); ?>" class="list-group-item <?php if($m==5): ?>active<?php endif; ?>"  style="padding-left: 85px;border-bottom: 2px #f5f5f5 solid;padding-top:10px;padding-bottom: 20px;">征文列表</a>
    <?php else: ?>
    <a href="<?php echo url('myessay'); ?>" class="list-group-item <?php if($m==5): ?>active<?php endif; ?>" style="padding-left: 85px;border-bottom: 2px #f5f5f5 solid;padding-top:9px;padding-bottom: 20px;"><i class="fa fa-file-text-o "></i>已投征文</a>
    <?php endif; ?>
    <a class="list-group-item " ><i class="fa fa-tags"></i>话题</a>
    <a href="<?php echo url('write_debate'); ?>" class="list-group-item <?php if($m==6): ?>active<?php endif; ?> " style="padding-left: 85px;padding-top:0px;padding-bottom: 10px;">写话题</a>
    <a href="<?php echo url('mydebate'); ?>" class="list-group-item <?php if($m==7): ?>active<?php endif; ?>"  style="padding-left: 85px;border-bottom: 2px #f5f5f5 solid;padding-top:10px;padding-bottom: 20px; ">话题列表</a>
    <a class="list-group-item " ><i class="fa fa-th"></i>数据</a>
    <a href="<?php echo url('tongji'); ?>" class="list-group-item <?php if($m==15): ?>active<?php endif; ?> " style="padding-left: 85px;padding-top:0px;padding-bottom: 10px;">总体</a>
    <a href="<?php echo url('danpian'); ?>" class="list-group-item <?php if($m==16): ?>active<?php endif; ?>"  style="padding-left: 85px;border-bottom: 2px #f5f5f5 solid;padding-top:10px;padding-bottom: 20px; ">单篇</a>
    <a href="<?php echo url('user_info'); ?>" class="list-group-item <?php if($m==8): ?>active<?php endif; ?>" style="border-bottom: 2px #f5f5f5 solid;"><i class="fa fa-gear "></i>我的资料</a>
    <a href="<?php echo url('draft'); ?>" class="list-group-item <?php if($m==9): ?>active<?php endif; ?>" style="border-bottom: 2px #f5f5f5 solid;"><i class="fa fa-folder-open-o "></i>我的草稿箱</a>
    <a href="<?php echo url('collect'); ?>" class="list-group-item <?php if($m==10): ?>active<?php endif; ?>" style="border-bottom: 2px #f5f5f5 solid;"><i class="fa fa-heart-o "></i>我的收藏</a>
    <a href="<?php echo url('gongwen'); ?>" class="list-group-item <?php if($m==21): ?>active<?php endif; ?>" ><i class="fa fa-bell-o "></i>公文管理</a>
    <a href="<?php echo url('temp_message'); ?>" class="list-group-item <?php if($m==11): ?>active<?php endif; ?>" ><i class="fa fa-bell-o "></i>模板消息</a>
    <?php if($member['id'] == 212212): ?>
    <a href="<?php echo url('picture'); ?>" class="list-group-item <?php if($m==19): ?>active<?php endif; ?>" style="border-bottom: 2px #f5f5f5 solid;"><i class="fa fa-heart-o "></i>图片管理</a>
    <a href="<?php echo url('nav_manager'); ?>" class="list-group-item <?php if($m==20): ?>active<?php endif; ?>" ><i class="fa fa-bell-o "></i>侧栏管理</a>
    <?php endif; ?>
</div>

<script type="text/javascript">
    function user_sign(){
        $.post('<?php echo url("article/user_sign"); ?>',
                {id:1},
                function(data){
                    if(data.code==1) {
                        $('#daysign').html('已签到');
                        var jifen = $('#jifen').html();

                        jifen1 = parseInt(jifen)+parseInt(data.data);
                        $('#jifen').html(jifen1);


                        layer.msg(data.msg,{icon:1,time:1000,shade: 0.1,});
                    }else{
                        layer.msg(data.msg,{icon:5,time:1000,shade: 0.1,});
                    }
                });
    }
</script>
        </div><!--/.sidebar-offcanvas-->
        <div class="col-xs-12 col-sm-9">

            <div class="panel panel-primary">
                <div class="panel-heading" >
                    公文详情
                </div>
                <div class="panel-body">
                    <div class="wrapper wrapper-content animated fadeInRight">
                        <div class="row">


                            <div class="col-sm-12">
                                <label class="col-sm-2 control-label">发文方</label>
                                <label class="col-sm-6 control-label"><?php echo $gongwen['fa_name']; ?></label>
                            </div>

                            <div class="col-sm-12">
                                <label class="col-sm-2 control-label">收文方</label>
                                <label class="col-sm-6 control-label"><?php echo $gongwen['shou_name']; ?></label>
                            </div>

                            <div class="col-sm-12">
                                <label class="col-sm-2 control-label">公文标题</label>
                                <label class="col-sm-6 control-label"><?php echo $gongwen['title']; ?></label>
                            </div>

                            <div class="col-sm-12">
                                <label class="col-sm-2 control-label">公文内容</label>
                                <label class="col-sm-10 control-label"><?php echo $gongwen['content']; ?></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/row-->

    <hr>

    <footer  style="text-align: center;">
        <p>&copy; Company 2017</p>
    </footer>

</div><!--/.container-->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->



</body>
</html>
