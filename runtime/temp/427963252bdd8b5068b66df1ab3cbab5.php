<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:85:"/data/www/html/web2017/public_html/../application/index/view/article/user_column.html";i:1498462006;}*/ ?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>我的栏目</title>
    <!-- 最新版本的 Bootstrap 核心 CSS 文件 -->
    <!--<link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">-->
    <link rel="stylesheet" href="__STATIC__/style/weui.min.css">
    <link rel="stylesheet" href="__STATIC__/style/cmgd.css">
    <script src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
    <script type="text/javascript" src="__STATIC__/admin/js/layer/layer.js" ></script>
    <!--奥森图标-->
    <link rel="stylesheet" href="__STATIC__/style/FontAwesome_4.2.0/css/font-awesome.min.css">
    <style>
        .text{padding-left:9px; }
        .fa-list-ul{padding-right:8px;}
        .weui-uploader__input-box{margin-left: 10px;}
        .weui-btn_primary{width: 90%;}
        .weui-btn_default{width: 90%;background:#969696; color:#fff;}
    </style>
</head>
<body>

<div class="container" id="container">

    <!--顶部搜索-->
    <div class="page home js_show">

        <form id="form" action="" enctype="multipart/form-data" method="post">

            <div class="page__bd" style="height: 100%;">

                <?php if(is_array($lists) || $lists instanceof \think\Collection || $lists instanceof \think\Paginator): if( count($lists)==0 ) : echo "" ;else: foreach($lists as $key=>$vo): ?>
                    <div class="weui-flex" style="margin:10px; background: #fff;"  id="dc<?php echo $vo['id']; ?>" >
                        <div class="weui-flex__item">
                            <a href="<?php echo url('article/edit_column',array('id'=>$vo['id'])); ?>">
                            <div class="placeholder" style="width: 80%;float: left;">

                                    <span style="color:#000;">
                                            <?php echo $vo['sort']; ?>:<?php echo $vo['user_column']; ?>
                                    </span>

                            </div>
                            </a>

                            <div class="placeholder" style="width: 20%;float: left;">

                                <span style="float:right; padding: 3px 5px; border-radius: 3px;background: red; color:#fff;" onclick="del_column(<?php echo $vo['id']; ?>)"> 删除 </span>
                            </div>

                        </div>
                    </div>
                <?php endforeach; endif; else: echo "" ;endif; if($vs<5): ?>
                    <a href="<?php echo url('article/add_column'); ?>"  class="weui-btn weui-btn_primary" style="width: 90%;margin-top: 20px;margin-bottom: 50px;">新增栏目</a>
                <?php endif; ?>'
            </div>
        </form>
    </div>
</div>
</body>
<script type="text/javascript" src="/static/admin/webupload/webuploader.min.js"></script>
<script type="text/javascript">

    function del_column(id){

        layer.confirm('确认删除此栏目吗?', {icon: 3, title:'提示'}, function(index){
            $.getJSON('<?php echo url("article/del_column"); ?>', {'id': id}, function(res){
                if(res.code == 1){
                    layer.msg(res.msg,{icon:1,time:1500,shade: 0.1});
                    var cds = '#dc'+id;
                    $(cds).remove();
                }else{
                    layer.msg(res.msg,{icon:0,time:1500,shade: 0.1});
                }
            });
            layer.close(index);
        })

    }

    //底部导航
    $(function(){
        $('.weui-tabbar__item').on('click', function () {
            $(this).addClass('weui-bar__item_on').siblings('.weui-bar__item_on').removeClass('weui-bar__item_on');
        });
    });


</script>

</html>