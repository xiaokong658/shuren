<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:77:"/data/www/html/web2017/public_html/../application/web/view/article/write.html";i:1501833388;s:78:"/data/www/html/web2017/public_html/../application/web/view/public/header4.html";i:1502875458;s:82:"/data/www/html/web2017/public_html/../application/web/view/public/user_center.html";i:1498798586;}*/ ?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <meta name="description" content="">
    <title>用户中心-写文章</title>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="http://v3.bootcss.com/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <link href="/static/pc/cmgd.css" rel="stylesheet">
    <link href="/static/pc/style.css" rel="stylesheet">
    <link rel="stylesheet" href="/static/pc/swiper.3.2.0.min.css">

    <link rel="stylesheet" type="text/css" href="/static/admin/webupload/webuploader.css">
    <link rel="stylesheet" type="text/css" href="/static/admin/webupload/style.css">

    <!--[if lt IE 9]><script src="http://v3.bootcss.com/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="http://v3.bootcss.com/assets/js/ie-emulation-modes-warning.js"></script>
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="/static/admin/js/jquery.min.js?v=2.1.4"></script>
    <script src="/static/admin/js/jquery.form.js"></script>
    <script src="/static/admin/js/plugins/layer/laydate/laydate.js"></script>
    <script src="/static/pc/js/layer/layer.js"></script>
    <script src="/static/admin/js/laypage/laypage.js"></script>
    <script src="/static/admin/js/laytpl/laytpl.js"></script>
    <link href="/static/admin/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">

    <style>

        .list-group-item.active, .list-group-item.active:focus, .list-group-item.active:hover{
            background: #fff;
            color: #2fab87;
            border-color:#f5f5f5;
            border-bottom: 0px;
        }
.list-group-item:first-child {border-radius: 0px;   }
.fa{padding-right: 15px;font-size:18px;}
.list-group-item{font-size:16px; border-top:0px;border-bottom: 1px #f5f5f5 solid;padding-top:20px;padding-bottom:20px; padding-left: 40px;
    border-left:0px;            border-right:0px;}
.panel-heading{background: #f5f5f5;}

.panel-primary>.panel-heading {
    padding: 25px 0 20px 0px;
    color: #000;
    background-color: #fff;
    border-color: #fff;
    border-bottom: 1px #f5f5f5 solid;
}
.myinfo{
    padding: 25px 20px 20px 20px;
    border-bottom: 2px #ffb400 solid;
}

.panel-primary {
    border-color: #fff;
}


body{background:#f5f5f5;}
.col-xs-12, .col-sm-9{padding-left: 0px;}

.btn-primary{
    background-color: #2fab87;
    border-color: #2fab87;
}
label{font-weight: 100;}

.form-horizontal .control-label{text-align: left;letter-spacing:3px; width: 130px;}
.webuploader-pick{ background-color: #2fab87;
    border-color: #2fab87;}
.form-group{padding-top: 20px;}

</style>
</head>

<body >
<header id="main-header">
    <div class="area">
        <div class="head-nav left">
            <ul>
                <li class="index"><a href="<?php echo url('index/index'); ?>"><img src="/static/pc/image/logo.png" STYLE="height: 40px;padding-right: 20px;"></a></li>
                <li><a href="<?php echo url('index/index'); ?>">树人网</a></li>             
                <li><a href="<?php echo url('article/essay'); ?>" target="_blank">征文活动</a></li>
                <li><a href="http://club.shuren100.com/"  target="_blank">树人论坛</a></li>
				<li><a href="http://baozhi.shuren100.com/" target="_blank">电子报纸</a></li>
				<li><a href="http://qikan.shuren100.com/" target="_blank">电子期刊</a></li>
				<li><a href="http://www.shuren100.com//indexold.html" target="_blank">返回老版</a></li>

            </ul>
        </div>
        <div id="head-login" class="right login">
            <div class="login">
                <?php if($is_login==1): ?>
                <a href="<?php echo url('article/user_index'); ?>" data-role="login-btn" class="login-sohu"><img src="/static/pc/image/user.png" ><?php echo $my_user_info['nickname']; ?></a>
                <a href="<?php echo url('user/center'); ?>" data-role="login-btn" class="login-sohu"><img src="/static/pc/image/home.png" >我的主页</a>
                <a href="<?php echo url('index/loginout'); ?>" data-role="login-btn" class="login-sohu">退出</a>
                <?php else: ?>
                <a href="<?php echo url('user/login'); ?>"  data-role="login-btn" class="login-sohu"><img src="/static/pc/image/user.png" >用户登录</a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</header>

<div class="container body_bg" style="margin-top:80px;">

    <div class="row row-offcanvas row-offcanvas-right">
        <div class="col-xs-3 col-sm-3 sidebar-offcanvas" id="sidebar">

            <!--<div style="width: 100%;height:80px;padding-top: 10px;padding-left: 10px; background: #fff;" >-->
                <!--<div style="width:60px;float: left;margin-right:10px;">-->
                    <!--<img src="/uploads/face/<?php echo $member['head_img']; ?>" style="width: 60px;height: 60px;">-->
                <!--</div>-->
                <!--<div style="float: left;width: 178px;">-->
                    <!--<div style="width: 178px;overflow: hidden;padding-top: 3px;"><?php echo $member['nickname']; ?></div>-->
                    <!--<div style="width: 178px;overflow: hidden;padding-top: 12px;">ID:<?php echo $member['account']; ?></div>-->
                <!--</div>-->
            <!--</div>-->


            <div style="width: 100%;height:120px;padding-top: 20px;padding-top: 20px;padding-left: 10px; background: #fff;" >
    <div style="width:60px;float: left;margin-right:10px;">
        <img src="/uploads/face/<?php echo $member['head_img']; ?>" id="img11"  onerror="this.src='/static/admin/images/head_default.gif'"  style="width: 60px;height: 60px;">
    </div>
    <div style="float: left;width: 178px;">
        <div style="width: 178px;overflow: hidden;padding-top: 3px;"><?php echo $member['nickname']; ?></div>
        <div style="width: 178px;overflow: hidden;padding-top: 12px;">帐号:<?php echo $member['account']; ?></div>
        <div style="width: 178px;overflow: hidden;padding-top: 12px;">积分:<span id="jifen"><?php echo $member['integral']; ?></span> <span style="float: right;padding-right: 20px;">  <a href="javascript:;" onclick="user_sign()" >

                        <span style="padding: 3px 5px;color: #fff;background: #1ee0a5;border-radius: 3px;font-size: 12px;" id="daysign">
                         <?php if($is_sign==0): ?>签到<?php else: ?>已签到 <?php endif; ?>
                        </span>
        </a></span></div>
    </div>
</div>

<div class="list-group">
    <a href="<?php echo url('user_index'); ?>" class="list-group-item <?php if($m==1): ?>active<?php endif; ?>" style="border-bottom: 2px #f5f5f5 solid;"><i class="fa fa-home "></i>首页</a>

    <a class="list-group-item " ><i class="fa fa-tags"></i>文章</a>
    <a href="<?php echo url('write'); ?>" class="list-group-item <?php if($m==2): ?>active<?php endif; ?>" style="padding-left: 85px;padding-top:0px;padding-bottom: 10px;">写文章</a>
    <a href="<?php echo url('user_column'); ?>" class="list-group-item <?php if($m==18): ?>active<?php endif; ?>"  style="padding-left: 85px;padding-top:10px;padding-bottom: 20px; ">栏目管理</a>
    <a href="<?php echo url('myarticle'); ?>" class="list-group-item <?php if($m==3): ?>active<?php endif; ?>"  style="padding-left: 85px;border-bottom: 2px #f5f5f5 solid;padding-top:10px;padding-bottom: 20px; ">文章列表</a>
    <a class="list-group-item" ><i class="fa fa-file-text-o"></i>征文</a>
    <?php if($member['group_id'] == 8): ?>
    <a href="<?php echo url('write_essay'); ?>" class="list-group-item <?php if($m==4): ?>active<?php endif; ?>"  style="padding-left: 85px;padding-top:0px;padding-bottom: 10px;">发征文</a>
    <a href="<?php echo url('myessay'); ?>" class="list-group-item <?php if($m==5): ?>active<?php endif; ?>"  style="padding-left: 85px;border-bottom: 2px #f5f5f5 solid;padding-top:10px;padding-bottom: 20px;">征文列表</a>
    <?php else: ?>
    <a href="<?php echo url('myessay'); ?>" class="list-group-item <?php if($m==5): ?>active<?php endif; ?>" style="padding-left: 85px;border-bottom: 2px #f5f5f5 solid;padding-top:9px;padding-bottom: 20px;"><i class="fa fa-file-text-o "></i>已投征文</a>
    <?php endif; ?>
    <a class="list-group-item " ><i class="fa fa-tags"></i>话题</a>
    <a href="<?php echo url('write_debate'); ?>" class="list-group-item <?php if($m==6): ?>active<?php endif; ?> " style="padding-left: 85px;padding-top:0px;padding-bottom: 10px;">写话题</a>
    <a href="<?php echo url('mydebate'); ?>" class="list-group-item <?php if($m==7): ?>active<?php endif; ?>"  style="padding-left: 85px;border-bottom: 2px #f5f5f5 solid;padding-top:10px;padding-bottom: 20px; ">话题列表</a>
    <a class="list-group-item " ><i class="fa fa-th"></i>数据</a>
    <a href="<?php echo url('tongji'); ?>" class="list-group-item <?php if($m==15): ?>active<?php endif; ?> " style="padding-left: 85px;padding-top:0px;padding-bottom: 10px;">总体</a>
    <a href="<?php echo url('danpian'); ?>" class="list-group-item <?php if($m==16): ?>active<?php endif; ?>"  style="padding-left: 85px;border-bottom: 2px #f5f5f5 solid;padding-top:10px;padding-bottom: 20px; ">单篇</a>
    <a href="<?php echo url('user_info'); ?>" class="list-group-item <?php if($m==8): ?>active<?php endif; ?>" style="border-bottom: 2px #f5f5f5 solid;"><i class="fa fa-gear "></i>我的资料</a>
    <a href="<?php echo url('draft'); ?>" class="list-group-item <?php if($m==9): ?>active<?php endif; ?>" style="border-bottom: 2px #f5f5f5 solid;"><i class="fa fa-folder-open-o "></i>我的草稿箱</a>
    <a href="<?php echo url('collect'); ?>" class="list-group-item <?php if($m==10): ?>active<?php endif; ?>" style="border-bottom: 2px #f5f5f5 solid;"><i class="fa fa-heart-o "></i>我的收藏</a>
    <a href="<?php echo url('gongwen'); ?>" class="list-group-item <?php if($m==21): ?>active<?php endif; ?>" ><i class="fa fa-bell-o "></i>公文管理</a>
    <a href="<?php echo url('temp_message'); ?>" class="list-group-item <?php if($m==11): ?>active<?php endif; ?>" ><i class="fa fa-bell-o "></i>模板消息</a>
    <?php if($member['id'] == 212212): ?>
    <a href="<?php echo url('picture'); ?>" class="list-group-item <?php if($m==19): ?>active<?php endif; ?>" style="border-bottom: 2px #f5f5f5 solid;"><i class="fa fa-heart-o "></i>图片管理</a>
    <a href="<?php echo url('nav_manager'); ?>" class="list-group-item <?php if($m==20): ?>active<?php endif; ?>" ><i class="fa fa-bell-o "></i>侧栏管理</a>
    <?php endif; ?>
</div>

<script type="text/javascript">
    function user_sign(){
        $.post('<?php echo url("article/user_sign"); ?>',
                {id:1},
                function(data){
                    if(data.code==1) {
                        $('#daysign').html('已签到');
                        var jifen = $('#jifen').html();

                        jifen1 = parseInt(jifen)+parseInt(data.data);
                        $('#jifen').html(jifen1);


                        layer.msg(data.msg,{icon:1,time:1000,shade: 0.1,});
                    }else{
                        layer.msg(data.msg,{icon:5,time:1000,shade: 0.1,});
                    }
                });
    }
</script>
        </div><!--/.sidebar-offcanvas-->
        <div class="col-xs-12 col-sm-9">

            <div class="panel panel-primary" style="padding-left: 30px; min-height: 500px;">
                <div class="panel-heading">
                     <span class="myinfo">写文章</span>
                </div>
                <div class="panel-body">
                    <div class="wrapper wrapper-content animated fadeInRight">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <!--<div class="ibox-tools">-->
                                            <!--<a class="collapse-link">-->
                                                <!--<i class="fa fa-chevron-up"></i>-->
                                            <!--</a>-->
                                            <!--<a class="dropdown-toggle" data-toggle="dropdown" href="form_basic.html#">-->
                                                <!--<i class="fa fa-wrench"></i>-->
                                            <!--</a>-->
                                            <!--<a class="close-link">-->
                                                <!--<i class="fa fa-times"></i>-->
                                            <!--</a>-->
                                        <!--</div>-->
                                    </div>
                                    <div class="ibox-content">
                                        <form class="form-horizontal m-t" name="add" id="add" method="post" action="write">

                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">标题</label>
                                                <div class="input-group col-sm-4">
                                                    <input id="title" type="text" class="form-control" name="title" placeholder="输入文章标题" value="<?php echo $info['title']; ?>">
                                                </div>
                                            </div>



                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">摘要</label>
                                                <div class="input-group col-sm-4">
                                                    <textarea type="text" rows="5" name="remark" id="remark" placeholder="输入文章描述" class="form-control"><?php echo $info['remark']; ?></textarea>
                                                </div>
                                            </div>


                                            <!--<div class="hr-line-dashed"></div>-->
                                            <!--<div class="form-group">-->
                                            <!--<label class="col-sm-3 control-label">关键字</label>-->
                                            <!--<div class="input-group col-sm-4">-->
                                            <!--<input id="keyword" type="text" class="form-control" name="keyword" placeholder="输入文章关键字">-->
                                            <!--</div>-->
                                            <!--</div>-->

                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">封面</label>
                                                <div class="input-group col-sm-4">
                                                    <input type="hidden" id="data_photo" name="photo" value="<?php echo $info['photo']; ?>">
                                                    <div id="fileList" class="uploader-list" style="float:right"></div>
                                                    <div id="imgPicker" style="float:left">选择图片</div>      <span style="margin-left: 200px;margin-top:20px;">
                                                                <a onclick="delimg()" >删除图片</a>
                                                </span>
                                                    <img id="img_data" height="100px" style="float:left;margin-left: 50px;margin-top: -10px;" src="/uploads/images/<?php echo $info['photo']; ?>"  onerror="this.src='/static/admin/images/no_img.jpg'"     />
                                                </div>
                                            </div>

                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">所属分类</label>
                                                <div class="input-group col-sm-4">
                                                    <select class="form-control m-b chosen-select" name="cate_id" id="cate_id">
                                                        <option value="">==请选择==</option>
                                                        <?php if(!empty($cate)): if(is_array($cate) || $cate instanceof \think\Collection || $cate instanceof \think\Paginator): if( count($cate)==0 ) : echo "" ;else: foreach($cate as $key=>$vo): ?>
                                                        <option value="<?php echo $vo['id']; ?>"  <?php if($info['cate_id'] == $vo['id']): ?> selected<?php endif; ?>   ><?php echo $vo['name']; ?></option>
                                                        <?php if(!empty($vo['son'])): if(is_array($vo['son']) || $vo['son'] instanceof \think\Collection || $vo['son'] instanceof \think\Paginator): if( count($vo['son'])==0 ) : echo "" ;else: foreach($vo['son'] as $key=>$vv): ?>
                                                        <option value="<?php echo $vv['id']; ?>"  <?php if($info['cate_id'] == $vv['id']): ?> selected<?php endif; ?>   >
                                                        &nbsp;&nbsp;---<?php echo $vv['name']; ?></option>
                                                        <?php endforeach; endif; else: echo "" ;endif; endif; endforeach; endif; else: echo "" ;endif; endif; ?>
                                                    </select>
                                                </div>

                                            </div>


                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">文章栏目</label>
                                                <div class="input-group col-sm-8">
                                                    <select class="form-control m-b chosen-select" name="column_id" id="column_id" style="width: 50%;">
                                                        <option value="">==请选择==</option>
                                                        <?php if(!empty($lanmu)): if(is_array($lanmu) || $lanmu instanceof \think\Collection || $lanmu instanceof \think\Paginator): if( count($lanmu)==0 ) : echo "" ;else: foreach($lanmu as $key=>$vo): ?>
                                                        <option value="<?php echo $vo['id']; ?>"  <?php if($info['column_id'] == $vo['id']): ?> selected<?php endif; ?>   ><?php echo $vo['user_column']; ?></option>
                                                        <?php endforeach; endif; else: echo "" ;endif; endif; ?>
                                                    </select>

                                                    <span id="addcolumn" style="margin-left: 15px;line-height: 34px;padding: 5px; border-radius: 5px;color: #fff; background:#2fab87;" onclick="addcolumn();">添加新栏目</span>

                                                    <span id="addcolumn1"  style="display: none;"><input id="column" type="text" class="form-control"  placeholder="输入新栏目" style="width: 130px;margin-left: 10px;"> </span>
                                                    <span id="addcolumn2"  style="display: none;margin-left: 10px;line-height: 34px;padding: 5px; border-radius: 5px;color: #fff;background:#2fab87;" onclick="doadd()" >添加</span>
                                                </div>

                                            </div>




                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label " for="myEditor">内容</label>
                                                <div class="input-group col-sm-9">
                                                    <script src="/static/admin/ueditor/ueditor.config.js" type="text/javascript"></script>
                                                    <script src="/static/admin/ueditor/ueditor.all.js" type="text/javascript"></script>
                                                    <textarea name="content" style="width:90%" id="myEditor"><?php echo $info['content']; ?></textarea>
                                                    <script type="text/javascript">
                                                        var editor = UE.getEditor('myEditor',{
                                                            //这里可以选择自己需要的工具按钮名称,此处仅选择如下五个
                                                            toolbars:[['undo', 'redo', '|',
                                                                'bold', 'italic', 'underline', 'formatmatch', 'forecolor','paragraph', 'fontfamily', 'fontsize', '|',
                                                                'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|', 'simpleupload', 'insertimage', 'insertvideo','attachment'
//                                            'directionalityltr', 'directionalityrtl', 'indent', '|', 'touppercase', 'tolowercase', '|',  'link', 'unlink', 'anchor', '|',  'imagenone', 'imageleft', 'imageright', 'imagecenter', '|',
//                                            'emotion', 'scrawl', 'music','map', 'gmap', 'insertframe', 'insertcode', 'webapp','template', 'background', '|','fullscreen',   'splittocols', 'charts', '|',
//                                            'horizontal', 'date', 'time', 'spechars', 'snapscreen', 'wordimage', '|','print', 'preview', 'searchreplace', 'drafts', 'help',  'pagebreak', 'splittorows',
//                                            'inserttable', 'deletetable', 'insertparagraphbeforetable', 'insertrow', 'deleterow', 'insertcol', 'deletecol', 'mergecells', 'mergeright', 'mergedown', 'splittocells',
                                                            ]],
//                                                            autoClearinitialContent:true,
                                                            wordCount:false,
                                                            elementPathEnabled:false,
                                                        });
                                                    </script>
                                                </div>
                                            </div>

                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <div class="col-sm-4 col-sm-offset-3">

                                                    <input type="hidden" name="id" value="<?php echo $info['id']; ?>">

                                                    <button class="btn btn-primary" type="submit"> 提&nbsp;&nbsp;&nbsp;交</button>&nbsp;&nbsp;&nbsp;

                                                    <a class="btn btn-primary" href="javascript:;" onclick="save_cg()"> 保存草稿箱</a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/row-->

    <hr>

    <footer style="text-align: center;">
        <p>&copy; Company 2017</p>
    </footer>

</div>







<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<script type="text/javascript" src="/static/admin/webupload/webuploader.min.js"></script>

<script type="text/javascript">
    var $list = $('#fileList');
    //上传图片,初始化WebUploader
    var uploader = WebUploader.create({

        auto: true,// 选完文件后，是否自动上传。
        swf: '/static/admin/js/webupload/Uploader.swf',// swf文件路径
        server: "<?php echo url('Web/Upload/upload'); ?>",// 文件接收服务端。
        duplicate :true,// 重复上传图片，true为可重复false为不可重复
        pick: '#imgPicker',// 选择文件的按钮。可选。

        accept: {
            title: 'Images',
            extensions: 'gif,jpg,jpeg,bmp,png',
            mimeTypes: 'image/jpg,image/jpeg,image/png'
        },

        'onUploadSuccess': function(file, data, response) {
            $("#data_photo").val(data._raw);
            $("#img_data").attr('src', '/uploads/images/' + data._raw).show();
        }
    });

    uploader.on( 'fileQueued', function( file ) {
        $list.html( '<div id="' + file.id + '" class="item">' +
                '<h4 class="info">' + file.name + '</h4>' +
                '<p class="state">正在上传...</p>' +
                '</div>' );
    });

    // 文件上传成功
    uploader.on( 'uploadSuccess', function( file ) {
        $( '#'+file.id ).find('p.state').text('上传成功！');
    });

    // 文件上传失败，显示上传出错。
    uploader.on( 'uploadError', function( file ) {
        $( '#'+file.id ).find('p.state').text('上传出错!');
    });

    function save_cg(){
        $('#add').attr('action','cg_add_article').submit();
    }

    $(function(){
        $('#add').ajaxForm({
            beforeSubmit: checkForm, // 此方法主要是提交前执行的方法，根据需要设置
            success: complete, // 这是提交后的方法
            dataType: 'json'
        });

        function checkForm(){

          var act =    $('#add').attr('action');

            if(act=='write'){

                if( '' == $.trim($('#title').val())){
                layer.msg('标题不能为空', {icon: 5,time:1500,shade: 0.1}, function(index){
                    layer.close(index);
                });
                return false;
            }

            if( '' == $.trim($('#cate_id').val())){
                layer.msg('分类不能为空', {icon: 5,time:1500,shade: 0.1}, function(index){
                    layer.close(index);
                });
                return false;
            }

            if( '' == $.trim($('#remark').val())){
                layer.msg('描述不能为空', {icon: 5,time:1500,shade: 0.1}, function(index){
                    layer.close(index);
                });
                return false;
            }

//            if( '' == $.trim($('#data_photo').val())){
//                layer.msg('请上传图片', {icon: 5,time:1500,shade: 0.1}, function(index){
//                    layer.close(index);
//                });
//                return false;
//            }

//            if( '' == $.trim($('#content').val())){
//                layer.msg('内容不能为空', {icon: 5,time:1500,shade: 0.1}, function(index){
//                    layer.close(index);
//                });
//                return false;
//            }

            }

        }

        function complete(data){
            var act =    $('#add').attr('action');
            if(data.code == 1){
                layer.msg(data.msg, {icon: 6,time:1500,shade: 0.1}, function(index){
                    layer.close(index);

                    if(act == 'cg_add_article'){
                        window.location.href="<?php echo url('article/draft'); ?>";
                    }else{
                        window.location.href="<?php echo url('article/myarticle'); ?>";
                    }
                    //window.location.href="<?php echo url('article/index'); ?>";

                });
            }else{
                layer.msg(data.msg, {icon: 5,time:1500,shade: 0.1}, function(index){
                    layer.close(index);
                });
                return false;
            }
        }

    });

    function addcolumn(){
        $('#addcolumn').hide();
        $('#addcolumn1').show();
        $('#addcolumn2').show();
    }

    function doadd(){

        var user_column = $('#column').val();
        console.log(user_column);
        if( '' == $.trim($('#column').val())){
            layer.msg('新增的分类不能为空', {icon: 5,time:1500,shade: 0.1}, function(index){
                layer.close(index);
            });
            return false;
        }

        $.post('<?php echo url("article/add_column"); ?>', {user_column: user_column}, function(data){

            if(data.code==1){
                $("#column_id").append("<option value='"+data.new_id+"'>"+user_column+"</option>"); //为Select追加一个Option(下拉项)
                $("#column_id").val(data.new_id);
                $('#column').val('');
                $('#addcolumn').show();
                $('#addcolumn1').hide();
                $('#addcolumn2').hide();
            }else{
                layer.msg(data.msg, {icon: 5,time:1500,shade: 0.1}, function(index){
                    layer.close(index);
                });
            }


        });

    }

    function delimg(){
        $("#data_photo").val('');
        $("#WU_FILE_0").html('');
        $("#img_data").attr('src', '/static/admin/images/no_img.jpg').show();
    }


</script>
</body>
</html>
