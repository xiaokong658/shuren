<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:76:"/data/www/html/web2017/public_html/../application/admin/view/instit/edu.html";i:1494246048;s:79:"/data/www/html/web2017/public_html/../application/admin/view/public/header.html";i:1484102488;s:79:"/data/www/html/web2017/public_html/../application/admin/view/public/footer.html";i:1487735376;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo config('WEB_SITE_TITLE'); ?></title>
    <link href="/static/admin/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/admin/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/static/admin/css/animate.min.css" rel="stylesheet">
    <link href="/static/admin/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="/static/admin/css/plugins/chosen/chosen.css" rel="stylesheet">
    <link href="/static/admin/css/plugins/switchery/switchery.css" rel="stylesheet">
    <link href="/static/admin/css/style.min.css?v=4.1.0" rel="stylesheet">
    <link href="/static/admin/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <style type="text/css">
    .long-tr th{
        text-align: center
    }
    .long-td td{
        text-align: center
    }
    </style>
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>机构列表</h5>
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-sm-12">
                    <div  class="col-sm-2">
                        <div class="input-group" >
                            <!--<button type="button" class="btn btn-primary btn-outline" data-toggle="modal" data-target="#myModal">添加机构</button> -->
                            <a href="<?php echo url('add_edu'); ?>"><button class="btn btn-outline btn-primary" type="button">添加机构</button></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="example-wrap">
                <div class="example">
                    <form id="eduorder" name="eduorder" method="post" action="<?php echo url('eduorder'); ?>" >
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr class="long-tr">
                                <th width="6%">ID</th>
                                <th width="19%">机构名称</th>
                                <!--<th width="17%">节点</th>-->
                                <th width="12%">状态</th>
                                <th width="15%">添加时间</th>
                                <th width="7%">排序</th>
                                <th width="7%">添加子机构</th>
                                <th width="15%">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): if( count($list)==0 ) : echo "" ;else: foreach($list as $key=>$v): ?>
                            <tr class="long-td">
                                <td><?php echo $v['id']; ?></td>
                                <td style='text-align:left;padding-left:<?php if($v['leftpin'] != 0): ?><?php echo $v['leftpin']; ?>px<?php endif; ?>'><?php echo $v['lefthtml']; ?><?php echo $v['name']; ?></td>
                                <!--<td><?php echo $v['name']; ?></td>-->
                                <td>

                                    <?php if($v['status'] == 1): ?>
                                    <a class="red" href="javascript:;" onclick="edu_state(<?php echo $v['id']; ?>);">
                                        <div id="zt<?php echo $v['id']; ?>"><span class="label label-info">开启</span></div>
                                    </a>
                                    <?php else: ?>
                                    <a class="red" href="javascript:;" onclick="edu_state(<?php echo $v['id']; ?>);">
                                        <div id="zt<?php echo $v['id']; ?>"><span class="label label-danger">禁用</span></div>
                                    </a>
                                    <?php endif; ?>
                                </td>
                                <td><?php echo $v['create_time']; ?></td>
                                <td style="padding: 3px" >
                                    <div >
                                        <?php echo $v['sort']; ?>
                                    </div>
                                </td>

                                <td>
                                    <a href="<?php echo url('add_edu',['pid'=>$v['id']]); ?>" class="btn btn-primary btn-xs">
                                        <i class="fa fa-plus"></i></a>&nbsp;&nbsp;
                                </td>


                                <td>

                                    <a href="<?php echo url('edu_edit',['id'=>$v['id']]); ?>" class="btn btn-primary btn-xs">
                                        <i class="fa fa-paste"></i> 编辑</a>&nbsp;&nbsp;
                                    <a href="javascript:;" onclick="del_edu(<?php echo $v['id']; ?>)" class="btn btn-danger btn-xs">
                                        <i class="fa fa-trash-o"></i> 删除</a>
                                </td>
                            </tr>
                            <?php endforeach; endif; else: echo "" ;endif; ?>
                            <tr>
                                <!--<td colspan="8" align="right">-->
                                    <!--<button type="submit"  id="btnorder" class="btn btn-info">更新排序</button></td>-->
                            </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
<script src="__JS__/jquery.min.js?v=2.1.4"></script>
<script src="__JS__/bootstrap.min.js?v=3.3.6"></script>
<script src="__JS__/content.min.js?v=1.0.0"></script>
<script src="__JS__/plugins/chosen/chosen.jquery.js"></script>
<script src="__JS__/plugins/iCheck/icheck.min.js"></script>
<script src="__JS__/plugins/layer/laydate/laydate.js"></script>
<script src="__JS__/plugins/switchery/switchery.js"></script><!--IOS开关样式-->
<script src="__JS__/jquery.form.js"></script>
<script src="__JS__/layer/layer.js"></script>
<script src="__JS__/laypage/laypage.js"></script>
<script src="__JS__/laytpl/laytpl.js"></script>
<script src="__JS__/lunhui.js"></script>
<script>
    $(document).ready(function(){$(".i-checks").iCheck({checkboxClass:"icheckbox_square-green",radioClass:"iradio_square-green",})});
</script>

<script type="text/javascript">

    $(function(){
        $('#add_edu').ajaxForm({
            beforeSubmit: checkForm,
            success: complete,
            dataType: 'json'
        });

        function checkForm(){
            if( '' == $.trim($('#title').val())){
                layer.msg('请输入机构名称',{icon:2,time:1500,shade: 0.1}, function(index){
                    layer.close(index);
                });
                return false;
            }

            if( '' == $.trim($('#name').val())){
                layer.msg('控制器/方法不能为空',{icon:0,time:1500,shade: 0.1}, function(index){
                    layer.close(index);
                });
                return false;
            }
        }


        function complete(data){
            if(data.code==1){
                layer.msg(data.msg, {icon: 6,time:1500,shade: 0.1}, function(index){
                    window.location.href="<?php echo url('menu/index'); ?>";
                });
            }else{
                layer.msg(data.msg, {icon: 6,time:1500,shade: 0.1});
                return false;
            }
        }

    });


    //更新排序
    $(function(){
        $('#eduorder').ajaxForm({
            success: complete,
            dataType: 'json'
        });

        function complete(data){
            if(data.code==1){
                layer.msg(data.msg, {icon: 1,time:1500,shade: 0.1}, function(index){
                    window.location.href="<?php echo url('menu/index'); ?>";
                });
            }else{
                layer.msg(data.msg, {icon: 2,time:1500,shade: 0.1}, function(index){
                    layer.close(index);
                    window.location.href=data.url;
                });
            }
        }
    });


    /**
     * [del_edu 删除机构]
     * @Author[田建龙 864491238@qq.com]
     * @param   {[type]}    id [用户id]
     */
    function del_edu(id){
        layer.confirm('确认删除此机构?', {icon: 3, title:'提示'}, function(index){
            //do something
            $.getJSON('./del', {'id' : id}, function(res){
                if(res.code == 1){
                    layer.msg(res.msg,{icon:1,time:1500,shade: 0.1},function(index){
                        layer.close(index);
                        window.location.href="<?php echo url('instit/edu'); ?>";
                    });

                }else{
                    layer.msg(res.msg,{icon:0,time:1500,shade: 0.1});
                }
            });

            layer.close(index);
        })

    }

    //机构状态
    function edu_state(id){
        lunhui.status(id,'<?php echo url("instit_state"); ?>');
    }

    //IOS开关样式配置
    var elem = document.querySelector('.js-switch');
    var switchery = new Switchery(elem, {
        color: '#1AB394'
    });
    var config = {
        '.chosen-select': {},
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
</body>
</html>