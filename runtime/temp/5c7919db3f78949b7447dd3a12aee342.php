<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:84:"/data/www/html/web2017/public_html/../application/index/view/usercenter/gongwen.html";i:1498806394;}*/ ?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>树人100-微信UI-1.0</title>
    <!-- 最新版本的 Bootstrap 核心 CSS 文件 -->
    <!--<link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">-->
    <link rel="stylesheet" href="__STATIC__/style/weui.min.css">
    <link rel="stylesheet" href="__STATIC__/style/cmgd.css">
    <script src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
    <!--奥森图标-->
    <link rel="stylesheet" href="__STATIC__/style/FontAwesome_4.2.0/css/font-awesome.min.css">
    <script src="/static/admin/js/plugins/layer/laydate/laydate.js"></script>
    <script src="/static/pc/js/layer/layer.js"></script>
    <script src="/static/admin/js/laypage/laypage.js"></script>
    <script src="/static/admin/js/laytpl/laytpl.js"></script>
    <style>
        .text{padding-left:9px; }
        .weui-media-box__title{font-size:15px;}
        .weui-media-box__thumb{width:45px;height:45px;border-radius: 23.5px;border:1px #ccc solid;}
        .weui-media-box_appmsg .weui-media-box__thumb{width:45px;height:45px;border-radius: 23.5px;border:1px #ccc solid;}
    </style>
</head>
<body>

<div class="container" id="container">
    <!--顶部搜索-->
    <div class="page home js_show">
        <div class="page__bd" style="height: 100%;">
            <!--选择导航-->
            <div class="weui-tab__panel">
                <div class="weui-panel articlelist">
                    <div class="page__bd">
                        <div class="weui-panel weui-panel_access">
                            <div class="weui-panel__hd" style="font-size:16px;color:#000;text-align:center;height:32px;"><b>公文管理</b></div>
                            <div class="weui-panel__bd">
                                <div id="myarticle"></div>
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="weui-panel weui-panel_access">
                        <script id="list-template" type="text/html">
                            {{# for(var i=0;i<d.length;i++){  }}
                                <div class="weui-panel__bd">
                                    <a href="/index/usercenter/gongwen_read/id/{{d[i].id}}.html" class="weui-media-box weui-media-box_appmsg">
                                        <div class="weui-media-box__bd">
                                            <h4 class="weui-media-box__title">标题:{{d[i].title}}</h4>
                                            <p class="weui-media-box__desc">发文方:{{d[i].fa_name}}</p>
                                            <p class="weui-media-box__desc">收文方:{{d[i].shou_name}}</p>
                                            <p class="weui-media-box__desc">发送时间:{{d[i].addtime1}}</p>
                                        </div>
                                    </a>
                                </div>
                            {{#} }}
                        </script>
                        <div id="list-content"></div>
                    </div>
            </div>
        </div>

    </div>
</div>

<?php if($can ==1): ?>
    <a href="/index/usercenter/gongwen_add.html">
    <div class="weui-tabbar footer" >
            <button class="weui-btn weui-btn_primary" type="submit"> 发公文 </button>
    </div>
    </a>
<?php endif; ?>
</body>

<script type="text/javascript">

    var ccc =1;
    Ajaxpage();
    $(window).scroll(
            function() {
                var scrollTop = $(this).scrollTop();
                var scrollHeight = $(document).height();
                var windowHeight = $(this).height();
                if (scrollTop + windowHeight == scrollHeight) {
                    Ajaxpage();
                }
            });



    function Ajaxpage(){

        $.getJSON('<?php echo url("usercenter/gongwen"); ?>', {
            page: ccc
        }, function(data){      //data是后台返回过来的JSON数据

            console.log(data);

//            $(".spiner-example").css('display','none'); //数据加载完关闭动画
            if(data==''){
//                $("#list-content").html('<td colspan="20" style="padding-top:10px;padding-bottom:10px;font-size:16px;text-align:center">暂无数据</td>');
            }else{
                var tpl = document.getElementById('list-template').innerHTML;
                laytpl(tpl).render(data, function(html){
                 //   document.getElementById('list-content').innerHTML = html;

                    $('#list-content').append(html);

                });
                ccc ++;
            }
        });
    }

    //底部导航
    $(function(){
        $('.weui-tabbar__item').on('click', function () {
            $(this).addClass('weui-bar__item_on').siblings('.weui-bar__item_on').removeClass('weui-bar__item_on');
        });
    });


</script>

</html>