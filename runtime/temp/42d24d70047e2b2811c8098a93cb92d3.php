<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:85:"/data/www/html/web2017/public_html/../application/index/view/article/essay_write.html";i:1498450880;}*/ ?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>发征文</title>
    <!-- 最新版本的 Bootstrap 核心 CSS 文件 -->
    <!--<link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">-->
    <link rel="stylesheet" href="__STATIC__/style/weui.min.css">
    <link rel="stylesheet" href="__STATIC__/style/cmgd.css">
    <script src="/static/admin/js/jquery.min.js?v=2.1.4"></script>
    <script src="/static/admin/js/jquery.form.js"></script>
    <script src="/static/admin/js/plugins/layer/laydate/laydate.js"></script>
    <script src="/static/pc/js/layer/layer.js"></script>
    <script src="/static/admin/js/laypage/laypage.js"></script>
    <script src="/static/admin/js/laytpl/laytpl.js"></script>
    <link rel="stylesheet" type="text/css" href="/static/admin/webupload/webuploader.css">
    <!--奥森图标-->
    <link rel="stylesheet" href="__STATIC__/style/FontAwesome_4.2.0/css/font-awesome.min.css">
    <style>
        .text{padding-left:9px; }
        .fa-list-ul{padding-right:8px;}
        .weui-uploader__input-box{margin-left: 10px;}
        .weui-btn_primary{width: 90%;}
        .weui-btn_default{width: 90%;background:#969696; color:#fff;}
    </style>
</head>
<body>

<div class="container" id="container">

    <!--顶部搜索-->
    <div class="page home js_show">

        <form id="form" action="<?php echo url('essay_write'); ?>" enctype="multipart/form-data" method="post">

            <div class="page__bd" style="height: 100%;">
                <div class="weui-cells__title">标题</div>
                <div class="weui-cells">
                    <div class="weui-cell">
                        <div class="weui-cell__bd">
                            <input class="weui-input" name="title" id="title" type="text" placeholder="请输入标题" value="">
                        </div>
                    </div>
                </div>


                <div class="weui-cells__title">征文对象</div>
                <div class="weui-cells">
                    <div class="weui-cell">
                        <div class="weui-cell__bd">
                            <input class="weui-input" name="object_people" id="object_people" type="text" placeholder="请输入征文对象" value="">
                        </div>
                    </div>
                </div>


                <div class="weui-cells__title">截止时间</div>
                <div class="weui-cells">
                    <div class="weui-cell">
                        <div class="weui-cell__bd">
                            <input class="weui-input"  name="end_time" id="end_time" type="text" placeholder="征文截止时间"  onclick="laydate()">

                            <!--<input type="text" name="end_time" id="end_time" class="form-control layer-date" placeholder="征文截止时间"/>-->
                        </div>
                    </div>
                </div>


                <div class="weui-cells__title">描述</div>
                <div class="weui-cells weui-cells_form">
                    <div class="weui-cell">
                        <div class="weui-cell__bd">
                            <textarea class="weui-textarea" name="remark"  id="remark" placeholder="请输入描述" rows="3"></textarea>
                        </div>
                    </div>
                </div>

                <div class="weui-cells__title">内容</div>
                <div class="weui-cells weui-cells_form">
                    <div class="weui-cell">
                        <div class="weui-cell__bd">
                            <textarea class="weui-textarea" name="content"  id="content" placeholder="请输入内容 " rows="5"></textarea>
                        </div>
                    </div>
                </div>


                <div class="weui-cells__title">上传封面</div>
                <div class="weui-cells weui-cells_form">
                    <div class="weui-cell">
                        <div class="weui-cell__bd">
                            <input type="hidden" id="data_photo" name="photo" value="">
                            <div id="fileList" class="uploader-list" style="float:right"></div>
                            <div id="imgPicker" style="float:left">选择图片</div>
                            <img id="img_data" height="100px" style="float:left;" src=""     />
                        </div>
                    </div>
                </div>




                <!--<div class="weui-cells__title">上传封面</div>-->
                <!--<div class="weui-cells weui-cells_form" style="padding-top:6px;">-->
                    <!--<div class="weui-cell__bd">-->
                        <!--&lt;!&ndash;<div class="weui-uploader">&ndash;&gt;-->
                            <!--&lt;!&ndash;<div class="weui-uploader__bd">&ndash;&gt;-->
                                <!--&lt;!&ndash;<ul class="weui-uploader__files" id="uploaderFiles"></ul>&ndash;&gt;-->
                                <!--<div class="weui-uploader__input-box">-->
                                   <!---->
                                <!--</div>-->
                            <!--&lt;!&ndash;</div>&ndash;&gt;-->
                        <!--&lt;!&ndash;</div>&ndash;&gt;-->
                    <!--</div>-->
                <!--</div>-->



                <div class="weui-flex">
                    <div class="weui-flex__item">
                        <div class="placeholder" style="padding: 0 5px;">
                            <!--<a href="javascript:;" class="weui-btn weui-btn_primary" style="" onclick="submit();" >发布</a>-->
                            <button class="weui-btn weui-btn_primary" type="submit" onclick="check()" > 发布 </button>
                        </div></div>
                </div>



            </div>
        </form>
    </div>
</div>
</body>
<script type="text/javascript" src="/static/admin/webupload/webuploader.min.js"></script>

<script type="text/javascript">
    var $list = $('#fileList');
    //上传图片,初始化WebUploader
    var uploader = WebUploader.create({

        auto: true,// 选完文件后，是否自动上传。
        swf: '/static/admin/js/webupload/Uploader.swf',// swf文件路径
        server: "<?php echo url('Web/Upload/upload'); ?>",// 文件接收服务端。
        duplicate :true,// 重复上传图片，true为可重复false为不可重复
        pick: '#imgPicker',// 选择文件的按钮。可选。

        accept: {
            title: 'Images',
            extensions: 'gif,jpg,jpeg,bmp,png',
            mimeTypes: 'image/jpg,image/jpeg,image/png'
        },

        'onUploadSuccess': function(file, data, response) {
            $("#data_photo").val(data._raw);
            $("#img_data").attr('src', '/uploads/images/' + data._raw).show();
        }
    });

    uploader.on( 'fileQueued', function( file ) {
        $list.html( '<div id="' + file.id + '" class="item">' +
                '<h4 class="info">' + file.name + '</h4>' +
                '<p class="state">正在上传...</p>' +
                '</div>' );
    });

    // 文件上传成功
    uploader.on( 'uploadSuccess', function( file ) {
        $( '#'+file.id ).find('p.state').text('上传成功！');
    });

    // 文件上传失败，显示上传出错。
    uploader.on( 'uploadError', function( file ) {
        $( '#'+file.id ).find('p.state').text('上传出错!');
    });


    $(function(){
        var tmpl = '<li class="weui-uploader__file" style="background-image:url(#url#)"></li>',
                $gallery = $("#gallery"), $galleryImg = $("#galleryImg"),
                $uploaderInput = $("#uploaderInput"),
                $uploaderFiles = $("#uploaderFiles");
        $uploaderInput.on("change", function(e){
            var src, url = window.URL || window.webkitURL || window.mozURL, files = e.target.files;
            for (var i = 0, len = files.length; i < len; ++i) {
                var file = files[i];
                if (url) {
                    src = url.createObjectURL(file);
                } else {
                    src = e.target.result;
                }
                $uploaderFiles.html($(tmpl.replace('#url#', src)));
            }
        });
        $uploaderFiles.on("click", "li", function(){
            $galleryImg.attr("style", this.getAttribute("style"));
            $gallery.fadeIn(100);
        });
        $gallery.on("click", function(){
            $gallery.fadeOut(100);
        });





    });


    $(function(){
        $('#form').ajaxForm({
            beforeSubmit: checkForm, // 此方法主要是提交前执行的方法，根据需要设置
            success: complete, // 这是提交后的方法
            dataType: 'json'
        });

        function checkForm(){
                var title =   $('#title').val();
                var object_people =   $('#object_people').val();
                var end_time =   $('#end_time').val();
                var content =   $('#content').val();
                var photo =   $('#data_photo').val();
                var remark =   $('#remark').val();

                if(title==''){
                    layer.msg('标题不能为空',{icon:5,time:1000,shade: 0.1,});
                    return false;
                }

                if(end_time==''){
                    layer.msg('截止时间不能为空',{icon:5,time:1000,shade: 0.1,});
                    return false;
                }
                if(content==''){
                    layer.msg('内容不能为空',{icon:5,time:1000,shade: 0.1,});
                    return false;
                }
                if(remark==''){
                    layer.msg('描述不能为空',{icon:5,time:1000,shade: 0.1,});
                    return false;
                }
                if(object_people==''){
                    layer.msg('征文对象不能为空',{icon:5,time:1000,shade: 0.1,});
                    return false;
                }
        }

        function complete(data){
            if(data.code == 1){
                layer.msg(data.msg, {icon: 6,time:1500,shade: 0.1}, function(index){
                    layer.close(index);
                    window.location.href="<?php echo url('Usercenter/index'); ?>";
                });
            }else{
                layer.msg(data.msg, {icon: 5,time:1500,shade: 0.1}, function(index){
                    layer.close(index);
                });
                return false;
            }
        }

    });




    //底部导航
    $(function(){
        $('.weui-tabbar__item').on('click', function () {
            $(this).addClass('weui-bar__item_on').siblings('.weui-bar__item_on').removeClass('weui-bar__item_on');
        });
    });


</script>

</html>