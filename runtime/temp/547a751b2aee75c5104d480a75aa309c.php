<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:86:"/data/www/html/web2017/public_html/../application/index/view/usercenter/myarticle.html";i:1498460386;}*/ ?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>我的文章</title>
    <!-- 最新版本的 Bootstrap 核心 CSS 文件 -->
    <!--<link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">-->
    <link rel="stylesheet" href="__STATIC__/style/weui.min.css">
    <link rel="stylesheet" href="__STATIC__/style/cmgd.css">
    <script src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="/static/admin/js/layer/layer.js"></script>
    <!--奥森图标-->
    <link rel="stylesheet" href="__STATIC__/style/FontAwesome_4.2.0/css/font-awesome.min.css">
    <style>
        .text{padding-left:9px; }
        .fa-list-ul{padding-right:3px;}
        .weui-media-box__desc{height: 35px;line-height: 35px;}
        .weui-media-box__desc img{width:22px;height:22px;}
        .weui-navbar{top:5px;}
        .weui-navbar+.weui-tab__panel{
            padding-top: 62px;
            padding-bottom: 60px;
        }
    </style>
</head>
<body>

<div class="container" id="container">

    <!--顶部搜索-->
    <div class="page home js_show">

        <div class="page__bd" style="height: 100%;">
            <!--选择导航-->
            <div class="weui-tab" id="navbar">

                <div class="weui-navbar">
                    <div class="weui-navbar__item weui-bar__item_on">我的文章</div>
                    <?php if($group_id == 8): ?>
                    <div class="weui-navbar__item">我的征文</div>
                    <?php endif; ?>
                    <div class="weui-navbar__item">我的话题</div>
                </div>

                <div class="weui-tab__panel">
                    <div id="wenzhang">
                        <div class="weui-panel articlelist">
                            <div class="page__bd" >
                                <div class="weui-panel weui-panel_access">
                                    <div class="weui-panel__hd" style="font-size:16px;color: #000;"><i class="fa fa-list-ul"></i>我的文章</div>
                                    <div class="weui-panel__bd">
                                        <?php if(is_array($article) || $article instanceof \think\Collection || $article instanceof \think\Paginator): if( count($article)==0 ) : echo "" ;else: foreach($article as $key=>$article): ?>
                                        <div class="weui-media-box weui-media-box_appmsg">
                                            <?php if($article['photo'] !=''): ?>
                                        <a href="<?php echo url('index/article_detail',array('id'=>$article['id'])); ?>">
                                            <div class="weui-media-box__hd">
                                                <img class="weui-media-box__thumb" src="/uploads/images/<?php echo $article['photo']; ?>" height="100%" alt="">
                                            </div>
                                        </a>
                                            <?php endif; ?>
                                            <div class="weui-media-box__bd">
                                                <a href="<?php echo url('index/article_detail',array('id'=>$article['id'])); ?>" style="color: #000;">  <h4 class="weui-media-box__title"><?php echo $article['title']; ?></h4> </a>
                                                <p class="weui-media-box__desc" >
                                                     <?php echo $article['create_time']; ?>
                                                    &nbsp;&nbsp;
                                                <?php if($article['status'] == -4): ?><span style="color: red;">已驳回</span><?php endif; if($article['status'] == 0): ?><span style="color: red;">待审核</span><?php endif; if($article['status'] == 1): ?>已发布
                                                    <?php if($article['is_want_tui'] == 0): ?>
                                                    <span style="padding: 5px;background:#2fab87;color: #fff;border-radius: 8px;" id="tj<?php echo $article['id']; ?>"  onclick="wytj(<?php echo $article['id']; ?>)">我要推荐</span>
                                                    <?php endif; if($article['is_want_tui'] == 1): ?> <span style="padding: 5px;background:#2fab87;color: #fff;border-radius: 8px;"  >已推荐</span><?php endif; endif; ?>

                                                    <a href="javascript:;" onclick="zhiding(<?php echo $article['id']; ?>,'article')"  class="btn btn-primary btn-xs btn-outline" id="zhiding" style="padding: 5px;background:#2fab87;color: #fff;border-radius: 8px;">
                                                      <?php if($article['my_ding']==0): ?>
                                                        置顶
                                                      <?php else: ?>
                                                        已置顶
                                                      <?php endif; ?>
                                                    </a>

                                                </p>
                                            </div>

                                            </div>
                                        <?php endforeach; endif; else: echo "" ;endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php if($group_id == 8): ?>
                    <div id="zhengwen">

                        <div class="weui-panel articlelist">
                            <div class="page__bd" >
                                <div class="weui-panel weui-panel_access">
                                    <div class="weui-panel__hd" style="font-size:16px;color: #000;"><i class="fa fa-list-ul"></i>我的征文</div>
                                    <div class="weui-panel__bd">
                                        <?php if(is_array($essay) || $essay instanceof \think\Collection || $essay instanceof \think\Paginator): if( count($essay)==0 ) : echo "" ;else: foreach($essay as $key=>$essay): ?>
                                        <div class="weui-media-box weui-media-box_appmsg">
                                            <?php if($essay['photo'] !=''): ?>
                                            <a href="<?php echo url('index/essay_detail',array('id'=>$essay['id'])); ?>">
                                                <div class="weui-media-box__hd">
                                                    <img class="weui-media-box__thumb" src="/uploads/images/<?php echo $essay['photo']; ?>" height="100%" alt="">
                                                </div>
                                            </a>
                                            <?php endif; ?>
                                            <div class="weui-media-box__bd">
                                                <a href="<?php echo url('index/essay_detail',array('id'=>$essay['id'])); ?>" style="color: #000;">  <h4 class="weui-media-box__title"><?php echo $essay['title']; ?></h4> </a>
                                                <p class="weui-media-box__desc" >
                                                    <?php echo $essay['create_time']; ?>
                                                    <a href="javascript:;" onclick="zhiding(<?php echo $essay['id']; ?>,'essay')"  class="btn btn-primary btn-xs btn-outline" id="zhiding2" style="color: #fff;background:#2fab87;">
                                                        <?php if($essay['my_ding']==0): ?>
                                                        置顶
                                                        <?php else: ?>
                                                        已置顶
                                                        <?php endif; ?>
                                                    </a>

                                                </p>
                                            </div>
                                        </div>
                                        <?php endforeach; endif; else: echo "" ;endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>

                <?php endif; ?>

                    <div id="huati">

                        <div class="weui-panel articlelist">
                            <div class="page__bd" >
                                <div class="weui-panel weui-panel_access">
                                    <div class="weui-panel__hd" style="font-size:16px;color: #000;"><i class="fa fa-list-ul"></i>我的话题</div>
                                    <div class="weui-panel__bd">
                                        <?php if(is_array($debate) || $debate instanceof \think\Collection || $debate instanceof \think\Paginator): if( count($debate)==0 ) : echo "" ;else: foreach($debate as $key=>$debate): ?>
                                        <div class="weui-media-box weui-media-box_appmsg">
                                            <?php if($debate['photo'] !=''): ?>
                                            <a href="<?php echo url('index/debate_detail',array('id'=>$debate['id'])); ?>">
                                                <div class="weui-media-box__hd">
                                                    <img class="weui-media-box__thumb" src="/uploads/images/<?php echo $debate['photo']; ?>" height="100%" alt="">
                                                </div>
                                            </a>
                                            <?php endif; ?>
                                            <div class="weui-media-box__bd">
                                                <a href="<?php echo url('index/debate_detail',array('id'=>$debate['id'])); ?>" style="color: #000;">  <h4 class="weui-media-box__title"><?php echo $debate['title']; ?></h4> </a>
                                                <p class="weui-media-box__desc" >
                                                    <?php echo $debate['create_time']; ?>
                                                    <a href="javascript:;" onclick="zhiding(<?php echo $debate['id']; ?>,'debate')"  class="btn btn-primary btn-xs btn-outline" id="zhiding1" style="color: #fff;background:#2fab87;">
                                                        <?php if($debate['my_ding']==0): ?>
                                                        置顶
                                                        <?php else: ?>
                                                        已置顶
                                                        <?php endif; ?>
                                                    </a>

                                                </p>
                                            </div>
                                        </div>
                                        <?php endforeach; endif; else: echo "" ;endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>

<script type="text/javascript">
    //        顶部搜索
    $("#zhengwen").hide();
    $("#huati").hide();

    //顶部导航
    $(function(){
        $('.weui-navbar__item').on('click', function () {
            $(this).addClass('weui-bar__item_on').siblings('.weui-bar__item_on').removeClass('weui-bar__item_on');

            var cc = $(this).text();
            if(cc=='我的文章'){
                $("#zhengwen").hide();
                $("#huati").hide();
                $("#wenzhang").show();
            }

            if(cc=='我的话题'){
                $("#wenzhang").hide();
                $("#zhengwen").hide();
                $("#huati").show();

            }

            if(cc=='我的征文'){
                $("#wenzhang").hide();
                $("#zhengwen").show();
                $("#huati").hide();

            }
        });
    });
    //底部导航
    $(function(){
        $('.weui-tabbar__item').on('click', function () {
            $(this).addClass('weui-bar__item_on').siblings('.weui-bar__item_on').removeClass('weui-bar__item_on');
        });
    });

    function wytj(i){
        var cq = '#tj'+i;
        var cw = $(cq).html();
        if(cw=='我要推荐'){
            $.post('<?php echo url("usercenter/want_recommend_article"); ?>',
                    {id:i},
                    function(data){
//                        if(data.code==11){
//                            layer.msg(data.msg,{icon:5,time:1500,shade: 0.1,});
//                            window.location.href="<?php echo url('user/login'); ?>";
//                        }else {
//
//
//                        }
                        if(data.code==1){
                            $(cq).html('已推荐');
                        }
                        layer.msg(data.msg,{icon:data.code,time:1500,shade: 0.1,});




                    });
        }
    }

    function zhiding(id,type){

        $.getJSON('<?php echo url("base/index/my_ding"); ?>', {id:id,type:type}, function(res){
            if(res.code == 1){
                layer.msg(res.msg,{icon:1,time:1500,shade: 0.1});


                if(type=='article'){
                    $('#zhiding').html('已置顶');
                }

                if(type=='debate'){
                    $('#zhiding1').html('已置顶');
                }

                if(type=='essay'){
                    $('#zhiding2').html('已置顶');
                }


            }else{
                layer.msg(res.msg,{icon:1,time:1500,shade: 0.1});
                if(type=='article'){
                    $('#zhiding').html('置顶');
                }

                if(type=='debate'){
                    $('#zhiding1').html('置顶');
                }
                if(type=='essay'){
                    $('#zhiding2').html('置顶');
                }
            }
        });
    }
</script>

</html>