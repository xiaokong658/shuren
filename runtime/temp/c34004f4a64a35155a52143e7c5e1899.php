<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:86:"/data/www/html/web2017/public_html/../application/index/view/article/debate_write.html";i:1498578812;}*/ ?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>发表话题</title>
    <!-- 最新版本的 Bootstrap 核心 CSS 文件 -->
    <!--<link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">-->
    <link rel="stylesheet" href="__STATIC__/style/weui.min.css">
    <link rel="stylesheet" href="__STATIC__/style/cmgd.css">
    <script src="/static/admin/js/jquery.min.js?v=2.1.4"></script>
    <script src="/static/admin/js/jquery.form.js"></script>
    <script src="/static/admin/js/plugins/layer/laydate/laydate.js"></script>
    <script src="/static/pc/js/layer/layer.js"></script>
    <script src="/static/admin/js/laypage/laypage.js"></script>
    <script src="/static/admin/js/laytpl/laytpl.js"></script>
    <!--奥森图标-->
    <link rel="stylesheet" href="__STATIC__/style/FontAwesome_4.2.0/css/font-awesome.min.css">
    <style>
        .text{padding-left:9px; }
        .weui-uploader__input-box{margin-left: 10px;}
        .fa-list-ul{padding-right:8px;}
        .red{color:red;}
        .weui-btn_primary{width:90%;}
    </style>
</head>
<body>

<div class="container" id="container">

    <!--顶部搜索-->
    <div class="page home js_show">

        <form id="form" action="<?php echo url('debate_write'); ?>" enctype="multipart/form-data" method="post">

            <div class="page__bd" style="height: 100%;">
                <div class="weui-cells__title">标题<span class="red">*</span></div>
                <div class="weui-cells">
                    <div class="weui-cell">
                        <div class="weui-cell__bd">
                            <input class="weui-input" name="title" id="title" type="text" placeholder="请输入标题" value="">
                        </div>
                    </div>
                </div>

                <div class="weui-cells__title">是否存在正反观点</div>
                <div class="weui-cells">
                    <div class="weui-cell">
                        <div class="weui-cell__bd">
                            <select name="is_zf" class="weui-input" id="is_zf" >
                                <option value="0" >不存在</option>
                                <option value="1" >存在</option>
                            </select>
                        </div>
                    </div>
                </div>



                <div style="display: none;" id="zf">
                <div class="weui-cells__title">正方观点</div>
                <div class="weui-cells">
                    <div class="weui-cell">
                        <div class="weui-cell__bd">
                            <input class="weui-input" name="zhengfang" id="zhengfang" type="text" placeholder="请输入观点" value="">
                        </div>
                    </div>
                </div>

                <div class="weui-cells__title">反方观点</div>
                <div class="weui-cells">
                    <div class="weui-cell">
                        <div class="weui-cell__bd">
                            <input class="weui-input" name="fanfang" id="fanfang" type="text" placeholder="请输入观点" value="">
                        </div>
                    </div>
                </div>
                </div>


                <div class="weui-cells__title">截至时间<span class="red">*</span></div>
                    <div class="weui-cells">
                        <div class="weui-cell">
                            <div class="weui-cell__hd"><label for="" class="weui-label">日期</label></div>
                            <div class="weui-cell__bd">
                                <input class="weui-input" type="date" id="end_time" name="end_time" value="">
                            </div>
                        </div>
                    </div>



                <div class="weui-cells__title">内容<span class="red">*</span></div>
                <div class="weui-cells weui-cells_form">
                    <div class="weui-cell">
                        <div class="weui-cell__bd">
                            <textarea class="weui-textarea" name="content"  id="content" placeholder="请输入内容 " rows="5"></textarea>
                        </div>
                    </div>
                </div>



                <div class="weui-cells__title">上传封面<span class="red">*</span></div>
                <div class="weui-cells weui-cells_form" style="padding-top:6px;">
                    <div class="weui-cell__bd">
                        <div class="weui-uploader">
                            <div class="weui-uploader__bd">
                                <ul class="weui-uploader__files" id="uploaderFiles"></ul>
                                <div class="weui-uploader__input-box">
                                    <input id="uploaderInput" name="img" class="weui-uploader__input" type="file"  />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <button class="weui-btn weui-btn_primary" type="submit" id="fabu" > 发布 </button>

            </div>
        </form>
    </div>
</div>
</body>
<script type="text/javascript" src="/static/admin/webupload/webuploader.min.js"></script>
<script type="text/javascript">

    $(function(){
        var tmpl = '<li class="weui-uploader__file" style="background-image:url(#url#)"></li>',
                $gallery = $("#gallery"), $galleryImg = $("#galleryImg"),
                $uploaderInput = $("#uploaderInput"),
                $uploaderFiles = $("#uploaderFiles");
        $uploaderInput.on("change", function(e){
            var src, url = window.URL || window.webkitURL || window.mozURL, files = e.target.files;
            for (var i = 0, len = files.length; i < len; ++i) {
                var file = files[i];
                if (url) {
                    src = url.createObjectURL(file);
                } else {
                    src = e.target.result;
                }
                $uploaderFiles.html($(tmpl.replace('#url#', src)));
            }
        });
        $uploaderFiles.on("click", "li", function(){
            $galleryImg.attr("style", this.getAttribute("style"));
            $gallery.fadeIn(100);
        });
        $gallery.on("click", function(){
            $gallery.fadeOut(100);
        });


        $("#is_want_tui").bind("click", function () {

            if($("#is_want_tui").val()=="0"){
                $("#is_want_tui").val("1");
            }else{
                $("#is_want_tui").val("0");
            }

        });



    });


    $(function(){
        $('#form').ajaxForm({
            beforeSubmit: checkForm, // 此方法主要是提交前执行的方法，根据需要设置
            success: complete, // 这是提交后的方法
            dataType: 'json'
        });

        function checkForm(){

            var title =   $('#title').val();
            var content =   $('#content').val();
            var is_zf =   $('#is_zf').val();
            var fanfang =   $('#fanfang').val();
            var zhengfang =   $('#zhengfang').val();
            var end_time =   $('#end_time').val();
            var is_img =   $('#uploaderFiles').html();

            if(title==''){
                layer.msg('标题不能为空',{icon:5,time:1000,shade: 0.1,});
                return false;
            }

            if(is_zf==1){
                if(zhengfang==''){
                    layer.msg('正方观点不能为空',{icon:5,time:1000,shade: 0.1,});
                    return false;
                }

                if(fanfang==''){
                    layer.msg('反方观点不能为空',{icon:5,time:1000,shade: 0.1,});
                    return false;
                }
            }
            if(end_time==''){
                layer.msg('截至时间不能为空',{icon:5,time:1000,shade: 0.1,});
                return false;
            }

            if(content==''){
                layer.msg('内容不能为空',{icon:5,time:1000,shade: 0.1,});
                return false;
            }





        }

        function complete(data){
            if(data.code == 1){
                layer.msg(data.msg, {icon: 6,time:1500,shade: 0.1}, function(index){
                    layer.close(index);
                    window.location.href="<?php echo url('Usercenter/index'); ?>";
                });
            }else{
                layer.msg(data.msg, {icon: 5,time:1500,shade: 0.1}, function(index){
                    layer.close(index);
                });
                return false;
            }
        }

    });

    function save(){
        var title =   $('#title').val();

        if(title==''){
            layer.msg('标题不能为空',{icon:5,time:1000,shade: 0.1,});
            return false;
        }

        $('#form').submit()

    }


    //底部导航
    $(function(){
        $('.weui-tabbar__item').on('click', function () {
            $(this).addClass('weui-bar__item_on').siblings('.weui-bar__item_on').removeClass('weui-bar__item_on');
        });
    });


    $('#is_zf').change(
            function(){
                var c = $('#is_zf').val();
                if(c==0){
                    $('#zf').hide();
                }
                if(c==1){
                    $('#zf').show();
                }
            }
    );


</script>

</html>