<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:91:"/data/www/html/web2017/public_html/../application/admin/view/course/nianji_course_edit.html";i:1498352154;s:79:"/data/www/html/web2017/public_html/../application/admin/view/public/header.html";i:1484102488;s:79:"/data/www/html/web2017/public_html/../application/admin/view/public/footer.html";i:1487735376;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo config('WEB_SITE_TITLE'); ?></title>
    <link href="/static/admin/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/admin/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/static/admin/css/animate.min.css" rel="stylesheet">
    <link href="/static/admin/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="/static/admin/css/plugins/chosen/chosen.css" rel="stylesheet">
    <link href="/static/admin/css/plugins/switchery/switchery.css" rel="stylesheet">
    <link href="/static/admin/css/style.min.css?v=4.1.0" rel="stylesheet">
    <link href="/static/admin/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <style type="text/css">
    .long-tr th{
        text-align: center
    }
    .long-td td{
        text-align: center
    }
    </style>
</head>
<link rel="stylesheet" type="text/css" href="/static/admin/webupload/webuploader.css">
<link rel="stylesheet" type="text/css" href="/static/admin/webupload/style.css">
<style>
    .file-item{float: left; position: relative; width: 110px;height: 110px; margin: 0 20px 20px 0; padding: 4px;}
    .file-item .info{overflow: hidden;}
    .uploader-list{width: 100%; overflow: hidden;}
</style>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>编辑课程</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="form_basic.html#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" name="userEdit" id="userEdit" method="post" action="<?php echo url('nianji_course_edit'); ?>">

                        <div class="form-group" >
                            <table class="table table-bordered table-hover" id="table">
                                <thead>
                                <tr class="long-tr">
                                    <th>科目</th>
                                    <th>学时</th>
                                    <th>操作</th>
                                </tr>
                                </thead>

                                <?php if((!empty($info))): if(is_array($info) || $info instanceof \think\Collection || $info instanceof \think\Paginator): if( count($info)==0 ) : echo "" ;else: foreach($info as $key=>$vv): ?>
                                    <tr id="ccc_<?php echo $vv['id']; ?>">
                                        <td>
                                            <select class="form-control" name="name[]">
                                                <?php if(is_array($course) || $course instanceof \think\Collection || $course instanceof \think\Paginator): if( count($course)==0 ) : echo "" ;else: foreach($course as $key=>$vo): ?>
                                                     <option value='<?php echo $vo['name']; ?>' <?php if($vo['name'] == $vv['name']): ?> selected <?php endif; ?>><?php echo $vo['name']; ?></option>
                                                <?php endforeach; endif; else: echo "" ;endif; ?>
                                            </select>
                                        </td>
                                        <td><input type="text" name="num[]" class="form-control" value="<?php echo $vv['num']; ?>" ></td>
                                        <td style="text-align: center;"><a href="javascript:;" onclick="delinput(<?php echo $vv['id']; ?>)" class="btn btn-danger btn-xs">
                                            <i class="fa fa-trash-o"></i> 删除</a></td>
                                    </tr>
                                <?php endforeach; endif; else: echo "" ;endif; else: ?>
                                    <!--<tr id="ccc_1">-->
                                        <!--<td>-->
                                            <!--<select class="form-control" name="name[]">-->
                                                <!--<?php if(is_array($course) || $course instanceof \think\Collection || $course instanceof \think\Paginator): if( count($course)==0 ) : echo "" ;else: foreach($course as $key=>$vo): ?>-->
                                                <!--<option value='<?php echo $vo['name']; ?>'><?php echo $vo['name']; ?></option>-->
                                                <!--<?php endforeach; endif; else: echo "" ;endif; ?>-->
                                            <!--</select>-->
                                        <!--</td>-->
                                        <!--<td><input type="text" name="num[]" class="form-control"  ></td>-->
                                        <!--<td style="text-align: center;"><a href="javascript:;" onclick="delinput(1)" class="btn btn-danger btn-xs">-->
                                            <!--<i class="fa fa-trash-o"></i> 删除</a></td>-->
                                    <!--</tr>-->

                                <?php endif; ?>





                             </table>
                            <p style="text-align: center;">
                                <a href="javascript:;" class="btn btn-primary" onclick="addinput();"> 新增课程 </a>
                            </p>







                        </div>



                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-3">
                                <input type="hidden" name="nianji" value="<?php echo $id; ?>" >
                                <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> 保存</button>&nbsp;&nbsp;&nbsp;
                                <a class="btn btn-danger" href="javascript:history.go(-1);"><i class="fa fa-close"></i> 返回</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<script src="__JS__/jquery.min.js?v=2.1.4"></script>
<script src="__JS__/bootstrap.min.js?v=3.3.6"></script>
<script src="__JS__/content.min.js?v=1.0.0"></script>
<script src="__JS__/plugins/chosen/chosen.jquery.js"></script>
<script src="__JS__/plugins/iCheck/icheck.min.js"></script>
<script src="__JS__/plugins/layer/laydate/laydate.js"></script>
<script src="__JS__/plugins/switchery/switchery.js"></script><!--IOS开关样式-->
<script src="__JS__/jquery.form.js"></script>
<script src="__JS__/layer/layer.js"></script>
<script src="__JS__/laypage/laypage.js"></script>
<script src="__JS__/laytpl/laytpl.js"></script>
<script src="__JS__/lunhui.js"></script>
<script>
    $(document).ready(function(){$(".i-checks").iCheck({checkboxClass:"icheckbox_square-green",radioClass:"iradio_square-green",})});
</script>
<script type="text/javascript" src="/static/admin/webupload/webuploader.min.js"></script>
<script type="text/javascript">
    //提交
    $(function(){
        $('#userEdit').ajaxForm({
            beforeSubmit: checkForm,
            success: complete,
            dataType: 'json'
        });

        function checkForm(){
            $("input:text").each(function() {
                if($.trim($(this).val()) == "") {
                    layer.msg('学时不能为空', {icon: 5,time:1500,shade: 0.1});
                    return false;
                }
            });
        }

        function complete(data){
            if(data.code==1){
                layer.msg(data.msg, {icon: 6,time:1500,shade: 0.1}, function(index){
                    window.location.href="<?php echo url('course/nianji_course'); ?>";
                });
            }else{
                layer.msg(data.msg, {icon: 5,time:1500,shade: 0.1});
                return false;
            }
        }

    });

    function addinput(){

        var t='';
        for(var i=0;i<5;i++){
            t+=Math.floor(Math.random()*10);
        }
        t = "1"+t;
        var q = "ccc_"+t;
        var $htmlLi = $("<tr id='"+q+"'><td><select class='form-control' name='name[]'><?php if(is_array($course) || $course instanceof \think\Collection || $course instanceof \think\Paginator): if( count($course)==0 ) : echo "" ;else: foreach($course as $key=>$vo): ?><option value='<?php echo $vo['name']; ?>'><?php echo $vo['name']; ?></option><?php endforeach; endif; else: echo "" ;endif; ?></select></td><td><input type='text'name='num[]'class='form-control'></td><td style='text-align: center;'><a href='javascript:;' onclick='delinput("+t+")' class='btn btn-danger btn-xs'><i class='fa fa-trash-o'></i> 删除</a></td></tr>");  //创建DOM对象
        var $ul = $('#table');
        $ul.append($htmlLi);
    }

    function delinput(i){
        var q = "#ccc_"+i;
        $(q).remove();
    }



    //IOS开关样式配置
    var elem = document.querySelector('.js-switch');
    var switchery = new Switchery(elem, {
        color: '#1AB394'
    });
    var config = {
        '.chosen-select': {},
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

</script>
</body>
</html>