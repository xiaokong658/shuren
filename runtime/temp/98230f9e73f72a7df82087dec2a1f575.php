<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:85:"/data/www/html/web2017/public_html/../application/admin/view/article/articleread.html";i:1498730802;s:79:"/data/www/html/web2017/public_html/../application/admin/view/public/header.html";i:1484102488;s:79:"/data/www/html/web2017/public_html/../application/admin/view/public/footer.html";i:1487735376;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo config('WEB_SITE_TITLE'); ?></title>
    <link href="/static/admin/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/admin/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/static/admin/css/animate.min.css" rel="stylesheet">
    <link href="/static/admin/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="/static/admin/css/plugins/chosen/chosen.css" rel="stylesheet">
    <link href="/static/admin/css/plugins/switchery/switchery.css" rel="stylesheet">
    <link href="/static/admin/css/style.min.css?v=4.1.0" rel="stylesheet">
    <link href="/static/admin/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <style type="text/css">
    .long-tr th{
        text-align: center
    }
    .long-td td{
        text-align: center
    }
    </style>
</head>
<link rel="stylesheet" type="text/css" href="/static/admin/webupload/webuploader.css">
<link rel="stylesheet" type="text/css" href="/static/admin/webupload/style.css">
<style>
    .file-item{float: left; position: relative; width: 110px;height: 110px; margin: 0 20px 20px 0; padding: 4px;}
    .file-item .info{overflow: hidden;}
    .uploader-list{width: 100%; overflow: hidden;}
    .from{font-family:'微软雅黑';font-size: 16px;}
</style>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>查看文章</h5>
                </div>
                <div class="ibox-content">
                    <div class="from" style=" width: 100%;">
                        <h3> 作者原文：</h3>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group" style="float: left; width: 33%;">
                        <label class="col-sm-3 control-label">标题：</label>
                        <div class="input-group col-sm-8">
                            <?php echo $article['title']; ?>
                        </div>
                    </div>
                    <div class="form-group"  style="float: left; width: 33%;">
                        <label class="col-sm-3 control-label">描述：</label>
                        <div class="input-group col-sm-8">
                            <?php echo $article['remark']; ?>
                        </div>
                    </div>
                    <div class="form-group"  style="float: left; width: 33%;">
                        <label class="col-sm-3 control-label">封面：</label>
                        <div class="input-group col-sm-8">
                            <input type="hidden" id="data_photo" name="photo" value="<?php echo $article['photo']; ?>">
                            <div id="fileList" class="uploader-list" style="float:right"></div>
                            <!--<div id="imgPicker" style="float:left">选择图片</div>-->
                            <img id="img_data" height="100px" style="float:left;margin-left: 50px;margin-top: -10px;" onerror="this.src='/static/admin/images/no_img.jpg'" src="/uploads/images/<?php echo $article['photo']; ?>"/>
                        </div>
                    </div>







                    <div class="form-group" style="width: 100%;">
                        <label class="col-sm-1 control-label " for="myEditor">内容：</label>
                        <div class="input-group col-sm-11">
                            <?php echo $article['content']; ?>
                        </div>
                    </div>

                    <div class="form-group" style="width: 100%;">
                        <label class="col-sm-1 control-label " for="myEditor">时间：</label>
                        <div class="input-group col-sm-11">
                            <?php echo $article['create_time']; ?>
                        </div>
                    </div>

                    <?php if((!empty($article['baoming']))): ?>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group" >
                        <label class="col-sm-3 control-label">所属报名活动：</label>
                        <div class="input-group col-sm-4" style="text-align: left;"><?php echo $article['baoming']; ?></div>
                    </div>
                    <?php endif; if((!empty($article['vote']))): ?>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">所属投票活动：</label>
                        <div class="input-group col-sm-4"  style="text-align: left;"><?php echo $article['vote']; ?></div>
                    </div>
                    <?php endif; ?>

                </div>



                <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>


                <div class="ibox-content">
                    <div class="from" style=" width: 100%;">
                        <h3>修改来自：<?php echo $vo['username']; ?> </h3>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group" style="float: left; width: 33%;">
                        <label class="col-sm-3 control-label">标题：</label>
                        <div class="input-group col-sm-8">
                            <?php echo $vo['title']; ?>
                        </div>
                    </div>
                    <div class="form-group"  style="float: left; width: 33%;">
                        <label class="col-sm-3 control-label">描述：</label>
                        <div class="input-group col-sm-8">
                            <?php echo $vo['remark']; ?>
                        </div>
                    </div>
                    <div class="form-group"  style="float: left; width: 33%;">
                        <label class="col-sm-3 control-label">封面：</label>
                        <div class="input-group col-sm-8">
                            <input type="hidden" id="data_photo" name="photo" value="<?php echo $article['photo']; ?>">
                            <div id="fileList" class="uploader-list" style="float:right"></div>
                            <!--<div id="imgPicker" style="float:left">选择图片</div>-->
                            <img id="img_data" height="100px" style="float:left;margin-left: 50px;margin-top: -10px;" onerror="this.src='/static/admin/images/no_img.jpg'" src="/uploads/images/<?php echo $vo['photo']; ?>"/>
                        </div>
                    </div>
                    <div class="form-group" style="width: 100%;">
                        <label class="col-sm-1 control-label " for="myEditor">内容：</label>
                        <div class="input-group col-sm-11">
                            <?php echo $vo['content']; ?>
                        </div>
                    </div>

                    <div class="form-group" style="width: 100%;">
                        <label class="col-sm-1 control-label " for="myEditor">时间：</label>
                        <div class="input-group col-sm-11">
                            <?php echo $vo['update_time1']; ?>
                        </div>
                    </div>
                </div>




                <?php endforeach; endif; else: echo "" ;endif; ?>




            </div>

        </div>
    </div>
</div>
<script src="__JS__/jquery.min.js?v=2.1.4"></script>
<script src="__JS__/bootstrap.min.js?v=3.3.6"></script>
<script src="__JS__/content.min.js?v=1.0.0"></script>
<script src="__JS__/plugins/chosen/chosen.jquery.js"></script>
<script src="__JS__/plugins/iCheck/icheck.min.js"></script>
<script src="__JS__/plugins/layer/laydate/laydate.js"></script>
<script src="__JS__/plugins/switchery/switchery.js"></script><!--IOS开关样式-->
<script src="__JS__/jquery.form.js"></script>
<script src="__JS__/layer/layer.js"></script>
<script src="__JS__/laypage/laypage.js"></script>
<script src="__JS__/laytpl/laytpl.js"></script>
<script src="__JS__/lunhui.js"></script>
<script>
    $(document).ready(function(){$(".i-checks").iCheck({checkboxClass:"icheckbox_square-green",radioClass:"iradio_square-green",})});
</script>


</body>
</html>
