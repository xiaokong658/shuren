<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:85:"/data/www/html/web2017/public_html/../application/web/view/article/debate_detail.html";i:1498579250;s:78:"/data/www/html/web2017/public_html/../application/web/view/public/header4.html";i:1502875458;}*/ ?>
<html lang="zh-CN"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $info['title']; ?>-<?php echo $user_info['nickname']; ?>-树人号</title>

    <!-- Bootstrap core CSS -->
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="http://v3.bootcss.com/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/static/pc/cmgd1.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="http://v3.bootcss.com/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="http://v3.bootcss.com/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    
    <![endif]-->

    <link href="/static/admin/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <script type="text/javascript" src="__STATIC__/admin/js/jquery.min.js"></script>
    <script type="text/javascript" src="__STATIC__/weibo/js/jquery.json.js"></script>
    <script type="text/javascript" src="__STATIC__/admin/js/layer/layer.js" ></script>
    <script src="/static/admin/js/laytpl/laytpl.js"></script>
    <script src="/static/admin/js/laypage/laypage.js"></script>
    <link href="/static/pc/style.css" rel="stylesheet">
</head>

<body>

<header id="main-header">
    <div class="area">
        <div class="head-nav left">
            <ul>
                <li class="index"><a href="<?php echo url('index/index'); ?>"><img src="/static/pc/image/logo.png" STYLE="height: 40px;padding-right: 20px;"></a></li>
                <li><a href="<?php echo url('index/index'); ?>">树人网</a></li>             
                <li><a href="<?php echo url('article/essay'); ?>" target="_blank">征文活动</a></li>
                <li><a href="http://club.shuren100.com/"  target="_blank">树人论坛</a></li>
				<li><a href="http://baozhi.shuren100.com/" target="_blank">电子报纸</a></li>
				<li><a href="http://qikan.shuren100.com/" target="_blank">电子期刊</a></li>
				<li><a href="http://www.shuren100.com//indexold.html" target="_blank">返回老版</a></li>

            </ul>
        </div>
        <div id="head-login" class="right login">
            <div class="login">
                <?php if($is_login==1): ?>
                <a href="<?php echo url('article/user_index'); ?>" data-role="login-btn" class="login-sohu"><img src="/static/pc/image/user.png" ><?php echo $my_user_info['nickname']; ?></a>
                <a href="<?php echo url('user/center'); ?>" data-role="login-btn" class="login-sohu"><img src="/static/pc/image/home.png" >我的主页</a>
                <a href="<?php echo url('index/loginout'); ?>" data-role="login-btn" class="login-sohu">退出</a>
                <?php else: ?>
                <a href="<?php echo url('user/login'); ?>"  data-role="login-btn" class="login-sohu"><img src="/static/pc/image/user.png" >用户登录</a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</header>
<div class="container">
    <div class="blog-header">
        <ol class="breadcrumb">
            <li><a href="#">首页</a></li>
            <li><a href="#">话题</a></li>
            <li class="active">正文</li>
        </ol>

    </div>

    <div class="row">

        <div class="col-sm-3 ">
            <div class="column left">
                <div class="user-info" id="user-info">
                    <div class="user-pic">
                        <a href="<?php echo url('user/center',array('user_id'=>$user_id)); ?>" ><img src="/uploads/face/<?php echo $user_info['head_img']; ?>" alt="" style="width: 100%;"></a>
                    </div>
                    <h4><a href="<?php echo url('user/center',array('user_id'=>$user_id)); ?>" ><?php echo $user_info['nickname']; ?></a></h4>
                    <dl class="user-num">
                        <dd><span class="value" data-value="2729" data-role="info-article-num"><span class="num"><?php echo $author_info['article_count']; ?></span></span><p>文章</p></dd>
                        <dd><span class="value" data-value="" data-role="info-read-num"><span class="num"><?php echo $author_info['fans_count']; ?></span></span>粉丝</dd>
                    </dl>
                    <div class="user-more"><a href="<?php echo url('user/center',array('user_id'=>$author_info['id'])); ?>" >查看TA的文章&gt;</a></div>
                </div>
                <!--<div class="article-do" id="article-do" style="">-->
                <!--<div class="article-done">-->
                <!--<dl>-->
                <!--<dd class="comment-do"><a href="#comment_area"><span class="num" data-role="comment-count">0</span><em class="comment-icon icon"></em></a></dd>-->
                <!--</dl>-->
                <!--</div>-->
                <!--</div>-->
            </div>
        </div>
        <div class="col-sm-8 blog-main">
            <div class="blog-post">
                <h2 class="blog-post-title"><?php echo $info['title']; ?></h2>
                <p>开始时间:<?php echo $info['create_time1']; ?></p>
                <p>截止时间:<?php echo $info['end_time1']; ?></p>
                <?php if($info['is_zf'] ==1): ?>
                <p>正方：<?php echo $info['zhengfang']; ?></p>
                <p>反方：<?php echo $info['fanfang']; ?></p>  <?php endif; ?>
                <hr>
                <p><img src="/uploads/images/<?php echo $info['photo']; ?>" style="width: 100%;"></p>

                <?php echo $info['content']; ?>
            </div>

            <div style="text-align: center; font-size: 22px; font-weight: 0;"  >

                   <span style="padding: 13px 60px;border:1px #c2ccd1 solid;  margin-right: 20px; border-radius: 40px;">
                    <a class="cv1"  href="javascript:;"  onclick="dianzan(<?php echo $info['id']; ?>);"  >   <i id="zan" class="fa <?php if($is_zan == 1): ?>fa-thumbs-up<?php else: ?>fa-thumbs-o-up<?php endif; ?>"></i> </a>
                       <span id="zan1"><?php echo $zan; ?></span>
                       <b style="width:1px; padding:0 40px 0 40px; font-weight: 0;"><i style="border-left:1px #c2ccd1 solid;"></i></b>
                  <a class="cv1" href="javascript:;"  onclick="shoucang(<?php echo $info['id']; ?>);"  >  <i id="collect" class="fa <?php if($is_collect == 1): ?>fa-heart<?php else: ?>fa-heart-o<?php endif; ?>"></i> </a><span id="collect1"><?php echo $collect; ?></span>
                   </span>
            </div>

            <?php if($info['can']==1): ?>
            <div style="padding-top: 50px;">


                <div class="c-comment-header clear" style="height:50px;">
                    <div class="left c-opinion"><em class="c-line"></em>我来说两句</div>
                    <!--<div class="right"><span class="c-num-red">4</span>人参与，<span class="c-num-red">4</span>条评论</div>-->
                </div>

                <div style="margin-top: 18px;">
                    <?php if($info['is_zf'] ==1): ?>
                    <div style="margin:2px 0;">支持的观点:
                        <select name="type"  id="type" >
                                        <option value="0">不选择正反方观点，自己论述</option>
                                        <option value="1">支持正方</option>
                                        <option value="2">支持反方</option>
                            </select>
                    </div>
                    <?php endif; ?>
                    <textarea rows="5" style="width:757px; border-bottom: 0px;" id="content_msg" ></textarea>
                </div>
                <div style="text-align: center; padding-left: 645px; border-bottom: 1px rgb(169, 169, 169) solid;border-left: 1px rgb(169, 169, 169) solid;border-right: 1px rgb(169, 169, 169) solid;">
                    <a href="javascript:;" style="text-decoration: none;" onclick="send_pinglun(<?php echo $user_id; ?>,<?php echo $id; ?>)">  <p style="width: 110px;background: red; padding:5px 0 5px 0; margin: 0; font-size:14px; color: #fff; font-family: 'Microsoft Yahei'; "> 发表</p></a>
                </div>
            </div>
            <?php endif; ?>

            <div class="c-comment-content"><div>


                <div class="c-comment-title c-new" style="margin-top: 20px;">
                    <span><em class="c-line"></em>话题评论</span>
                </div>
                <div class="c-comment-main">



                    <script id="list-template" type="text/html">
                        {{# for(var i=0;i<d.length;i++){  }}
                        <div class="c-item-comment clear">
                            <div class="c-item-head left">
                                <div class="c-head-wrapper"><img src="/uploads/face/{{d[i].head_img}}" alt=""></div>
                            </div>
                            <div class="c-item-content">
                                <div class="c-item-info clear">
                                    <div class="c-username">{{d[i].nickname}}
                                        {{# if(d[i].note==''){ }}
                                        {{# }else{ }}
                                        <sapn style="margin-left: 40px; padding: 3px 5px; background:#2fab87; color:#fff; border-radius: 3px;">支持{{d[i].note}}</sapn>
                                        {{# } }}
                                    </div>
                                </div>
                                <div class="c-user-content">
                                    <div class="c-discuss">{{d[i].content}}</div>
                                    <div class="c-controller" >
                                        <div class="c-date left" >{{d[i].add_time}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{# } }}
                    </script>

                    <div id="list-content"></div>





                </div>



        </div>
    </div>
</div>
</div>

   </div>
    <script type="text/javascript">
        var now = 1;
        AjaxPage();
        function AjaxPage(){
            var article_id = '<?php echo $id; ?>';
            console.log(article_id+'|'+now);
            $.post('<?php echo url("article/debate_list"); ?>',
                    {id:article_id,page:now},
                    function(data){
                        if(data=='') {
                        }else{
                            console.log(data);
                            var tpl = document.getElementById('list-template').innerHTML;
                            //   console.log(tpl);
                            laytpl(tpl).render(data.list, function(html){
                                // document.getElementById('list-content').innerHTML = html;
                                $('#list-content').append(html);
                            });
                        }
                        now ++;
                    });
        }


        function dianzan(id){

            zan_count = $('#zan1').html();
            zan_count = parseInt(zan_count);

            $.post('<?php echo url("article/debate_list_zan_status"); ?>',
                    {id:id},
                    function(data){

                        console.log(data)


                        if(data.code==11){
                            layer.msg(data.msg,{icon:5,time:1500,shade: 0.1,});
                            window.location.href="<?php echo url('user/login'); ?>";
                        }else {
                            if(data.code==1){
                                $('#zan').removeClass('fa-thumbs-o-up');
                                $('#zan').addClass('fa-thumbs-up');

                                zan_count = zan_count+1;
                                $('#zan1').html(zan_count);

                                layer.msg(data.msg,{icon:1,time:1500,shade: 0.1,});
                            }else{
                                $('#zan').removeClass('fa-thumbs-up');
                                $('#zan').addClass('fa-thumbs-o-up');
                                zan_count = zan_count-1;
                                $('#zan1').html(zan_count);
                                layer.msg(data.msg,{icon:5,time:1500,shade: 0.1,});
                            }
                        }

                    });
        }
        function shoucang(id){
            collect_count = $('#collect1').html();
            collect_count = parseInt(collect_count);
            $.post('<?php echo url("article/debate_zan_status"); ?>',
                    {id:id},
                    function(data){
                        if(data.code==11){
                            layer.msg(data.msg,{icon:5,time:1500,shade: 0.1,});
                            window.location.href="<?php echo url('user/login'); ?>";
                        }else{
                            if(data.code==1){
                                $('#collect').removeClass('fa-heart-o');
                                $('#collect').addClass('fa-heart');
                                collect_count = collect_count+1;
                                $('#collect1').html(collect_count);
                                layer.msg(data.msg,{icon:1,time:1500,shade: 0.1,});
                            }else{

                                $('#collect').removeClass('fa-heart');
                                $('#collect').addClass('fa-heart-o');
                                collect_count = collect_count-1;
                                $('#collect1').html(collect_count);
                                layer.msg(data.msg,{icon:5,time:1500,shade: 0.1,});
                            }
                        }
                    });
        }



        function send_pinglun(user_id,article_id) {
            var content = $('#content_msg').val();
            var user_id =user_id;
            var id = article_id;
            var is_zf = '<?php echo $info['is_zf']; ?>';
            if(is_zf==1){
                type =  $('#type').val();
            }else{
                type = -1;
            }




            if (content == '') {
                layer.msg('请填写您的观点描述！', {icon: 5, time: 1500, shade: 0.1,});
                return false;
            }

            console.log(id +'|'+ user_id  +'|'+ content  +'|'+ type);

            $.post('<?php echo url("article/debate_comment"); ?>',
                    {id: id, user_id: user_id, content: content, type: type},
                    function (data) {
                        if (data.code == 11) {
                            layer.msg(data.msg, {icon: 5, time: 1500, shade: 0.1,});
                            window.location.href = "<?php echo url('user/login'); ?>";
                        } else {
                            if (data.code == 1) {
                                if (type == 1) {
                                    var types = "<sapn style='margin-left: 40px; padding: 3px 5px; background:#2fab87; color:#fff; border-radius: 3px;'>支持正方</sapn>";
                                } else if (type == 2) {
                                    var types = "<sapn style='margin-left: 40px; padding: 3px 5px; background:#2fab87; color:#fff; border-radius: 3px;'>支持反方</sapn>";
                                } else {
                                    var types = '';
                                }

                                //$html =  "<span  class='weui-media-box weui-media-box_appmsg'><div class='weui-media-box__hd'> <img class='weui-media-box__thumb' src='/uploads/face/"+data.list.img+"' alt=''></div><div class='weui-media-box__bd'><h5 class='weui-media-box__desc'>"+data.list.nickname+"</h5><p class='weui-media-box__desc commentlist'>"+data.list.content+"</p><small class='weui-media-box__desc minTime'> <span style='padding: 4px;background:#0d8ddb;color: #fff;'>"+types+"</span>&nbsp;&nbsp;&nbsp;"+data.list.add_time+"</small><small class='thumbup'> <a id='comment1' onclick='comment("+data.list.id+",this)'><i class='fa fa-comment-o' id='pl_"+data.list.id+"'>0</i></a><i class='fa fa-thumbs-o-up' onclick='thumbsUp("+data.list.id+",this)'> <span id='zan_"+data.list.id+"'>0</span></span></i></small> </div></span>";
                                $html = "<div class='c-item-comment clear'><div class='c-item-head left'><div class='c-head-wrapper'><img src='" + data.list.img + "' alt=''></div></div><div class='c-item-content'><div class='c-item-info clear'><div class='c-username'>" + data.list.nickname + types + "</div></div><div class='c-user-content'><div class='c-discuss'>" + data.list.content + "</div><div class='c-controller' ><div class='c-date left' >" + data.list.add_time + "</div></div></div></div></div>"


                                $("#list-content").prepend($html);
                                document.getElementById('content_msg').value='';
                            }
                        }
                    });
        }





        </script>

</body></html>