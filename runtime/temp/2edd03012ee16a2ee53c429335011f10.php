<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:86:"/data/www/html/web2017/public_html/../application/admin/view/course/nianji_course.html";i:1488960426;s:79:"/data/www/html/web2017/public_html/../application/admin/view/public/header.html";i:1484102488;s:79:"/data/www/html/web2017/public_html/../application/admin/view/public/footer.html";i:1487735376;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo config('WEB_SITE_TITLE'); ?></title>
    <link href="/static/admin/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/admin/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/static/admin/css/animate.min.css" rel="stylesheet">
    <link href="/static/admin/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="/static/admin/css/plugins/chosen/chosen.css" rel="stylesheet">
    <link href="/static/admin/css/plugins/switchery/switchery.css" rel="stylesheet">
    <link href="/static/admin/css/style.min.css?v=4.1.0" rel="stylesheet">
    <link href="/static/admin/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <style type="text/css">
    .long-tr th{
        text-align: center
    }
    .long-td td{
        text-align: center
    }
    </style>
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <!-- Panel Other -->
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>课程列表</h5>
        </div>
        <div class="ibox-content">
            <!--搜索框开始-->
            <div class="row">

            </div>
            <!--搜索框结束-->
            <div class="hr-line-dashed"></div>

            <div class="example-wrap">
                <div class="example">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr class="long-tr">
                            <th>ID</th>
                            <th>所属学校</th>
                            <th>年级</th>
                            <th>操作</th>
                        </tr>
                        </thead>


                        <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): if( count($list)==0 ) : echo "" ;else: foreach($list as $key=>$vo): ?>
                            <tr class="long-td">
                                <td><?php echo $vo['id']; ?></td>
                                <td><?php echo $instit_info['name']; ?></td>
                                <td><?php echo $vo['nianji']; ?></td>
                                <td>
                                    <a href="javascript:;" onclick="nianji_course_edit(<?php echo $vo['id']; ?>)" class="btn btn-primary btn-xs">
                                        <i class="fa fa-paste"></i> 编辑</a>&nbsp;&nbsp;
                                    <!--<a href="javascript:;" onclick="course_del({{d[i].id}})" class="btn btn-danger btn-xs">-->
                                    <!--<i class="fa fa-trash-o"></i> 删除</a>-->
                                </td>
                            </tr>
                        <?php endforeach; endif; else: echo "" ;endif; ?>

                        <tbody id="list-content"></tbody>
                    </table>
                    <div id="AjaxPage" style=" text-align: right;"></div>
                    <div id="allpage" style=" text-align: right;"></div>
                </div>
            </div>
            <!-- End Example Pagination -->
        </div>
    </div>
</div>
<!-- End Panel Other -->
</div>

<!-- 加载动画 -->


<script src="__JS__/jquery.min.js?v=2.1.4"></script>
<script src="__JS__/bootstrap.min.js?v=3.3.6"></script>
<script src="__JS__/content.min.js?v=1.0.0"></script>
<script src="__JS__/plugins/chosen/chosen.jquery.js"></script>
<script src="__JS__/plugins/iCheck/icheck.min.js"></script>
<script src="__JS__/plugins/layer/laydate/laydate.js"></script>
<script src="__JS__/plugins/switchery/switchery.js"></script><!--IOS开关样式-->
<script src="__JS__/jquery.form.js"></script>
<script src="__JS__/layer/layer.js"></script>
<script src="__JS__/laypage/laypage.js"></script>
<script src="__JS__/laytpl/laytpl.js"></script>
<script src="__JS__/lunhui.js"></script>
<script>
    $(document).ready(function(){$(".i-checks").iCheck({checkboxClass:"icheckbox_square-green",radioClass:"iradio_square-green",})});
</script>
<script type="text/javascript">
    //编辑课程
    function nianji_course_edit(id){
        location.href = './nianji_course_edit/id/'+id+'.html';
    }

    //删除课程
    //    function course_del(id){
    //        lunhui.confirm(id,'<?php echo url("course_del"); ?>');
    //    }


</script>
</body>
</html>