<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:84:"/data/www/html/web2017/public_html/../application/web/view/article/temp_message.html";i:1498094684;s:78:"/data/www/html/web2017/public_html/../application/web/view/public/header4.html";i:1502875458;s:82:"/data/www/html/web2017/public_html/../application/web/view/public/user_center.html";i:1498798586;}*/ ?>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <meta name="description" content="">
    <title>用户中心-模板消息列表</title>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="http://v3.bootcss.com/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <link href="/static/pc/cmgd.css" rel="stylesheet">
    <link href="/static/pc/style.css" rel="stylesheet">
    <link rel="stylesheet" href="/static/pc/swiper.3.2.0.min.css">

    <link rel="stylesheet" type="text/css" href="/static/admin/webupload/webuploader.css">
    <link rel="stylesheet" type="text/css" href="/static/admin/webupload/style.css">

    <!--[if lt IE 9]><script src="http://v3.bootcss.com/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="http://v3.bootcss.com/assets/js/ie-emulation-modes-warning.js"></script>
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="/static/admin/js/jquery.min.js?v=2.1.4"></script>
    <script src="/static/admin/js/layer/layer.js"></script>
    <script src="/static/admin/js/laypage/laypage.js"></script>
    <script src="/static/admin/js/laytpl/laytpl.js"></script>
    <link href="/static/admin/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <style>
        .list-group-item.active, .list-group-item.active:focus, .list-group-item.active:hover{
            background: #fff;
            color: #2fab87;
            border-color:#f5f5f5;
            border-bottom: 0px;
        }
        .list-group-item:first-child {border-radius: 0px;   }
        .fa{padding-right: 15px;font-size:18px;}
        .list-group-item{font-size:16px; border-top:0px;border-bottom: 1px #f5f5f5 solid;padding-top:20px;padding-bottom:20px; padding-left: 40px;
            border-left:0px;            border-right:0px;}
        .panel-heading{background: #f5f5f5;}

        .panel-primary>.panel-heading {
            padding: 25px 0 20px 0px;
            color: #000;
            background-color: #fff;
            border-color: #fff;
            border-bottom: 1px #f5f5f5 solid;
        }
        .myinfo{
            padding: 25px 20px 20px 20px;
            border-bottom: 2px #ffb400 solid;
        }
        .table-bordered>thead>tr>th {
            border: 0px solid #ddd;
        }
        .btn-primary{
            background-color: #2fab87;
            border-color: #2fab87;
        }
        th{font-weight: normal;font-size: 15px;}
        td{font-size: 14px;}
        .table-bordered>tbody>tr>td  {
            border: 0px solid #ddd;
        }

        .table-bordered{border:0px;}
        .long-tr{border:0px;background: #f7f7f7;}

        .panel-primary {
            border-color: #fff;
        }

        body{background:#f5f5f5;}
        .col-xs-12, .col-sm-9{padding-left: 0px;}
    </style>
</head>

<body>
<header id="main-header">
    <div class="area">
        <div class="head-nav left">
            <ul>
                <li class="index"><a href="<?php echo url('index/index'); ?>"><img src="/static/pc/image/logo.png" STYLE="height: 40px;padding-right: 20px;"></a></li>
                <li><a href="<?php echo url('index/index'); ?>">树人网</a></li>             
                <li><a href="<?php echo url('article/essay'); ?>" target="_blank">征文活动</a></li>
                <li><a href="http://club.shuren100.com/"  target="_blank">树人论坛</a></li>
				<li><a href="http://baozhi.shuren100.com/" target="_blank">电子报纸</a></li>
				<li><a href="http://qikan.shuren100.com/" target="_blank">电子期刊</a></li>
				<li><a href="http://www.shuren100.com//indexold.html" target="_blank">返回老版</a></li>

            </ul>
        </div>
        <div id="head-login" class="right login">
            <div class="login">
                <?php if($is_login==1): ?>
                <a href="<?php echo url('article/user_index'); ?>" data-role="login-btn" class="login-sohu"><img src="/static/pc/image/user.png" ><?php echo $my_user_info['nickname']; ?></a>
                <a href="<?php echo url('user/center'); ?>" data-role="login-btn" class="login-sohu"><img src="/static/pc/image/home.png" >我的主页</a>
                <a href="<?php echo url('index/loginout'); ?>" data-role="login-btn" class="login-sohu">退出</a>
                <?php else: ?>
                <a href="<?php echo url('user/login'); ?>"  data-role="login-btn" class="login-sohu"><img src="/static/pc/image/user.png" >用户登录</a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</header>
<div class="container body_bg" style="margin-top:80px;">



    <div class="row row-offcanvas row-offcanvas-right">
        <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">
            <!--<div style="width: 100%;height:100px;padding-top: 20px;padding-left: 10px;background: #fff;" >-->
                <!--<div style="width:60px;float: left;margin-right:10px;">-->
                    <!--<img src="/uploads/face/<?php echo $member['head_img']; ?>" style="width: 60px;height: 60px;">-->
                <!--</div>-->
                <!--<div style="float: left;width: 178px;">-->
                    <!--<div style="width: 178px;overflow: hidden;padding-top: 3px;"><?php echo $member['nickname']; ?></div>-->
                    <!--<div style="width: 178px;overflow: hidden;padding-top: 12px;">ID:<?php echo $member['account']; ?></div>-->
                <!--</div>-->
            <!--</div>-->
            <div style="width: 100%;height:120px;padding-top: 20px;padding-top: 20px;padding-left: 10px; background: #fff;" >
    <div style="width:60px;float: left;margin-right:10px;">
        <img src="/uploads/face/<?php echo $member['head_img']; ?>" id="img11"  onerror="this.src='/static/admin/images/head_default.gif'"  style="width: 60px;height: 60px;">
    </div>
    <div style="float: left;width: 178px;">
        <div style="width: 178px;overflow: hidden;padding-top: 3px;"><?php echo $member['nickname']; ?></div>
        <div style="width: 178px;overflow: hidden;padding-top: 12px;">帐号:<?php echo $member['account']; ?></div>
        <div style="width: 178px;overflow: hidden;padding-top: 12px;">积分:<span id="jifen"><?php echo $member['integral']; ?></span> <span style="float: right;padding-right: 20px;">  <a href="javascript:;" onclick="user_sign()" >

                        <span style="padding: 3px 5px;color: #fff;background: #1ee0a5;border-radius: 3px;font-size: 12px;" id="daysign">
                         <?php if($is_sign==0): ?>签到<?php else: ?>已签到 <?php endif; ?>
                        </span>
        </a></span></div>
    </div>
</div>

<div class="list-group">
    <a href="<?php echo url('user_index'); ?>" class="list-group-item <?php if($m==1): ?>active<?php endif; ?>" style="border-bottom: 2px #f5f5f5 solid;"><i class="fa fa-home "></i>首页</a>

    <a class="list-group-item " ><i class="fa fa-tags"></i>文章</a>
    <a href="<?php echo url('write'); ?>" class="list-group-item <?php if($m==2): ?>active<?php endif; ?>" style="padding-left: 85px;padding-top:0px;padding-bottom: 10px;">写文章</a>
    <a href="<?php echo url('user_column'); ?>" class="list-group-item <?php if($m==18): ?>active<?php endif; ?>"  style="padding-left: 85px;padding-top:10px;padding-bottom: 20px; ">栏目管理</a>
    <a href="<?php echo url('myarticle'); ?>" class="list-group-item <?php if($m==3): ?>active<?php endif; ?>"  style="padding-left: 85px;border-bottom: 2px #f5f5f5 solid;padding-top:10px;padding-bottom: 20px; ">文章列表</a>
    <a class="list-group-item" ><i class="fa fa-file-text-o"></i>征文</a>
    <?php if($member['group_id'] == 8): ?>
    <a href="<?php echo url('write_essay'); ?>" class="list-group-item <?php if($m==4): ?>active<?php endif; ?>"  style="padding-left: 85px;padding-top:0px;padding-bottom: 10px;">发征文</a>
    <a href="<?php echo url('myessay'); ?>" class="list-group-item <?php if($m==5): ?>active<?php endif; ?>"  style="padding-left: 85px;border-bottom: 2px #f5f5f5 solid;padding-top:10px;padding-bottom: 20px;">征文列表</a>
    <?php else: ?>
    <a href="<?php echo url('myessay'); ?>" class="list-group-item <?php if($m==5): ?>active<?php endif; ?>" style="padding-left: 85px;border-bottom: 2px #f5f5f5 solid;padding-top:9px;padding-bottom: 20px;"><i class="fa fa-file-text-o "></i>已投征文</a>
    <?php endif; ?>
    <a class="list-group-item " ><i class="fa fa-tags"></i>话题</a>
    <a href="<?php echo url('write_debate'); ?>" class="list-group-item <?php if($m==6): ?>active<?php endif; ?> " style="padding-left: 85px;padding-top:0px;padding-bottom: 10px;">写话题</a>
    <a href="<?php echo url('mydebate'); ?>" class="list-group-item <?php if($m==7): ?>active<?php endif; ?>"  style="padding-left: 85px;border-bottom: 2px #f5f5f5 solid;padding-top:10px;padding-bottom: 20px; ">话题列表</a>
    <a class="list-group-item " ><i class="fa fa-th"></i>数据</a>
    <a href="<?php echo url('tongji'); ?>" class="list-group-item <?php if($m==15): ?>active<?php endif; ?> " style="padding-left: 85px;padding-top:0px;padding-bottom: 10px;">总体</a>
    <a href="<?php echo url('danpian'); ?>" class="list-group-item <?php if($m==16): ?>active<?php endif; ?>"  style="padding-left: 85px;border-bottom: 2px #f5f5f5 solid;padding-top:10px;padding-bottom: 20px; ">单篇</a>
    <a href="<?php echo url('user_info'); ?>" class="list-group-item <?php if($m==8): ?>active<?php endif; ?>" style="border-bottom: 2px #f5f5f5 solid;"><i class="fa fa-gear "></i>我的资料</a>
    <a href="<?php echo url('draft'); ?>" class="list-group-item <?php if($m==9): ?>active<?php endif; ?>" style="border-bottom: 2px #f5f5f5 solid;"><i class="fa fa-folder-open-o "></i>我的草稿箱</a>
    <a href="<?php echo url('collect'); ?>" class="list-group-item <?php if($m==10): ?>active<?php endif; ?>" style="border-bottom: 2px #f5f5f5 solid;"><i class="fa fa-heart-o "></i>我的收藏</a>
    <a href="<?php echo url('gongwen'); ?>" class="list-group-item <?php if($m==21): ?>active<?php endif; ?>" ><i class="fa fa-bell-o "></i>公文管理</a>
    <a href="<?php echo url('temp_message'); ?>" class="list-group-item <?php if($m==11): ?>active<?php endif; ?>" ><i class="fa fa-bell-o "></i>模板消息</a>
    <?php if($member['id'] == 212212): ?>
    <a href="<?php echo url('picture'); ?>" class="list-group-item <?php if($m==19): ?>active<?php endif; ?>" style="border-bottom: 2px #f5f5f5 solid;"><i class="fa fa-heart-o "></i>图片管理</a>
    <a href="<?php echo url('nav_manager'); ?>" class="list-group-item <?php if($m==20): ?>active<?php endif; ?>" ><i class="fa fa-bell-o "></i>侧栏管理</a>
    <?php endif; ?>
</div>

<script type="text/javascript">
    function user_sign(){
        $.post('<?php echo url("article/user_sign"); ?>',
                {id:1},
                function(data){
                    if(data.code==1) {
                        $('#daysign').html('已签到');
                        var jifen = $('#jifen').html();

                        jifen1 = parseInt(jifen)+parseInt(data.data);
                        $('#jifen').html(jifen1);


                        layer.msg(data.msg,{icon:1,time:1000,shade: 0.1,});
                    }else{
                        layer.msg(data.msg,{icon:5,time:1000,shade: 0.1,});
                    }
                });
    }
</script>
        </div><!--/.sidebar-offcanvas-->
        <div class="col-xs-12 col-sm-9">



            <div class="panel panel-primary" style="padding-left: 30px; min-height: 855px;">

                <div class="panel-heading">
                    <span class="myinfo">我的模板消息</span>

                </div>
                <div class="panel-body">
                    <div style="margin-bottom: 10px;">
                        <?php if($auth=='teach'): ?>
                        <a href="<?php echo url('temp_add'); ?>" style="padding: 4px 7px;border-radius: 4px;background: #2fab87;color: #fff;">发送班级模板消息</a>
                        <?php endif; if($auth=='admin'): ?>
                        <a href="<?php echo url('temp_add1'); ?>" style="padding: 4px 7px;border-radius: 4px;background: #2fab87;color: #fff;">发送学校模板消息</a>
                        <?php endif; ?>
                    </div>
                    <div class="example-wrap">
                        <div class="example">
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr class="long-tr">
                                    <th width="3%">ID</th>
                                    <th width="15%">机构</th>
                                    <th width="10%">消息类型</th>
                                    <th width="10%">发送对象</th>
                                    <th width="10%">消息主内容</th>
                                    <th width="10%">发送时间</th>
                                    <th width="12%">操作</th>
                                </tr>
                                </thead>
                                <script id="arlist" type="text/html">
                                    {{# for(var i=0;i<d.length;i++){  }}
                                    <tr class="long-td">

                                        <td>{{d[i].id}}</td>
                                        <td>{{d[i].name}}</td>
                                        <td>{{d[i].temp}}</td>
                                        <td>{{d[i].duixiang}}</td>
                                        <td>{{d[i].message}}</td>
                                        <td>{{d[i].addtime1}}</td>
                                        <td>
                                            <a href="javascript:;" onclick="urlto({{d[i].id}})"  class="btn btn-primary btn-xs btn-outline">
                                                查看</a>
                                        </td>
                                    </tr>

                                    {{# } }}
                                </script>
                                <tbody id="article_list"></tbody>
                            </table>

                            <div id="AjaxPage" style=" text-align: right;"></div>
                            <div id="allpage" style=" text-align: right;"></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/row-->

    <hr>

    <footer  style="text-align: center;">
        <p>&copy; Company 2017</p>
    </footer>

</div><!--/.container-->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="http://v3.bootcss.com/assets/js/ie10-viewport-bug-workaround.js"></script>
<script src="http://v3.bootcss.com/examples/offcanvas/offcanvas.js"></script>
<script type="text/javascript">
    function Ajaxpage(curr){
        $.getJSON('<?php echo url("Article/temp_message"); ?>', {
            page: curr || 1
        }, function(data){      //data是后台返回过来的JSON数据

            $(".spiner-example").css('display','none'); //数据加载完关闭动画
            if(data==''){
                $("#article_list").html('<td colspan="20" style="padding-top:10px;padding-bottom:10px;font-size:16px;text-align:center">暂无数据</td>');
            }else{
                // console.log(data);
                article_list(data.list); //模板赋值
                laypage({
                    cont: $('#AjaxPage'),//容器。值支持id名、原生dom对象，jquery对象,
                    pages:'<?php echo $allpage; ?>',//总页数
                    skip: true,//是否开启跳页
                    skin: '#1AB5B7',//分页组件颜色
                    curr: curr || 1,
                    groups: 3,//连续显示分页数
                    jump: function(obj, first){

                        if(!first){
                            Ajaxpage(obj.curr)
                        }
                        $('#allpage').html('第'+ obj.curr +'页，共'+ obj.pages +'页');
                    }
                });
            }
        });
    }

    Ajaxpage();
    function article_list(list){

        var tpl = document.getElementById('arlist').innerHTML;
        laytpl(tpl).render(list, function(html){
            document.getElementById('article_list').innerHTML = html;
        });
    }

    function urlto(id){
        location.href = './temp_detail/id/'+id+'.html';
    }


</script>
</body>
</html>
