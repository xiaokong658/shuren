<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:76:"/data/www/html/web2017/public_html/../application/web/view/index/member.html";i:1498620194;s:78:"/data/www/html/web2017/public_html/../application/web/view/public/header4.html";i:1502875458;}*/ ?>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1">
    <meta name="description" content="">
    <title>树人教育平台</title>
    <link rel="icon" href="favicon.ico">
    <link href="/static/admin/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link rel="stylesheet" href="/static/pc/swiper.3.2.0.min.css">
    <!-- 最新版本的 Bootstrap 核心 CSS 文件 -->

    <link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="/static/admin/js/jquery.min.js?v=2.1.4"></script>
    <script src="/static/admin/js/jquery.form.js"></script>
    <!--<script src="/static/admin/js/plugins/layer/laydate/laydate.js"></script>-->
    <!--<script src="/static/pc/js/layer/layer.js"></script>-->
    <script src="/static/admin/js/laypage/laypage.js"></script>
    <script src="/static/admin/js/laytpl/laytpl.js"></script>
    <link href="/static/pc/style.css" rel="stylesheet">
    <style>
        .swiper-slide a img {width: 100%;}

        .media{border-bottom:1px #f0f0f0 solid; padding: 25px 0;margin-top: 0px; }

        .media-heading{height: 42px; overflow: hidden; font-family: Microsoft YaHei; font-size:20px;font-weight: 500; }
        .media-content {height:40px;overflow: hidden;color: #878787;}
        .media-time{margin-top: 26px;color: #878787;}
        .sidebar-module{padding:0;margin:0;}
        .list-unstyled{ background: #f3f3f3;}
        .list-unstyled li{
            font-size: 16px;
            line-height: 36px;
            padding-left: 25px;
            height: 36px;
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
            word-spacing:10px; letter-spacing: 2px;
        }
        .list-unstyled li:hover{
            font-size: 16px;
            line-height: 36px;
            padding-left: 25px;
            height: 36px;
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
            word-spacing:10px; letter-spacing: 2px;
            background: #ddd;
        }
    </style>
</head>

<body>
<header id="main-header">
    <div class="area">
        <div class="head-nav left">
            <ul>
                <li class="index"><a href="<?php echo url('index/index'); ?>"><img src="/static/pc/image/logo.png" STYLE="height: 40px;padding-right: 20px;"></a></li>
                <li><a href="<?php echo url('index/index'); ?>">树人网</a></li>             
                <li><a href="<?php echo url('article/essay'); ?>" target="_blank">征文活动</a></li>
                <li><a href="http://club.shuren100.com/"  target="_blank">树人论坛</a></li>
				<li><a href="http://baozhi.shuren100.com/" target="_blank">电子报纸</a></li>
				<li><a href="http://qikan.shuren100.com/" target="_blank">电子期刊</a></li>
				<li><a href="http://www.shuren100.com//indexold.html" target="_blank">返回老版</a></li>

            </ul>
        </div>
        <div id="head-login" class="right login">
            <div class="login">
                <?php if($is_login==1): ?>
                <a href="<?php echo url('article/user_index'); ?>" data-role="login-btn" class="login-sohu"><img src="/static/pc/image/user.png" ><?php echo $my_user_info['nickname']; ?></a>
                <a href="<?php echo url('user/center'); ?>" data-role="login-btn" class="login-sohu"><img src="/static/pc/image/home.png" >我的主页</a>
                <a href="<?php echo url('index/loginout'); ?>" data-role="login-btn" class="login-sohu">退出</a>
                <?php else: ?>
                <a href="<?php echo url('user/login'); ?>"  data-role="login-btn" class="login-sohu"><img src="/static/pc/image/user.png" >用户登录</a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</header>
<div class="area clear" id="main-container">
    <div id="left-nav" class="column" style="visibility: visible;">
        <div class="sidebar-module">
            <ol class="list-unstyled">
                <li style="border-bottom:2px #2fab87 solid; "><a href="<?php echo url('article/article'); ?>">全部文章</a></li>
                <?php if(is_array($cate) || $cate instanceof \think\Collection || $cate instanceof \think\Paginator): if( count($cate)==0 ) : echo "" ;else: foreach($cate as $key=>$vv): ?>
                <li><a href="<?php echo url('article/article',array('type'=>$vv['id'])); ?>"><?php echo $vv['name']; ?></a></li>
                <?php endforeach; endif; else: echo "" ;endif; ?>
            </ol>
        </div>
    </div>
    <div class="main left">
        <div id="search" class="search">
            <select id='lei' name="lei" class="search-input left" style="width: 70px;">
                <option value="user">用户</option>
                <option value="article">文章</option>
            </select>
            <input type="text" id="key" class="search-input left"  placeholder="搜一搜" name="key"  value="<?php echo $keys; ?>" style="width:549px;">
            <!--<a href=""  class="search-btn" id="search_info"><i class="fa fa-search"></i></a>-->
            <button  type="submit"  class="search-btn" id="search_info"><i class="fa fa-search"></i></button>
            <!--<form target="" style="display:none;width:0;height:0"></form>-->
        </div>
        <div>
            <div class="jumbotron_swiper">

                <div class="news-wrapper">
                    <script id="list-template" type="text/html">
                        {{# for(var i=0;i<d.length;i++){  }}


                        <div data-role="news-item" class="news-box clear   news-box-aa " data-media-id="{{d[i].id}}" data-loc="">
                            <div class="pic img-do left">
                                <a href="/web/user/center/user_id/{{d[i].id}}.html" >
                                    <img class="media-object"  alt="" src="/uploads/face/{{d[i].head_img}}"   onerror="this.src='/static/admin/images/head_default.gif'" data-holder-rendered="true" style="width:100px; height: 100px;">
                                </a>
                            </div>

                            <h4 style="height: 45px;"><a href="/web/user/center/user_id/{{d[i].id}}.html" >
                                {{d[i].nickname}}
                            </a></h4>
                            <div class="other">
                                {{d[i].desc}}
                            </div>
                        </div>







                        {{# } }}
                    </script>
                    <div id="list-content"></div>
                </div>
            </div>




            <!-- 如果需要分页器 -->

        </div>

    </div>


    <div class="right sidebar">

        <?php if($right_ad1['images']!=''): ?>
        <div class="ad_right" >
            <a href=""  target=""> <img src="/uploads/images/<?php echo $right_ad1['images']; ?>"></a>
        </div>
        <?php endif; ?>

        <div class="bord clear recommend" id="recommend-writer">
            <div class="titleR"><span class="tt">树人推荐</span>
            </div>
            <?php if(is_array($new_instit) || $new_instit instanceof \think\Collection || $new_instit instanceof \think\Paginator): if( count($new_instit)==0 ) : echo "" ;else: foreach($new_instit as $key=>$vt): ?>
            <div class="pic-txt clear">
                <div class="pic img-do"><a href="<?php echo url('user/center',array('user_id'=>$vt['id'])); ?>" target="_blank"><img src="/uploads/face/<?php echo $vt['head_img']; ?>" alt=""></a></div>
                <h4><a href="<?php echo url('user/center',array('user_id'=>$vt['id'])); ?>" target="_blank"><?php echo $vt['nickname']; ?></a></h4>
                <p>
                    <?php echo $vt['desc']; ?>.
                </p>
            </div>
            <?php endforeach; endif; else: echo "" ;endif; ?>
        </div>
        <?php if($right_ad2['images']!=''): ?>
        <div class="ad_right" style="margin-top:10px;">
            <a href=""  target=""> <img src="/uploads/images/<?php echo $right_ad2['images']; ?>"></a>
        </div>
        <?php endif; ?>

        <div class="hot-article clear bord" id="hot-news">
            <div class="titleR">
                <span class="tt">24小时热点</span>
            </div>

            <?php if(is_array($hot_article) || $hot_article instanceof \think\Collection || $hot_article instanceof \think\Paginator): if( count($hot_article)==0 ) : echo "" ;else: foreach($hot_article as $key=>$vv): ?>
            <div class="pic-txt clear ">
                <div class="pic img-do">
                    <a target="_blank" href="/web/article/index/id/<?php echo $vv['id']; ?>.html">


                        <img alt="" src="/uploads/images/<?php echo $vv['photo']; ?>">

                        <span class="sn"><?php echo $vv['sort']; ?></span>

                    </a>
                </div>
                <p><a target="_blank" href="/web/article/index/id/<?php echo $vv['id']; ?>.html"><?php echo $vv['title']; ?></a>
                </p>
            </div>
            <?php endforeach; endif; else: echo "" ;endif; ?>



        </div>


        <div class="cooperation clear bord">
            <div class="titleR"><span class="tt">机构导航</span></div>
            <div class="cooper">
                <p>
                    <a target="_blank" href="">郑州市教育局</a> 丨

                    <a target="_blank" href="">开封市教育局</a> 丨

                    <a target="_blank" href="">洛阳市教育局</a> 丨

                    <a target="_blank" href="">平顶山市教育局</a> 丨

                    <a target="_blank" href="">新乡市教育局</a> 丨

                    <a target="_blank" href="">许昌市教育局</a> 丨

                    <a target="_blank" href="">安阳市教育局</a> 丨

                    <a target="_blank" href="">濮阳市教育局</a> 丨

                    <a target="_blank" href="">信阳市教育局</a> 丨


                </p>
            </div>
        </div>


        <div class="cooperation clear bord">
            <div class="titleR"><span class="tt">更多</span></div>
            <div class="cooper">
                <p>
                    <a target="_blank" href="">媒体合作&nbsp;&nbsp;&nbsp;</a> 丨

                    <a target="_blank" href="">广告合作&nbsp;&nbsp;&nbsp;</a> 丨

                    <a target="_blank" href="">产品合作&nbsp;&nbsp;&nbsp;</a> 丨

                    <a target="_blank" href="">用户协议&nbsp;&nbsp;&nbsp;</a> 丨

                    <a target="_blank" href="">广告投放&nbsp;&nbsp;&nbsp;</a> 丨

                    <a target="_blank" href="">侵权投诉&nbsp;&nbsp;&nbsp;</a> 丨

                </p>
            </div>
        </div>


        <div class="cooperation clear bord">
            <div class="titleR"><span class="tt">关于河育报刊社</span></div>
            <div class="cooper">
                <p class="cinfo"> <span>地址：</span>河南省郑州市顺河路11号</p>
                <p class="cinfo"> <span>办公室联系电话：</span>0371-66320712</p>
                <p class="cinfo"> <span>报刊订阅咨询电话：</span>3006597019</p>
                <p class="cinfo"> <span>树人网联系电话：</span>0371-66323059</p>
                <p class="cinfo">  河南教育报刊社版权所有 </p>
            </div>
        </div>


        <div class="cooperation clear bord">
            <div class="titleR"><span class="tt">友情链接</span></div>
            <div class="cooper">
                <p>
                    <a target="_blank" href="">教育时报</a>丨

                    <a target="_blank" href="">小学生学习报</a>丨

                    <a target="_blank" href="">学生英语报纸</a>丨



                </p>
            </div>
        </div>


    </div>

</div>
<script>
    $('.column').height($(window).height());

    var wid = $(window).width();
    var widths = (parseInt(wid) - 1200 )/2
    console.log(widths);

    var ccc =1;
    Ajaxpage();
    $(window).scroll(
            function() {

                $('.column').css('position','fixed');
                $('.column').css('left',widths);


                var scrollTop = $(this).scrollTop();
                var scrollHeight = $(document).height();
                var windowHeight = $(this).height();
                if (scrollTop + windowHeight == scrollHeight) {
                    Ajaxpage();
                }
            });

    function Ajaxpage(){
        key = '<?php echo $keys; ?>';
        $.post('<?php echo url("index/member"); ?>', {page:ccc,key:key}, function(data){
            //  $(".spiner-example").css('display','none'); //数据加载完关闭动画
            console.log(data);
            if(data.code==1){
                var tpl = document.getElementById('list-template').innerHTML;
                laytpl(tpl).render(data.list, function(html){
                    // document.getElementById('list-content').innerHTML = html;
                    $('#list-content').append(html);
//                    $('.weui-grid__icon').css('height',$('.weui-grid__icon').css('width'));
                    ccc++;
                });
                // $("#list-content").html('<td colspan="20" style="padding-top:10px;padding-bottom:10px;font-size:16px;text-align:center">暂无数据</td>');
            }else{


            }
        });
    }


    $('#search_info').click(function(){
                var  key =  $('#key').val();
                var  lei =  $('#lei').val();

                if(lei=='article'){
                    if(key==''){
                        location.href = '/web/index/index.html';
                    }else{

                        location.href = '/web/article/article/keyword/'+key+'.html';

                    }
                }

                if(lei=='user'){
                    if(key==''){
                        location.href = '/web/index/member.html';
                    }else{

                        location.href = '/web/index/member/key/'+key+'.html';

                    }
                }




            }
    );


</script>

<script src="/static/pc/swiper.3.2.0.min.js"></script>
<script>
    var mySwiper = new Swiper ('.swiper-container', {
        direction: 'horizontal',
        loop: true,
        autoplay : 3000,
        // 如果需要分页器
        pagination: '.swiper-pagination',
    })
    var myHot = new Swiper ('.hot-container', {
        direction: 'horizontal',
        loop: true,
        autoplay : 6000,
        // 如果需要分页器
        pagination: '.hot-pagination',
    })
    var mymiddleAdd = new Swiper ('.middle-container', {
        direction: 'horizontal',
        loop: true,
        autoplay : 3000,
        // 如果需要分页器
        pagination: '.middle-pagination',
    })
    var mymiddleAdd = new Swiper ('.middle2-container', {
        direction: 'horizontal',
        loop: true,
        autoplay : 3000,
        // 如果需要分页器
        pagination: '.middle-pagination',
    })
</script>

</body>
</html>
